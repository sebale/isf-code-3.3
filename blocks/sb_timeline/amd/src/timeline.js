// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is an empty module, that is required before all other modules.
 * Because every module is returned from a request for any other module, this
 * forces the loading of all modules with a single request.
 *
 * This function also sets up the listeners for ajax requests so we can tell
 * if any requests are still in progress.
 *
 * @package    block_sb_timeline
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define(['jquery', 'core/ajax', 'core/log', 'core/config', 'core/str'], function($, ajax, log, mdlcfg, str) {

    window.timeline_load_more = function (courseid, modules_count){
        var val = parseInt(jQuery("#load_more").attr("st"));
        var k = parseInt(jQuery("#load_more").attr("pos"));
        var count = (modules_count > 0) ? modules_count : 0;
        jQuery.get(mdlcfg.wwwroot+"/blocks/sb_timeline/ajax.php?action=load_timeline&id="+courseid+"&start="+val+"&k="+k, function(data){
            jQuery(".ts-bottom").remove();
            jQuery(".timeline").append(data);
        });
    }

    window.toggleTimeline = function (courseid){
        if ($('section#region-main').hasClass('has-blocks')) {
            $('section#region-main').removeClass('has-blocks');
            $('section[data-region="blocks-column"]').addClass('hidden');
            $('.timeline-toggler').addClass('active');
            var value = 0;
        } else {
            $('section#region-main').addClass('has-blocks');
            $('section[data-region="blocks-column"]').removeClass('hidden');
            $('.timeline-toggler').removeClass('active');
            var value = 1;
        }

        ajax.call([{
            methodname: 'format_isf_save_user_preferences',
            args: { name: 'show_timeline',
                    instanceid: 0,
                    value: value,
                    courseid: courseid
                  }
        }])[0]
        .done(function(response) {
          try {
            response = JSON.parse(response);
          } catch (Error){
            log.debug(Error.message);
          }
        }).fail(function(ex) {
          log.debug(ex.message);
        });
    }

    var Timeline = {

        init: function(courseid, timeline_limit, modules_count, show_timeline) {

            $('.timeline-header i').click(function(event){
                $('.timeline-header i').removeClass('active');
                $(this).addClass('active');
                if ($(this).attr('data-value') == 'list') {
                    $('.graph .timeline').addClass('list');
                    $('.graph .timeline').removeClass('time');
                } else {
                    $('.graph .timeline').removeClass('list');
                    $('.graph .timeline').addClass('time');
                }
            });

            if (!$('.timeline-toggler').length) {
                str.get_string('toggletimeline', 'block_sb_timeline').done(function(string) {
                    $('.crumbs-region .header-action').after('<div class="popover-region collapsed popover-region-timeline" id="nav-timeline-popover-container" data-userid="" data-region="popover-region" title="'+string+'" onclick="toggleTimeline('+courseid+');"><div class="popover-region-toggle nav-link timeline-toggler'+((show_timeline > 0) ? '' : ' active')+'" data-region="popover-region-toggle" aria-role="button" aria-haspopup="false" tabindex="0"><i class="fa fa-arrows-h"></i></div></div>');
                    if (show_timeline == 0){
                        toggleTimeline(courseid);
                    }
                }.bind(this));
            }

        },

    };


    /**
    * @alias module:blocks/sb_timeline
    */
    return Timeline;

});

