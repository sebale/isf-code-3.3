// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is an empty module, that is required before all other modules.
 * Because every module is returned from a request for any other module, this
 * forces the loading of all modules with a single request.
 *
 * This function also sets up the listeners for ajax requests so we can tell
 * if any requests are still in progress.
 *
 * @package    block_sb_coursenav
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define(['jquery', 'core/ajax', 'core/log', 'core/config'], function($, ajax, log, mdlcfg) {

    var Navigation = {

        init: function(courseid) {

            $('.course-navigation-block .course-menu a').click(function(e){
                $(this).parent().toggleClass('active');
                $(this).parent().find('ul').slideToggle('fast');
            });

            $(".course-navigation-block .title").click(function(e){
                $(this).toggleClass("swap");
                $(this).next().toggleClass("swap");
            });

            if ($(".list-group-item[data-key='coursenavigation']").hasClass('active') && !$("#nav-drawer").hasClass('closed')){
                $('.sidebar, .sidebar-block').css('opacity', '1');
                $('.sidebar, .sidebar-block').show();
                $('body').addClass('nav-with-panels');
            }

        },

    };


    /**
    * @alias module:blocks/sb_coursenav
    */
    return Navigation;

});

