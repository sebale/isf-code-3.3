<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Time Line
 *
 * @package    block_sb_coursenav
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

class block_sb_coursenav_renderer extends plugin_renderer_base {

    public function sb_coursenav($course, $issite = false, $cm = 0) {
        global $CFG, $PAGE, $OUTPUT;

        require_once($CFG->dirroot.'/course/lib.php');

        $context = context_course::instance($course->id, MUST_EXIST);
        $course = course_get_format($course)->get_course();

        $output = '';
        $modfullnames = array(); $archetypes = array();

        $displaysection = optional_param('section', 0, PARAM_INT);
        $modinfo = get_fast_modinfo($course);

        $toggler = html_writer::start_tag('div', array('class'=>'course-nav-toggler'));
        $toggler .= html_writer::tag('i', '', array('class'=>'fa fa-chevron-up'));
        $toggler .= html_writer::tag('i', '', array('class'=>'fa fa-chevron-down'));
        $toggler .= html_writer::end_tag('div');

        $output .= html_writer::start_tag('div', array('class'=>'course-navigation-block'));

        $image_url = "$CFG->wwwroot/course/format/isf/pix/thumbnail.jpg";
        $fs = get_file_storage();
        $imgs = $fs->get_area_files($context->id, 'format_isf', 'thumbnail', $course->id);
        foreach ($imgs as $file) {
            $filename = $file->get_filename();
            $filetype = $file->get_mimetype();
            if ($filename == '.' or !$filetype) continue;
            $url = moodle_url::make_pluginfile_url($context->id, 'format_isf', 'thumbnail', $course->id, '/', $filename);
            $image_url = $url->out();
        }

        $coursename = theme_primaryisf_get_coursename($course);
        $output .= html_writer::start_tag('div', array('class'=>'course-img'));
            $url = new moodle_url('/course/view.php',
                                    array('id' => $course->id));
            $cimg = html_writer::tag('img', '', array('src'=>$image_url, 'alt'=>$coursename));
            $output .= html_writer::link($url, $cimg, array('title'=>$coursename));
        $output .= html_writer::end_tag('div');
        $output .= html_writer::start_tag('div', array('class'=>'title swap'));
        $output .= html_writer::tag('i', '', array('class'=>'fa fa-compass'));
        $output .= get_string('pluginname', 'block_sb_coursenav').$toggler;
        $output .= html_writer::end_tag('div');
        $output .= html_writer::start_tag('ul', array('class'=>'swap'));

            $output .= html_writer::start_tag('li', array('class'=>'course-menu course-dashboard '.(($displaysection == 0 and !isset($cm->section)) ? 'current' : '')));
                $icon = html_writer::tag('i', '', array('class'=>'fa fa-home'));
                $url = new moodle_url('/course/view.php',
                                        array('id' => $course->id));
                $output .= html_writer::link($url, $icon.get_string('home', 'block_sb_coursenav'));
            $output .= html_writer::end_tag('li');

            foreach ($modinfo->get_section_info_all() as $section => $thissection) {
                if ($section == 0) continue;
                if (isset($course->numsections) && $section > $course->numsections) break;
                if (!$thissection->uservisible) continue;

                $output .= html_writer::start_tag('li', array('class'=>'course-menu '.(($displaysection == $section or (isset($cm->section) and $cm->section == $thissection->id)) ? 'current' : '')));

                    $output .= html_writer::link('javascript:void(0);', ((strlen(get_section_name($course, $thissection)) > 27) ? substr(get_section_name($course, $thissection), 0, 26).'...' : get_section_name($course, $thissection)), array('title'=>get_section_name($course, $thissection)));

                    $output .= html_writer::start_tag('span', array('class'=>'open-section', 'onclick'=>"location='".$CFG->wwwroot.'/course/view.php?id='.$course->id.'&section='.$section."'"));
                        $output .= html_writer::tag('i', '', array('class'=>'fa fa-external-link'));
                    $output .= html_writer::end_tag('span');

                    $output .= html_writer::start_tag('ul');
                        if (!empty($modinfo->sections[$thissection->section])) {
                            foreach ($modinfo->sections[$thissection->section] as $modnumber) {
                                $mod = $modinfo->cms[$modnumber];

                                $output .= html_writer::start_tag('li', array('class'=>'submenu '.((isset($cm->id) and $cm->id == $mod->id) ? 'current' : '')));
                                    $linktext = html_writer::tag('img', '', array('src'=>$mod->get_icon_url(), 'class'=>'course-nav-mod-icon', 'alt'=>$mod->get_formatted_name()));
                                    $linktext .= ((strlen($mod->get_formatted_name()) > 27) ? substr($mod->get_formatted_name(), 0, 26).'...' : $mod->get_formatted_name());
                                    $output .= html_writer::link($mod->url, $linktext, array('title'=>$mod->get_formatted_name(), 'onclick'=>"location='".$mod->url."'"));
                                $output .= html_writer::end_tag('li');
                            }
                        }
                    $output .= html_writer::end_tag('ul');
                $output .= html_writer::end_tag('li');
            }
        $output .= html_writer::end_tag('ul');

        foreach($modinfo->cms as $cm) {
            // Exclude activities which are not visible or have no link (=label)
            if (!$cm->uservisible or !$cm->has_view()) {
                continue;
            }
            if (array_key_exists($cm->modname, $modfullnames)) {
                continue;
            }
            if (!array_key_exists($cm->modname, $archetypes)) {
                $archetypes[$cm->modname] = plugin_supports('mod', $cm->modname, FEATURE_MOD_ARCHETYPE, MOD_ARCHETYPE_OTHER);
            }
            if ($archetypes[$cm->modname] == MOD_ARCHETYPE_RESOURCE) {
                if (!array_key_exists('resources', $modfullnames)) {
                    $modfullnames['resources'] = get_string('resources');
                }
            } else {
                $modfullnames[$cm->modname] = $cm->modplural;
            }
        }
        core_collator::asort($modfullnames);

        if (count($modfullnames) > 0){
            $output .= html_writer::start_tag('div', array('class'=>'title swap title-slider'));
            $output .= html_writer::tag('i', '', array('class'=>'fa fa-list'));
            $output .= get_string('courseactivities', 'block_sb_coursenav').$toggler;
            $output .= html_writer::end_tag('div');
                $output .= html_writer::start_tag('ul', array('class'=>'submenubar active swap'));

                $output .= html_writer::start_tag('li', array('class'=>'course-menu home-link qbank-link'));
                    $icon = html_writer::tag('i', '', array('class'=>'fa fa-question-circle'));
                    $url = new moodle_url('/question/edit.php',
                                            array('courseid' => $course->id));
                    $output .= html_writer::link($url, $icon.html_writer::tag('span', get_string('questionbank', 'block_sb_coursenav')));
                $output .= html_writer::end_tag('li');

            foreach ($modfullnames as $modname => $modfullname) {
                if ($modname === 'resources') {
                    $icon = $OUTPUT->pix_icon('icon', '', 'mod_page', array('class' => 'icon'));
                    $output .= html_writer::start_tag('li', array('class'=>'course-menu home-link'));
                    $url = new moodle_url('/course/resources.php',
                                            array('id' => $course->id));
                    $output .= html_writer::link($url, $icon.$modfullname);
                    $output .= html_writer::end_tag('li');
                } else {
                    if(method_exists ( $OUTPUT, 'image_url')){
                        $image = $OUTPUT->image_url('icon', $modname);
                    } else{
                        $image = $OUTPUT->pix_url('icon', $modname);
                    }
                    $icon = html_writer::empty_tag('img', array('src'=>$image, 'class'=>'icon', 'alt'=>$modfullname));
                    $output .= html_writer::start_tag('li', array('class'=>'course-menu home-link'));
                    $url = new moodle_url('/mod/'.$modname.'/index.php',
                                            array('id' => $course->id));
                    $output .= html_writer::link($url, $icon.$modfullname);
                    $output .= html_writer::end_tag('li');
                }
            }
            $output .= html_writer::end_tag('ul');
        }

        $output .= html_writer::end_tag('div');


        $PAGE->requires->js_call_amd('block_sb_coursenav/coursenav', 'init', array('courseid'=>$course->id));

        return $output;
    }

}
