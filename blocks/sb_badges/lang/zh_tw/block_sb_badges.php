<?php

$string['pluginname'] = '徽章';
$string['numbadgestodisplay'] = '顯示的徽章數目';
$string['nothingtodisplay'] = '你沒有徽章顯示';
$string['nothingtodisplay_certificates'] = '你沒有證書顯示';
$string['sb_badges:addinstance'] = '添加新的徽章塊';
$string['sb_badges:myaddinstance'] = '添加新的徽章塊到儀表板';
$string['allbadges'] = '所有徽章';
$string['earnedbadges'] = '已賺取的徽章';
$string['notearnedbadges'] = '非已賺取的徽章';
$string['stat'] = '我的數據';
$string['points'] = '積分';
$string['certificates'] = '證書';
$string['mycertificates'] = '我的證書';
$string['mybadges'] = '我的徽章';
$string['mycalendar'] = '我的日曆';
$string['cohorts'] = '群組';
$string['cohortsdesc'] = '已選群組的顯示屏';
