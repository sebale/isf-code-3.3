<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Block for displaying earned local badges to users
 *
 * @package    block_sb_badges
 * @copyright  2016 SEBALE {@link http://www.sebale.net/}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . "/badgeslib.php");

/**
 * Displays recent badges
 */
class block_sb_badges extends block_base {

    public function init() {
        $this->title = get_string('pluginname', 'block_sb_badges');
    }

    public function instance_allow_multiple() {
        return true;
    }

    public function has_config() {
        return true;
    }

    public function instance_allow_config() {
        return true;
    }

    public function applicable_formats() {
        return array(
            'admin' => false,
            'site-index' => true,
            'course-view' => true,
            'mod' => false,
            'my' => true
        );
    }

    public function specialization() {
        if (empty($this->config->title)) {
            $this->title = get_string('pluginname', 'block_sb_badges');
        } else {
            $this->title = $this->config->title;
        }
    }

    public function get_content() {
        global $USER, $PAGE, $CFG, $DB;

        require_once('locallib.php');
        
        if ($this->content !== null) {
            return $this->content;
        }

        if (empty($this->config)) {
            $this->config = new stdClass();
        }

        $can_view = false;
        $cohorts = get_config('block_sb_badges', 'cohorts');
        if (!empty($cohorts)){
            $can_view = false; $params = array('userid'=>$USER->id);
            list($sql_in, $params_in) = $DB->get_in_or_equal(explode(',', $cohorts), SQL_PARAMS_NAMED);
            $user_cohorts = $DB->get_records_sql("SELECT id FROM {cohort_members} WHERE userid = :userid AND cohortid $sql_in", $params+$params_in);
            if (count($user_cohorts)){
                $can_view = true;
            }
        }

        if (!$can_view){
            $this->content = new stdClass();
            $this->content->text = '';
            $this->content->footer = $this->get_course_search_form();
            return $this->content;
        }

        // Number of badges to display.
        if (!isset($this->config->numberofbadges)) {
            $this->config->numberofbadges = 10;
        }

        // Create empty content.
        $this->content = new stdClass();
        $this->content->text = '';

        $courseid = $this->page->course->id;
        if ($courseid == SITEID) {
            $courseid = null;
        }

        $this->content->text .= html_writer::start_tag('div', array('class'=>'block-side-item'));
            $renderer = $PAGE->get_renderer('local_calendar');
        $this->content->text .= html_writer::tag('h3', "<i class='fa fa-star-o'></i>".get_string('mycalendar', 'block_sb_badges'),array('class'=>'sub-title'));
            $this->content->text .= $renderer->print_calendar_widget();
        $this->content->text .= html_writer::end_tag('div');

        $this->content->text .= html_writer::start_tag('div', array('class'=>'block-side-item'));
            $this->content->text .= html_writer::tag('h3', "<i class='fa fa-star-o'></i>".get_string('stat', 'block_sb_badges'),array('class'=>'sub-title'));
            $this->content->text .= $this->print_block_stat_list();

            if (!empty($CFG->enablebadges)) {
                $this->content->text .= html_writer::tag('h3', "<i class='fa fa-star-o'></i>".get_string('mybadges', 'block_sb_badges'),array('class'=>'sub-title'));
                if ($badges = sb_badges_get_user_badges()) {
                    $this->content->text .= html_writer::tag('div', $this->print_block_badges_list($badges),array('class'=>'item clearfix'));
                } else {
                    $this->content->text .= html_writer::tag('div', get_string('nothingtodisplay', 'block_sb_badges'),array('class'=>'alert alert-success'));
                }
            }

            $this->content->text .= html_writer::tag('h3', "<i class='fa fa-star-o'></i>".get_string('mycertificates', 'block_sb_badges'),array('class'=>'sub-title'));
            if ($certificates = sb_badges_get_user_certificates()) {
                $this->content->text .= html_writer::tag('div', $this->print_block_certificates_list($certificates),array('class'=>'item clearfix'));
            } else {
                $this->content->text .= html_writer::tag('div', get_string('nothingtodisplay_certificates', 'block_sb_badges'),array('class'=>'alert alert-success'));
            }

        $this->content->text .= html_writer::end_tag('div');
        $this->content->text .= html_writer::tag('div','',array('class'=>'clear'));
        $this->content->footer = $this->get_course_search_form();

        return $this->content;
    }

    public function print_block_badges_list($badges) {
        global $OUTPUT;

        $items = array();
        foreach ($badges as $badge) {
            $context = ($badge->type == BADGE_TYPE_SITE) ? context_system::instance() : context_course::instance($badge->courseid);
            $bname = $badge->name;
            $imageurl = moodle_url::make_pluginfile_url($context->id, 'badges', 'badgeimage', $badge->id, '/', 'f1', false);

            $issued = ($badge->issuedid) ? true : false;

            $download = $status = $push = $points = $actions = '';
            if (isset($badge->points) && $badge->points>0){
                $points = html_writer::tag('span', (($issued) ? get_string('pointsearned', 'badges', $badge->points) : get_string('pointscanear', 'badges', $badge->points)), array('class' => 'badge-points'));
            }

            $name = $bname.$points;

            $url = new moodle_url('/badges/mybadges.php', array('download' => $badge->id, 'hash' => $badge->uniquehash, 'sesskey' => sesskey()));
            $image = html_writer::empty_tag('img', array('src' => $imageurl, 'class' => 'badge-image'));
            $image = html_writer::link($url,$image,array('class'=>'img'));
            if (!empty($badge->dateexpire) && $badge->dateexpire < time()) {
                $image .= $OUTPUT->pix_icon('i/expired',
                    get_string('expireddate', 'badges', userdate($badge->dateexpire)),
                    'moodle',
                    array('class' => 'expireimage'));
                $name .= '(' . get_string('expired', 'badges') . ')';
            }

            if ($issued){
                $download = $OUTPUT->action_icon($url, new pix_icon('t/download', get_string('download')));
                if ($badge->visible) {
                    $url = new moodle_url('/badges/mybadges.php', array('hide' => $badge->issuedid, 'sesskey' => sesskey()));
                    $status = $OUTPUT->action_icon($url, new pix_icon('t/hide', get_string('makeprivate', 'badges')));
                } else {
                    $url = new moodle_url('/badges/mybadges.php', array('show' => $badge->issuedid, 'sesskey' => sesskey()));
                    $status = $OUTPUT->action_icon($url, new pix_icon('t/show', get_string('makepublic', 'badges')));
                }
            }

            $url = ($issued) ? new moodle_url('/badges/badge.php', array('hash' => $badge->uniquehash)) : 'javascript:void(0)';

            if ($issued){
                $actions = html_writer::tag('div', $push . $download . $status, array('class' => 'badge-actions'));
            }

            $item = html_writer::start_tag('div',array('class'=>'badge-item '.(($issued) ? 'issued' : 'notissued')));
            $item .= html_writer::link($url,$name, array('title' => $name, 'class'=>(($issued) ? 'issued' : 'notissued')));
            $item .= $image;
            $item .= $actions;
            $item .= html_writer::end_tag('div');

            $items[] = $item;
        }
        $script = '<script>
                        $(document).ready(function() {
                          var $carousel = $(\'#badge_carousel\').carousel({loop:false});
                    
                            $(\'#badge_carousel_prev\').on(\'click\', function(ev) {
                                $carousel.carousel(\'prev\');
                            });
                            $(\'#badge_carousel_next\').on(\'click\', function(ev) {
                                $carousel.carousel(\'next\');
                            });
                        });                        
                    </script>';

        $control = '<label class="carousel-control left" id="badge_carousel_prev"><span class="fa fa-chevron-left"></span></label>';
        $control .= '<label class="carousel-control right" id="badge_carousel_next"><span class="fa fa-chevron-right"></span></label>';

        return html_writer::alist($items, array('class' => 'badges clearfix','id'=>'badge_carousel')).$control.$script;
    }

    public function print_block_certificates_list($certificates) {

        foreach ($certificates as $certificate) {
            if(!empty($certificate->icon)){
                $image = html_writer::tag('i', '', array('class' => 'fa ' . $certificate->icon));
            }else{
                $image = html_writer::tag('i', '', array('class' => 'fa fa-eye-slash'));
            }

            $url = new moodle_url('/mod/certificate/view.php', array('id' => $certificate->cmid));

            $item = html_writer::tag('h4', $certificate->name, array('class' => 'certificate-name'));
            $item .= html_writer::tag('div', $image, array('class' => 'certificate-image'));
            $item .= html_writer::tag('h4', $certificate->course_name, array('class' => 'certificate-course'));

            $items[] = html_writer::link($url,$item, array('title' => $certificate->name, 'class'=>'certificate-item'));
        }
        $script = '<script>
                        $(document).ready(function() {
                          var $carousel2 = $(\'#certificate_carousel\').carousel({loop:false});
                    
                            $(\'#certificate_carousel_prev\').on(\'click\', function(ev) {
                                $carousel2.carousel(\'prev\');
                            });
                            $(\'#certificate_carousel_next\').on(\'click\', function(ev) {
                                $carousel2.carousel(\'next\');
                            });
                        });                        
                    </script>';

        $control = '<label class="carousel-control left" id="certificate_carousel_prev"><span class="fa fa-chevron-left"></span></label>';
        $control .= '<label class="carousel-control right" id="certificate_carousel_next"><span class="fa fa-chevron-right"></span></label>';

        return html_writer::alist($items, array('class' => 'certificates clearfix','id'=>'certificate_carousel')).$control.$script;
    }

    public function print_block_stat_list(){
        global $CFG;

        $stat = sb_badges_get_user_stat();
        $items = array();

        $item = html_writer::start_tag('div', array('class' => 'stat-item'));
        $item .= html_writer::tag('h5', get_string('points', 'block_sb_badges'));
        $item .= html_writer::tag('span', $stat->points);
        $item .= html_writer::end_tag('div');
        $items[] = $item;

        if(!empty($CFG->enablebadges)){
            $item = html_writer::start_tag('div', array('class' => 'stat-item'));
            $item .= html_writer::tag('h5', get_string('pluginname', 'block_sb_badges'));
            $item .= html_writer::tag('span', $stat->badges);
            $item .= html_writer::end_tag('div');
            $items[] = $item;
        }

        $item = html_writer::start_tag('div', array('class' => 'stat-item'));
        $item .= html_writer::tag('h5', get_string('certificates', 'block_sb_badges'));
        $item .= html_writer::tag('span', $stat->certificates);
        $item .= html_writer::end_tag('div');
        $items[] = $item;

        $script = '<script>
                        $(document).ready(function() {
                            $(\'.block.block_sb_badges .user-stat li .stat-item span\').each(function() {
                              var value = $(this).html();
                              $(this).animate(
                                { num: value },
                                {
                                    duration: 5000,
                                    step: function (num){
                                        this.innerHTML = num.toFixed(0)
                                    }
                                }
                              );
                            });
                        });                        
                    </script>';

        return html_writer::alist($items, array('class' => 'user-stat clearfix')).$script;
    }

    public function get_course_search_form($value = '') {

        global $PAGE;

        if (isloggedin()) {

            // Can only be seen on front page.
            if ($PAGE->pagetype == 'site-index') {

                $formid = 'coursesearch';
                $inputid = 'coursesearchbox';
                $inputsize = 30;

                $strsearchcourses = get_string("searchcourses");
                $searchurl = new moodle_url('/course/search.php');

                $form = array(
                    'id' => $formid,
                    'action' => $searchurl,
                    'method' => 'get',
                    'class' => "form-inline",
                    'role' => 'form');
                $output = html_writer::start_tag('form', $form);
                $output .= html_writer::start_div('form-group');
                $output .= html_writer::tag('label', $strsearchcourses, array(
                    'for' => $inputid,
                    'class' => 'sr-only'));
                $search = array(
                    'type' => 'text',
                    'id' => $inputid,
                    'size' => $inputsize,
                    'name' => 'search',
                    'class' => 'form-control',
                    'value' => s($value),
                    'placeholder' => $strsearchcourses);
                $output .= html_writer::empty_tag('input', $search);
                $output .= html_writer::end_div(); // Close form-group.
                $button = array(
                    'type' => 'submit',
                    'class' => 'btn btn-default',
                    'id' => 'isfsearchcourses');
                $output .= html_writer::tag('button', get_string('go'), $button);
                $output .= html_writer::end_tag('form');

                return $output;
            }
        }
    }
}