<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version details
 *
 * @package    theme_primaryisf
 * @copyright  2016 Frederic Nevers (www.iteachwithmoodle.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

// Variable setting
$choices1to12 = array();
for ($i = 0; $i < 13; $i++) {
    $choices1to12[$i] = $i;
}


// Frontpage Slider.
$temp = new admin_settingpage('theme_primaryisf_frontpage_slider', get_string('frontpageslidersettings', 'theme_primaryisf'));

$temp->add(new admin_setting_heading('theme_primaryisf_slideshow', get_string('slideshowsettingsheading', 'theme_primaryisf'),
    format_text(get_string('slideshowdesc', 'theme_primaryisf') .
        get_string('slideroption2snippet', 'theme_primaryisf'), FORMAT_MARKDOWN)));

$name = 'theme_primaryisf/sliderenabled';
$title = get_string('sliderenabled', 'theme_primaryisf');
$description = get_string('sliderenableddesc', 'theme_primaryisf');
$setting = new admin_setting_configcheckbox($name, $title, $description, 0);
$temp->add($setting);

$name = 'theme_primaryisf/newstickerenabled';
$title = get_string('newstickerenabled', 'theme_primaryisf');
$description = get_string('newstickerenableddesc', 'theme_primaryisf');
$setting = new admin_setting_configcheckbox($name, $title, $description, 0);
$temp->add($setting);

$cohorts = $DB->get_records_select_menu('cohort', 'visible > 0', array(), 'name', 'id, name');
$name = 'theme_primaryisf/slidercohorts';
$title = get_string('slidercohorts', 'theme_primaryisf');
$description = get_string('slidercohortsdesc', 'theme_primaryisf');
$setting = new admin_setting_configmultiselect($name, $title, $description, array(), $cohorts);
$temp->add($setting);

// Number of Sliders.
$name = 'theme_primaryisf/slidercount';
$title = get_string('slidercount', 'theme_primaryisf');
$description = get_string('slidercountdesc', 'theme_primaryisf');
$default = THEME_primaryisf_DEFAULT_SLIDERCOUNT;
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices1to12);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// If we don't know how many sliders should be shown, default to the preset.
$slidercount = get_config('theme_primaryisf', 'slidercount');
if (!$slidercount) {
    $slidercount = THEME_primaryisf_DEFAULT_SLIDERCOUNT;
}

for ($sliderindex = 1; $sliderindex <= $slidercount; $sliderindex++) {
    $fileid = 'p' . $sliderindex;
    $name = 'theme_primaryisf/p' . $sliderindex;
    $title = get_string('sliderimage', 'theme_primaryisf');
    $description = get_string('sliderimagedesc', 'theme_primaryisf');
    $setting = new admin_setting_configstoredfile($name, $title, $description, $fileid);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_primaryisf/p' . $sliderindex . 'url';
    $title = get_string('sliderurl', 'theme_primaryisf');
    $description = get_string('sliderurldesc', 'theme_primaryisf');
    $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
    $temp->add($setting);

    $name = 'theme_primaryisf/p' . $sliderindex . 'cap';
    $title = get_string('slidercaption', 'theme_primaryisf');
    $description = get_string('slidercaptiondesc', 'theme_primaryisf');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $temp->add($setting);
}
