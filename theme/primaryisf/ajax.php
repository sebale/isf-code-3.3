<?php 

require_once('../../config.php');

global $CFG;

$action = required_param('action', PARAM_TEXT);

/** 
get pseudotabs storage 
(just return from session JSON string and create var if it isn't exists yet) 
@param string $action - action name
*/
if($action == 'get_pseudotabs_config'){
	if(!isset($_SESSION['pseudotabs_storage'])){
		$_SESSION['pseudotabs_storage'] = '{}';
	}
	echo $_SESSION['pseudotabs_storage'];
	exit;
} 

/**
save pseudotabs config
@param string $config - JSON encoded string
*/
else if ($action == 'set_pseudotabs_config'){
	try {
		$config = $_POST['config'];
		$_SESSION['pseudotabs_storage'] = json_encode($config);
		echo '{"status": true}';
	} catch (Exception $e) {
		echo '{"error": "'+$e->getMessage()+'"}';
	}
}

?>