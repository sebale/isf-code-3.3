<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Theme functions.
 *
 * @package    theme_primaryisf
 * @copyright  2016 Frédéric Massart - FMCorz.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if(!defined('THEME_primaryisf_DEFAULT_TOOLSMENUSCOUNT')){
    define('THEME_primaryisf_DEFAULT_TOOLSMENUSCOUNT', '3');
}
if(!defined('THEME_primaryisf_DEFAULT_SLIDERCOUNT')){
    define('THEME_primaryisf_DEFAULT_SLIDERCOUNT', '3');
}

/**
 * Post process the CSS tree.
 *
 * @param string $tree The CSS tree.
 * @param theme_config $theme The theme config object.
 */
function theme_primaryisf_css_tree_post_processor($tree, $theme) {
    $prefixer = new theme_primaryisf\autoprefixer($tree);
    $prefixer->prefix();
}

/**
 * Inject additional SCSS.
 *
 * @param theme_config $theme The theme config object.
 * @return string
 */
function theme_primaryisf_get_extra_scss($theme) {
    return !empty($theme->settings->scss) ? $theme->settings->scss : '';
}

/**
 * Returns the main SCSS content.
 *
 * @param theme_config $theme The theme config object.
 * @return string
 */
function theme_primaryisf_get_main_scss_content($theme) {
    global $CFG;

    $scss = '';
    $filename = !empty($theme->settings->preset) ? $theme->settings->preset : null;
    $fs = get_file_storage();

    $context = context_system::instance();
    if ($filename == 'default.scss') {
        $scss .= file_get_contents($CFG->dirroot . '/theme/primaryisf/scss/preset/default.scss');
    } else if ($filename == 'plain.scss') {
        $scss .= file_get_contents($CFG->dirroot . '/theme/primaryisf/scss/preset/plain.scss');
    } else if ($filename && ($presetfile = $fs->get_file($context->id, 'theme_primaryisf', 'preset', 0, '/', $filename))) {
        $scss .= $presetfile->get_content();
    } else {
        // Safety fallback - maybe new installs etc.
        $scss .= file_get_contents($CFG->dirroot . '/theme/primaryisf/scss/preset/default.scss');
    }

    return $scss;
}

/**
 * Get SCSS to prepend.
 *
 * @param theme_config $theme The theme config object.
 * @return array
 */
function theme_primaryisf_get_pre_scss($theme) {
    global $CFG, $DB;

    $scss = '';
    $configurable = [
        // Config key => [variableName, ...].
        'brandcolor' => ['brand-primary'],
    ];

    // Prepend variables first.
    foreach ($configurable as $configkey => $targets) {
        $value = isset($theme->settings->{$configkey}) ? $theme->settings->{$configkey} : null;
        if (empty($value)) {
            continue;
        }
        array_map(function($target) use (&$scss, $value) {
            $scss .= '$' . $target . ': ' . $value . ";\n";
        }, (array) $targets);
    }

    // process local/manager styles
    $styles = $DB->get_records('local_manager_styles', array('state'=>1, 'theme'=>'primary'));
    if (count($styles)){
        $font_import = '';
        $font_family = '';
        foreach ($styles as $style){
            if (empty($style->value) and empty($style->defaultvalue)) continue;

            $value = (!empty($style->value)) ? $style->value : $style->defaultvalue;

            if ($style->type == 'color'){
                $scss .= '$' . $style->name . ': #' . $value . ";\n";
            }elseif($style->type == 'size'){
                $scss .= '$' . $style->name . ': ' . $value . ";\n";
            }elseif($style->type == 'font-import'){
                $font_import = $value;
            }elseif($style->type == 'font'){
                $font_family = $value;
            }

            if ($style->name == 'breadcrumb-bg') {
                $scss .= '.full-header .crumbs-region {background-image:none !important; background-color: #'.$value.' !important;}\n';
            }
        }

        if(!empty($font_family) && !empty($font_import)){
            $scss .= "@font-face {
                        font-family: $font_family;
                        src: url([[font:theme|$font_import]]);
                       }";
            //default-font-family
            $scss .= '$default-font-family: "' . $font_family . "\";\n";
        }

        $loginbackground  = theme_primaryisf_file(0, 'loginbackground');
        if ($loginbackground){
            $scss .= 'body#page-login-index .card {background-image: url('.$loginbackground.') !important}';
        }
    }

    // Prepend pre-scss.
    if (!empty($theme->settings->scsspre)) {
        $scss .= $theme->settings->scsspre;
    }

    return $scss;
}

function theme_primaryisf_page_init(moodle_page $page) {
    $page->requires->jquery();

    $page->requires->jquery_plugin('bootstrap-tooltip', 'theme_primaryisf');
    $page->requires->jquery_plugin('bwlJqueryNewsTicker', 'theme_primaryisf');
    $page->requires->jquery_plugin('isf-mods', 'theme_primaryisf');
    $page->requires->jquery_plugin('isf-translations', 'theme_primaryisf');
    $page->requires->jquery_plugin('jq.carousel.min', 'theme_primaryisf');

    theme_primaryisf_layout_settings();
    theme_primaryisf_check_editing($page);

    $page->requires->js_call_amd('theme_primaryisf/colorinit', 'init', array());

}

function theme_primaryisf_check_editing(moodle_page $page) {
    global $USER, $DB;

    if ($page->course->id > 1){
        $context = context_course::instance($page->course->id);
        if (has_capability('moodle/course:manageactivities', $context)){
            $USER->editing = 1;
        }
    }
}


function theme_primaryisf_layout_settings(){
    global $OUTPUT, $DB, $PAGE, $THEME;

    $settings = new stdClass();
    $styles = $DB->get_records('local_manager_styles', array('state'=>1, 'theme'=>'primary'));
    if (count($styles)){
        foreach ($styles as $style){
            $settings->{$style->name} = (!empty($style->value)) ? $style->value : $style->defaultvalue;
        }

        $loginbackground  = theme_primaryisf_file(4, 'loginbackground');
        if ($loginbackground){
            $settings->loginbackground = $loginbackground->out();
        }
    }
    if (!$THEME){
        $THEME = new stdClass();
    }
    $THEME->theme_styles = $settings;
    global $THEME;

    return $settings;
}

function theme_primaryisf_flatnavigation() {
    global $PAGE, $DB, $USER, $CFG;
    $menu = array();

    if (!isloggedin()){
        return $menu;
    }
    try {
        $data = $DB->get_records('local_manager_sidebar',array('core'=>1,'theme'=>'primary'));
    } catch (Exception $e) {
        $data = array();
    }

    $manu_items = array();
    foreach($data as $datum){
        $manu_items[$datum->types] = $datum;
    }
    $currentlang = current_language();

    if($manu_items['dashboard']->state == 1){
        $item = array(
            'action' => new moodle_url($CFG->wwwroot),
            'key' => 'dashboard',
            'isactive' => ($PAGE->url->compare(new moodle_url('/'), URL_MATCH_BASE) or $PAGE->url->compare(new moodle_url('/my/'), URL_MATCH_BASE)),
            'text' => ($currentlang == 'zh_tw' || $currentlang == 'zh_cn')?$manu_items['dashboard']->title_ch:$manu_items['dashboard']->title
        );

        $menu[] = theme_primaryisf_set_menuitem_icon($manu_items['dashboard'],$item);
    }

    if ($PAGE->course->id > 1 && $manu_items['coursenavigation']->state == 1){
        $item = array(
            'action'   => '#coursenavigation',
            'key' => 'coursenavigation',
            'isactive' => false,
            'toggler' => '1',
            'title' => ($currentlang == 'zh_tw' || $currentlang == 'zh_cn')?$manu_items['coursenavigation']->title_ch:$manu_items['coursenavigation']->title
        );

        $menu[] = theme_primaryisf_set_menuitem_icon($manu_items['coursenavigation'],$item);
    }

    if($manu_items['courses']->state == 1){
        $item = array(
            'action' => '#courses',
            'key' => 'courses',
            'isactive' => false,
            'toggler' => '1',
            'text' => ($currentlang == 'zh_tw' || $currentlang == 'zh_cn')?$manu_items['courses']->title_ch:$manu_items['courses']->title
        );

        $menu[] = theme_primaryisf_set_menuitem_icon($manu_items['courses'],$item);
    }

    require_once($CFG->dirroot.'/local/manager/lib.php');
    $sidebar_menu = local_manager_get_sidebar_menu('primary');
    $menu = array_merge($menu, $sidebar_menu);

    if (has_capability('local/manager:is_admin', context_system::instance())){
        if($manu_items['manage']->state == 1){
            $item = array(
                'action' => '#manage',
                'key' => 'settings',
                'isactive' => false,
                'toggler' => '1',
                'text' => ($currentlang == 'zh_tw' || $currentlang == 'zh_cn')?$manu_items['manage']->title_ch:$manu_items['manage']->title
            );
            $menu[] = theme_primaryisf_set_menuitem_icon($manu_items['manage'],$item);
        }

        if($manu_items['settings']->state == 1){
            $item = array(
                'action' => new moodle_url($CFG->wwwroot . '/admin/search.php'),
                'key' => 'settings',
                'isactive' => false,
                'text' => ($currentlang == 'zh_tw' || $currentlang == 'zh_cn')?$manu_items['settings']->title_ch:$manu_items['settings']->title
            );
            $menu[] = theme_primaryisf_set_menuitem_icon($manu_items['settings'],$item);
        }

        if (isset($USER->editing) && $manu_items['addblock']->state == 1) {
            $item = array(
                'action' => new moodle_url($PAGE->url, array('bui_addblock'=>1, 'sesskey'=>sesskey(), 'redirect'=>0)),
                'key' => 'addblock',
                'isactive' => false,
                'text' => ($currentlang == 'zh_tw' || $currentlang == 'zh_cn')?$manu_items['addblock']->title_ch:$manu_items['addblock']->title
            );
            $menu[] = theme_primaryisf_set_menuitem_icon($manu_items['addblock'],$item);
        }
    }

    return $menu;
}

function theme_primaryisf_set_menuitem_icon($record, $item) {
    $fs = get_file_storage();
    $context = context_system::instance();

    $file = null;
    $files = $fs->get_area_files($context->id, 'local_manager', 'shortcutimage', $record->id);
    foreach ($files as $f) {
        if ($f->get_filename() != '.'){
            $file = $f;
        }
    }

    if (!empty($record->svg)){
        $item['svg'] = $record->svg;
    } else if ($file) {
        $imageurl = moodle_url::make_pluginfile_url($context->id, 'local_manager', 'shortcutimage', $record->id, '/', $file->get_filename());
        $item['image'] = $imageurl->out();
    } else if (!empty($record->icon)) {
        $item['icon'] = $record->icon;
    }
    return $item;
}

function theme_primaryisf_flatnavigation_actions() {
    global $PAGE, $DB, $USER, $CFG, $OUTPUT;
    $blocks = array();

    if (!isloggedin()) {
        return $blocks;
    }

    $blockshtml = $OUTPUT->blocks('side-course');

    if ($PAGE->course->id > 1 or $blockshtml) {

        $coursenav = '';
        if ($PAGE->course->id > 1) {
            require_once($CFG->dirroot.'/blocks/sb_coursenav/block_sb_coursenav.php');
            $block = new block_sb_coursenav;
            $content = $block->get_content();
            $coursenav = $content->text;
        }

        $blocks[] = array(
            'key'   => 'coursenavigation',
            'icon'  => 'fa fa-book',
            'title' => get_string('coursenavigation', 'theme_primaryisf'),
            'content'  => $coursenav.$blockshtml
        );
    }

    /* courses block */
    $secondblock = array();
    $menu = array(); $enrolledcourses = enrol_get_my_courses('id, fullname, shortname, format', 'fullname', 0);
    if (count($enrolledcourses)) {
        $mycourses = theme_primaryisf_get_courses($enrolledcourses);

        if (count($mycourses['courses'])) {
            foreach ($mycourses['courses'] as $course) {
                $course = course_get_format($course)->get_course();
                $item = array(
                    'action' => new moodle_url($CFG->wwwroot.'/course/view.php', array('id'=>$course->id)),
                    'title' => theme_primaryisf_get_coursename($course)
                );
                if (!empty($course->icon)){
                    $item['customicon'] = strpos($course->icon,'class') != false;
                    $item['menuicon'] = $course->icon;
                }
                if (!empty($course->color)){
                    $item['attributes'] = 'style="box-shadow: inset 2px 0px 0px 0px #'.$course->color.'"';
                    $item['iconattributes'] = 'style="color: #'.$course->color.';"';
                }

                $menu[] = $item;
            }
        }

        $secondmenu = array();
        if (count($mycourses['mastercourses'])) {
            foreach ($mycourses['mastercourses'] as $course) {
                $course = course_get_format($course)->get_course();
                $item = array(
                    'action' => new moodle_url($CFG->wwwroot.'/course/view.php', array('id'=>$course->id)),
                    'title' => theme_primaryisf_get_coursename($course)
                );
                if (!empty($course->icon)){
                    $item['customicon'] = strpos($course->icon,'class') != false;
                    $item['menuicon'] = $course->icon;
                }
                if (!empty($course->color)){
                    $item['attributes'] = 'style="box-shadow: inset 2px 0px 0px 0px #'.$course->color.'"';
                    $item['iconattributes'] = 'style="color: #'.$course->color.';"';
                }

                $secondmenu[] = $item;
            }
        }
        if (count($secondmenu)) {
            $secondblock = array(
                'icon'  => 'fa fa-book',
                'title' => get_string('mastercourses', 'theme_primaryisf'),
                'menu'  => $secondmenu
            );
        }
    }
    $blocks[] = array(
        'key'   => 'courses',
        'icon'  => 'fa fa-book',
        'title' => (isset($mycourses) and !count($mycourses['courses']) and count($mycourses['mastercourses'])) ? get_string('mastercourses', 'theme_primaryisf') : get_string('courses', 'theme_primaryisf'),
        'menu'  => $menu,
        'secondmenu' => $secondblock
    );

    /* manage block */
    $items = array();

    // users menu
    $list = array();
    $list[] = array(
        'key'   => 'users',
        'action' => new moodle_url($CFG->wwwroot.'/local/manager/users/index.php'),
        'title' => get_string('users', 'theme_primaryisf')
    );
    $list[] = array(
        'key'   => 'cohorts',
        'action' => new moodle_url($CFG->wwwroot.'/cohort/index.php'),
        'title' => get_string('cohorts', 'cohort')
    );
    $items[] = array(
        'action' => 'users',
        'icon' => 'fa fa-users',
        'title' => get_string('users', 'theme_primaryisf'),
        'list' => $list
    );

    // curricula menu
    $list = array();
    $list[] = array(
        'key'   => 'courses',
        'action' => new moodle_url($CFG->wwwroot.'/local/manager/courses/index.php'),
        'title' => get_string('courses', 'theme_primaryisf')
    );
    $list[] = array(
        'key'   => 'categories',
        'action' => new moodle_url($CFG->wwwroot.'/local/manager/categories/index.php'),
        'title' => get_string('categories', 'theme_primaryisf')
    );
    $items[] = array(
        'action' => 'curricula',
        'icon' => 'fa fa-list',
        'title' => get_string('coursesandcurriculum', 'theme_primaryisf'),
        'list' => $list
    );

    // site settings menu
    if (has_capability('local/manager:is_admin', context_system::instance())){
        $list = array();
        $list[] = array(
            'key'   => 'appearance',
            'action' => new moodle_url($CFG->wwwroot.'/local/manager/settings/appearance.php'),
            'title' => get_string('appearance', 'theme_primaryisf')
        );
        $list[] = array(
            'key'   => 'sidebar',
            'action' => new moodle_url($CFG->wwwroot.'/local/manager/settings/sidebar.php'),
            'title' => get_string('sidebarsettings', 'theme_primaryisf')
        );
        $list[] = array(
            'key'   => 'activitypicker',
            'action' => new moodle_url($CFG->wwwroot.'/admin/settings.php', array('section'=>'local_manager_activity_picker')),
            'title' => get_string('activitypicker', 'theme_primaryisf')
        );
        $list[] = array(
            'key'   => 'activityimages',
            'action' => new moodle_url($CFG->wwwroot.'/local/manager/settings/courseimages.php'),
            'title' => get_string('activityimages', 'theme_primaryisf')
        );
        $list[] = array(
            'key'   => 'quickmodule',
            'action' => new moodle_url($CFG->wwwroot.'/local/quickadd/index.php'),
            'title' => get_string('quickmodules', 'theme_primaryisf')
        );
        $items[] = array(
            'action' => 'settings',
            'icon' => 'fa fa-cogs',
            'title' => get_string('sitesettings', 'theme_primaryisf'),
            'list' => $list
        );

        $blocks[] = array(
            'key'   => 'manage',
            'icon'  => 'fa fa-wrench',
            'title' => get_string('manage', 'theme_primaryisf'),
            'items' => $items
        );
    }

    return $blocks;
}

function theme_primaryisf_get_courses($enrolledcourses) {
    global $DB, $CFG;

    $data = array('mastercourses'=>array(), 'courses'=>array());

    $courses = array();
    foreach ($enrolledcourses as $course) {
        $courses[] = $course->id;
    }

    list($sql_in, $params) = $DB->get_in_or_equal($courses, SQL_PARAMS_NAMED);

    $master_courses = $DB->get_records_select_menu("local_isfdup_courses", "courseid $sql_in AND master = 1", $params, '', 'id, courseid');

    if (count($master_courses)) {
        foreach($enrolledcourses as $course) {
            if (in_array($course->id, $master_courses)) {
                $data['mastercourses'][] = $course;
            } else {
                $data['courses'][] = $course;
            }
        }
    } else {
        $data['courses'] = $enrolledcourses;
    }

    return $data;
}

function theme_primaryisf_file($id, $name, $component = 'theme_primaryisf') {
    global $DB, $CFG, $USER, $SITE;

    $fs = get_file_storage();
    $files = $fs->get_area_files(context_system::instance()->id, $component, $name, $id, 'sortorder DESC, id ASC', false); // TODO: this is not very efficient!!
    if (count($files) < 1) {
        return '';
    } else {
        $file = reset($files);
        unset($files);
        $path = '/'.context_system::instance()->id.'/'.$component.'/'.$name.'/'.$id.'/'.$file->get_filename();
        return moodle_url::make_file_url('/pluginfile.php', $path);
    }
}

/**
 * Get theme setting
 * @param string $setting
 * @param string $format = false
 */
function theme_primaryisf_get_setting($setting, $format = false) {
    global $CFG;
    require_once($CFG->dirroot . '/lib/weblib.php');

    static $theme;
    if (empty($theme)) {
        $theme = theme_config::load('primaryisf');
    }

    if (empty($theme->settings->$setting)) {
        return false;
    } else if (!$format) {
        return $theme->settings->$setting;
    } else if ($format === 'format_text') {
        return format_text($theme->settings->$setting, FORMAT_PLAIN);
    } else if ($format === 'format_html') {
        return format_text($theme->settings->$setting, FORMAT_HTML, array('trusted' => true, 'noclean' => true));
    } else {
        return format_string($theme->settings->$setting);
    }
}

/**
 * Serves any files associated with the theme settings.
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param context $context
 * @param string $filearea
 * @param array $args
 * @param bool $forcedownload
 * @param array $options
 * @return bool
 */
function theme_primaryisf_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options = array()) {
    static $theme;
    if (empty($theme)) {
        $theme = theme_config::load('primaryisf');
    }

    if ($context->contextlevel == CONTEXT_SYSTEM) {

        if ($filearea == 'loginbackground' or $filearea == 'favicon' or $filearea == 'faviconpng' or $filearea == 'logo'){
            $itemid = array_shift($args); // The first item in the $args array.
            $filename = array_pop($args); // The last item in the $args array.
            if (!$args) {
                $filepath = '/'; // $args is empty => the path is '/'
            } else {
                $filepath = '/'.implode('/', $args).'/'; // $args contains elements of the filepath
            }


            // Retrieve the file from the Files API.
            $fs = get_file_storage();
            $file = $fs->get_file($context->id, 'theme_primaryisf', $filearea, $itemid, $filepath, $filename);
            if (!$file) {
                return false; // The file does not exist.
            }

            send_stored_file($file, 86400, 0, $forcedownload, $options);
        } elseif ($filearea === 'logo') {
            return $theme->setting_file_serve('logo', $args, $forcedownload, $options);
        } else if ($filearea === 'style') {
            theme_essential_serve_css($args[1]);
        } else if ($filearea === 'homebk') {
            return $theme->setting_file_serve('homebk', $args, $forcedownload, $options);
        } else if ($filearea === 'pagebackground') {
            return $theme->setting_file_serve('pagebackground', $args, $forcedownload, $options);
        } else if (preg_match("/p[1-9][0-9]/", $filearea) !== false) {
            return $theme->setting_file_serve($filearea, $args, $forcedownload, $options);
        } else if ((substr($filearea, 0, 9) === 'marketing') && (substr($filearea, 10, 5) === 'image')) {
            return $theme->setting_file_serve($filearea, $args, $forcedownload, $options);
        } else if ($filearea === 'iphoneicon') {
            return $theme->setting_file_serve('iphoneicon', $args, $forcedownload, $options);
        } else if ($filearea === 'iphoneretinaicon') {
            return $theme->setting_file_serve('iphoneretinaicon', $args, $forcedownload, $options);
        } else if ($filearea === 'ipadicon') {
            return $theme->setting_file_serve('ipadicon', $args, $forcedownload, $options);
        } else if ($filearea === 'ipadretinaicon') {
            return $theme->setting_file_serve('ipadretinaicon', $args, $forcedownload, $options);
        } else if ($filearea === 'fontfilettfheading') {
            return $theme->setting_file_serve('fontfilettfheading', $args, $forcedownload, $options);
        } else if ($filearea === 'fontfilettfbody') {
            return $theme->setting_file_serve('fontfilettfbody', $args, $forcedownload, $options);
        } else if ($filearea === 'secondaryisfmarkettingimages') {
            return $theme->setting_file_serve('secondaryisfmarkettingimages', $args, $forcedownload, $options);
        }
    } else {
        send_file_not_found();
    }
}

function theme_primaryisf_get_coursename($course) {
    if ($course->format == 'isf') {
        return course_get_format($course)->get_course_name();
    } else {
        return $course->fullname;
    }
}
