<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package   theme_primaryisf
 * @copyright 2016 Ryan Wyllie
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

if(!defined('THEME_primaryisf_DEFAULT_TOOLSMENUSCOUNT')){
    define('THEME_primaryisf_DEFAULT_TOOLSMENUSCOUNT', '3');
}
if(!defined('THEME_primaryisf_DEFAULT_SLIDERCOUNT')){
    define('THEME_primaryisf_DEFAULT_SLIDERCOUNT', '3');
}

if ($ADMIN->fulltree) {
    $settings = new theme_primaryisf_admin_settingspage_tabs('themesettingprimaryisf', get_string('configtitle', 'theme_primaryisf'));
    $page = new admin_settingpage('theme_primaryisf_general', get_string('generalsettings', 'theme_primaryisf'));

    // Preset.
    $name = 'theme_primaryisf/preset';
    $title = get_string('preset', 'theme_primaryisf');
    $description = get_string('preset_desc', 'theme_primaryisf');
    $default = 'default.scss';

    $context = context_system::instance();
    $fs = get_file_storage();
    $files = $fs->get_area_files($context->id, 'theme_primaryisf', 'preset', 0, 'itemid, filepath, filename', false);

    $choices = [];
    foreach ($files as $file) {
        $choices[$file->get_filename()] = $file->get_filename();
    }
    // These are the built in presets.
    $choices['default.scss'] = 'default.scss';
    $choices['plain.scss'] = 'plain.scss';

    $setting = new admin_setting_configselect($name, $title, $description, $default, $choices);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);


    // Login image file setting.
    $name = 'theme_primaryisf/loginbackground';
    $title = get_string('login_img','theme_primaryisf');
    $description = get_string('login_imgdesc', 'theme_primaryisf');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'loginbackground');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // Raw SCSS to include before the content.
    $setting = new admin_setting_scsscode('theme_primaryisf/scsspre',
        get_string('rawscsspre', 'theme_primaryisf'), get_string('rawscsspre_desc', 'theme_primaryisf'), '', PARAM_RAW);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    // Raw SCSS to include after the content.
    $setting = new admin_setting_scsscode('theme_primaryisf/scss', get_string('rawscss', 'theme_primaryisf'),
        get_string('rawscss_desc', 'theme_primaryisf'), '', PARAM_RAW);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $page->add($setting);

    $settings->add($page);

    // $nav_settings = new theme_primaryisf_admin_settingspage_tabs('theme_primaryisf_header_navbar_menu', get_string('navbarmenusettings', 'theme_primaryisf'));

    include(dirname(__FILE__) . '/settings/navbar.php');
    $settings->add($temp);

    include(dirname(__FILE__) . '/settings/frontpage_slider.php');
    $settings->add($temp);
}
