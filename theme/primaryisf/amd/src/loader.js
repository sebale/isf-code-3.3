// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Template renderer for Moodle. Load and render Moodle templates with Mustache.
 *
 * @module     core/templates
 * @package    core
 * @class      templates
 * @copyright  2015 Damyon Wiese <damyon@moodle.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @since      2.9
 */
define(['jquery', './tether', 'core/event'], function(jQuery, Tether, Event) {

    window.jQuery = jQuery;
    window.Tether = Tether;

    require(['theme_primaryisf/util',
            'theme_primaryisf/alert',
            'theme_primaryisf/button',
            'theme_primaryisf/carousel',
            'theme_primaryisf/collapse',
            'theme_primaryisf/dropdown',
            'theme_primaryisf/modal',
            'theme_primaryisf/scrollspy',
            'theme_primaryisf/tab',
            'theme_primaryisf/tooltip',
            'theme_primaryisf/popover',
            'theme_primaryisf/designification'],
        function() {

            jQuery('body').popover({
                selector: '[data-toggle="popover"]',
                trigger: 'focus'
            });

            // We need to call popover automatically if nodes are added to the page later.
            Event.getLegacyEvents().done(function(events) {
                jQuery(document).on(events.FILTER_CONTENT_UPDATED, function() {
                    jQuery('body').popover({
                        selector: '[data-toggle="popover"]',
                        trigger: 'focus'
                    });
                });
            });
        });

    window.toggleFilemanager = function(obj){
        jQuery(obj).toggleClass('active');
        jQuery(obj).next().toggleClass('active');
    }


    jQuery(window).ready(function () {
        jQuery('.path-grade-report #user-grades').scroll(function () {
            window.scrollBy(0,-1);
            window.scrollBy(0,1);
        });
    });

    return {};
});
