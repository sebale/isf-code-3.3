// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Contain the logic for a main nav and sidebar top-level actions
 *
 * @package    theme_primaryisf
 * @module     theme_primaryisf/pseudotabs
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define(['jquery', 'core/log', 'theme_primaryisf/pseudotabs_config', 'core/str', 'core/config'], function($, Log, Config, Str, conf) {

  "use strict";

  Log.debug('init of tabs');

  const body = $('body');

  const Pseudotabs = {

    storage: {
      // index of default displayed item
      current: 0,
      // count of items on storage
      count: 0
    },

    // this var is used for re-opening of tabs with prev closed item
    tmpTab: -1,

    // preview all flag
    reviewFlag: false,

    // default localization
    localization: {
      next: 'Next',
      prev: 'Previous',
      preview: 'Preview',
      previewAll: 'Preview All',
      basic: 'Show all options',
      advanced: 'Show Advanced'
    },

    loadStorageConfig: function(){
      return new Promise(function(resolve, reject){
        Log.debug('get local storage into App');
        var id = $('body').attr('id').replace(/-/g, '_');
        $.getJSON(conf.wwwroot+'/theme/primaryisf/ajax.php?action=get_pseudotabs_config', function(storage){

        if(storage && storage != null){
            storage[id] =  typeof storage[id] == 'undefined' ? Pseudotabs.storage : storage[id];
        } else {
            // here is one more if
            if(storage == null) storage = {};
            storage[id] = Pseudotabs.storage;
        }

          Log.debug('storage '+id);
          Log.debug(storage[id]);

          // save config to current Pseudotabs object
          Pseudotabs.storage = storage[id];
          Pseudotabs.pageId = id
          Pseudotabs.configStorage = storage;
          resolve(true);
        });
      });
    },

    saveStorage: function(){
      const data = {
        config: Pseudotabs.configStorage
      };

      $.ajax({
          url: conf.wwwroot+'/theme/primaryisf/ajax.php?action=set_pseudotabs_config',
          type: "POST",
          data: data,
      }).done(function( data ) {
          Log.debug(data);
      });
    },

    loadLocalization: function (cb) {
      Str.get_strings([
        {'key' : 'next', component : 'theme_primaryisf'},
        {'key' : 'prev', component : 'theme_primaryisf'},
        {'key' : 'preview', component : 'theme_primaryisf'},
        {'key' : 'showbasic', component : 'theme_primaryisf'},
        {'key' : 'showall', component : 'theme_primaryisf'}
      ]).done(function(Loc) {
        Pseudotabs.localization = {
          next: Loc[0],
          prev: Loc[1],
          preview: Loc[2],
          previewAll: Loc[2],
          basic: Loc[3],
          advanced: Loc[4]
        };

        cb();
      });
    },

    transform: function(){
      return new Promise(function(resolve, reject){
        this.fields = body.find('form fieldset.collapsible');
        this.storage.count = this.fields.length - 1;

        $('<ul id="pseudotabs-tab-box" class="nav nav-tabs" role="tablist"></ul>').insertBefore(this.fields.eq(0).parent());

        const nav = $('#pseudotabs-tab-box');

        this.header = nav;
        this.fields.each(function(ind){
          const name = $(this).children('legend.ftoggler').text();
          var required = $(this).find('[title="Required"]').length;

          if(Config.checkRequired($(this))) {
            required = true;
          }

          if(required) {
              $(this).addClass('pseudotabs-req-tab');
          }

          $(this).prepend('<h3 class="hidden pseudotabs-h3">' + name +  '</h3>');
          $(this).attr('data-location', ind);
          nav.append('<li class="nav-item"><a class="btn" href="#" data-ind="' + ind + '">' + name + '</a></li>');
          $(this).addClass('hidden').children('legend.ftoggler').addClass('hidden');
        });

        /* advanced code to display "show/less" blocks if they are required */
        var bodyId = '#' + $(body).attr('id');
        if(typeof Config.customRequired[bodyId] != 'undefined'){
          var elems = Config.customRequired[bodyId].fields;
          for (var i=0, c=elems.length; i<c; i++) {
            if($(elems[i]).length && $(elems[i]).closest('.fcontainer').find('.moreless-toggler').length ){
              Log.debug('founded req. showless elem, show it');
              $(elems[i]).closest('.fcontainer').find('.moreless-toggler')[0].click();
            }
          }
        }

        nav.append('<li class="nav-item nav-item-preview"><a class="btn" href="#">' + Pseudotabs.localization.preview + '</a></li>');

        //this.fields.eq(0).parent().addClass('pseudotabs-form');
          body.find('.mform').addClass('pseudotabs-form');
        body.find('.collapsible-actions').addClass('hidden');

        $(document).on('click', '#pseudotabs-tab-box .btn', function () {
          const index = parseInt($(this).attr('data-ind'), 10);
          Log.debug('show from top nav ');
          Log.debug(index);
          Pseudotabs.postPreview();
          Pseudotabs.showTab(index);
        });

        resolve('all tabs was generates successfull');
      }.bind(this));
    },

    createNav: function(){
      return new Promise(function(resolve, reject){
        $('.collapsible-actions').remove();
        var box;

        if($('.pseudotabs-form').find('.form-group .btn-cancel').length){
          box = $('.pseudotabs-form').find('.form-group .btn-cancel').parent('div').addClass('pseudotabs-footer-nav');
        } else if($('.pseudotabs-form').find('.form-group input[type="submit"]')) {
          box = $('.pseudotabs-form').find('.form-group input[type="submit"]').parent('div').addClass('pseudotabs-footer-nav');
        }

        this.nav = box;
        this.nav.removeClass('col-md-9').addClass('col-md-12');
        // create nav elements

        $('<div class="btn btn-primary pseudotabs-btn-advanced closed">' + Pseudotabs.localization.advanced + '</div>').insertBefore('.pseudotabs-form');

        $('<div class="form-group fitem">\
              <div class="btn btn-primary hidden pseudotabs-btn-prev">' + Pseudotabs.localization.prev + '</div>\
          </div>\
          <div class="form-group fitem">\
            <div class="btn btn-primary hidden pseudotabs-btn-next">' + Pseudotabs.localization.next + '</div>\
          </div>\
          <div class="form-group fitem">\
              <div class="btn btn-primary hidden pseudotabs-btn-preview">' + Pseudotabs.localization.preview + '</div>\
          </div>').prependTo(box);

        // register events

        $(document).on('click', '.pseudotabs-btn-advanced', function (e) {
          Pseudotabs.tmpTab = Pseudotabs.storage.current;
          Pseudotabs.manageShowAllBtn($(e.target));
        });

        $(document).on('click', '.pseudotabs-btn-next', function (e) {
          Pseudotabs.showNextTab(e);
        });

        $(document).on('click', '.pseudotabs-btn-prev', function (e) {
          Pseudotabs.showPrevTab(e);
        });

        $(document).on('click', '.pseudotabs-btn-preview', function (e) {
          Pseudotabs.showPreview(e);
        });

        $(document).on('click', '.nav-item-preview', function (e) {
          Pseudotabs.showPreview(e);
        });

        $('.pseudotabs-form').submit(function(e){
          // e.preventDefaut();
          Pseudotabs.clearStorage();
          // $('.pseudotabs-form').submit();
        });

        resolve(true);

      }.bind(this));
    },

    clearStorage: function(){
      Log.debug('clear storage for this page');
      this.storage = {};
      var id = $('body').attr('id').replace(/-/g, '_');
      delete this.configStorage[id];
      this.saveStorage();
    },

    manageShowAllBtn(el, flag){
      flag = typeof flag == 'undefined' ? 0 : 1;
      let text = Pseudotabs.localization.advanced;

      if(el.hasClass('closed')){
        text = Pseudotabs.localization.basic;
        el.removeClass('closed')
        if(!flag) Pseudotabs.showAdvanced(el);
      } else {
        el.addClass('closed');
        if(!flag) Pseudotabs.hideAdvanced(el);
      }
      el.text(text);
      return true;
    },

    showNextTab: function(){
      const next = this.storage.current + 1;
      this.showTab(next);
    },

    showPrevTab: function(e){
      var prev = this.storage.current;
      if(this.reviewFlag){
        this.postPreview();
        this.fields.find('h3').addClass('hidden');
      } else {
        prev = this.storage.current - 1;
      }
      this.showTab(prev);
    },

    showPreview: function(e){
      console.log('show all content');
      this.reviewFlag = true;
      // this.header.addClass('hidden');
      this.header.find('.active').removeClass('active');
      this.fields.each(function(){
        $(this).removeClass('hidden');
        if ($(this).hasClass('collapsed')) {
          $(this).children('legend').children('a')[0].click();
        }
        $(this).children('.pseudotabs-h3').removeClass('hidden');
      });
      this.nav.find('.pseudotabs-btn-preview, .pseudotabs-btn-next').addClass('hidden');
      this.nav.find('.pseudotabs-btn-prev').removeClass('hidden');

      this.storage.current = this.storage.count;
    },

    postPreview: function() {
      if(this.reviewFlag){
        this.reviewFlag = false;
        this.fields.find('h3').addClass('hidden');
      }
    },

    showTab: function(index, notSaveFlag) {

      if ( index <= 0 ) {
        Log.debug('hide prev');
        this.nav.find('.pseudotabs-btn-prev').addClass('hidden');
      }

      if ( index < 0 ) {
        Log.debug('wrong index it must be int');
        return false;
      }

      if ( index > this.storage.count ) {
        Log.debug('last tab');
        return false;
      }

      if( index > 0){
        this.nav.find('.pseudotabs-btn-prev').removeClass('hidden');
      }

      if (index > 0 && index < this.storage.count) {
        this.nav.find('.pseudotabs-btn-next').removeClass('hidden');
      }

      if (index == this.storage.count) {
        Log.debug('next preview');
        this.nav.find('.pseudotabs-btn-next').addClass('hidden');
        this.nav.find('.pseudotabs-btn-preview').removeClass('hidden');
      } else {
        this.nav.find('.pseudotabs-btn-preview').addClass('hidden');
      }

      if(typeof notSaveFlag == 'undefined'){
        this.storage.current = index;
        Log.debug('save storage')
        this.saveStorage();
      }
      Log.debug(this.storage);
      
      this.showItem(index);
    },

    showItem: function(index){
      index = typeof index === "undefined" ? this.storage.current : index;
      const field = $('.pseudotabs-form').children('fieldset[data-location="' + index + '"]');

      this.hideItem();
      $('#pseudotabs-tab-box').find('li > a[data-ind="' + index + '"]').parent().addClass('active');
      // field.css({'opacity':0});
      
      // setTimeout(function(){
        field.removeClass('hidden');
        // field.animate({opacity: 1}, 200);
      // }, 200);
      
      if (field.hasClass('collapsed')) {
        field.children('legend').children('a')[0].click();
      }
      this.storage.current = index;
    },

    hideItem: function(index){
      const fields = $('.pseudotabs-form').children('fieldset');

      $('#pseudotabs-tab-box').find('li.active').removeClass('active');
      
      fields.each(function(){
        // $(this).animate({opacity: 0}, 100, function(){    
          $(this).addClass('hidden');
          // $(this).removeAttr('style');
          
          if (!$(this).hasClass('collapsed')) {
            $(this).children('legend').children('a')[0].click();
          }
        // });
      });

    },

        showBasicForm: function(){
      this.header.addClass('hidden');
      this.nav.find('.pseudotabs-btn-prev, .pseudotabs-btn-next').addClass('hidden');

      try {
        this.fields.each(function(){
          $(this).find('h3').addClass('hidden');
          if ($(this).hasClass('pseudotabs-req-tab')) {
              $(this).removeClass('hidden');
              if ($(this).hasClass('collapsed')) {
                //$(this).children('legend').children('a').eq(0).click();
                  $(this).removeClass('collapsed');
              }
          } else {
            $(this).addClass('hidden');
            if (!$(this).hasClass('collapsed') && $(this).children('legend').children('a').length) {
              $(this).children('legend').children('a').eq(0).click();
            }
          }
        });
        if (!$('.pseudotabs-req-tab').length) {
          this.fields.eq(0).removeClass('hidden');
          if (this.fields.eq(0).hasClass('collapsed')) {
            this.fields.eq(0).children('legend').children('a').eq(0).click();
          }
        }
      } catch(e){
        Log.debug('error on clicking element');
      };
    },

    showAdvanced: function(e) {
      this.header.removeClass('hidden');
      this.nav.find('.pseudotabs-btn-next').removeClass('hidden');
      if(Pseudotabs.tmpTab > -1){
        this.showTab(Pseudotabs.tmpTab);  
      } else {
        this.showTab(0, true);
      }
      
    },

    hideAdvanced: function(e) {
      this.header.addClass('hidden');
      this.nav.find('.pseudotabs-btn-preview').addClass('hidden');
      this.showBasicForm();
      this.hideMoreLess();
    },

    hideMoreLess: function(){
      $('.moreless-toggler.moreless-less').each(function(){
        Log.debug('close all less/more');
        $(this)[0].click();
      });
    },

    showMainBlock: function(){
      setTimeout(function(){
        $('div[role="main"]').animate({"opacity": 1}, 300);
      }, 1500);
    },

    init: function(){
      Log.debug('Pseudotabs init');

      if( $('form > fieldset').length > 1 && $('form > fieldset > legend.ftoggler').length) {
        
        const flag = Config.isBlocked();

        if (!flag) {
          Log.debug('pseudotabs isn`t blocked on this page');
          
          this.hideMoreLess();

          $('div[role="main"]').animate({"opacity": 0}, 200);

          this.loadStorageConfig();

          const tmpStorage = this.storage;

          Log.debug('this storage is' + JSON.stringify(tmpStorage));

          this.loadLocalization(function(){
            Pseudotabs
            .transform()
            .then(function(res){
              Pseudotabs
                .createNav()
                .then(function(){
                  Log.debug('tabs inited');
                  
                  if(tmpStorage.current == 0){
                    Pseudotabs.showBasicForm();
                    Pseudotabs.showMainBlock();
                  } else {
                    Log.debug('show previously opened item')
                    setTimeout(function(){
                      
                      Pseudotabs.manageShowAllBtn($('.pseudotabs-btn-advanced').eq(0), true);

                      setTimeout(function(){
                        Pseudotabs.storage = tmpStorage;
                        Log.debug('show tab on init '+tmpStorage.current)
                        Pseudotabs.showTab(tmpStorage.current);
                      }, 10);

                      Pseudotabs.showMainBlock();

                    }, 2000);
                  }
                });
            });
          });

        }
      }

      return this;
    }

  };

  return Pseudotabs.init();
});
