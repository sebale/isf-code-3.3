// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Contain the logic for a main nav and sidebar top-level actions
 *
 * @package    theme_primaryisf
 * @module     theme_primaryisf/designification
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define(['jquery', 'core/config', 'core/log', 'theme_primaryisf/pseudotabs'], function($, cfgconfig, Log) {

    window.blogLike = function(obj, postid){
        var state = 0;
        var likes = parseInt($(obj).find('span').text());
        $(obj).toggleClass('liked');
        if ($(obj).hasClass('liked')){
            state = 1;
            likes++;
        } else {
            likes--;
        }
        $(obj).find('span').text(likes)

        var newtitle = $(obj).attr('data-newtitle');
        var title = $(obj).attr('title');
        $(obj).attr('data-newtitle', title);
        $(obj).attr('title', newtitle);

        $.get( cfgconfig.wwwroot+'/local/manager/ajax.php?action=blog_set_like&id='+postid+'&state='+state, function( data ) {
        });
    }

    window.blogHelpful = function(obj, postid){
        var state = 0;
        var count = parseInt($(obj).find('span').text());
        $(obj).toggleClass('helpful');
        if ($(obj).hasClass('helpful')){
            state = 1;
            count++;
        } else {
            count--;
        }
        $(obj).find('span').text(count)

        var newtitle = $(obj).attr('data-newtitle');
        var title = $(obj).attr('title');
        $(obj).attr('data-newtitle', title);
        $(obj).attr('title', newtitle);

        $.get( cfgconfig.wwwroot+'/local/manager/ajax.php?action=blog_set_helpful&id='+postid+'&state='+state, function( data ) {
        });
    }

  var Extenders = {
    page_message_index: function(){
      Log.debug('run fix design');
      $('.btn-link:not([data-action="view-contact-profile"])').removeClass('btn-link').addClass('btn-info');

      setTimeout(function(){
        Extenders.page_message_index();
      }, 100);
    },

    page_login_index: function(){
      // clear pseudotabs storage when user login
       localStorage.setItem('pseudotabs', '{}');
    },

    fixActionMenu: function(){
      // hide eleemnts
      $('.dropdown-menu[id*="action-menu"] > .dropdown-item .currentlink').each(function(){
        $(this).addClass('toggle-nav closed').parent().nextAll('div:has(.p-l-2)').addClass('hidden');
      });

      // add events
      $('.dropdown-menu[id*="action-menu"] > .dropdown-item .currentlink').click(function(e){
        var el = $(this).closest('.dropdown-menu').prev();


        setTimeout(function(){
          el.click();
        }, 1);

        if($(this).hasClass('closed')){
          $(this).removeClass('closed');
          $(this).parent().nextAll('div:has(.p-l-2)').removeClass('hidden');
        } else {
          $(this).addClass('closed');
          $(this).parent().nextAll('div:has(.p-l-2)').addClass('hidden');
        }

        console.log('click on nav')
      });
    },

    fixDesignThings: function(){
      $('.section-cm-edit-actions + a[onclick*="if(confirm("]').prev().addClass('withLockedIcon');

      var res = $('<ul class="list-unstyled p-t-1">\
        <li>\
          <ul class="list-unstyled m-l-1"></ul>\
        </li>\
      </ul>');

      $('.navbar-nav .dropdown').eq(1).find('a').each(function(i){
        if(i==0){
          res.prepend('<li><a href="' + $(this).attr('href') + '" title="' + $(this).text() + '">' + $(this).text() + '</a></li>');
        } else {
          res.find('ul').append('<li><a href="' + $(this).attr('href') + '" title="' + $(this).text() + '">' + $(this).text() + '</a></li>');
        }
      });

      $('footer .navbar-nav').append(res);
    }

  };

  Designification = {

    init: function(){
      Log.debug('Extend design init');
      var body = $('body').attr('id').replace(/-/g, '_');

      Extenders.fixActionMenu();
      Extenders.fixDesignThings();

      if (typeof Extenders[body] !== 'undefined') {
        Extenders[body]();
      }

      return {};
    }

  };

  return Designification.init();
});
