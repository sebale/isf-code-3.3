// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Contain the logic for a main nav and sidebar top-level actions
 *
 * @package    theme_primaryisf
 * @module     theme_primaryisf/pseudotabs_config
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define(['jquery', 'core/log'], function($, Log) {

  "use strict";

  const PseudotabsConfig = {

    // if this selectors will found on page Pseudotabs will not inject on it
    block: [
      'body.path-admin-setting',
      '#page-course-view-isf',
      'body.path-local-agenda',
      '#page-question-type-ddmarker'
    ],

    customRequired: {
      '#page-mod-assign-mod': {
        fields: [
          'label[for="id_duedate"]',
          'label[id="id_powerschool_mod_sync"]',
          'label[id="id_mod_count_in_final_grade"]',
          'label[id="id_mod_standard_sync"]',
          'label[for="id_grade_modgrade_point"]',
          'label[id^="id_outcome_"]',
        ]
      },
      '#page-mod-label-mod': {
        fields: [
          'div[data-formgorup-fieldtype="editor"]'
        ]
      },
      "#page-mod-lesson-mod": {
        fields: [
          '.filemanager-toggler'
        ]
      }
    },

    /**
    * check if Pseudotabs are blocked on this page
    * @return [bool] flag
    */
    isBlocked: function(){
      var flag = false;
      Log.debug('start blocking checking')
      for (var i=0, c=this.block.length; i<c; i++) {
        Log.debug('track ' + this.block[i]);
        if($(this.block[i]).length) {
          Log.debug('block ' + this.block[i]);
          flag = true;
        }
      }

      Log.debug('flag is ' + flag);
      return flag;
    },

    /**
    * check if Pseudotabs are blocked on this page
    * @return [bool] flag
    */
    checkRequired: function(fieldset){
      var flag = false;

      $.each(this.customRequired, function(key, el){
        if($(key).length) {
          for (var i=0, c=el.fields.length; i<c; i++) {
            if(fieldset.find(el.fields[i]).length) {
              flag = true;
            }
          }
        }
      });

      return flag;
    }

  };

  return PseudotabsConfig;
});