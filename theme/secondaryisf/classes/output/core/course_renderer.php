<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Course renderer.
 *
 * @package    theme_noanme
 * @copyright  2016 Frédéric Massart - FMCorz.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace theme_secondaryisf\output\core;
defined('MOODLE_INTERNAL') || die();

use coding_exception;
use html_writer;
use tabobject;
use tabtree;
use custom_menu_item;
use custom_menu;
use block_contents;
use navigation_node;
use action_link;
use stdClass;
use moodle_url;
use preferences_groups;
use action_menu;
use help_icon;
use single_button;
use single_select;
use paging_bar;
use url_select;
use context_course;
use pix_icon;
use cm_info;
use moodle_page;
use context_module;
use completion_info;
use core_text;
use action_menu_link_secondary;
use course_in_list;

require_once($CFG->dirroot . '/course/renderer.php');
require_once($CFG->dirroot.'/course/format/isf/renderer.php');

/**
 * Course renderer class.
 *
 * @package    theme_noanme
 * @copyright  2016 Frédéric Massart - FMCorz.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class course_renderer extends \core_course_renderer {

    public $_user_preferences = array();

    /**
     * Override the constructor so that we can initialise the string cache
     *
     * @param moodle_page $page
     * @param string $target
     */
    public function __construct(moodle_page $page, $target) {
        $this->strings = new stdClass;
        parent::__construct($page, $target);

        $this->get_user_preferences();
    }

    public function get_user_preferences() {
        global $CFG;
        require_once($CFG->dirroot.'/course/format/isf/lib.php');

        $course = $this->page->course;

        $preferences_list = format_isf_get_preferences_list($course);

        foreach ($preferences_list as $name=>$default) {
            $pref_name = str_replace('_'.$course->id, '', $name);
            $this->_user_preferences[$pref_name] = get_user_preferences($name, $default);

            if ($pref_name == 'fav_modules'){
                if ($this->_user_preferences[$pref_name] != '0') {
                    $modules = explode(',', $this->_user_preferences[$pref_name]);
                    $this->_user_preferences[$pref_name] = $modules;
                }
            }
        }
    }

    /**
     * Renders html to display a course search form.
     *
     * @param string $value default value to populate the search field
     * @param string $format display format - 'plain' (default), 'short' or 'navbar'
     * @return string
     */
    public function course_search_form($value = '', $format = 'plain') {
        static $count = 0;
        $formid = 'coursesearch';
        if ((++$count) > 1) {
            $formid .= $count;
        }

        switch ($format) {
            case 'navbar' :
                $formid = 'coursesearchnavbar';
                $inputid = 'navsearchbox';
                $inputsize = 20;
                break;
            case 'short' :
                $inputid = 'shortsearchbox';
                $inputsize = 12;
                break;
            default :
                $inputid = 'coursesearchbox';
                $inputsize = 30;
        }

        $data = (object) [
            'searchurl' => (new moodle_url('/course/search.php'))->out(false),
            'id' => $formid,
            'inputid' => $inputid,
            'inputsize' => $inputsize,
            'value' => $value
        ];

        return $this->render_from_template('theme_primaryisf/course_search_form', $data);
    }

    /**
     * Renders HTML for the menus to add activities and resources to the current course
     *
     * @param stdClass $course
     * @param int $section relative section number (field course_sections.section)
     * @param int $sectionreturn The section to link back to
     * @param array $displayoptions additional display options, for example blocks add
     *     option 'inblock' => true, suggesting to display controls vertically
     * @return string
     */
    function course_section_add_cm_control($course, $section, $sectionreturn = null, $displayoptions = array()) {
        global $CFG;

        $vertical = !empty($displayoptions['inblock']);

        // check to see if user can add menus and there are modules to add
        if (!has_capability('moodle/course:manageactivities', context_course::instance($course->id))
            || !$this->page->user_is_editing()
            || !($modnames = get_module_types_names()) || empty($modnames)) {
            return '';
        }

        // Retrieve all modules with associated metadata
        $modules = get_module_metadata($course, $modnames, $sectionreturn);
        $urlparams = array('section' => $section);

        // We'll sort resources and activities into two lists
        $activities = array(MOD_CLASS_ACTIVITY => array(), MOD_CLASS_RESOURCE => array());

        foreach ($modules as $module) {
            $activityclass = MOD_CLASS_ACTIVITY;
            if ($module->archetype == MOD_ARCHETYPE_RESOURCE) {
                $activityclass = MOD_CLASS_RESOURCE;
            } else if ($module->archetype === MOD_ARCHETYPE_SYSTEM) {
                // System modules cannot be added by user, do not add to dropdown.
                continue;
            }
            $link = $module->link->out(true, $urlparams);
            $activities[$activityclass][$link] = $module->title;
        }

        $straddactivity = get_string('addactivity');
        $straddresource = get_string('addresource');
        $sectionname = get_section_name($course, $section);
        $strresourcelabel = get_string('addresourcetosection', null, $sectionname);
        $stractivitylabel = get_string('addactivitytosection', null, $sectionname);

        $output = html_writer::start_tag('div', array('class' => 'section_add_menus', 'id' => 'add_menus-section-' . $section));

        if (!$vertical) {
            $output .= html_writer::start_tag('div', array('class' => 'horizontal'));
        }

        if (!empty($activities[MOD_CLASS_RESOURCE])) {
            $select = new url_select($activities[MOD_CLASS_RESOURCE], '', array(''=>$straddresource), "ressection$section");
            $select->set_help_icon('resources');
            $select->set_label($strresourcelabel, array('class' => 'accesshide'));
            $output .= $this->output->render($select);
        }

        if (!empty($activities[MOD_CLASS_ACTIVITY])) {
            $select = new url_select($activities[MOD_CLASS_ACTIVITY], '', array(''=>$straddactivity), "section$section");
            $select->set_help_icon('activities');
            $select->set_label($stractivitylabel, array('class' => 'accesshide'));
            $output .= $this->output->render($select);
        }

        if (!$vertical) {
            $output .= html_writer::end_tag('div');
        }

        $output .= html_writer::end_tag('div');

        if (course_ajax_enabled($course) && $course->id == $this->page->course->id) {
            // modchooser can be added only for the current course set on the page!
            $straddeither = get_string('add', 'theme_secondaryisf');
            // The module chooser link
            $modchooser = html_writer::start_tag('div', array('class' => 'mdl-right'));

            // quick add
            $modchooser.= html_writer::start_tag('div', array('class' => 'section-modchooser'));

            $modinfo = get_fast_modinfo($course);
            $section = $modinfo->get_section_info($section);
            $modchooser .= html_writer::start_tag('span', array('class'=>'quickmodule-chooser', 'data-section-id'=>$section->id));
            $modchooser .= html_writer::tag('span', '<i class="fa fa-bolt"></i>', array('class' => 'btn btn-primary', 'title'=>get_string('addquickmodule', 'theme_secondaryisf')));
            $modchooser .= html_writer::end_tag('span');

            $icon = $this->output->pix_icon('t/add', '');
            $span = html_writer::tag('span', $straddeither, array('class' => 'section-modchooser-text'));
            $modchooser .= html_writer::tag('span', $icon . $span, array('class' => 'section-modchooser-link btn btn-primary', 'id'=>'section-modchooser-'.$section->id));
            $modchooser.= html_writer::end_tag('div');
            $modchooser.= html_writer::end_tag('div');

            // Wrap the normal output in a noscript div
            $usemodchooser = get_user_preferences('usemodchooser', $CFG->modchooserdefault);
            if ($usemodchooser) {
                $output = html_writer::tag('div', $output, array('class' => 'hiddenifjs addresourcedropdown'));
                $modchooser = html_writer::tag('div', $modchooser, array('class' => 'visibleifjs addresourcemodchooser'));
            } else {
                // If the module chooser is disabled, we need to ensure that the dropdowns are shown even if javascript is disabled
                $output = html_writer::tag('div', $output, array('class' => 'show addresourcedropdown'));
                $modchooser = html_writer::tag('div', $modchooser, array('class' => 'hide addresourcemodchooser'));
            }
            $output = $this->course_modchooser($modules, $course) . $modchooser . $output;
        }

        return $output;
    }

    /**
     * Renders HTML to display a list of course modules in a course section
     * Also displays "move here" controls in Javascript-disabled mode
     *
     * This function calls {@link core_course_renderer::course_section_cm()}
     *
     * @param stdClass $course course object
     * @param int|stdClass|section_info $section relative section number or section object
     * @param int $sectionreturn section number to return to
     * @param int $displayoptions
     * @return void
     */
    public function course_section_cm_list($course, $section, $sectionreturn = null, $displayoptions = array()) {
        global $USER;

        $output = '';
        $modinfo = get_fast_modinfo($course);
        if (is_object($section)) {
            $section = $modinfo->get_section_info($section->section);
        } else {
            $section = $modinfo->get_section_info($section);
        }
        $completioninfo = new completion_info($course);

        // check if we are currently in the process of moving a module with JavaScript disabled
        $ismoving = $this->page->user_is_editing() && ismoving($course->id);
        if ($ismoving) {
            $movingpix = new pix_icon('movehere', get_string('movehere'), 'moodle', array('class' => 'movetarget'));
            $strmovefull = strip_tags(get_string("movefull", "", "'$USER->activitycopyname'"));
        }

        // Get the list of modules visible to user (excluding the module being moved if there is one)
        $moduleshtml = array();
        if (!empty($modinfo->sections[$section->section])) {
            foreach ($modinfo->sections[$section->section] as $modnumber) {
                $mod = $modinfo->cms[$modnumber];

                if ($ismoving and $mod->id == $USER->activitycopy) {
                    // do not display moving mod
                    continue;
                }

                if ($modulehtml = $this->course_section_cm_list_item($course,
                    $completioninfo, $mod, $sectionreturn, $displayoptions)) {
                    $moduleshtml[$modnumber] = $modulehtml;
                }
            }
        }

        $sectionoutput = '';
        if (!empty($moduleshtml) || $ismoving) {
            foreach ($moduleshtml as $modnumber => $modulehtml) {
                if ($ismoving) {
                    $movingurl = new moodle_url('/course/mod.php', array('moveto' => $modnumber, 'sesskey' => sesskey()));
                    $sectionoutput .= html_writer::tag('li',
                        html_writer::link($movingurl, $this->output->render($movingpix), array('title' => $strmovefull)),
                        array('class' => 'movehere'));
                }

                $sectionoutput .= $modulehtml;
            }

            if ($ismoving) {
                $movingurl = new moodle_url('/course/mod.php', array('movetosection' => $section->id, 'sesskey' => sesskey()));
                $sectionoutput .= html_writer::tag('li',
                    html_writer::link($movingurl, $this->output->render($movingpix), array('title' => $strmovefull)),
                    array('class' => 'movehere'));
            }
        }

        // Always output the section module list.
        $output .= html_writer::tag('ul', $sectionoutput, array('class' => 'section clearfix img-text'));

        return $output;
    }

    /**
     * Renders HTML to display one course module for display within a section.
     *
     * This function calls:
     * {@link core_course_renderer::course_section_cm()}
     *
     * @param stdClass $course
     * @param completion_info $completioninfo
     * @param cm_info $mod
     * @param int|null $sectionreturn
     * @param array $displayoptions
     * @return String
     */
    public function course_section_cm_list_item($course, &$completioninfo, cm_info $mod, $sectionreturn, $displayoptions = array()) {
        $output = '';
        if ($modulehtml = $this->course_section_cm($course, $completioninfo, $mod, $sectionreturn, $displayoptions)) {

            $archetype = plugin_supports('mod', $mod->modname, FEATURE_MOD_ARCHETYPE, MOD_ARCHETYPE_OTHER);
            $type = ($archetype == MOD_ARCHETYPE_RESOURCE || $archetype == MOD_ARCHETYPE_SYSTEM) ? 'res' : 'act';

            $modclasses = 'activity '.$type.' ' . $mod->modname . ' modtype_' . $mod->modname . ' ' . $mod->extraclasses;

            if ($mod->uservisible) {
                $conditionalhidden = $this->is_cm_conditionally_hidden($mod);
                $accessiblebutdim = (!$mod->visible || $conditionalhidden) &&
                    has_capability('moodle/course:viewhiddenactivities', $mod->context);
                if ($accessiblebutdim) {
                    $modclasses .= ' dimmed';
                }
            } else {
                $modclasses .= ' dimmed';
            }

            $style = '';

            if (isset($course->coursetype) and $course->coursetype){
                $image_url = course_get_format($course->id)->get_activity_imageurl($course, $mod);
                $style = 'background:url("'.$image_url.'") no-repeat scroll center center; background-size: cover;';
            }

            $output .= html_writer::tag('li', $modulehtml, array('class' => $modclasses, 'id' => 'module-' . $mod->id, 'style' => $style));
        }
        return $output;
    }

    /**
     * Displays one course in the list of courses.
     *
     * This is an internal function, to display an information about just one course
     * please use {@link core_course_renderer::course_info_box()}
     *
     * @param coursecat_helper $chelper various display options
     * @param course_in_list|stdClass $course
     * @param string $additionalclasses additional classes to add to the main <div> tag (usually
     *    depend on the course position in list - first/last/even/odd)
     * @return string
     */
    protected function coursecat_coursebox(\coursecat_helper $chelper, $course, $additionalclasses = '') {
        global $CFG,$PAGE;

        if($PAGE->pagelayout != "frontpage"){
            if (!isset($this->strings->summary)) {
                $this->strings->summary = get_string('summary');
            }
            if ($chelper->get_show_courses() <= self::COURSECAT_SHOW_COURSES_COUNT) {
                return '';
            }
            if ($course instanceof stdClass) {
                require_once($CFG->libdir. '/coursecatlib.php');
                $course = new course_in_list($course);
            }
            $content = '';
            $classes = trim('coursebox clearfix '. $additionalclasses);
            if ($chelper->get_show_courses() >= self::COURSECAT_SHOW_COURSES_EXPANDED) {
                $nametag = 'h3';
            } else {
                $classes .= ' collapsed';
                $nametag = 'div';
            }

            // .coursebox
            $content .= html_writer::start_tag('div', array(
                'class' => $classes,
                'data-courseid' => $course->id,
                'data-type' => self::COURSECAT_TYPE_COURSE,
            ));
            $course_format = course_get_format($course->id)->get_course();
            $url_params = array('id' => $course->id);
            if($course_format->marker > 0){
                $url_params['section'] = $course_format->marker;
            }

            $content .= html_writer::start_tag('div', array('class' => 'info'));

            // course name
            $coursename = $chelper->get_course_formatted_name($course);
            $coursenamelink = html_writer::link(new moodle_url('/course/view.php', $url_params),
                $coursename, array('class' => $course->visible ? '' : 'dimmed'));
            $content .= html_writer::tag($nametag, $coursenamelink, array('class' => 'coursename'));
            // If we display course in collapsed form but the course has summary or course contacts, display the link to the info page.
            $content .= html_writer::start_tag('div', array('class' => 'moreinfo'));
            if ($chelper->get_show_courses() < self::COURSECAT_SHOW_COURSES_EXPANDED) {
                if ($course->has_summary() || $course->has_course_contacts() || $course->has_course_overviewfiles()) {
                    $url = new moodle_url('/course/info.php', array('id' => $course->id));
                    $image = html_writer::empty_tag('img', array('src' => $this->output->pix_url('i/info'),
                        'alt' => $this->strings->summary));
                    $content .= html_writer::link($url, $image, array('title' => $this->strings->summary));
                    // Make sure JS file to expand course content is included.
                    $this->coursecat_include_js();
                }
            }
            $content .= html_writer::end_tag('div'); // .moreinfo

            // print enrolmenticons
            if ($icons = enrol_get_course_info_icons($course)) {
                $content .= html_writer::start_tag('div', array('class' => 'enrolmenticons'));
                foreach ($icons as $pix_icon) {
                    $content .= $this->render($pix_icon);
                }
                $content .= html_writer::end_tag('div'); // .enrolmenticons
            }

            $content .= html_writer::end_tag('div'); // .info

            $content .= html_writer::start_tag('div', array('class' => 'content'));
            $content .= $this->coursecat_coursebox_content($chelper, $course);
            $content .= html_writer::end_tag('div'); // .content

            $content .= html_writer::end_tag('div'); // .coursebox
            return $content;
        }else{

            if(!isset($this->strings->summary)){
                $this->strings->summary = get_string('summary');
            }
            if($chelper->get_show_courses() <= self::COURSECAT_SHOW_COURSES_COUNT){
                return '';
            }
            if($course instanceof stdClass){
                require_once($CFG->libdir . '/coursecatlib.php');
                $course = new course_in_list($course);
            }
            $content = '';
            $classes = trim('coursebox clearfix ' . $additionalclasses);
            if($chelper->get_show_courses() >= self::COURSECAT_SHOW_COURSES_EXPANDED){
                $nametag = 'h3';
            }else{
                $classes .= ' collapsed';
                $nametag = 'div';
            }

            $course_format = course_get_format($course->id)->get_course();

            $content .= html_writer::start_tag('div', array('class' => 'course-box-container'));
            // .coursebox
            $content .= html_writer::start_tag('div', array(
                'class' => $classes,
                'data-courseid' => $course->id,
                'data-type' => self::COURSECAT_TYPE_COURSE,
                'style' => (!empty($course_format->color))?"background-color:#$course_format->color;":'background-color:#3ac57e;',
            ));

            $content .= html_writer::start_tag('div', array('class' => 'info'));

            if(!empty($course_format->icon) && strpos($course_format->icon,'class') != false){
                $icon = $course_format->icon;
            }elseif(!empty($course_format->icon)){
                $icon = html_writer::tag('i', '', array('class' => 'fa ' . $course_format->icon));
            }else{
                $icon = html_writer::tag('i', '', array('class' => 'fa fa-eye-slash'));
            }

            // course name
            $coursebox = html_writer::tag('h4', $chelper->get_course_formatted_name($course), array('class' => 'course'));
            $coursebox .= html_writer::tag('div', $icon, array('class' => 'courseicon'));
            if(has_capability('local/manager:is_teacher', \context_system::instance())
                   || has_capability('local/manager:is_courseauthor', \context_system::instance())
                   || has_capability('local/manager:is_admin', \context_system::instance())){

                $coursebox .= html_writer::tag('h4', $course->idnumber, array('class' => 'coursecat'));
            }else{
                $coursebox .= html_writer::tag('h4', \coursecat::get($course_format->category)->name, array('class' => 'coursecat'));
            }


            $url_params = array('id' => $course->id);
            if($course_format->marker > 0){
                $url_params['section'] = $course_format->marker;
            }
            $content .= html_writer::link(new moodle_url('/course/view.php', $url_params), $coursebox, array('class' => $course->visible?'':'dimmed'));

            $liked = get_user_preferences('liked_course_'.$course->id);
            $content .= html_writer::link(new moodle_url('/local/manager/ajax.php', array('action'=>'like-course','id'=>$course->id)),
                html_writer::tag('i', '', array('class' => 'fa '.(($liked)?'fa-heart':'fa-heart-o'))),
                array('class' => 'like-course'));

            $content .= html_writer::end_tag('div'); // .info
            $content .= html_writer::end_tag('div'); // .coursebox
            $content .= html_writer::end_tag('div'); // .container
            return $content;
        }
    }

    public function frontpage_available_courses() {
        global $CFG;

        $html = $this->get_frontpage_slider();

        $chelper = new coursecat_helper();
        $chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED)->
        set_courses_display_options(array(
            'recursive' => true,
            'limit' => $CFG->frontpagecourselimit,
            'viewmoreurl' => new moodle_url('/course/index.php'),
            'viewmoretext' => new lang_string('fulllistofcourses')));

        $chelper->set_attributes(array('class' => 'frontpage-course-list-all'));
        $courses_unshort = coursecat::get(0)->get_courses($chelper->get_courses_display_options());
        $totalcount = coursecat::get(0)->get_courses_count($chelper->get_courses_display_options());
        if (!$totalcount && !$this->page->user_is_editing() && has_capability('moodle/course:create', context_system::instance())) {
            // Print link to create a new course, for the 1st available category.
            return $this->add_new_course_button();
        }

        $courses = array();
        foreach($courses_unshort as $item){
            if(get_user_preferences('liked_course_'.$item->id)){
                array_unshift($courses, $item);
            }else{
                $courses[] = $item;
            }
        }
        $html .= $this->coursecat_courses($chelper, $courses, $totalcount);

        $html .= "<script>
                    $(window).ready(function() {
                      $('a.like-course').click(function(e) {
                        e.preventDefault();
                        var liked = $(this).children('.fa').hasClass('fa-heart-o');
                        var link = $(this).attr('href');

                        $.ajax({
                          url: link,
                          method: 'POST',
                          data: { liked: liked }
                        });

                        $(this).children('.fa').toggleClass('fa-heart-o');
                        $(this).children('.fa').toggleClass('fa-heart');
                      });

                      $('a.hide-course-carousel').click(function(e) {
                        e.preventDefault();
                        var opened = $(this).children('.fa').hasClass('fa-chevron-up');
                        var link = $(this).attr('href');

                        $.ajax({
                          url: link,
                          method: 'POST',
                          data: { opened: opened }
                        });

                        $(this).children('.fa').toggleClass('fa-chevron-up');
                        $(this).children('.fa').toggleClass('fa-chevron-down');
                        $('.frontpage-course-list-all .carousel').toggleClass('carousel-hidden');
                      });
                    });
                    </script>";

        return $html;
    }

    /**
     * Returns HTML to print list of courses user is enrolled to for the frontpage
     *
     * Also lists remote courses or remote hosts if MNET authorisation is used
     *
     * @return string
     */
    public function frontpage_my_courses() {
        global $USER, $CFG, $DB;

        if (!isloggedin() or isguestuser()) {
            return '';
        }

        $output = $this->get_frontpage_slider();
        if (!empty($CFG->navsortmycoursessort)) {
            // sort courses the same as in navigation menu
            $sortorder = 'visible DESC,'. $CFG->navsortmycoursessort.' ASC';
        } else {
            $sortorder = 'visible DESC,sortorder ASC';
        }
        $courses_unshort  = enrol_get_my_courses('summary, summaryformat', $sortorder);
        $rhosts   = array();
        $rcourses = array();
        if (!empty($CFG->mnet_dispatcher_mode) && $CFG->mnet_dispatcher_mode==='strict') {
            $rcourses = get_my_remotecourses($USER->id);
            $rhosts   = get_my_remotehosts();
        }

        $courses = array();
        foreach($courses_unshort as $item){
            if(get_user_preferences('liked_course_'.$item->id)){
                array_unshift($courses, $item);
            }else{
                $courses[] = $item;
            }
        }

        if (!empty($courses) || !empty($rcourses) || !empty($rhosts)) {

            $chelper = new \coursecat_helper();
            if (count($courses) > $CFG->frontpagecourselimit) {
                // There are more enrolled courses than we can display, display link to 'My courses'.
                $totalcount = count($courses);
                $courses = array_slice($courses, 0, $CFG->frontpagecourselimit, true);
                $chelper->set_courses_display_options(array(
                    'viewmoreurl' => new moodle_url('/my/'),
                    'viewmoretext' => new lang_string('mycourses')
                ));
            } else {
                // All enrolled courses are displayed, display link to 'All courses' if there are more courses in system.
                $chelper->set_courses_display_options(array(
                    'viewmoreurl' => new moodle_url('/course/index.php'),
                    'viewmoretext' => new \lang_string('fulllistofcourses')
                ));
                $totalcount = $DB->count_records('course') - 1;
            }
            $chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED)->
            set_attributes(array('class' => 'frontpage-course-list-enrolled frontpage-course-list-all'));
            $output .= $this->coursecat_courses($chelper, $courses, $totalcount);

            // MNET
            if (!empty($rcourses)) {
                // at the IDP, we know of all the remote courses
                $output .= html_writer::start_tag('div', array('class' => 'courses'));
                foreach ($rcourses as $course) {
                    $output .= $this->frontpage_remote_course($course);
                }
                $output .= html_writer::end_tag('div'); // .courses
            } elseif (!empty($rhosts)) {
                // non-IDP, we know of all the remote servers, but not courses
                $output .= html_writer::start_tag('div', array('class' => 'courses'));
                foreach ($rhosts as $host) {
                    $output .= $this->frontpage_remote_host($host);
                }
                $output .= html_writer::end_tag('div'); // .courses
            }
        }

        $output .= "<script>
                    $(window).ready(function() {
                      $('a.like-course').click(function(e) {
                        e.preventDefault();
                        var liked = $(this).children('.fa').hasClass('fa-heart-o');
                        var link = $(this).attr('href');

                        $.ajax({
                          url: link,
                          method: 'POST',
                          data: { liked: liked }
                        });

                        $(this).children('.fa').toggleClass('fa-heart-o');
                        $(this).children('.fa').toggleClass('fa-heart');
                      });

                      $('a.hide-course-carousel').click(function(e) {
                        e.preventDefault();
                        var opened = $(this).children('.fa').hasClass('fa-chevron-up');
                        var link = $(this).attr('href');

                        $.ajax({
                          url: link,
                          method: 'POST',
                          data: { opened: opened }
                        });

                        $(this).children('.fa').toggleClass('fa-chevron-up');
                        $(this).children('.fa').toggleClass('fa-chevron-down');
                        $('.frontpage-course-list-all .carousel').toggleClass('carousel-hidden');
                      });
                    });
                    </script>";

        return $output;
    }

    protected function coursecat_courses(\coursecat_helper $chelper, $courses, $totalcount = null) {
        global $CFG, $PAGE;
        if($PAGE->pagelayout != "frontpage"){
            return parent::coursecat_courses($chelper, $courses, $totalcount);
        }

        if ($totalcount === null) {
            $totalcount = count($courses);
        }
        if (!$totalcount) {
            // Courses count is cached during courses retrieval.
            return '';
        }

        if ($chelper->get_show_courses() == self::COURSECAT_SHOW_COURSES_AUTO) {
            // In 'auto' course display mode we analyse if number of courses is more or less than $CFG->courseswithsummarieslimit
            if ($totalcount <= $CFG->courseswithsummarieslimit) {
                $chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_EXPANDED);
            } else {
                $chelper->set_show_courses(self::COURSECAT_SHOW_COURSES_COLLAPSED);
            }
        }

        // prepare content of paging bar if it is needed
        $paginationurl = $chelper->get_courses_display_option('paginationurl');
        $paginationallowall = $chelper->get_courses_display_option('paginationallowall');
        if ($totalcount > count($courses)) {
            // there are more results that can fit on one page
            if ($paginationurl) {
                // the option paginationurl was specified, display pagingbar
                $perpage = $chelper->get_courses_display_option('limit', $CFG->coursesperpage);
                $page = $chelper->get_courses_display_option('offset') / $perpage;
                $pagingbar = $this->paging_bar($totalcount, $page, $perpage,
                    $paginationurl->out(false, array('perpage' => $perpage)));
                if ($paginationallowall) {
                    $pagingbar .= html_writer::tag('div', html_writer::link($paginationurl->out(false, array('perpage' => 'all')),
                        get_string('showall', '', $totalcount)), array('class' => 'paging paging-showall'));
                }
            } else if ($viewmoreurl = $chelper->get_courses_display_option('viewmoreurl')) {
                // the option for 'View more' link was specified, display more link
                $viewmoretext = $chelper->get_courses_display_option('viewmoretext', new \lang_string('viewmore'));
                $morelink = html_writer::tag('div', html_writer::link($viewmoreurl, $viewmoretext),
                    array('class' => 'paging paging-morelink'));
            }
        } else if (($totalcount > $CFG->coursesperpage) && $paginationurl && $paginationallowall) {
            // there are more than one page of results and we are in 'view all' mode, suggest to go back to paginated view mode
            $pagingbar = html_writer::tag('div', html_writer::link($paginationurl->out(false, array('perpage' => $CFG->coursesperpage)),
                get_string('showperpage', '', $CFG->coursesperpage)), array('class' => 'paging paging-showperpage'));
        }

        $hidden = get_user_preferences('hide-course-carousel');
        // display list of courses
        $content = html_writer::start_tag('div', $chelper->get_and_erase_attributes('courses'));
        $content .= html_writer::tag('h3', get_string('myclasses','theme_secondaryisf').
                        html_writer::link(new moodle_url('/local/manager/ajax.php', array('action'=>'hide-course-carousel')),
                            html_writer::tag('i', '', array('class' => 'fa '.(($hidden)?'fa-chevron-down':'fa-chevron-up'))),
                            array('class' => 'hide-course-carousel')));
        $content .= html_writer::start_tag('div', array('class'=>'carousel '.(($hidden)?'carousel-hidden':'')));
        $content .= html_writer::start_tag('div', array('id'=>'courses_carousel','class'=>'carousel_inner'));

        if (!empty($pagingbar)) {
            $content .= $pagingbar;
        }

        $coursecount = 0;
        foreach ($courses as $course) {
            $coursecount ++;
            $classes = ($coursecount%2) ? 'carousel_box odd' : 'carousel_box even';
            if ($coursecount == 1) {
                $classes .= ' first';
            }
            if ($coursecount >= count($courses)) {
                $classes .= ' last';
            }
            $content .= $this->coursecat_coursebox($chelper, $course, $classes);
        }

        if (!empty($pagingbar)) {
            $content .= $pagingbar;
        }
        if (!empty($morelink)) {
            $content .= $morelink;
        }
        $content .= html_writer::end_tag('div'); // .carousel_inner


        $content .= '<label class="carousel-control left" id="carousel_prev">';
        $content .= '<span class="fa fa-chevron-left"></span>';
        $content .= '</label>';
        $content .= '<label class="carousel-control right" id="carousel_next">';
        $content .= '<span class="fa fa-chevron-right"></span>';
        $content .= '</label>';



        $content .= html_writer::end_tag('div'); // .carousel
        $content .= html_writer::end_tag('div'); // .courses
        return $content;
    }

    public function get_frontpage_slider() {
        global $PAGE, $OUTPUT, $USER, $DB;

        if(!get_config('theme_secondaryisf', 'sliderenabled')){
            return '';
        }

        // Can only be seen on front page, by logged in users.
        if ($PAGE->pagetype == 'site-index' && isloggedin() && !isguestuser()) {

            $noslides = $PAGE->theme->settings->slidercount;
            $cohorts = $PAGE->theme->settings->slidercohorts;
            $can_view = true;

            if (!empty($cohorts)){
                $can_view = false; $params = array('userid'=>$USER->id);
                list($sql_in, $params_in) = $DB->get_in_or_equal(explode(',', $cohorts), SQL_PARAMS_NAMED);
                $user_cohorts = $DB->get_records_sql("SELECT id FROM {cohort_members} WHERE userid = :userid AND cohortid $sql_in", $params+$params_in);
                if (count($user_cohorts)){
                    $can_view = true;
                }
            }

            if (!$can_view){
                return '';
            }

            $retval = '';
            $retval .= '<div class="primary-carousel">';
            $retval .= '<div id="myCarousel" class="carousel slide" data-ride="carousel">';
            $retval .= '<!-- Carousel indicators -->';
            $retval .= '<ol class="carousel-indicators">';

            // Carousel indicator IDs start at 0.
            for ($i = 1; $i <= $noslides; $i++) {
                $retval .= '<li data-target="#myCarousel" data-slide-to=" ' . ($i - 1) . ' " ';

                // Only the first indicator needs to show as an 'active' item.
                if ($i == 1) {
                    $retval .= 'class="active"></li>';
                } else {
                    $retval .= '></li>';
                }
            }

            $retval .= '</ol>'; // End of carousel-indicators.
            $retval .= '<!-- Wrapper for carousel items -->';
            $retval .= '<div class="carousel-inner">';

            for ($i = 1; $i <= $noslides; $i++) {
                $sliderimage = 'p' . $i;
                $sliderurl = 'p' . $i . 'url';
                $slidercaption = 'p' . $i . 'cap';
                $closelink = '';

                // If there is an image, show it.
                if (!empty($PAGE->theme->settings->$sliderimage)) {

                    // Set the first item as 'active', to allow for navigation between slides.
                    if ($i == 1) {
                        $retval .= '<div class="carousel-item active">';
                    } else {
                        $retval .= '<div class="carousel-item">';
                    }

                    // If the slide has a link attached to it, make the image clickable.
                    if (!empty($PAGE->theme->settings->$sliderurl)) {
                        $retval .= '<a href="' . $PAGE->theme->settings->$sliderurl . '">';
                        $closelink = '</a>';
                    }

                    // Retrieve the image.
                    $retval .= '<img src="' . $PAGE->theme->setting_file_url($sliderimage, $sliderimage)
                        . '" alt="' . $sliderimage . '"/>' .$closelink;

                    $retval .= '<div class="carousel-caption">';
                    $retval .= format_text($PAGE->theme->settings->$slidercaption, FORMAT_HTML, array('trusted' => true));
                    $retval .= '</div>'; // End of carousel-caption.

                    $retval .= '</div>'; // End of item.

                }
            }
            $retval .= '</div>'; // End of carousel-inner.

            $retval .= '<!-- Carousel controls -->';
            $retval .= '<a class="carousel-control left" href="#myCarousel" data-slide="prev">';
            $retval .= '<span class="fa fa-chevron-left"></span>';
            $retval .= '</a>';
            $retval .= '<a class="carousel-control right" href="#myCarousel" data-slide="next">';
            $retval .= '<span class="fa fa-chevron-right"></span>';
            $retval .= '</a>';
            $retval .= '</div>'; // End of my carousel.
            $retval .= '</div>'; // End of bs-example.
            return $retval;
        }
    }

    /**
     * Course search form
     *
     * @param string $value
     * @return string
     */
    public function get_course_search_form($value = '') {

        global $PAGE;

        if (isloggedin()) {

            // Can only be seen on front page.
            if ($PAGE->pagetype == 'site-index') {

                $formid = 'coursesearch';
                $inputid = 'coursesearchbox';
                $inputsize = 30;

                $strsearchcourses = get_string("searchcourses");
                $searchurl = new moodle_url('/course/search.php');

                $form = array(
                    'id' => $formid,
                    'action' => $searchurl,
                    'method' => 'get',
                    'class' => "form-inline",
                    'role' => 'form');
                $output = html_writer::start_tag('form', $form);
                $output .= html_writer::start_div('form-group');
                $output .= html_writer::tag('label', $strsearchcourses, array(
                    'for' => $inputid,
                    'class' => 'sr-only'));
                $search = array(
                    'type' => 'text',
                    'id' => $inputid,
                    'size' => $inputsize,
                    'name' => 'search',
                    'class' => 'form-control',
                    'value' => s($value),
                    'placeholder' => $strsearchcourses);
                $output .= html_writer::empty_tag('input', $search);
                $output .= html_writer::end_div(); // Close form-group.
                $button = array(
                    'type' => 'submit',
                    'class' => 'btn btn-default',
                    'id' => 'isfsearchcourses');
                $output .= html_writer::tag('button', get_string('go'), $button);
                $output .= html_writer::end_tag('form');

                return $output;
            }
        }
    }

    /**
     * Renders HTML to display one course module in a course section
     *
     * This includes link, content, availability, completion info and additional information
     * that module type wants to display (i.e. number of unread forum posts)
     *
     * This function calls:
     * {@link core_course_renderer::course_section_cm_name()}
     * {@link core_course_renderer::course_section_cm_text()}
     * {@link core_course_renderer::course_section_cm_availability()}
     * {@link core_course_renderer::course_section_cm_completion()}
     * {@link course_get_cm_edit_actions()}
     * {@link core_course_renderer::course_section_cm_edit_actions()}
     *
     * @param stdClass $course
     * @param completion_info $completioninfo
     * @param cm_info $mod
     * @param int|null $sectionreturn
     * @param array $displayoptions
     * @return string
     */
    public function course_section_cm($course, &$completioninfo, cm_info $mod, $sectionreturn, $displayoptions = array()) {
        $output = '';
        // We return empty string (because course module will not be displayed at all)
        // if:
        // 1) The activity is not visible to users
        // and
        // 2) The 'availableinfo' is empty, i.e. the activity was
        //     hidden in a way that leaves no info, such as using the
        //     eye icon.
        if (!$mod->uservisible && empty($mod->availableinfo)) {
            return $output;
        }

        $indentclasses = 'mod-indent';
        if (!empty($mod->indent)) {
            $indentclasses .= ' mod-indent-'.$mod->indent;
            if ($mod->indent > 15) {
                $indentclasses .= ' mod-indent-huge';
            }
        }

        $output .= html_writer::start_tag('div', array('class' => 'clearfix'));

        if ($this->page->user_is_editing()) {
            $output .= course_get_cm_move($mod, $sectionreturn);
        } else {
            $output .= html_writer::tag('span', html_writer::tag('i', '', array('class'=>'fa '.((is_array($this->_user_preferences['fav_modules']) and in_array($mod->id, $this->_user_preferences['fav_modules'])) ? 'fa-heart' : 'fa-heart-o'))), array('class' => 'fav-action'.((is_array($this->_user_preferences['fav_modules']) and in_array($mod->id, $this->_user_preferences['fav_modules'])) ? ' active' : ''), 'onclick'=>'cmToggleFav('.$mod->id.', '.$course->id.')'));
        }

        if (isset($course->coursetype) and $course->coursetype and $this->_user_preferences['course_view']){
            $image_url = course_get_format($course->id)->get_activity_imageurl($course, $mod);
            $activity_url_options = $this->course_section_cm_url($mod);

            // This div is used to indent the content.
            $output .= html_writer::start_tag('div', array('class' => 'activity-image'));
            $style = 'background:url("'.$image_url.'") no-repeat scroll center center; background-size: cover;';
            if (is_array($activity_url_options) and $mod->uservisible) {
                $output .= html_writer::link($activity_url_options['url'], '', array('class' => $activity_url_options['class'], 'onclick' => $activity_url_options['onclick'], 'onclick' => ((!empty($activity_url_options['accesstext'])) ? $activity_url_options['accesstext'] : $activity_url_options['alt']), 'style' => $style));
            } else {
                $output .= html_writer::tag('span', '', array('class' => 'activity-img-link', 'style' => $style));
            }
            $output .= html_writer::end_tag('div');
        }

        $output .= html_writer::start_tag('div', array('class' => 'mod-indent-outer'));

        // This div is used to indent the content.
        $output .= html_writer::div('', $indentclasses);

        // Start a wrapper for the actual content to keep the indentation consistent
        $output .= html_writer::start_tag('div');

        // Display the link to the module (or do nothing if module has no url)
        $cmname = $this->course_section_cm_name($mod, $displayoptions);

        if (!empty($cmname)) {
            // Start the div for the activity title, excluding the edit icons.
            $output .= html_writer::start_tag('div', array('class' => 'activityinstance'));
            $output .= $cmname;


            // Module can put text after the link (e.g. forum unread)
            $output .= $mod->afterlink;

            // Closing the tag which contains everything but edit icons. Content part of the module should not be part of this.
            $output .= html_writer::end_tag('div'); // .activityinstance
        }

        // If there is content but NO link (eg label), then display the
        // content here (BEFORE any icons). In this case cons must be
        // displayed after the content so that it makes more sense visually
        // and for accessibility reasons, e.g. if you have a one-line label
        // it should work similarly (at least in terms of ordering) to an
        // activity.
        $contentpart = $this->course_section_cm_text($mod, $displayoptions);
        $url = $mod->url;
        if (empty($url)) {
            $output .= $contentpart;
        }

        $modicons = '';
        if ($this->page->user_is_editing()) {
            $editactions = course_get_cm_edit_actions($mod, $mod->indent, $sectionreturn);
            $modicons .= ' '. $this->course_section_cm_edit_actions($editactions, $mod, $displayoptions);
            $modicons .= \local_isfdup\hooks::link_icon($mod->id);
            $modicons .= $mod->afterediticons;
        }

        $modicons .= $this->course_section_cm_completion($course, $completioninfo, $mod, $displayoptions);

        if (!empty($modicons)) {
            $output .= html_writer::span($modicons, 'actions');
        }

        // If there is content AND a link, then display the content here
        // (AFTER any icons). Otherwise it was displayed before
        if (!empty($url)) {
            $output .= $contentpart;
        }

        // show availability info (if module is not available)
        $output .= $this->course_section_cm_availability($mod, $displayoptions);

        $output .= html_writer::end_tag('div'); // $indentclasses

        // End of indentation div.
        $output .= html_writer::end_tag('div');

        $output .= html_writer::end_tag('div');

        return $output;
    }

    /**
     * Renders HTML for displaying the sequence of course module editing buttons
     *
     * @see course_get_cm_edit_actions()
     *
     * @param action_link[] $actions Array of action_link objects
     * @param cm_info $mod The module we are displaying actions for.
     * @param array $displayoptions additional display options:
     *     ownerselector => A JS/CSS selector that can be used to find an cm node.
     *         If specified the owning node will be given the class 'action-menu-shown' when the action
     *         menu is being displayed.
     *     constraintselector => A JS/CSS selector that can be used to find the parent node for which to constrain
     *         the action menu to when it is being displayed.
     *     donotenhance => If set to true the action menu that gets displayed won't be enhanced by JS.
     * @return string
     */
    public function course_section_cm_edit_actions($actions, cm_info $mod = null, $displayoptions = array()) {
        global $CFG;

        if (empty($actions)) {
            return '';
        }

        if (isset($displayoptions['ownerselector'])) {
            $ownerselector = $displayoptions['ownerselector'];
        } else if ($mod) {
            $ownerselector = '#module-'.$mod->id;
        } else {
            debugging('You should upgrade your call to '.__FUNCTION__.' and provide $mod', DEBUG_DEVELOPER);
            $ownerselector = 'li.activity';
        }

        if (isset($displayoptions['constraintselector'])) {
            $constraint = $displayoptions['constraintselector'];
        } else {
            $constraint = '.course-content';
        }

        $menu = new action_menu();
        $menu->set_owner_selector($ownerselector);
        $menu->set_constraint($constraint);
        $menu->set_alignment(action_menu::TR, action_menu::BR);
        $menu->set_menu_trigger(get_string('edit'));

        $actions['quickadd'] = new action_menu_link_secondary(
            new moodle_url('javascript:void(0)'),
            new pix_icon('t/quickadd', get_string('add'), 'moodle', array('class' => 'iconsmall', 'title' => '')),
            get_string('addquickbeforemodule', 'theme_secondaryisf'),
            array('class' => 'editing_quickadd quickmodule-chooser', 'data-section-id'=>$mod->section, 'data-module-id'=>$mod->id)
        );

        $actions['add'] = new action_menu_link_secondary(
            new moodle_url('javascript:void(0)'),
            new pix_icon('t/add', get_string('add'), 'moodle', array('class' => 'iconsmall', 'title' => '')),
            get_string('addbeforemodule', 'theme_secondaryisf'),
            array('class' => 'editing_add', 'data-action' => 'add', 'onclick'=>'formatToggleChooser('.$mod->section.', '.$mod->id.')')
        );

        foreach ($actions as $action) {
            if ($action instanceof \action_menu_link) {
                $action->add_class('cm-edit-action');
            }
            $menu->add($action);
        }

        $menu->attributes['class'] .= ' section-cm-edit-actions commands';

        // Prioritise the menu ahead of all other actions.
        $menu->prioritise = true;

        return $this->render($menu);
    }

    /**
     * Renders html to display a name with the link to the course module on a course page
     *
     * If module is unavailable for user but still needs to be displayed
     * in the list, just the name is returned without a link
     *
     * Note, that for course modules that never have separate pages (i.e. labels)
     * this function return an empty string
     *
     * @param cm_info $mod
     * @param array $displayoptions
     * @return string
     */
    public function course_section_cm_name_title(cm_info $mod, $displayoptions = array()) {
        $output = '';

        if (!$mod->uservisible && empty($mod->availableinfo)) {
            // Nothing to be displayed to the user.
            return $output;
        }
        $url = $mod->url;
        if (!$url) {
            return $output;
        }

        //Accessibility: for files get description via icon, this is very ugly hack!
        $instancename = $mod->get_formatted_name();
        $altname = $mod->modfullname;
        // Avoid unnecessary duplication: if e.g. a forum name already
        // includes the word forum (or Forum, etc) then it is unhelpful
        // to include that in the accessible description that is added.
        if (false !== strpos(core_text::strtolower($instancename),
                core_text::strtolower($altname))) {
            $altname = '';
        }
        // File type after name, for alphabetic lists (screen reader).
        if ($altname) {
            $altname = get_accesshide(' '.$altname);
        }

        // For items which are hidden but available to current user
        // ($mod->uservisible), we show those as dimmed only if the user has
        // viewhiddenactivities, so that teachers see 'items which might not
        // be available to some students' dimmed but students do not see 'item
        // which is actually available to current student' dimmed.
        $linkclasses = 'activity-link';
        $accesstext = '';
        $textclasses = '';
        if ($mod->uservisible) {
            $conditionalhidden = $this->is_cm_conditionally_hidden($mod);
            $accessiblebutdim = (!$mod->visible || $conditionalhidden) &&
                has_capability('moodle/course:viewhiddenactivities', $mod->context);
            if ($accessiblebutdim) {
                $linkclasses .= ' dimmed';
                $textclasses .= ' dimmed_text';
                if ($conditionalhidden) {
                    $linkclasses .= ' conditionalhidden';
                    $textclasses .= ' conditionalhidden';
                }
                // Show accessibility note only if user can access the module himself.
                $accesstext = get_accesshide(get_string('hiddenfromstudents').':'. $mod->modfullname);
            }
        } else {
            $linkclasses .= ' dimmed';
            $textclasses .= ' dimmed_text';
        }

        // Get on-click attribute value if specified and decode the onclick - it
        // has already been encoded for display (puke).
        $onclick = htmlspecialchars_decode($mod->onclick, ENT_QUOTES);

        $groupinglabel = $this->get_powerschool_link($mod).$mod->get_grouping_label($textclasses);

        // Display link itself.
        $activitylink = html_writer::empty_tag('img', array('src' => $mod->get_icon_url(),
                'class' => 'iconlarge activityicon', 'alt' => ' ', 'role' => 'presentation')) . $accesstext .
            html_writer::tag('span', $instancename . $altname, array('class' => 'instancename'));
        if ($mod->uservisible) {
            $output .= html_writer::link($url, $activitylink, array('class' => $linkclasses, 'onclick' => $onclick)) .
                $groupinglabel;
        } else {
            // We may be displaying this just in order to show information
            // about visibility, without the actual link ($mod->uservisible)
            $output .= html_writer::tag('div', $activitylink, array('class' => $textclasses)) .
                $groupinglabel;
        }
        return $output;
    }

    private function get_powerschool_link($mod_info){
        global $DB, $COURSE, $USER;
        $html = array(' ');
        $show_only_links = false;

        if (!$DB->record_exists('powerschool_course_fields',array('courseid'=>$COURSE->id))) {
            return '';
        }

        $sections = $DB->get_records_sql('SELECT pa.*, pcf.course_type, pcf.gradebooktype
                                          FROM {powerschool_assignment} pa
                                              LEFT JOIN {powerschool_course_fields} pcf ON pcf.section_id=pa.section_id
                                          WHERE pa.cmid=:cmid AND pa.courseid=:courseid GROUP BY pa.id',array('cmid'=>$mod_info->id,'courseid'=>$COURSE->id));


        $connection = $DB->get_record('powerschool_connections',array('id'=>\PowerSchool::get_connection_from_courseid($COURSE->id)));
        $sync_params = json_decode($connection->sync_params);
        $context = context_course::instance($COURSE->id);

        if(user_has_role_assignment($USER->id,$sync_params->teacher_role,$context->id) || user_has_role_assignment($USER->id,$sync_params->teacher_lead_role,$context->id) || is_siteadmin()){
            foreach($sections as $section){
                if($section->gradebooktype == 2){
                    if($section->course_type == 'groups'){
                        $group = $DB->get_record('groups', array('id' => $section->groupid));
                        $section_name = (isset($group->name))?$group->name:'';
                    }else{
                        $section_name = $COURSE->fullname;
                    }

                    $html[] = html_writer::link($connection->url . '/teachers/index.html#/classes/assignments/score_assignment?sectionId=' . $section->section_id . '&assignmentId=' . $section->assignmentid,
                        html_writer::img(new moodle_url('/theme/primaryisf/pix/powerschool_logo.svg'),'', array('class' => 'iconsmall')), array('title' => $section_name, 'target'=>'_blank', 'class'=>'power-link'));
                    $show_only_links = true;
                }else{
                    $html['img'] = html_writer::img(new moodle_url('/theme/isfprimary/pix/powerschool_logo.svg'),'',array('class' => 'iconsmall power-icon'));
                }
            }
        }

        if($show_only_links)
            unset($html['img']);

        return html_writer::tag('div', implode(' ',$html), array('class'=>'power-links-box'));
    }

    /**
     * Renders html to display a name with the link to the course module on a course page
     *
     * If module is unavailable for user but still needs to be displayed
     * in the list, just the name is returned without a link
     *
     * Note, that for course modules that never have separate pages (i.e. labels)
     * this function return an empty string
     *
     * @param cm_info $mod
     * @return string
     */
    public function course_section_cm_url(cm_info $mod) {
        $output = '';
        if (!$mod->uservisible && empty($mod->availableinfo)) {
            // Nothing to be displayed to the user.
            return $output;
        }
        $url = $mod->url;
        if (!$url) {
            return $output;
        }

        //Accessibility: for files get description via icon, this is very ugly hack!
        $instancename = $mod->get_formatted_name();
        $altname = $mod->modfullname;
        // Avoid unnecessary duplication: if e.g. a forum name already
        // includes the word forum (or Forum, etc) then it is unhelpful
        // to include that in the accessible description that is added.
        if (false !== strpos(core_text::strtolower($instancename),
                core_text::strtolower($altname))) {
            $altname = '';
        }
        // File type after name, for alphabetic lists (screen reader).
        if ($altname) {
            $altname = get_accesshide(' '.$altname);
        }

        // For items which are hidden but available to current user
        // ($mod->uservisible), we show those as dimmed only if the user has
        // viewhiddenactivities, so that teachers see 'items which might not
        // be available to some students' dimmed but students do not see 'item
        // which is actually available to current student' dimmed.
        $linkclasses = 'activity-img-link';
        $accesstext = '';
        $textclasses = '';
        if ($mod->uservisible) {
            $conditionalhidden = $this->is_cm_conditionally_hidden($mod);
            $accessiblebutdim = (!$mod->visible || $conditionalhidden) &&
                has_capability('moodle/course:viewhiddenactivities', $mod->context);
            if ($accessiblebutdim) {
                $linkclasses .= ' dimmed';
                $textclasses .= ' dimmed_text';
                if ($conditionalhidden) {
                    $linkclasses .= ' conditionalhidden';
                    $textclasses .= ' conditionalhidden';
                }
                // Show accessibility note only if user can access the module himself.
                $accesstext = get_accesshide(get_string('hiddenfromstudents').':'. $mod->modfullname);
            }
        } else {
            $linkclasses .= ' dimmed';
            $textclasses .= ' dimmed_text';
        }

        // Get on-click attribute value if specified and decode the onclick - it
        // has already been encoded for display (puke).
        $onclick = htmlspecialchars_decode($mod->onclick, ENT_QUOTES);

        return array('url'=>$url, 'class'=>$linkclasses, 'onclick'=>$onclick, 'alt'=>$instancename, 'accesstext'=>$accesstext);
    }

    /**
     * Render a modchooser.
     *
     * @param renderable $modchooser The chooser.
     * @return string
     */
    public function render_modchooser(\renderable $modchooser) {

        $data = $modchooser->export_for_template($this);
        $currentlang = current_language();

        $items_cat = array();
        foreach($data->sections as &$section){
            foreach($section->items as $item){
                if($currentlang == 'zh_tw' || $currentlang == 'zh_cn'){
                    $help_link = get_config('local_manager', 'chooser_mod_help_ch_' . $item->id);
                    $example_link = get_config('local_manager', 'chooser_mod_example_ch_' . $item->id);
                    $cat = get_config('local_manager', 'chooser_mod_cat_ch_'.$item->id);
                }else{
                    $help_link = get_config('local_manager', 'chooser_mod_help_' . $item->id);
                    $example_link = get_config('local_manager', 'chooser_mod_example_' . $item->id);
                    $cat = get_config('local_manager', 'chooser_mod_cat_'.$item->id);
                }

                $item->color = get_config('local_manager', 'chooser_mod_color_'.$item->id);
                $item->help_link = (!empty($help_link))?html_writer::link($help_link,get_string('help'),array('target'=>'_blank')):'';
                $item->example_link = (!empty($example_link))?html_writer::link($example_link,get_string('example','local_manager'),array('target'=>'_blank')):'';
                $item->description = strip_tags($item->description);
                $item->description = str_replace('"',"'",$item->description);

                $cat = (empty($cat))?' '.get_string('other'):$cat;
                if(!isset($items_cat[$cat])){
                    $items_cat[$cat] = new stdClass();
                    $items_cat[$cat]->name = $cat;
                    $items_cat[$cat]->only_advanced = true;
                }

                if(get_config('local_manager', 'chooser_mod_advanced_'.$item->id)){
                    $item->advanced = true;
                }else{
                    $items_cat[$cat]->only_advanced = false;
                }



                $items_cat[$cat]->items[] = $item;
            }
        }
        asort($items_cat);
        $section = new stdClass();
        $section->cat = array_values($items_cat);
        $data->sections = array($section);
        $data->resources = get_string('resources');
        $data->activities = get_string('activities');

        return $this->render_from_template('core_course/isf_modchoser', $data);
    }

}
