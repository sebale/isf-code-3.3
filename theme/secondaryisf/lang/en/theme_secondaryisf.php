<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Language file.
 *
 * @package   theme_secondaryisf
 * @copyright 2016 Frédéric Massart
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$string['advancedsettings'] = 'Advanced settings';
$string['brandcolor'] = 'Brand colour';
$string['brandcolor_desc'] = 'The accent colour.';
$string['choosereadme'] = 'ISF secondary is a modern highly-customisable theme. This theme is intended to be used directly, or as a parent theme when creating new themes utilising Bootstrap 4.';
$string['currentinparentheses'] = '(current)';
$string['configtitle'] = 'ISF secondary';
$string['generalsettings'] = 'General settings';
$string['pluginname'] = 'Secondary ISF';
$string['presetfiles'] = 'Additional theme preset files';
$string['presetfiles_desc'] = '';
$string['preset'] = 'Theme preset';
$string['preset_desc'] = 'Pick a preset to broadly change the look of the theme.';
$string['rawscss'] = 'Raw SCSS';
$string['rawscss_desc'] = 'Use this field to provide SCSS or CSS code which will be injected at the end of the style sheet.';
$string['rawscsspre'] = 'Raw initial SCSS';
$string['rawscsspre_desc'] = 'In this field you can provide initialising SCSS code, it will be injected before everything else. Most of the time you will use this setting to define variables.';
$string['region-side-pre'] = 'Right';
$string['dashboard'] = 'Dashboard';
$string['calendar'] = 'Calendar';
$string['settings'] = 'Settings';
$string['courses'] = 'Courses';
$string['manage'] = 'Manage';
$string['users'] = 'Users';
$string['categories'] = 'Categories';
$string['courses'] = 'Courses';
$string['manageappearance'] = 'Appearance';
$string['coursesandcurriculum'] = 'Courses & Curriculum';
$string['sitesettings'] = 'Site Settings';
$string['appearance'] = 'Appearance';
$string['features'] = 'Features';
$string['languages'] = 'Languages';
$string['add'] = 'Add';
$string['sidebarsettings'] = 'Sidebar Settings';
$string['quickmodules'] = 'Quick Modules';

// Settings

$string['navbarmenusettings'] = 'ISF secondary Menus';
$string['newstickerenabled'] = 'Enable Newsticker';
$string['newstickerenableddesc'] = 'Enable a Newsticker at the top of your home page';

// ISF Menus
$string['enabletoolsmenus'] = 'Enable ISF secondary Menus';
$string['enabletoolsmenusdesc'] = 'It is recommended you leave this off if menus are not in use for performance reasons';

$string['disablecustommenu'] = 'Disable Moodle Custom Menu';
$string['disablecustommenudesc'] = 'Disable Moodle Custom Menus in the navigation bar (will still render in other themes you may have installed)';

$string['toolsmenuheading'] = 'ISF Menus (in main nagivation)';
$string['toolsmenuheadingdesc'] = 'You can configure links to be shown under an ISF secondary menu (in main navigation bar). Please note
there are distinct sections for English and Chinese.
 The format is similar to that used for Moodle custom menus but allows you to add fa icons to menu items:
<pre>
&lt;span class=&quot;fa fa-youtube-play&quot;&gt;&lt;/span&gt; YouTube|http://www.youtube.com" target="_blank|YouTube
&lt;span class=&quot;fa-area-chart&quot;&gt;&lt;/span&gt; Tableau Public|http://www.tableau-public.com" target="_blank|Tableau Public
&lt;span class=&quot;fa-wordpress&quot;&gt;&lt;/span&gt; WordPress|http://www.wordpress.com" target="_blank|WordPress
</pre><br />';

$string['toolsmenuscount'] = 'Number of ISF secondary Menus';
$string['toolsmenuscountdesc'] = 'Set the number of ISF secondary Menus you want to add to the main navigation bar';

$string['toolsmenuheading'] = 'ISF secondary Menu ';
$string['toolsmenu'] = 'English Dropdown';
$string['toolsmenudesc'] = 'Add an English drop down menu to the main navigation bar';
$string['toolschinesemenu'] = 'Chinese Dropdown';
$string['toolschinesemenudesc'] = 'Add a Chinese drop down menu to the main navigation bar';

$string['toolsmenutitle'] = 'English Title';
$string['toolsmenuchinesetitle'] = 'Chinese title';
$string['toolsmenutitledefault'] = 'Title';
$string['toolsmenuchinesetitledefault'] = '標題';
$string['toolsmenutitledesc'] = 'Add the title of the secondary Menu you would like to display in English, in the main navigation bar';
$string['toolschinesemenutitledesc'] = 'Add the title of the secondary Menu you would like to display in Chinese, in the main navigation bar';

$string['toolsmenulabel'] = 'ISF secondary menus';

$string['toolsmenufield'] = 'User type (optional)';
$string['toolsmenufielddesc'] = 'E.g. teacher, student, parent, office, admin (only one item from list, no spaces, all lowercase';
$string['togglefilemanager'] = 'Toggle Filemanager';
$string['activitypicker'] = 'Activity Picker';
$string['activityimages'] = 'Activity Images';
$string['coursenavigation'] = 'Course Navigation';
$string['add'] = 'Add';
$string['addquickbeforemodule'] = 'Add Quick Module before';
$string['addquickmodule'] = 'Add new Quick Module';
$string['addbeforemodule'] = 'Add Module before';
$string['savechanges_next_user'] = 'Save changes and next user';


// Slider

$string['frontpageslidersettings'] = 'ISF secondary Carousel';

$string['slideshowsettings'] = 'Slideshow';
$string['slideshowsettingsheading'] = 'Customize the carousel on the front page. Please make sure to only use 1500px * 300px images';
$string['slideshowdesc'] = 'Upload the images (1500px * 300px), add the links and description for the carousel on the front page.';

$string['sliderimage'] = 'Slider Picture';
$string['sliderimagedesc'] = 'Add an image for your slide.';

$string['slidercaption'] = 'Slider Caption';
$string['slidercaptiondesc'] = 'Add a caption for your slide. See above for an example';

$string['sliderurl'] = 'Slide Link URL';
$string['sliderurldesc'] = 'Add a URL to which your slide links to when clicked.';

$string['sliderenabled'] = 'Enable Slider';
$string['sliderenableddesc'] = 'Enable a slider at the top of your home page';

$string['slidercount'] = 'Slider Count';
$string['slidercountdesc'] = 'Select how many slides you want to add <strong>then click SAVE</strong> to load the input fields';

$string['slidercohorts'] = 'Cohorts';
$string['slidercohortsdesc'] = 'Display slider to selected cohorts';

$string['slideroption2snippet'] = '<p>Sample HTML for Slider Captions:</p>
<pre>
&#x3C;div class=&#x22;span6 col-sm-6&#x22;&#x3E;
&#x3C;h3&#x3E;Hand-crafted&#x3C;/h3&#x3E; &#x3C;h4&#x3E;pixels and code for the Moodle community&#x3C;/h4&#x3E;
&#x3C;a href=&#x22;#&#x22; class=&#x22;submit&#x22;&#x3E;Please favorite our theme!&#x3C;/a&#x3E;
</pre>';

// Cache definitions.
$string['cachedef_userdata'] = 'A session cache used to store user specific data.';
$string['myclasses'] = 'My classes';
$string['login_img'] = 'Login image';
$string['login_imgdesc'] = 'Login page image background';
