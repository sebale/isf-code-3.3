<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') || die();

/**
 * A login page layout for the primaryisf theme.
 *
 * @package   theme_primaryisf
 * @copyright 2016 Damyon Wiese
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */



if(isset($_SESSION['login_lang'])){
    $lang = $_SESSION['login_lang'];
} else {
    $lang = 'en';
}

$lang = optional_param('lang', $lang, PARAM_RAW);


// check if lang is valid
$langs = get_string_manager()->get_list_of_translations(false);
foreach($langs as $k=>$v){
    if ($k == $lang) {
        $CFG->lang = $lang;
        $_SESSION['login_lang'] = $lang;

        if(!empty($_SESSION['SESSION']->wantsurl)){
            $_SESSION['SESSION']->wantsurl = (strpos($_SESSION['SESSION']->wantsurl,'?') === false)?$_SESSION['SESSION']->wantsurl.'?lang='.$lang:$_SESSION['SESSION']->wantsurl.'&lang='.$lang;
        }else{
            $_SESSION['SESSION']->wantsurl = $CFG->wwwroot.'?lang='.$lang;
        }

    }
}

$bodyattributes = $OUTPUT->body_attributes();

$templatecontext = [
    'sitename' => format_string($SITE->shortname, true, ['context' => context_course::instance(SITEID), "escape" => false]),
    'output' => $OUTPUT,
    'bodyattributes' => $bodyattributes
];

echo $OUTPUT->render_from_template('theme_primaryisf/login', $templatecontext);
