<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version details
 *
 * @package    theme_secondaryisf
 * @copyright  2016 Frederic Nevers (www.iteachwithmoodle.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

// Variable setting
$choices1to12 = array();
for ($i = 0; $i < 13; $i++) {
    $choices1to12[$i] = $i;
}


// Frontpage Slider.
$temp = new admin_settingpage('theme_secondaryisf_frontpage_slider', get_string('frontpageslidersettings', 'theme_secondaryisf'));

$temp->add(new admin_setting_heading('theme_secondaryisf_slideshow', get_string('slideshowsettingsheading', 'theme_secondaryisf'),
    format_text(get_string('slideshowdesc', 'theme_secondaryisf') .
        get_string('slideroption2snippet', 'theme_secondaryisf'), FORMAT_MARKDOWN)));

$name = 'theme_secondaryisf/sliderenabled';
$title = get_string('sliderenabled', 'theme_secondaryisf');
$description = get_string('sliderenableddesc', 'theme_secondaryisf');
$setting = new admin_setting_configcheckbox($name, $title, $description, 0);
$temp->add($setting);

$name = 'theme_secondaryisf/newstickerenabled';
$title = get_string('newstickerenabled', 'theme_secondaryisf');
$description = get_string('newstickerenableddesc', 'theme_secondaryisf');
$setting = new admin_setting_configcheckbox($name, $title, $description, 0);
$temp->add($setting);

$cohorts = $DB->get_records_select_menu('cohort', 'visible > 0', array(), 'name', 'id, name');
$name = 'theme_secondaryisf/slidercohorts';
$title = get_string('slidercohorts', 'theme_secondaryisf');
$description = get_string('slidercohortsdesc', 'theme_secondaryisf');
$setting = new admin_setting_configmultiselect($name, $title, $description, array(), $cohorts);
$temp->add($setting);

// Number of Sliders.
$name = 'theme_secondaryisf/slidercount';
$title = get_string('slidercount', 'theme_secondaryisf');
$description = get_string('slidercountdesc', 'theme_secondaryisf');
$default = THEME_secondaryisf_DEFAULT_SLIDERCOUNT;
$setting = new admin_setting_configselect($name, $title, $description, $default, $choices1to12);
$setting->set_updatedcallback('theme_reset_all_caches');
$temp->add($setting);

// If we don't know how many sliders should be shown, default to the preset.
$slidercount = get_config('theme_secondaryisf', 'slidercount');
if (!$slidercount) {
    $slidercount = THEME_secondaryisf_DEFAULT_SLIDERCOUNT;
}

for ($sliderindex = 1; $sliderindex <= $slidercount; $sliderindex++) {
    $fileid = 'p' . $sliderindex;
    $name = 'theme_secondaryisf/p' . $sliderindex;
    $title = get_string('sliderimage', 'theme_secondaryisf');
    $description = get_string('sliderimagedesc', 'theme_secondaryisf');
    $setting = new admin_setting_configstoredfile($name, $title, $description, $fileid);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_secondaryisf/p' . $sliderindex . 'url';
    $title = get_string('sliderurl', 'theme_secondaryisf');
    $description = get_string('sliderurldesc', 'theme_secondaryisf');
    $setting = new admin_setting_configtext($name, $title, $description, '', PARAM_URL);
    $temp->add($setting);

    $name = 'theme_secondaryisf/p' . $sliderindex . 'cap';
    $title = get_string('slidercaption', 'theme_secondaryisf');
    $description = get_string('slidercaptiondesc', 'theme_secondaryisf');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $temp->add($setting);
}
