// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is an empty module, that is required before all other modules.
 * Because every module is returned from a request for any other module, this
 * forces the loading of all modules with a single request.
 *
 * This function also sets up the listeners for ajax requests so we can tell
 * if any requests are still in progress.
 *
 * @package    format_isf
 * @module     format_isf/course
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define(['jquery', 'core/ajax', 'core/log', 'core/str', 'core/config', 'core/modal_factory', 'core/modal_events', 'local_manager/datatable', 'theme_primaryisf/circliful'], function($, ajax, log, str, mdlcfg, ModalFactory, ModalEvents, Datatable, circliful) {

    Datatable.init($, window, document, undefined);
    circliful.init($);

    window.cmToggleFav = function(cid, courseid) {
        var toggler = $('#module-'+cid).find('.fav-action');
        if (toggler.hasClass('active')){
            $(toggler).html('<i class="fa fa-heart-o"></i>');
            var value = 0;
        } else {
            $(toggler).html('<i class="fa fa-heart"></i>');
            var value = 1;
        }
        toggler.toggleClass('active');

        ajax.call([{
            methodname: 'format_isf_save_user_preferences',
            args: { name: 'fav_course_modules',
                    instanceid: cid,
                    value: value,
                    courseid: courseid
                  }
        }])[0]
        .done(function(response) {
          try {
            response = JSON.parse(response);
          } catch (Error){
            log.debug(Error.message);
          }
        }).fail(function(ex) {
          log.debug(ex.message);
        });
    }

    window.raToggleBox = function(obj) {
        $(obj).parent().next().slideToggle();
        $(obj).parent().find('.box-checker').addClass('active');
        $(obj).removeClass('active');
    }

    window.raLoadRecent = function(obj, courseid) {
        var id = $(obj).attr('id');
        var items = $(obj).val();
        $(obj).parent().parent().next().html('<div class="loader"><i class="fa fa-spin fa-2x fa-spinner"></i></div>').load( mdlcfg.wwwroot+"/course/format/isf/ajax.php?action=get_recent_activity&courseid="+courseid+"&items="+items+'&type='+id, function() {
            $(obj).parent().parent().next().find('fa-spinner').remove();
        });
    }

    window.formatToggleChooser = function(section, cmid) {
        $('#section-modchooser-'+section+' .section-modchooser-text').trigger('click');
        $('#chooserform #typeformdiv').append('<input name="cmid" value="'+cmid+'" type="hidden" />');
        $('#chooserform input[name="jumplink"]').each(function () {
            $(this).val($(this).val()+"&cmid="+cmid);
        });
    }

    var Course = {

        init: function(courseid, activetab) {

            $('.section-modchooser-text').click(function(e){
                if ($('#chooserform input[name="cmid"]').length) {
                    $('#chooserform input[name="cmid"]').remove();
                }
            });

            $('.section-toggler').click(function(e){
                var section = $(this).parents('.section.main').attr('data-section-id');
                var value = ($(this).parents('.section.main').hasClass('toggled')) ? 0 : 1;
                $(this).parents('.section.main').toggleClass('toggled');

                ajax.call([{
                    methodname: 'format_isf_save_user_preferences',
                    args: { name: 'toggled_course_sections',
                            instanceid: section,
                            value: value,
                            courseid: courseid
                          }
                }])[0]
                .done(function(response) {
                  try {
                    response = JSON.parse(response);
                  } catch (Error){
                    log.debug(Error.message);
                  }
                }).fail(function(ex) {
                  log.debug(ex.message);
                });
            });

            $('.view-toggler i').click(function(e){
                if ($(this).hasClass('fa-th-large')){
                    var value = 'grid-view';
                } else {
                    var value = 'list-view';
                }
                $('.view-toggler i').toggleClass('hidden');
                $('.course-content').toggleClass('list-view');
                $('.course-content').toggleClass('grid-view');

                ajax.call([{
                    methodname: 'format_isf_save_user_preferences',
                    args: { name: 'course_view',
                            instanceid: 0,
                            value: value,
                            courseid: courseid
                          }
                }])[0]
                .done(function(response) {
                  try {
                    response = JSON.parse(response);
                  } catch (Error){
                    log.debug(Error.message);
                  }
                }).fail(function(ex) {
                  log.debug(ex.message);
                });
            });

            $('.activity-filter .sections-toggler').click(function(e){
                if ($('.course-content.active').hasClass('toggled')){
                    $('.section.main').removeClass('toggled');
                    var value = '0';
                } else {
                    $('.section.main').addClass('toggled');
                    var value = '1';
                }
                $('.course-content.active').toggleClass('toggled');

                ajax.call([{
                    methodname: 'format_isf_save_user_preferences',
                    args: { name: 'toggled_all_course_sections',
                            instanceid: '-1',
                            value: value,
                            courseid: courseid
                          }
                }])[0]
                .done(function(response) {
                  try {
                    response = JSON.parse(response);
                  } catch (Error){
                    log.debug(Error.message);
                  }
                }).fail(function(ex) {
                  log.debug(ex.message);
                });
            });

            $('#filtersearch').change( function () {
                var filter = $(this).val().toLowerCase();

                if (filter) {
                    $(".course-content li.activity").hide();
                    $('.course-content .instancename').each(function() {
                        if($(this).text().toLowerCase().indexOf(filter) >= 0){
                            $(this).parents('.activity').show();
                        }
                    });
                    $('.course-content .activity .contentafterlink').each(function(e) {
                        if($(this).text().toLowerCase().indexOf(filter) >= 0){
                            $(this).parents('li.activity').show();
                        }
                    });

                } else {
                    $(".course-content li.activity").show();
                }
            }).keyup( function () {
                $(this).change();
            });


            $('#filtercompleted').change(function(event){
                if($(this).prop("checked")){
                    $('.togglecompletion :input[value="1"]').parents('li.activity').hide();
                    $('.autocompletion.not-completed').parents('li.activity').hide();
                }else{
                    $('.togglecompletion :input[value="1"]').parents('li.activity').show();
                    $('.autocompletion.not-completed').parents('li.activity').show();
                }
            });

            $('#filteract').change(function(event){
                if($(this).prop("checked")){
                    $('.course-content ul.section li.activity').hide();
                    $('.course-content ul.section li.activity.act').show();
                }else{
                    $('.course-content ul.section li.activity').show();
                }
            });

            $('#filterres').change(function(event) {
                if($(this).prop("checked")) {
                    $('.course-content ul.section li.activity').hide();
                    $('.course-content ul.section li.activity.res').show();
                } else {
                    $('.course-content ul.section li.activity').show();
                }
            });

            $('#filterstarfav').change(function(event) {
                if($(this).prop("checked")) {
                    $('.fav-action').parents('li.activity').hide();
                    $('.fav-action.active').parents('li.activity').show();
                } else {
                    $('.fav-action').parents('li.activity').show();
                }
            });

            $('.rtype').change(function(event) {
                var filter = $(this).val();

                if(filter == '-1') {
                    $('.course-content ul.section li.activity').show();
                } else {
                    $('.course-content ul.section li.activity').hide();
                    $('.course-content ul.section li.activity.'+filter).show();
                }
            });


            // course tabs process
            $('.course-content-header .nav-link').on('click', function (event) {
                var tab = $(this).attr('href').substr(1);
                var type = $(this).attr('data-type');

                if (type == 'link') {
                    location = $(this).attr('data-link');
                } else {
                    if (tab != 'course' && tab != 'courseinfo') {
                        Course.loadContent(courseid, tab);
                    }

                    ajax.call([{
                        methodname: 'format_isf_save_user_preferences',
                        args: { name: 'activetab',
                                instanceid: 1,
                                value: tab,
                                courseid: courseid
                              }
                    }])[0]
                    .done(function(response) {
                      try {
                        response = JSON.parse(response);
                      } catch (Error){
                        log.debug(Error.message);
                      }
                    }).fail(function(ex) {
                      log.debug(ex.message);
                    });
                }
            });

            if (activetab != 'course') {
                Course.loadContent(courseid, activetab);
            }

            // course image show/hide actions
            // hide image
            $('#close-img').click(function(e) {
                e.preventDefault();
                $('#show-img').removeClass('hidden');
                $('.course-img .image').toggleClass('hidden');

                ajax.call([{
                    methodname: 'format_isf_save_user_preferences',
                    args: { name: 'show_wallimage',
                            instanceid: 0,
                            value: 0,
                            courseid: courseid
                          }
                }])[0]
                .done(function(response) {
                  try {
                    response = JSON.parse(response);
                  } catch (Error){
                    log.debug(Error.message);
                  }
                }).fail(function(ex) {
                  log.debug(ex.message);
                });
            });

            // show image
            $('#show-img').click(function(e) {
                e.preventDefault();
                $('#show-img').addClass('hidden');
                $('.course-img .image').toggleClass('hidden');

                ajax.call([{
                    methodname: 'format_isf_save_user_preferences',
                    args: { name: 'show_wallimage',
                            instanceid: 0,
                            value: 1,
                            courseid: courseid
                          }
                }])[0]
                .done(function(response) {
                  try {
                    response = JSON.parse(response);
                  } catch (Error){
                    log.debug(Error.message);
                  }
                }).fail(function(ex) {
                  log.debug(ex.message);
                });
            });

        },

        loadContent: function(courseid, tab) {

            if ($(".course-content .tab-content .tab-pane#"+tab).html() == '') {
                $(".course-content .tab-content .tab-pane#"+tab).html('<div class="loader"><i class="fa fa-spin fa-3x fa-spinner"></i></div>').load( mdlcfg.wwwroot+"/course/format/isf/ajax.php?action=load_"+tab+"&courseid="+courseid, function(e) {
                    $(".course-content .tab-content .tab-pane#"+tab+" .fa-spinner").remove();

                    if (tab == 'gradessummary') {

                        str.get_strings([
                            { key: 'all', component: 'format_isf' },
                        ]).then(function(all) {
                            var report = jQuery('.grade-summary').dataTable({
                                "sDom": 'T<"clear">lfrtip',
                                "sPaginationType": "full_numbers",
                                "bPaginate": true,
                                "bLengthChange": true,
                                "bFilter": false,
                                "bSort": true,
                                "bInfo": false,
                                "bAutoWidth": false,
                                "lengthMenu": [ [10, 25, 50, -1], [10, 25, 50, all] ],
                                'iDisplayLength': -1,
                                "oTableTools": {
                                 "aButtons": []
                                 },
                            });

                            $('.info-avarage').each(function(){
                                var text = $(this).html();
                                var value = $(this).attr('data-value');
                                $(this).html('<div class="avarage-stat" data-dimension="15" data-text="'+text+'" data-info="" data-width="3" data-fontsize="18" data-percent="'+value+'" data-fgcolor="#61a9dc" data-bgcolor="#eee"></div>');
                                $('.avarage-stat', this).circliful();
                            });

                            $('.grade-summary').on('click', '.viewdetails', function () {
                                var button = $(this);
                                var uid = $(this).attr('value');
                                var nTr = $(this).parents('tr')[0];
                                if ( report.fnIsOpen(nTr) ){
                                    report.fnClose( nTr );
                                    $(button).html('<i class="fa fa-angle-down"></i>');
                                    $(button).next().hide();
                                }else{
                                    $.ajax({
                                        url: mdlcfg.wwwroot + '/course/format/isf/ajax.php?action=get_gs_activity&courseid='+courseid+'&userid='+uid,
                                        dataType: "html",
                                        beforeSend: function(){
                                            $(button).html('<i class="fa fa-refresh fa-spin"></i>');
                                        }
                                    }).done(function( data ) {
                                        $(button).html('<i class="fa fa-angle-up"></i>');
                                        report.fnOpen( nTr, data, 'details' );
                                        $(button).next().show();
                                    });

                                }
                            });
                        });

                    }
                });
            }
        }

    };

    /**
    * @alias module:format_isf/course
    */
    return Course;

});

