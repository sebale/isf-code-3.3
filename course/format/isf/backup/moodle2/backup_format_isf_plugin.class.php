<?php

/**
 * Defines backup_format_isf_plugin class
 *
 * @package     format_isf
 * @category    backup
 * @copyright   2017 ISF
 */

defined('MOODLE_INTERNAL') || die;

/**
 * Provides the steps to perform one complete backup of the format instance
 */
class backup_format_isf_plugin extends backup_format_plugin {

    protected function define_course_plugin_structure() {

        // Define the virtual plugin element with the condition to fulfill.
        $plugin = $this->get_plugin_element(null, '/course/format', 'isf');

        // Create plugin container element with standard name
        $pluginwrapper = new backup_nested_element($this->get_recommended_name());

        // Add wrapper to plugin
        $plugin->add_child($pluginwrapper);

        // agenda //
        $agendas = new backup_nested_element('agendas');

        $agenda = new backup_nested_element('local_agenda_posts', array('id'),
            array('courseid',
                'userid',
                'title',
                'post',
                'question',
                'link',
                'link_title',
                'link_info',
                'files',
                'poll',
                'options',
                'allow_likes',
                'allow_comments',
                'timeopen',
                'visible',
                'timecreated'));

        // agenda comments //
        $agenda_comments = new backup_nested_element('local_agenda_comments', array('id'),
            array('instanceid',
                'courseid',
                'userid',
                'text',
                'timecreated'));


        // agenda likes //
        $agenda_likes = new backup_nested_element('local_agenda_likes', array('id'),
            array('courseid',
                'instanceid',
                'userid',
                'timecreated'));

        // agenda options //
        $agenda_options = new backup_nested_element('local_agenda_options', array('id'),
            array('courseid',
                'instanceid',
                'name'));

        // agenda poll values //
        $agenda_poll_values = new backup_nested_element('local_agenda_poll_values', array('id'),
            array('courseid',
                'instanceid',
                'optionid',
                'userid',
                'timecreated'));

        $pluginwrapper->add_child($agendas);
        $agendas->add_child($agenda);
        $agenda->add_child($agenda_comments);
        $agenda->add_child($agenda_likes);
        $agenda->add_child($agenda_options);
        $agenda->add_child($agenda_poll_values);

        $agenda->set_source_table('local_agenda_posts', array('courseid' => backup::VAR_COURSEID));

        $agenda_comments->set_source_table('local_agenda_comments', array('courseid' => backup::VAR_COURSEID, 'instanceid' => backup::VAR_PARENTID));

        $agenda_likes->set_source_table('local_agenda_likes', array('courseid' => backup::VAR_COURSEID, 'instanceid' => backup::VAR_PARENTID));

        $agenda_options->set_source_table('local_agenda_options', array('courseid' => backup::VAR_COURSEID, 'instanceid' => backup::VAR_PARENTID));

        $agenda_poll_values->set_source_table('local_agenda_poll_values', array('courseid' => backup::VAR_COURSEID, 'instanceid' => backup::VAR_PARENTID));

        $agenda->annotate_ids('user', 'userid');
        $agenda_comments->annotate_ids('user', 'userid');
        $agenda_likes->annotate_ids('user', 'userid');
        $agenda_poll_values->annotate_ids('user', 'userid');
        $agenda_poll_values->annotate_ids('local_agenda_options', 'optionid');

        $agenda->annotate_files('local_agenda', 'files', backup::VAR_PARENTID);
        $agenda->annotate_files('local_agenda', 'postfile', backup::VAR_PARENTID);
        $agenda->annotate_files('local_agenda', 'questionfile', backup::VAR_PARENTID);
        $agenda->annotate_files('local_agenda', 'poll', backup::VAR_PARENTID);

        // annotate course files //
        $pluginwrapper->annotate_files('format_isf', 'thumbnail', null);
        $pluginwrapper->annotate_files('format_isf', 'wallimage', null);

        return $plugin;
    }

    /**
     * Returns the format information to attach to section element
     */
    protected function define_module_plugin_structure() {
        // Define the virtual plugin element with the condition to fulfill.
        $plugin = $this->get_plugin_element(null, $this->get_format_condition(), 'isf');
        // Create one standard named plugin element (the visible container).
        // The sectionid and courseid not required as populated on restore.
        $pluginwrapper = new backup_nested_element($this->get_recommended_name());
        // Connect the visible container ASAP.
        $plugin->add_child($pluginwrapper);

        // course activityimages //
        $activityimages = new backup_nested_element('activityimages');
        $activityimage = new backup_nested_element('activityimage', array('id'),
            array('id',
                'course',
                'module'
            ));
        $pluginwrapper->add_child($activityimages);
        $activityimages->add_child($activityimage);
        $activityimage->set_source_table('course_modules', array('id'=>backup::VAR_MODID, 'course' => backup::VAR_COURSEID));

        $activityimage->annotate_files('format_isf', 'activityimage', 'id');

        return $plugin;
    }

}
