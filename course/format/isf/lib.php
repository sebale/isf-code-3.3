<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains main class for the course format Topic
 *
 * @since     Moodle 2.0
 * @package   format_isf
 * @copyright 2007 ISF
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
require_once($CFG->dirroot. '/course/format/lib.php');

/**
 * Main class for the isf course format
 *
 * @package    format_isf
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class format_isf extends format_base {

    public function course_content_header() {
        global $CFG;

        require_once($CFG->dirroot. '/course/format/isf/renderer.php');
        return new format_isf_course_content_header;
    }

    public function course_content_footer() {
        global $CFG;

        require_once($CFG->dirroot. '/course/format/isf/renderer.php');
        return new format_isf_course_content_footer;
    }

    /**
     * Returns true if this course format uses sections
     *
     * @return bool
     */
    public function uses_sections() {
        return true;
    }

    /**
     * Returns the display name of the given section that the course prefers.
     *
     * Use section name is specified by user. Otherwise use default ("Topic #")
     *
     * @param int|stdClass $section Section object from database or just field section.section
     * @return string Display name that the course format prefers, e.g. "Topic 2"
     */
    public function get_section_name($section) {
        $section = $this->get_section($section);
        if ((string)$section->name !== '') {
            return format_string($section->name, true,
                    array('context' => context_course::instance($this->courseid)));
        } else {
            return $this->get_default_section_name($section);
        }
    }

    /**
     * Returns the default section name for the topics course format.
     *
     * If the section number is 0, it will use the string with key = section0name from the course format's lang file.
     * If the section number is not 0, the base implementation of format_base::get_default_section_name which uses
     * the string with the key = 'sectionname' from the course format's lang file + the section number will be used.
     *
     * @param stdClass $section Section object from database or just field course_sections section
     * @return string The default value for the section name.
     */
    public function get_default_section_name($section) {
        if ($section->section == 0) {
            // Return the general section.
            return get_string('section0name', 'format_isf');
        } else {
            // Use format_base::get_default_section_name implementation which
            // will display the section name in "Topic n" format.
            return parent::get_default_section_name($section);
        }
    }

    /**
     * The URL to use for the specified course (with section)
     *
     * @param int|stdClass $section Section object from database or just field course_sections.section
     *     if omitted the course view page is returned
     * @param array $options options for view URL. At the moment core uses:
     *     'navigation' (bool) if true and section has no separate page, the function returns null
     *     'sr' (int) used by multipage formats to specify to which section to return
     * @return null|moodle_url
     */
    public function get_view_url($section, $options = array()) {
        global $CFG, $PAGE;

        $course = $this->get_course();
        $url = new moodle_url('/course/view.php', array('id' => $course->id));

        $sr = null;
        if (array_key_exists('sr', $options)) {
            $sr = $options['sr'];
        }
        if (is_object($section)) {
            $sectionno = $section->section;
        } else {
            $sectionno = $section;
        }
        if ($sectionno !== null) {
            if ($sr !== null) {
                if ($sr) {
                    $usercoursedisplay = COURSE_DISPLAY_MULTIPAGE;
                    $sectionno = $sr;
                } else {
                    $usercoursedisplay = COURSE_DISPLAY_SINGLEPAGE;
                }
            } else {
                $usercoursedisplay = $course->coursedisplay;
            }
            if ($sectionno != 0 && $usercoursedisplay == COURSE_DISPLAY_MULTIPAGE) {
                $url->param('section', $sectionno);
            } else {
                $url->set_anchor('section-'.$sectionno);
            }
        }
        return $url;
    }

    /**
     * Returns the information about the ajax support in the given source format
     *
     * The returned object's property (boolean)capable indicates that
     * the course format supports Moodle course ajax features.
     *
     * @return stdClass
     */
    public function supports_ajax() {
        $ajaxsupport = new stdClass();
        $ajaxsupport->capable = true;
        return $ajaxsupport;
    }

    /**
     * Loads all of the course sections into the navigation
     *
     * @param global_navigation $navigation
     * @param navigation_node $node The course node within the navigation
     */
    public function extend_course_navigation($navigation, navigation_node $node) {
        global $PAGE;
        // if section is specified in course/view.php, make sure it is expanded in navigation
        if ($navigation->includesectionnum === false) {
            $selectedsection = optional_param('section', null, PARAM_INT);
            if ($selectedsection !== null && (!defined('AJAX_SCRIPT') || AJAX_SCRIPT == '0') &&
                    $PAGE->url->compare(new moodle_url('/course/view.php'), URL_MATCH_BASE)) {
                $navigation->includesectionnum = $selectedsection;
            }
        }

        // check if there are callbacks to extend course navigation
        parent::extend_course_navigation($navigation, $node);

        // We want to remove the general section if it is empty.
        $modinfo = get_fast_modinfo($this->get_course());
        $sections = $modinfo->get_sections();
        if (!isset($sections[0])) {
            // The general section is empty to find the navigation node for it we need to get its ID.
            $section = $modinfo->get_section_info(0);
            $generalsection = $node->get($section->id, navigation_node::TYPE_SECTION);
            if ($generalsection) {
                // We found the node - now remove it.
                $generalsection->remove();
            }
        }
    }

    /**
     * Custom action after section has been moved in AJAX mode
     *
     * Used in course/rest.php
     *
     * @return array This will be passed in ajax respose
     */
    function ajax_section_move() {
        global $PAGE;
        $titles = array();
        $course = $this->get_course();
        $modinfo = get_fast_modinfo($course);
        $renderer = $this->get_renderer($PAGE);
        if ($renderer && ($sections = $modinfo->get_section_info_all())) {
            foreach ($sections as $number => $section) {
                $titles[$number] = $renderer->section_title($section, $course);
            }
        }
        return array('sectiontitles' => $titles, 'action' => 'move');
    }

    /**
     * Returns the list of blocks to be automatically added for the newly created course
     *
     * @return array of default blocks, must contain two keys BLOCK_POS_LEFT and BLOCK_POS_RIGHT
     *     each of values is an array of block names (for left and right side columns)
     */
    public function get_default_blocks() {
        return array(
            BLOCK_POS_LEFT => array('sb_timeline'),
            BLOCK_POS_RIGHT => array()
        );
    }

    /**
     * Definitions of the additional options that this course format uses for course
     *
     * Topics format uses the following options:
     * - coursedisplay
     * - numsections
     * - hiddensections
     *
     * @param bool $foreditform
     * @return array of options
     */
    public function course_format_options($foreditform = false) {
        global $PAGE, $CFG, $DB;

        static $courseformatoptions = false;
        if ($courseformatoptions === false) {
            $courseconfig = get_config('moodlecourse');
            $courseformatoptions = array(
                'coursename' => array(
                    'default' => '',
                    'type' => PARAM_RAW,
                ),
                'numsections' => array(
                    'default' => $courseconfig->numsections,
                    'type' => PARAM_INT,
                ),
                'hiddensections' => array(
                    'default' => 1,
                    'type' => PARAM_INT,
                ),
                'coursedisplay' => array(
                    'default' => COURSE_DISPLAY_MULTIPAGE,
                    'type' => PARAM_INT,
                ),
                'coursetype' => array(
                    'default' => 0,
                    'type' => PARAM_INT,
                ),
                'columns' => array(
                    'default' => 2,
                    'type' => PARAM_INT,
                ),
                'icon' => array(
                    'default' => '',
                    'type' => PARAM_RAW,
                ),
                'color' => array(
                    'default' => '',
                    'type' => PARAM_RAW,
                ),
                'tabagenda' => array(
                    'default' => 1,
                    'type' => PARAM_INT,
                ),
                'tabinfo' => array(
                    'default' => 1,
                    'type' => PARAM_INT,
                ),
                'tabgrades' => array(
                    'default' => 1,
                    'type' => PARAM_INT,
                ),
                'tabgradebook' => array(
                    'default' => 1,
                    'type' => PARAM_INT,
                ),
                'tabra' => array(
                    'default' => 1,
                    'type' => PARAM_INT,
                ),
            );
        }
        if ($foreditform && !isset($courseformatoptions['coursedisplay']['label'])) {
            $courseconfig = get_config('moodlecourse');
            $max = $courseconfig->maxsections;
            if (!isset($max) || !is_numeric($max)) {
                $max = 52;
            }
            $sectionmenu = array();
            for ($i = 0; $i <= $max; $i++) {
                $sectionmenu[$i] = "$i";
            }
            $courseformatoptionsedit = array(
                'coursename' => array(
                    'label' => get_string('coursename', 'format_isf'),
                    'element_type' => 'text',
                    'help' => 'coursename',
                    'help_component' => 'format_isf',
                ),
                'coursetype' => array(
                    'label' => get_string('coursetype', 'format_isf'),
                    'element_type' => 'select',
                    'element_attributes' => array(array(0=>get_string('secondary', 'format_isf'), 1=>get_string('primary', 'format_isf'))),
                    'help' => 'coursetype',
                    'help_component' => 'format_isf',
                ),
                'columns' => array(
                    'label' => get_string('columnsnum', 'format_isf'),
                    'element_type' => 'select',
                    'element_attributes' => array(array(2=>2,3=>3,4=>4)),
                    'help' => 'columns',
                    'help_component' => 'format_isf',
                ),
                'numsections' => array(
                    'label' => get_string('rootsections', 'format_isf'),
                    'element_type' => 'hidden',
                ),
                'hiddensections' => array(
                    'label' => new lang_string('hiddensections'),
                    'help' => 'hiddensections',
                    'help_component' => 'moodle',
                    'element_type' => 'hidden',
                    'value' => 1
                ),
                'coursedisplay' => array(
                    'label' => new lang_string('coursedisplay'),
                    'element_type' => 'hidden',
                    'default' => (isset($format_options['coursedisplay'])) ? $format_options['coursedisplay'] : $courseconfig->coursedisplay,
                ),
                'icon' => array(
                    'label' => get_string('icon', 'format_isf'),
                    'element_type' => 'text',
                    'help' => 'icon',
                    'help_component' => 'format_isf',
                ),
                'color' => array(
                    'label' => get_string('color', 'format_isf'),
                    'element_type' => 'text',
                    'element_attributes' => array(
                        array('class' => 'ColorPicker')
                    ),
                    'help' => 'color',
                    'help_component' => 'format_isf',
                ),
                'tabagenda' => array(
                    'label' => get_string('tabagenda', 'format_isf'),
                    'element_type' => 'select',
                    'element_attributes' => array(array(1=>get_string('showforall', 'format_isf'), 2=>get_string('hideforstudent', 'format_isf'), 3=>get_string('hideforparent', 'format_isf'), 0=>get_string('hideforall', 'format_isf'))),
                    'help' => 'tabagenda',
                    'help_component' => 'format_isf',
                ),
                'tabinfo' => array(
                    'label' => get_string('tabinfo', 'format_isf'),
                    'element_type' => 'select',
                    'element_attributes' => array(array(1=>get_string('showforall', 'format_isf'), 2=>get_string('hideforstudent', 'format_isf'), 3=>get_string('hideforparent', 'format_isf'), 0=>get_string('hideforall', 'format_isf'))),
                    'help' => 'tabinfo',
                    'help_component' => 'format_isf',
                ),
                'tabgrades' => array(
                    'label' => get_string('tabgrades', 'format_isf'),
                    'element_type' => 'select',
                    'element_attributes' => array(array(1=>get_string('showforall', 'format_isf'), 2=>get_string('hideforstudent', 'format_isf'), 3=>get_string('hideforparent', 'format_isf'), 0=>get_string('hideforall', 'format_isf'))),
                    'help' => 'tabgrades',
                    'help_component' => 'format_isf',
                ),
                'tabgradebook' => array(
                    'label' => get_string('tabgradebook', 'format_isf'),
                    'element_type' => 'select',
                    'element_attributes' => array(array(1=>get_string('showforall', 'format_isf'), 2=>get_string('hideforstudent', 'format_isf'), 3=>get_string('hideforparent', 'format_isf'), 0=>get_string('hideforall', 'format_isf'))),
                    'help' => 'tabgradebook',
                    'help_component' => 'format_isf',
                ),
                'tabra' => array(
                    'label' => get_string('tabra', 'format_isf'),
                    'element_type' => 'select',
                    'element_attributes' => array(array(1=>get_string('showforall', 'format_isf'), 2=>get_string('hideforstudent', 'format_isf'), 3=>get_string('hideforparent', 'format_isf'), 0=>get_string('hideforall', 'format_isf'))),
                    'help' => 'tabra',
                    'help_component' => 'format_isf',
                ),
            );

            $courseformatoptions = array_merge_recursive($courseformatoptions, $courseformatoptionsedit);
        }
        return $courseformatoptions;
    }

    /**
     * Adds format options elements to the course/section edit form.
     *
     * This function is called from {@link course_edit_form::definition_after_data()}.
     *
     * @param MoodleQuickForm $mform form the elements are added to.
     * @param bool $forsection 'true' if this is a section edit form, 'false' if this is course edit form.
     * @return array array of references to the added form elements.
     */
    public function create_edit_form_elements(&$mform, $forsection = false) {
        global $PAGE, $DB;

        $elements = parent::create_edit_form_elements($mform, $forsection);

        // Increase the number of sections combo box values if the user has increased the number of sections
        // using the icon on the course page beyond course 'maxsections' or course 'maxsections' has been
        // reduced below the number of sections already set for the course on the site administration course
        // defaults page.  This is so that the number of sections is not reduced leaving unintended orphaned
        // activities / resources.
        if (!$forsection) {
            $maxsections = get_config('moodlecourse', 'maxsections');
            $numsections = $mform->getElementValue('numsections');
            $numsections = $numsections[0];
            if ($numsections > $maxsections) {
                $element = $mform->getElement('numsections');
                for ($i = $maxsections+1; $i <= $numsections; $i++) {
                    $element->addOption("$i", $i);
                }
            }

            $course = $DB->get_record('course', array('id'=>$PAGE->course->id));
            if (isset($course->id)){
                $course = get_course($course->id);
                $coursecontext = context_course::instance($course->id);
            } else {
                $course = null;
                $coursecontext = null;
            }
            $courseconfig = get_config('moodlecourse');

            $filesoptions = course_overviewfiles_options($course);
            $filesoptions['accepted_types'] = array('.jpg', '.gif', '.png');

            if (isset($course->id)){
                file_prepare_standard_filemanager($course, 'wallimage', $filesoptions, $coursecontext,
                                               'format_isf', 'wallimage', $course->id);

                file_prepare_standard_filemanager($course, 'thumbnail', $filesoptions, $coursecontext,
                                               'format_isf', 'thumbnail', $course->id);
            }

            $wallimage_element = $mform->addElement('filemanager', 'wallimage_filemanager', get_string('coursewallimage', 'format_isf'), null, $filesoptions);
            $mform->addHelpButton('wallimage_filemanager', 'wallimage', 'format_isf');
            array_unshift($elements, $wallimage_element);

            $thumb_element = $mform->addElement('filemanager', 'thumbnail_filemanager', get_string('coursethumbnail', 'format_isf'), null, $filesoptions);
            $mform->addHelpButton('thumbnail_filemanager', 'coursethumbnail', 'format_isf');
            array_unshift($elements, $thumb_element);

        }

        return $elements;
    }

    /**
     * This function is called after the course has been set on the page. We use this to include various JS,
     * depending on the course page.
     *
     *
     * @global moodle_page $PAGE
     * @global type $COURSE
     * @param moodle_page $page
     */
    public function page_set_course(moodle_page $page) {
        parent::page_set_cm($page);
    }

    /**
     * Updates format options for a course
     *
     * In case if course format was changed to 'topics', we try to copy options
     * 'coursedisplay', 'numsections' and 'hiddensections' from the previous format.
     * If previous course format did not have 'numsections' option, we populate it with the
     * current number of sections
     *
     * @param stdClass|array $data return value from {@link moodleform::get_data()} or array with data
     * @param stdClass $oldcourse if this function is called from {@link update_course()}
     *     this object contains information about the course before update
     * @return bool whether there were any changes to the options values
     */
    public function update_course_format_options($data, $oldcourse = null) {
        global $DB, $CFG, $USER;
        require_once($CFG->libdir. '/gdlib.php');

        $data = (array)$data;
        $data['wallimage'] = (isset($data['wallimage_filemanager'])) ? $data['wallimage_filemanager'] : null;
        $data['thumbnail'] = (isset($data['thumbnail_filemanager'])) ? $data['thumbnail_filemanager'] : null;

        if ($oldcourse !== null) {
            $oldcourse = (array)$oldcourse;
            $options = $this->course_format_options();
            foreach ($options as $key => $unused) {
                if (!array_key_exists($key, $data)) {
                    if (array_key_exists($key, $oldcourse)) {
                        $data[$key] = $oldcourse[$key];
                    } else if ($key === 'numsections') {
                        // If previous format does not have the field 'numsections'
                        // and $data['numsections'] is not set,
                        // we fill it with the maximum section number from the DB
                        $maxsection = $DB->get_field_sql('SELECT max(section) from {course_sections}
                            WHERE course = ?', array($this->courseid));
                        if ($maxsection) {
                            // If there are no sections, or just default 0-section, 'numsections' will be set to default
                            $data['numsections'] = $maxsection;
                        }
                    }
                }
            }
        }

        $course = get_course($this->courseid);
        $coursecontext = context_course::instance($course->id);
        $editoroptions = array('maxfiles' => 1, 'maxbytes'=>$CFG->maxbytes, 'trusttext'=>false, 'noclean'=>true, 'context'=>$coursecontext, 'subdirs'=>0);

        $fs = get_file_storage();
        if ($data['thumbnail']){
            $usercontext = context_user::instance($USER->id);
            $draftfiles = $fs->get_area_files($usercontext->id, 'user', 'draft', $data['thumbnail'], 'id');
            if (count($draftfiles)){
                $fs->delete_area_files($coursecontext->id, 'format_isf', 'thumbnail', $this->courseid);
                file_save_draft_area_files($data['thumbnail'], $coursecontext->id, 'format_isf', 'thumbnail', $this->courseid, array('subdirs' => 0, 'maxfiles' => 1));
            }
        }

        if ($data['wallimage']){
            $usercontext = context_user::instance($USER->id);
            $draftfiles = $fs->get_area_files($usercontext->id, 'user', 'draft', $data['wallimage'], 'id');
            if (count($draftfiles)){
                $fs->delete_area_files($coursecontext->id, 'format_isf', 'wallimage', $this->courseid);
                file_save_draft_area_files($data['wallimage'], $coursecontext->id, 'format_isf', 'wallimage', $this->courseid, array('subdirs' => 0, 'maxfiles' => 1));
            }
        }

        $changed = $this->update_format_options($data);
        if ($changed && array_key_exists('numsections', $data)) {
            // If the numsections was decreased, try to completely delete the orphaned sections (unless they are not empty).
            $numsections = (int)$data['numsections'];
            $maxsection = $DB->get_field_sql('SELECT max(section) from {course_sections}
                        WHERE course = ?', array($this->courseid));
            for ($sectionnum = $maxsection; $sectionnum > $numsections; $sectionnum--) {
                if (!$this->delete_section($sectionnum, false)) {
                    break;
                }
            }
        }
        return $changed;
    }

    /**
     * Whether this format allows to delete sections
     *
     * Do not call this function directly, instead use {@link course_can_delete_section()}
     *
     * @param int|stdClass|section_info $section
     * @return bool
     */
    public function can_delete_section($section) {
        return true;
    }

    /**
     * Prepares the templateable object to display section name
     *
     * @param \section_info|\stdClass $section
     * @param bool $linkifneeded
     * @param bool $editable
     * @param null|lang_string|string $edithint
     * @param null|lang_string|string $editlabel
     * @return \core\output\inplace_editable
     */
    public function inplace_editable_render_section_name($section, $linkifneeded = true,
                                                         $editable = null, $edithint = null, $editlabel = null) {
        if (empty($edithint)) {
            $edithint = new lang_string('editsectionname', 'format_isf');
        }
        if (empty($editlabel)) {
            $title = get_section_name($section->course, $section);
            $editlabel = new lang_string('newsectionname', 'format_isf', $title);
        }
        return parent::inplace_editable_render_section_name($section, $linkifneeded, $editable, $edithint, $editlabel);
    }

    /**
     * Indicates whether the course format supports the creation of a news forum.
     *
     * @return bool
     */
    public function supports_news() {
        return true;
    }

    public function get_activityimages_chooser($course, $cm = null, &$mform) {
        $coursecontext = context_course::instance($course->id);

        $mform->addElement('hidden', 'cmid', 0); // cmid
        $mform->setType('cmid', PARAM_INT); // cmid

        $filesoptions = course_overviewfiles_options($course);
        $filesoptions['accepted_types'] = array('.jpg', '.gif', '.png');

        $mform->addElement('filemanager', 'activityimage_filemanager', get_string('activityimage', 'format_isf'), null, $filesoptions);

        $imagechooser = $this->get_images_chooser($mform, $course);
        if (!empty($imagechooser)){
            $mform->addElement('html', $imagechooser, array('class'=>'custom-imagechooser'));
        }

    }

    public function process_activityimages_chooser($course, $fromform, $data) {
        global $DB, $USER, $CFG;

        require_once($CFG->libdir. '/gdlib.php');

        $fs = get_file_storage();
        $context = context_module::instance($fromform->coursemodule);

        if (isset($data->image) and $data->image > 0 and $fileinfo = $DB->get_record('files', array('id'=>$data->image))) {

            if ($file = $fs->get_file($fileinfo->contextid, $fileinfo->component, $fileinfo->filearea, $fileinfo->itemid, $fileinfo->filepath, $fileinfo->filename)) {

                if ($imagefile = $file->copy_content_to_temp()) {

                    $fs->delete_area_files($context->id, 'format_isf', 'activityimage', $fromform->coursemodule);
                    $newimageid = format_isf_process_icon($context, 'format_isf', 'activityimage', $fromform->coursemodule, $imagefile, true, $fileinfo->filename);
                    @unlink($imagefile);
                }
            }
        } elseif(isset($data->activityimage_filemanager)) {
            $usercontext = context_user::instance($USER->id);
            $draftfiles = $fs->get_area_files($usercontext->id, 'user', 'draft', $data->activityimage_filemanager, 'id');
            if (count($draftfiles)){
                $fs->delete_area_files($context->id, 'format_isf', 'activityimage', $fromform->coursemodule);
                file_save_draft_area_files($data->activityimage_filemanager, $context->id, 'format_isf', 'activityimage', $fromform->coursemodule, array('subdirs' => 0, 'maxfiles' => 1));
            }
        }
    }

    public function get_images_chooser(&$mform, $course) {
        global $CFG, $DB, $USER, $PAGE;
        require_once($CFG->dirroot . '/lib/filelib.php');
        $output = '';

        $browser = get_file_browser();
        $context = context_system::instance();
        $coursecontext = context_course::instance($course->id);

        $fs = get_file_storage();

        $files = $fs->get_area_files($coursecontext->id, 'local_manager', 'courseimages', 0);
        if (!count($files)){
            $files = $fs->get_area_files($context->id, 'local_manager', 'courseimages', 0);
        }

        $coursefiles = array();
        $radioarray=array();

        foreach ($files as $file) {
            $filename = $file->get_filename();
            $filetype = $file->get_mimetype();
            if ($filename == '.' or !$filetype) continue;
            $coursefiles[$file->get_id()] = $file;

            $radioarray[] = $mform->createElement('radio', 'image', '', '', $file->get_id());
        }

        $customimg = (isset($_POST['image'])) ? $_POST['image'] : 0;

        if (count($coursefiles)){
            $output .= html_writer::start_tag('div', array('class' => 'activity-images-container'));
            $output .= html_writer::start_tag('ul', array('class' => 'clearfix'));
            foreach ($coursefiles as $file){
                $url = moodle_url::make_pluginfile_url($file->get_contextid(), 'local_manager', 'courseimages', $file->get_itemid(), $file->get_filepath(), $file->get_filename());

                $output .= html_writer::start_tag('li', array('class' => 'course-images-box cfile_'.$file->get_id().(($customimg > 0 and $customimg == $file->get_id()) ? ' checked' : ''), 'onclick'=>'chooseActivityImage('.$file->get_id().');'));
                $output .= html_writer::empty_tag('img', array('src' => $url->out()));
                $output .= html_writer::end_tag('li');
            }
            $output .= html_writer::end_tag('ul');
            $output .= html_writer::end_tag('div');
            $mform->addGroup($radioarray, 'image_arr', '', array(' '), false);

            $PAGE->requires->js_call_amd('format_isf/activityimagechooser', 'init', array());
        }

        return $output;
    }

    public function get_activity_imageurl($course, $mod = null) {
        global $CFG;

        $context = context_system::instance();
        $modcontext = context_module::instance($mod->id);
        $coursecontext = context_course::instance($course->id);

        $image_url = '';
        $image_files = array();
        $fs = get_file_storage();
        $imgfiles = $fs->get_area_files($modcontext->id, 'format_isf', 'activityimage', $mod->id);
        foreach ($imgfiles as $file) {
            $filename = $file->get_filename();
            $filetype = $file->get_mimetype();
            if ($filename == '.' or !$filetype) continue;
            $url = moodle_url::make_pluginfile_url($modcontext->id, 'format_isf', 'activityimage', $mod->id, '/', $filename);
            $image_url = $url->out();
        }

        // default module image (course level)
        if($image_url == ""){
            $defaultmodfile = $fs->get_area_files($coursecontext->id, 'local_manager', 'defaultimage_'.$mod->modname, 0);
            foreach ($defaultmodfile as $file) {
                $filename = $file->get_filename();
                $filetype = $file->get_mimetype();
                if ($filename == '.' or !$filetype) continue;
                $url = moodle_url::make_pluginfile_url($coursecontext->id, 'local_manager', 'defaultimage_'.$mod->modname, 0, '/', $filename);
                $image_url = $url->out();
            }
        }
        // default module image (system level)
        if($image_url == ""){
            $systemmodfile = $fs->get_area_files($context->id, 'local_manager', 'defaultimage_'.$mod->modname, 0);
            foreach ($systemmodfile as $file) {
                $filename = $file->get_filename();
                $filetype = $file->get_mimetype();
                if ($filename == '.' or !$filetype) continue;
                $url = moodle_url::make_pluginfile_url($context->id, 'local_manager', 'defaultimage_'.$mod->modname, 0, '/', $filename);
                $image_url = $url->out();
            }
        }

        // default image (course level)
        if($image_url == ""){
            $defaultmodfile = $fs->get_area_files($coursecontext->id, 'local_manager', 'defaultimage', 0);
            foreach ($defaultmodfile as $file) {
                $filename = $file->get_filename();
                $filetype = $file->get_mimetype();
                if ($filename == '.' or !$filetype) continue;
                $url = moodle_url::make_pluginfile_url($coursecontext->id, 'local_manager', 'defaultimage', 0, '/', $filename);
                $image_url = $url->out();
            }
        }
        // default image (system level)
        if($image_url == ""){
            $systemmodfile = $fs->get_area_files($context->id, 'local_manager', 'defaultimage', 0);
            foreach ($systemmodfile as $file) {
                $filename = $file->get_filename();
                $filetype = $file->get_mimetype();
                if ($filename == '.' or !$filetype) continue;
                $url = moodle_url::make_pluginfile_url($context->id, 'local_manager', 'defaultimage', 0, '/', $filename);
                $image_url = $url->out();
            }
        }

        if($image_url == ""){
            $image_url = $CFG->wwwroot."/theme/primaryisf/pix/frontcoursebg.jpg";
        }

        return $image_url;
    }

    public function get_course_name() {
        $ch_langs = array('zh_tw', 'zh_cn');

        $course = $this->get_course();
        if (in_array(current_language(), $ch_langs)) {
            if (isset($course->coursename) and !empty($course->coursename)) {
                return $course->coursename;
            }
        }

        return $course->fullname;
    }
}

/**
 * Implements callback inplace_editable() allowing to edit values in-place
 *
 * @param string $itemtype
 * @param int $itemid
 * @param mixed $newvalue
 * @return \core\output\inplace_editable
 */
function format_isf_inplace_editable($itemtype, $itemid, $newvalue) {
    global $DB, $CFG;
    require_once($CFG->dirroot . '/course/lib.php');
    if ($itemtype === 'sectionname' || $itemtype === 'sectionnamenl') {
        $section = $DB->get_record_sql(
            'SELECT s.* FROM {course_sections} s JOIN {course} c ON s.course = c.id WHERE s.id = ? AND c.format = ?',
            array($itemid, 'isf'), MUST_EXIST);
        return course_get_format($section->course)->inplace_editable_update_section_name($section, $itemtype, $newvalue);
    }
}

function format_isf_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options=array()) {
    global $CFG;
    require_once($CFG->dirroot . '/repository/lib.php');
    // Check the contextlevel is as expected - if your plugin is a block, this becomes CONTEXT_BLOCK, etc.
    if ($context->contextlevel != CONTEXT_COURSE and $context->contextlevel != CONTEXT_MODULE) {
        return false;
    }
    // Leave this line out if you set the itemid to null in make_pluginfile_url (set $itemid to 0 instead).
    $itemid = array_shift($args); // The first item in the $args array.

    // Use the itemid to retrieve any relevant data records and perform any security checks to see if the
    // user really does have access to the file in question.

    // Extract the filename / filepath from the $args array.
    $filename = array_pop($args); // The last item in the $args array.
    if (!$args) {
        $filepath = '/'; // $args is empty => the path is '/'
    } else {
        $filepath = '/'.implode('/', $args).'/'; // $args contains elements of the filepath
    }

    // Retrieve the file from the Files API.
    $fs = get_file_storage();
    $file = $fs->get_file($context->id, 'format_isf', $filearea, $itemid, $filepath, $filename);
    if (!$file) {
        return false; // The file does not exist.
    }

    send_stored_file($file, 86400, 0, $forcedownload, $options);
}

function format_isf_get_preferences_list($course) {

    return array(
        'toggled_sections_'.$course->id => '0',
        'toggledall_sections_'.$course->id => '0',
        'activetab_'.$course->id => 'course',
        'show_wallimage_'.$course->id => '1',
        'fav_modules_'.$course->id => '0',
        'course_view_'.$course->id => 'grid-view'
    );
}

function format_isf_process_icon($context, $component, $filearea, $itemid, $originalfile, $preferpng = false, $filename = '') {
    global $CFG;
    require_once($CFG->libdir.'/filelib.php');
    require_once($CFG->libdir.'/gdlib.php');

    if (!is_file($originalfile)) {
        return false;
    }

    $imageinfo = getimagesize($originalfile);
    $imagefnc = '';

    if (empty($imageinfo)) {
        return false;
    }

    $image = new stdClass();
    $image->width  = $imageinfo[0];
    $image->height = $imageinfo[1];
    $image->type   = $imageinfo[2];

    $t = null;
    switch ($image->type) {
        case IMAGETYPE_GIF:
            if (function_exists('imagecreatefromgif')) {
                $im = imagecreatefromgif($originalfile);
            } else {
                debugging('GIF not supported on this server');
                return false;
            }
            // Guess transparent colour from GIF.
            $transparent = imagecolortransparent($im);
            if ($transparent != -1) {
                $t = imagecolorsforindex($im, $transparent);
            }
            break;
        case IMAGETYPE_JPEG:
            if (function_exists('imagecreatefromjpeg')) {
                $im = imagecreatefromjpeg($originalfile);
            } else {
                debugging('JPEG not supported on this server');
                return false;
            }
            // If the user uploads a jpeg them we should process as a jpeg if possible.
            if (!$preferpng && function_exists('imagejpeg')) {
                $imagefnc = 'imagejpeg';
                $imageext = '.jpg';
                $filters = null; // Not used.
                $quality = 100;
            }
            break;
        case IMAGETYPE_PNG:
            if (function_exists('imagecreatefrompng')) {
                $im = imagecreatefrompng($originalfile);
            } else {
                debugging('PNG not supported on this server');
                return false;
            }
            break;
        default:
            return false;
    }

    // The conversion has not been decided yet, let's apply defaults (png with fallback to jpg).
    if (empty($imagefnc)) {
        if (function_exists('imagepng')) {
            $imagefnc = 'imagepng';
            $imageext = '.png';
            $filters = PNG_NO_FILTER;
            $quality = 1;
        } else if (function_exists('imagejpeg')) {
            $imagefnc = 'imagejpeg';
            $imageext = '.jpg';
            $filters = null; // Not used.
            $quality = 100;
        } else {
            debugging('Jpeg and png not supported on this server, please fix server configuration');
            return false;
        }
    }

    if (function_exists('imagecreatetruecolor')) {
        $im1 = imagecreatetruecolor($image->width, $image->height);

        if ($image->type != IMAGETYPE_JPEG and $imagefnc === 'imagepng') {
            if ($t) {
                // Transparent GIF hacking...
                $transparentcolour = imagecolorallocate($im1 , $t['red'] , $t['green'] , $t['blue']);
                imagecolortransparent($im1 , $transparentcolour);
            }

            imagealphablending($im1, false);
            $color = imagecolorallocatealpha($im1, 0, 0,  0, 127);
            imagefill($im1, 0, 0,  $color);
            imagesavealpha($im1, true);
        }
    } else {
        $im1 = imagecreate($image->width, $image->height);
    }

    imagecopybicubic($im1, $im, 0, 0, 0, 0, $image->width, $image->height, $image->width, $image->height);

    $fs = get_file_storage();

    $icon = array('contextid'=>$context->id, 'component'=>$component, 'filearea'=>$filearea, 'itemid'=>$itemid, 'filepath'=>'/');

    ob_start();
    if (!$imagefnc($im1, NULL, $quality, $filters)) {
        // keep old icons
        ob_end_clean();
        return false;
    }
    $data = ob_get_clean();
    imagedestroy($im1);
    $icon['filename'] = (!empty($filename)) ? $filename : 'f1'.$imageext;
    $fs->delete_area_files($context->id, $component, $filearea, $itemid);
    $file1 = $fs->create_file_from_string($icon, $data);

    return $file1->get_id();
}

function course_copy_mod($courseid = 0, $section = 0, $cmid = 0, $movebefore = 0){
    global $CFG, $DB, $USER;

    require_once($CFG->dirroot . '/backup/util/includes/backup_includes.php');
    require_once($CFG->dirroot . '/backup/util/includes/restore_includes.php');
    require_once($CFG->libdir . '/filelib.php');

    $cmdb = $DB->get_record('course_modules', array('id'=>$cmid));
    // The id of the course we are importing FROM (will only be set if past first stage
    $importcourseid   = $cmdb->course;
    $importcourse     = $DB->get_record('course', array('id' => $importcourseid), '*', MUST_EXIST);
    $importcontext    = context_course::instance($importcourseid);
    // The courseid we are importing to
    $course     = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
    $context    = context_course::instance($courseid);

    $cm         = get_coursemodule_from_id('', $cmid, $importcourse->id, true, MUST_EXIST);
    $cmcontext  = context_module::instance($cm->id);
    $cm->course = $courseid;
    $DB->update_record('course_modules', $cm);

    // backup the activity
    $bc = new backup_controller(backup::TYPE_1ACTIVITY, $cm->id, backup::FORMAT_MOODLE,
            backup::INTERACTIVE_NO, backup::MODE_IMPORT, $USER->id);

    $backupid       = $bc->get_backupid();
    $backupbasepath = $bc->get_plan()->get_basepath();

    $tasks = $bc->get_plan()->get_tasks();

    $bc->execute_plan();
    $bc->destroy();

    // restore the backup immediately
    $rc = new restore_controller($backupid, $course->id,
            backup::INTERACTIVE_NO, backup::MODE_IMPORT, $USER->id, backup::TARGET_CURRENT_ADDING);

    if (!$rc->execute_precheck()) {
        $precheckresults = $rc->get_precheck_results();
        if (is_array($precheckresults) && !empty($precheckresults['errors'])) {
            if (empty($CFG->keeptempdirectoriesonbackup)) {
                fulldelete($backupbasepath);
            }
        }
    }
    $rc->execute_plan();

    // Now a bit hacky part follows - we try to get the cmid of the newly
    // restored copy of the module.
    $newcmid = null;
    $tasks = $rc->get_plan()->get_tasks();
    foreach ($tasks as $task) {
        if (is_subclass_of($task, 'restore_activity_task')) {
            if ($task->get_old_contextid() == $cmcontext->id) {
                $newcmid = $task->get_moduleid();
                break;
            }
        }
    }

    $cm->course = $importcourseid;
    $DB->update_record('course_modules', $cm);

    // If we know the cmid of the new course module, let us move it
    // right below the original one. otherwise it will stay at the
    // end of the section.
    if ($newcmid) {
        $info = get_fast_modinfo($course);
        $newcm = $info->get_cm($newcmid);
        $section = $DB->get_record('course_sections', array('id' => $section, 'course' => $course->id));
        moveto_module($newcm, $section, $movebefore);

        // Update calendar events with the duplicated module.
        $refresheventsfunction = $newcm->modname . '_refresh_events';
        if (function_exists($refresheventsfunction)) {
            call_user_func($refresheventsfunction, $newcm->course);
        }

        // Trigger course module created event. We can trigger the event only if we know the newcmid.
        $event = \core\event\course_module_created::create_from_cm($newcm);
        $event->trigger();
    }
    rebuild_course_cache($cm->course);

    $rc->destroy();

    if (empty($CFG->keeptempdirectoriesonbackup)) {
        fulldelete($backupbasepath);
    }

    return $newcm;
}
