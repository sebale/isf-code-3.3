<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'format_isf', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   format_isf
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['currentsection'] = 'This Learning Module';
$string['editsection'] = 'Edit Learning Module';
$string['editsectionname'] = 'Edit Learning Module name';
$string['deletesection'] = 'Delete Learning Module';
$string['newsectionname'] = 'New name for Learning Module {$a}';
$string['sectionname'] = 'Learning Module';
$string['pluginname'] = 'ISF format';
$string['section0name'] = 'General';
$string['page-course-view-topics'] = 'Any course main page in ISF format';
$string['page-course-view-topics-x'] = 'Any course page in ISF format';
$string['hidefromothers'] = 'Hide topic';
$string['showfromothers'] = 'Show topic';

$string['image'] = 'Image';
$string['icon'] = 'Icon';
$string['color'] = 'Color';
$string['image_help'] = 'Course Image file';
$string['icon_help'] = 'Course Icon for dashboard';
$string['color_help'] = 'Course Color for dashboard';

$string['course'] = 'Course';
$string['agenda'] = 'Agenda';
$string['courseinfo'] = 'Course Summary';
$string['gradessummary'] = 'Grades Summary';
$string['gradebook'] = 'Gradebook';
$string['recentactivity'] = 'Recent Activity';
$string['favorite'] = 'Favorite';
$string['activities'] = 'Activities';
$string['resources'] = 'Resources';
$string['viewall'] = 'View all';
$string['searchlearningmodules'] = 'Search Learning Modules';
$string['togglesection'] = 'Toggle Section';
$string['toggleallsection'] = 'Toggle All Section';
$string['coursetype'] = 'Course Type';
$string['mastercourse'] = 'Master Course';
$string['regularcourse'] = 'Regular Course';
$string['showforall'] = 'Show for all';
$string['hideforstudent'] = 'Hide for students';
$string['hideforparent'] = 'Hide for parents';
$string['hideforall'] = 'Hide for all';
$string['tabagenda'] = 'Display tab Agenda';
$string['tabinfo'] = 'Display tab Course Summary';
$string['tabgrades'] = 'Display tab Grades';
$string['tabgradebook'] = 'Display tab Gradebook';
$string['tabra'] = 'Display tab Recent Activities';
$string['coursethumbnail'] = 'Course Image';
$string['coursewallimage'] = 'Course Wall Image';
$string['wallimage'] = 'Course Wall Image';
$string['coursename'] = 'Alternative Course Name';
$string['hideimage'] = 'Hide';
$string['showimage'] = 'Show Image';
$string['courseupdates'] = 'Course Updates';
$string['showby'] = 'Show by';
$string['all'] = 'All';
$string['myrecentactivities'] = 'My Recent Activities';
$string['usersrecentactivities'] = 'Users Recent Activities';
$string['nothingsincelastlogin'] = 'Nothing new since your last login';
$string['secondary'] = 'Secondary';
$string['primary'] = 'Primary';
$string['activityimage'] = 'Activity Image';
$string['listview'] = 'List';
$string['gridview'] = 'Grid';
$string['rootsections'] = 'Root Sections';
$string['columnsnum'] = 'Number of grid columns';
$string['columns'] = 'Number of grid columns';
$string['coursename_help'] = 'Course name CH';
$string['coursetype_help'] = 'Course display type Primary or Secondary';
$string['columns_help'] = 'Number of grid columns in Pimary course type';
$string['tabagenda_help'] = 'Show/Hide Agenda tab settings';
$string['tabinfo_help'] = 'Show/Hide Course Summary tab settings';
$string['tabgrades_help'] = 'Show/Hide Grades tab settings';
$string['tabgradebook_help'] = 'Show/Hide Gradebook tab settings';
$string['tabra_help'] = 'Show/Hide Recent Activity tab settings';
$string['wallimage_help'] = 'Upload image for Course Page';
$string['coursethumbnail_help'] = 'Upload image for Active Course Block';
$string['createsection'] = 'Create Learning Module';
$string['name'] = 'Name';
$string['summary'] = 'Summary';
$string['insertlearningmodule'] = 'Insert Learning Module';
$string['before'] = 'Before';
$string['after'] = 'After';
$string['or'] = 'or';
$string['sectionnameisrequired'] = 'Learning Module Name is required';
$string['currentgrade'] = 'Current Grade';
$string['classavg'] = 'Class Avg';
$string['goalgrade'] = 'Goal Grade';
$string['student'] = 'Student';
$string['progress'] = 'Progress';
$string['letter'] = 'Letter';
$string['completed'] = 'Completed';
$string['score'] = 'Score';
$string['details'] = 'Details';
$string['activity'] = 'Activity';
$string['type'] = 'Type';
$string['completedon'] = 'Completed On';
$string['incomplete'] = 'Incomplete';
$string['completed'] = 'Completed';
$string['passed'] = 'Passed';
$string['failed'] = 'Failed';
$string['notfound'] = 'Data not found';
$string['activities'] = 'activities';
$string['oneactivity'] = 'activity';



