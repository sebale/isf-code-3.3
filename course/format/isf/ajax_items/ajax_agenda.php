<?php

require_once('../../../../config.php');
require_once($CFG->libdir . '/filelib.php');
require_once($CFG->libdir . '/completionlib.php');
require_once($CFG->dirroot . '/course/format/lib.php');

defined('MOODLE_INTERNAL') || die();

$courseid = required_param('courseid', PARAM_INT);

// make sure all sections are created
$course = course_get_format($courseid)->get_course();
course_create_sections_if_missing($course, range(0, $course->numsections));

context_helper::preload_course($course->id);
$context = context_course::instance($course->id, MUST_EXIST);
$PAGE->set_context($context);

?>

Agenda
