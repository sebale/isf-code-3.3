<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains main class for the course format Topic
 *
 * @since     Moodle 2.0
 * @package   format_isf
 * @copyright 2007 ISF
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
require_once($CFG->dirroot. '/course/format/lib.php');

class isfCourse {

    public $course = null;

    /**
     * Constructor method, calls the parent constructor
     *
     * @param $course
     */
    public function __construct($course) {
        $this->course = $course;
    }


    public function load_agenda() {
        global $PAGE, $CFG;
        require_once($CFG->dirroot . '/local/agenda/renderer.php');

        $renderer = $PAGE->get_renderer('local_agenda');
        $renderer->print_home($this->course->id);
    }

    public function get_recent_activity($tab) {
        global $DB, $CFG, $USER;

        $items        = optional_param('items', 0, PARAM_INT);
        $type         = optional_param('type', '', PARAM_RAW);
        $this->course->recent_count = $items;
        $context = context_course::instance($this->course->id, MUST_EXIST);

        $output = '';

        if ($type == 'course_resent'){
            $output .= $this->ra_recent_course_activity($this->course);
        } else {
            $param = new stdClass();
            $params->user   = (!has_capability('moodle/course:manageactivities', $context)) ? $USER->id : 0 ;
            $param->group  = 0;
            $param->date   = $this->course->timecreated;
            $param->id     = $this->course->id;
            $output .= $this->ra_resent_users_activity($param, $this->course);
        }

        echo $output;
    }

    public function load_recentactivity($tab) {
        global $DB, $CFG, $USER;
        $html = '';

        $context = context_course::instance($this->course->id);

        $this->course->recent_count = 5;

        $params = new stdClass();
        $params->user   = (!has_capability('moodle/course:manageactivities', $context)) ? $USER->id : 0 ;
        $params->group  = 0;
        $params->date   = $this->course->timecreated;
        $params->id     = $this->course->id;

        $html .= html_writer::start_tag('div', array('class' =>'users-activity-box'));
        $html .= html_writer::start_tag('h3');
        $html .= get_string('courseupdates', 'format_isf');
            $html .= html_writer::start_tag('div', array('class' =>'recent-items-count'));
            $html .= get_string('showby', 'format_isf');
            $html .= html_writer::start_tag('select', array('name' => 'items-count', 'id' => 'course_resent', 'onchange'=>'raLoadRecent(this, '.$this->course->id.')'));
                $html .= html_writer::tag('option', 5, array('value' => '5'));
                $html .= html_writer::tag('option', 15, array('value' => '15'));
                $html .= html_writer::tag('option', 30, array('value' => '30'));
                $html .= html_writer::tag('option', get_string('all', 'format_isf'), array('value' => '-1'));
            $html .= html_writer::end_tag('select');
            $html .= html_writer::end_tag('div');
            $html .= html_writer::tag('div', html_writer::tag('div', '', array('class'=>'fa fa-chevron-down')), array('class' =>'box-checker active', 'onclick'=>'raToggleBox(this)'));
            $html .= html_writer::tag('div', html_writer::tag('div', '', array('class'=>'fa fa-chevron-up')), array('class' =>'box-checker', 'onclick'=>'raToggleBox(this)'));
        $html .= html_writer::end_tag('h3');
        $html .= html_writer::start_tag('div', array('class' =>'recent-course-changes'));
            $html .= $this->ra_recent_course_activity($this->course);
        $html .= html_writer::end_tag('div');
        $html .= html_writer::end_tag('div');

        $html .= html_writer::start_tag('div', array('class' =>'users-activity-box'));
        $html .= html_writer::start_tag('h3');
        $html .= (($param->user > 0 and $param->user == $USER->id) ? get_string('myrecentactivities', 'format_isf') : get_string('usersrecentactivities', 'format_isf'));
            $html .= html_writer::start_tag('div', array('class' =>'recent-items-count'));
            $html .= get_string('showby', 'format_isf');
            $html .= html_writer::start_tag('select', array('name' => 'items-count', 'id' => 'user_resent', 'onchange'=>'raLoadRecent(this, '.$this->course->id.')'));
                $html .= html_writer::tag('option', 5, array('value' => '5'));
                $html .= html_writer::tag('option', 15, array('value' => '15'));
                $html .= html_writer::tag('option', 30, array('value' => '30'));
                $html .= html_writer::tag('option', get_string('all', 'format_isf'), array('value' => '-1'));
            $html .= html_writer::end_tag('select');
            $html .= html_writer::end_tag('div');
            $html .= html_writer::tag('div', html_writer::tag('div', '', array('class'=>'fa fa-chevron-down')), array('class' =>'box-checker active', 'onclick'=>'raToggleBox(this)'));
            $html .= html_writer::tag('div', html_writer::tag('div', '', array('class'=>'fa fa-chevron-up')), array('class' =>'box-checker', 'onclick'=>'raToggleBox(this)'));
        $html .= html_writer::end_tag('h3');
        $html .= html_writer::start_tag('div', array('class' =>'recent-user-changes'));
            $html .= $this->ra_resent_users_activity($param, $this->course);
        $html .= html_writer::end_tag('div');
        $html .= html_writer::end_tag('div');

        echo $html;
    }

    function ra_resent_users_activity($param, $course){
        global $CFG, $OUTPUT, $USER;

        $modinfo = get_fast_modinfo($course);
        $modnames = get_module_types_names();

        $sections = array();
        foreach ($modinfo->get_section_info_all() as $i => $section) {
            if (!empty($section->uservisible)) {
                $sections[$i] = $section;
            }
        }
        $modinfo->get_groups();
        $activities = array();
        $index = 0;

        foreach ($sections as $sectionnum => $section) {

            $activity = new stdClass();
            $activity->type = 'section';
            if ($section->section > 0) {
                $activity->name = get_section_name($course, $section);
            } else {
                $activity->name = '';
            }

            $activity->visible = $section->visible;
            $activities[$index++] = $activity;

            if (empty($modinfo->sections[$sectionnum])) {
                continue;
            }

            foreach ($modinfo->sections[$sectionnum] as $cmid) {
                $cm = $modinfo->cms[$cmid];

                if (!$cm->uservisible) {
                    continue;
                }

                $libfile = "$CFG->dirroot/mod/$cm->modname/lib.php";

                if (file_exists($libfile)) {
                    require_once($libfile);
                    $get_recent_mod_activity = $cm->modname."_get_recent_mod_activity";

                    if (function_exists($get_recent_mod_activity)) {
                        $activity = new stdClass();
                        $activity->type    = 'activity';
                        $activity->cmid    = $cmid;
                        $activities[$index++] = $activity;
                        $get_recent_mod_activity($activities, $index, $param->date, $course->id, $cmid, $param->user, $param->group);
                    }
                }
            }
        }

        $output = '';
        $detail = true;

        usort($activities, 'compare_activities_by_time_desc');

        if (!empty($activities)) {
            $clear_activities = array();
            foreach ($activities as $key => $activity) {
                if ($activity->type == 'section' || $activity->type == 'activity') {
                  continue;
                } else {
                    $clear_activities[$key] = $activity;
                }
            }
            $activities = $clear_activities;
        }

        if (!empty($activities)) {

            $newsection   = true;
            $lastsection  = '';
            $newinstance  = true;
            $lastinstance = '';
            $section = 0;

            $activity_count = count($activities);
            $viewfullnames  = array();

            $output .= '<ul>';
            $i = 0;
            foreach ($activities as $key => $activity) {
                if ($course->recent_count > 0 and $i == $course->recent_count) {
                    break;
                }
                $output .= '<li>';
                    if (!isset($viewfullnames[$activity->cmid])) {
                        $cm_context = context_module::instance($activity->cmid);
                        $viewfullnames[$activity->cmid] = has_capability('moodle/site:viewfullnames', $cm_context);
                    }
                    $print_recent_mod_activity = $activity->type.'_print_recent_mod_activity';
                    if (function_exists($print_recent_mod_activity)) {
                        ob_start();
                        $print_recent_mod_activity($activity, $course->id, $detail, $modnames, $viewfullnames[$activity->cmid]);
                        $output .= ob_get_clean();
                    }
                $output .= '</li>';
                $i++;
            }
            $output .= '</ul><script>jQuery(".glossary-activity-content").append("<div class=\'ra-arrow\'></div>"); jQuery(".users-activity-box .title").append("<div class=\'act-arrow\'></div>");</script>';
        } else {
            $output .= '<div class="recent-course-msg">'.get_string('nothingsincelastlogin', 'format_isf').'</div>';
        }

        return $output;
    }



    function ra_recent_course_activity($course) {

        $output = '';
        $structuralchanges = $this->ra_get_structural_changes($course, $course->timecreated);
        if (!empty($structuralchanges)) {
            $output .= html_writer::start_tag('ul');
            $i = 0;
            foreach ($structuralchanges as $changeinfo => $change) {
                if ($course->recent_count > 0 and $i == $course->recent_count){break;}
                $output .= $this->ra_structural_change($change);
                $i++;
            }
            $output .= html_writer::end_tag('ul');
        } else {
            $output .= html_writer::tag('div', get_string('nothingsincelastlogin', 'format_isf'), array('recent-course-msg'));
        }

        return $output;
    }


    function ra_get_timestart($course) {
        global $USER;

        $timestart = round(time() - COURSE_MAX_RECENT_PERIOD, -2);
        if (!isguestuser()) {
            if (!empty($USER->lastcourseaccess[$course->id])) {
                if ($USER->lastcourseaccess[$course->id] > $timestart) {
                    $timestart = $USER->lastcourseaccess[$course->id];
                }
            }
        }
        return $timestart;
    }

    function ra_get_structural_changes($course, $timestart) {
        global $DB, $CFG;
        $changelist = array();
        $logs = $DB->get_records_sql('SELECT l.*,
                                        u.id as uid, CONCAT(u.firstname, " ", u.lastname) as user, u.firstname, u.lastname,
                                        u.picture, u.imagealt, u.email
                                            FROM {logstore_standard_log} l
                                        LEFT JOIN {user} u ON l.userid = u.id
                                        WHERE   l.timecreated > '.$timestart.' AND l.courseid = '.$course->id.' AND
                                                l.target = "course_module" AND
                                                (l.action = "created" OR l.action = "updated")
                                        ORDER BY l.timecreated DESC');

        if ($logs) {
            $modinfo = get_fast_modinfo($course);
            $newgones = array(); // added and later deleted items
            foreach ($logs as $key => $log) {
                $info = unserialize($log->other);

                if (count($info) != 3) {
                    debugging("Incorrect log entry info: id = ".$log->id, DEBUG_DEVELOPER);
                    continue;
                }

                $modname    = $info['modulename'];
                $instanceid = $info['instanceid'];
                $name = $info['name'];
                if ($log->action == 'deleted') {
                    if (plugin_supports('mod', $modname, FEATURE_NO_VIEW_LINK, false)) {
                        // we should better call cm_info::has_view() because it can be
                        // dynamic. But there is no instance of cm_info now
                        continue;
                    }
                    // unfortunately we do not know if the mod was visible
                    if (!array_key_exists($log->info, $newgones)) {
                        $changelist[$log->info] = array('action' => $log->action,
                            'module' => (object)array(
                                'modname' => $modname,
                                'modfullname' => get_string('modulename', $modname),
                                'log'=>$log
                             ));
                    }
                } else {
                    if (!isset($modinfo->instances[$modname][$instanceid])) {
                        if ($log->action == 'created') {
                            // do not display added and later deleted activities
                            $newgones[$log->info] = true;
                        }
                        continue;
                    }
                    $cm = $modinfo->instances[$modname][$instanceid];
                    if ($cm->has_view() && $cm->uservisible && empty($changelist[$log->info])) {
                        $changelist[$log->info] = array('action' => $log->action, 'module' => $cm, 'log'=>$log);
                    }
                }
            }
        }

        return $changelist;
    }

    function ra_structural_change($change) {
        global $OUTPUT, $CFG;

        $cm = $change['module'];
        $log = $change['log'];
        $output = '';

        switch ($change['action']) {
            case 'created':
                $text = get_string('added', 'moodle', $cm->modfullname);
                break;
            case 'updated':
                $text = get_string('updated', 'moodle', $cm->modfullname);
                break;
            default:
                $text = '';
        }

        $user_icon = (object)array('id'=>$log->userid,'firstname'=>$log->firstname, 'lastname'=>$log->lastname, 'picture'=>$log->picture, 'imagealt'=>$log->imagealt, 'email'=>$log->email, 'firstnamephonetic'=>$log->firstname, 'lastnamephonetic'=>$log->lastname, 'middlename'=>'', 'alternatename'=>'');

        $output .= '<li class="clearfix course-changes">';
            $output .= '<table class="" border="0" cellspacing="0" cellpadding="3">';
                $output .= '<tr>';
                    $output .= '<td class="userpicture" valign="top">';
                        $output .= $OUTPUT->user_picture($user_icon, array('size'=>35));
                    $output .= '</td>';
                    $output .= '<td>';
                        $output .= '<div class="user">';
                            $output .= '<a href="'.$CFG->wwwroot.'/user/profile.php?id='.$log->userid.'">'.$log->user.'</a>';
                            $output .= '<span class="tl-action-text">'.$text.':</span>';
                            $output .= '<span class="modname">'.$OUTPUT->pix_icon('icon', '', $cm->modname, array('class' => 'icon'));
                            $output .= html_writer::link($cm->url, format_string($cm->name, true)).'</span>';
                        $output .= '</div>';
                        $output .= '<div class="title">';
                            $output .= '<div class="act-arrow"></div>';
                            $output .= '<span class="act-date">'.date('l, F d Y, h:i A', strtotime(userdate($log->timecreated))).'</span>';
                        $output .= '</div>';
                    $output .= '</td>';
                $output .= '</tr>';
            $output .= '</table>';
        $output .= '</li>';

        return $output;
    }

    function create_section() {
        global $CFG, $DB;

        require_once($CFG->dirroot.'/course/lib.php');

        $coursecontext = context_course::instance($this->course->id);
        require_login($this->course);
        require_capability('moodle/course:update', context_course::instance($this->course->id));

        $form    = (object)optional_param_array('form', array(), PARAM_RAW);
        $returnurl = new moodle_url("/course/view.php", array('id'=>$this->course->id));

        if ($form->name == '') {
            redirect($returnurl, get_string('sectionnameisrequired', 'format_isf'));
        }

        $courseformatoptions = course_get_format($this->course)->get_format_options();
        $courseformatoptions['numsections']++;
        course_get_format($this->course)->update_course_format_options(
                    array('numsections' => $courseformatoptions['numsections']));

        $course = course_get_format($this->course)->get_course();
        course_create_sections_if_missing($course, range(0, $course->numsections));
        $section = $DB->get_record('course_sections', array('course'=>$course->id, 'section'=>$course->numsections));

        $section->name = $form->name;
        $section->summary = $form->summary;
        $section->summaryformat = 1;
        $DB->update_record('course_sections', $section);
        $sectionid = $section->section;

        if ($form->target and $form->position){
            $move = true;
            if ($form->position == 'before') {
                if ($form->target-1 == $sectionid){
                    $move = false;
                }
                $value = ($form->target > $sectionid) ? $form->target-1 : $form->target;
            } else {
                if ($form->target+1 == $sectionid){
                    $move = false;
                }
                $value = ($form->target > $sectionid) ? $form->target : $form->target+1;
            }
            if ($move){
                move_section_to($course, $sectionid, $value);
            }
        }

        $response = course_get_format($course)->ajax_section_move();
        die('1');
    }

    public function load_gradessummary() {
        global $PAGE, $CFG, $USER, $DB, $OUTPUT;
        $grades = '';

        $hidden_filter = ' AND cm.visible = 1 ';
        $time_order = 'gf.timemodified ASC';
        $cm_order = 'ORDER BY '.$time_order;
        $where = '';

        $context = context_course::instance($this->course->id, MUST_EXIST);
        $can_edit = false;
        if (has_capability('moodle/course:manageactivities', $context)){
            $can_edit = true;
        }

        $grades = '<div class="gradesummary-box">';

        if ($can_edit){
            $users = $DB->get_records_sql("SELECT u.*, ccc.gradepass, cmc.cmcnums, ci.id as compl_enabled, ue.timecreated as enrolled, gc.avarage, cc.timecompleted as complete, c.id as cid, c.fullname as course, c.timemodified as start_date
                                    FROM mdl_user_enrolments as ue
                                        LEFT JOIN {user} as u ON u.id = ue.userid
                                        LEFT JOIN {enrol} as e ON e.id = ue.enrolid
                                        LEFT JOIN {course} as c ON c.id = e.courseid
                                        LEFT JOIN {context} as cx ON cx.instanceid = e.courseid AND cx.contextlevel = 50
                                        LEFT JOIN {role_assignments} ra ON ra.contextid = cx.id AND ra.userid = ue.userid
                                        LEFT JOIN {course_completions} as cc ON cc.course = e.courseid
                                        LEFT JOIN {course_completion_criteria} as ccc ON ccc.course = e.courseid AND ccc.criteriatype = 6
                                        LEFT JOIN (SELECT * FROM {course_completion_criteria} WHERE id > 0 GROUP BY course) as ci ON ci.course = e.courseid
                                        LEFT JOIN (SELECT cm.course, cmc.userid, count(cmc.id) as cmcnums FROM {course_modules} cm, {course_modules_completion} cmc WHERE cmc.coursemoduleid = cm.id $hidden_filter AND cm.completion > 0 AND cm.section > 0 GROUP BY cm.course, cmc.userid) as cmc ON cmc.course = c.id AND cmc.userid = u.id
                                        LEFT JOIN (SELECT gi.courseid, g.userid, AVG( (g.finalgrade/g.rawgrademax)*100 ) AS avarage FROM {grade_items} gi LEFT JOIN {grade_grades} g ON g.itemid = gi.id LEFT JOIN {course_modules} cm ON cm.instance = gi.iteminstance LEFT JOIN {modules} m ON m.id = cm.module WHERE gi.itemname != '' AND g.finalgrade IS NOT NULL AND cm.course = gi.courseid AND cm.section > 0 AND gi.itemmodule = m.name $hidden_filter GROUP BY gi.courseid, g.userid) as gc ON gc.courseid = c.id AND gc.userid = u.id
                                            WHERE (ra.roleid = 5 OR ra.roleid IS NULL) AND e.courseid = :courseid and u.id > 0 AND u.deleted = 0 $where GROUP BY ue.userid, e.courseid", array('courseid'=>$this->course->id));
            $grades .= '<div class="clearfix">';

            $grades .= '<div class="info-legeng clearfix">
                            <p><span class="current-grade"></span>'.get_string('currentgrade', 'format_isf').'</p>
                            <p><span class="avarage"></span>'.get_string('classavg', 'format_isf').'</p>
                            <p><span class="goal"></span>'.get_string('goalgrade', 'format_isf').'</p>
                        </div></div>';
            $grades .= '<table width="100%" class="generaltable grade-summary">';
            $grades .= '<thead>
                        <tr>
                            <th align="left" class="active asc"><span>'.get_string('student', 'format_isf').'</span></th>
                            <th align="left"><span>'.get_string('progress', 'format_isf').'</span></th>
                            <th class="left" style="text-align:left;"><span>'.get_string('letter', 'format_isf').'</span></th>
                            <th class="left" style="text-align:left;"><span>'.get_string('completed', 'format_isf').'</span></th>
                            <th class="left" style="text-align:left;"><span>'.get_string('score', 'format_isf').'</span></th>
                            <th class="left" style="text-align:left;"><span>'.get_string('details', 'format_isf').'</span></th>
                        </tr>
                        </thead>';
            $grades .= '<tbody>';

            $users_avg = array();
            foreach($users as $item) {
                if($this->course->id == $item->cid) {
                    $users_avg[$item->id] = ($item->avarage > 0) ? round($item->avarage, 0) : '-';
                }
            }

            $sum = 0; $all = 0;
            foreach($users as $item) {
                if($this->course->id == $item->cid) {
                    if($item->avarage) {
                        $sum = (int)$item->avarage + $sum;
                        $all++;
                    }
                }
            }
            $avarage = ($sum and $all) ? $sum/$all : 0;

            foreach($users as $item) {
                $grades .= '<tr>
                    <td>
                        <div class="clearfix gradebook-username">
                            <span class="user-pic">'.$OUTPUT->user_picture($item, array('size'=>30)).'</span><a target="_blank" href="'.$CFG->wwwroot.'/user/profile.php?id=' . $item->id .' ">' . $item->lastname .', '. $item->firstname .'</a>
                        </div>
                    </td>
                    <td class="left">
                        <div class="info-progress">
                            <span class="current-grade" style="width:' . ((int)$users_avg[$item->id]) .'%"></span>
                            <span class="avarage" style="width:' .( (int)$avarage) .'%"></span>
                            <span class="goal" style="width:' . ((int)$item->gradepass) .'%"></span>
                        </div>
                        <div class="info-hidden">'.get_string('currentgrade', 'format_isf').': ' . ((int)$users_avg[$item->id]) .'% | '.get_string('goalgrade', 'format_isf').' ' . ( (int)$avarage) .'% | '.get_string('classavg', 'format_isf').': ' .  ((int)$item->gradepass) .'%</div>
                    </td>
                    <td class="left" style="text-align:left;">' .  $this->get_grade_letter($users_avg[$item->id]) .'</td>
                    <td class="left" style="text-align:left;">' .  (((int)$item->cmcnums > 1) ? (int)$item->cmcnums.' '.get_string('activities', 'format_isf') : (int)$item->cmcnums.' '.get_string('oneactivity', 'format_isf')) .'</td>
                    <td class="left info-avarage" data-value="'.(($users_avg[$item->id] != '-') ? $users_avg[$item->id] : 0).'" style="text-align:left;" id="avarage' . $item->id .'">' .  $users_avg[$item->id] .'</td>
                    <td class="left"><button class="btn btn-primary viewdetails" value="' . $item->id .'"><i class="fa fa-angle-down"></i></button></td>
                </tr>';
            }
            $grades .= '</tbody>';
            $grades .= '</table>';
            $can_see_gradebook = true;
        }else{
            $rows = $DB->get_records_sql("SELECT cm.id, gg.rawgrademax, gg.rawgrademin, cm.course, gf.id AS itemid, (gg.finalgrade / gg.rawgrademax) *100 AS avarage, cm.instance, m.name AS module, cmc.completionstate, cmc.timemodified AS completiondate, gc.fullname as category, gf.categoryid
                FROM mdl_course_modules cm
                    LEFT JOIN mdl_modules m ON m.id = cm.module
                    LEFT JOIN mdl_user AS u ON u.id = :userid
                    LEFT JOIN mdl_grade_items AS gf ON gf.iteminstance = cm.instance
                    LEFT JOIN mdl_grade_categories AS gc ON gc.id = gf.categoryid
                    LEFT JOIN mdl_grade_grades AS gg ON gg.itemid = gf.id AND gg.userid = u.id
                    LEFT JOIN mdl_course_modules_completion AS cmc ON cmc.coursemoduleid = cm.id AND cmc.userid = u.id
                WHERE cm.course = :courseid AND gf.courseid = cm.course AND gf.itemmodule = m.name AND cm.section > 0 $hidden_filter ORDER BY gf.timemodified DESC", array('userid' => $USER->id, 'courseid' => $this->course->id));

            $grades .= '<table class="generaltable grade-summary">';
            $grades .= '<thead>';
            $grades .= '<th class="left">'.get_string('activity', 'format_isf').'</th>';
            $grades .= '<th class="left">'.get_string('type', 'format_isf').'</th>';
            $grades .= '<th class="left" style="text-align:left;">'.get_string('grade').'</th>';
            $grades .= '<th class="left" style="text-align:left;">'.get_string('status').'</th>';
            $grades .= '<th class="left">'.get_string('completedon', 'format_isf').'</th>';
            $grades .= '</thead>';
            $grades .= '<tbody>';

            $categories = array();
            $cat_usergrades = array();
            $cat_usergrades_num = array();
            foreach($rows as $item){
                $categories[$item->categoryid] = $item->category;
                if ($item->avarage > 0){
                    if (isset($cat_usergrades_num[$item->categoryid])){
                        $cat_usergrades[$item->categoryid] += $item->avarage;
                        $cat_usergrades_num[$item->categoryid]++;
                    } else {
                        $cat_usergrades[$item->categoryid] = $item->avarage;
                        $cat_usergrades_num[$item->categoryid] = 1;
                    }
                }
                $ins = $DB->get_record_sql("SELECT ins.name FROM mdl_$item->module ins WHERE ins.id = $item->instance");

                $grades .= '<tr>';
                $html .= '<td><a href="'.$CFG->wwwroot.'/mod/'.$item->module.'/view.php?id='.$item->id.'">'.$ins->name.'</td>';
                $grades .= '<td>'.ucfirst(get_string('pluginname', 'mod_'.$item->module)).'</td>';
                $grades .= '<td class="left" data-value="'.(($item->avarage > 0) ? round($item->avarage, 0) : 0).'">'.(($item->avarage > 0) ? round($item->avarage, 0) : '-').'</td>';
                $grades .= '<td class="left">
                                '.(($item->completionstate == 0) ? get_string('incomplete', 'format_isf') : '') .'
                                '.(($item->completionstate == 1) ? get_string('completed', 'format_isf') : '') .'
                                '.(($item->completionstate == 2) ? get_string('passed', 'format_isf') : '') .'
                                '.(($item->completionstate == 3) ? get_string('failed', 'format_isf') : '') .'
                            </td>';
                $grades .= '<td class="left date">'. (($item->completionstate == 1 or $item->completionstate == 2) ? userdate($item->completiondate, '%b, %d %Y, %I:%M %p') : '-') .'</td>';
                $grades .= '</tr>';
            }

            $grades .= '</tbody>';
            $grades .= '</table>';

            $grades .= '';
            $can_see_gradebook = false;
        }
        $grades .= '</div>';

        echo $grades;
    }

    public function get_gs_activity() {
        global $DB, $CFG, $USER;

        $userid        = optional_param('userid', 0, PARAM_INT);
        $course = $this->course;

        $rows = $DB->get_records_sql("SELECT cm.id, gg.rawgrademax, gg.rawgrademin, cm.course, gf.id AS itemid, (gg.finalgrade / gg.rawgrademax) *100 AS avarage, gg.finalgrade, cm.instance, m.name AS module, cmc.completionstate, cmc.timemodified AS completiondate
            FROM mdl_course_modules cm
                LEFT JOIN mdl_modules m ON m.id = cm.module
                LEFT JOIN mdl_user AS u ON u.id = :userid
                LEFT JOIN mdl_grade_items AS gf ON gf.iteminstance = cm.instance
                LEFT JOIN mdl_grade_grades AS gg ON gg.itemid = gf.id AND gg.userid = u.id
                LEFT JOIN mdl_course_modules_completion AS cmc ON cmc.coursemoduleid = cm.id AND cmc.userid = u.id
            WHERE cm.course = :courseid AND gf.courseid = cm.course AND gf.itemmodule = m.name AND cm.section > 0 AND cm.visible = 1 ORDER BY gf.timemodified DESC",
                                    array('userid' => $userid, 'courseid' => $course->id));

        $html = '';
        if($rows){
            $html .= '<table class="generaltable grade-summary-details" width="100">';
            $html .= '<thead>';
            $html .= '<th class="left">'.get_string('activity', 'format_isf').'</th>';
            $html .= '<th class="left">'.get_string('type', 'format_isf').'</th>';
            $html .= '<th class="left" style="text-align:left;">'.get_string('grade').'</th>';
            $html .= '<th class="left" style="text-align:left;">'.get_string('status').'</th>';
            $html .= '<th class="left">'.get_string('completedon', 'format_isf').'</th>';
            $html .= '</thead>';
            $html .= '<tbody>';

            foreach($rows as $item){
                $ins = $DB->get_record_sql("SELECT ins.name FROM mdl_$item->module ins WHERE ins.id = $item->instance");

                $html .= '<tr>';
                $html .= '<td><a href="'.$CFG->wwwroot.'/mod/'.$item->module.'/view.php?id='.$item->id.'">'.$ins->name.'</td>';
                $html .= '<td>'.ucfirst(get_string('pluginname', 'mod_'.$item->module)).'</td>';
                $html .= '<td class="center" data-value="'.(($item->avarage > 0) ? round($item->avarage, 0) : 0).'">'.(($item->avarage > 0) ? round($item->avarage, 0) : '-').'</td>';
                $html .= '<td class="left">
                                '.(($item->completionstate == 0) ? get_string('incomplete', 'format_isf') : '') .'
                                '.(($item->completionstate == 1) ? get_string('completed', 'format_isf') : '') .'
                                '.(($item->completionstate == 2) ? get_string('passed', 'format_isf') : '') .'
                                '.(($item->completionstate == 3) ? get_string('failed', 'format_isf') : '') .'
                </td>';
                $html .= '<td class="left date">'. (($item->completionstate == 1 or $item->completionstate == 2) ? userdate($item->completiondate, '%b, %d %Y, %I:%M %p') : '-') .'</td>';
                $html .= '</tr>';
            }

            $html .= '</tbody>';
            $html .= '</table>';

        }else{
            $html = get_string('notfound', 'format_isf');
        }
        die($html);
    }


    public function get_grade_letter($sum){
        $letter = '';
        if($sum >= 93){
            $letter = "A";
        }elseif($sum >= 90){
            $letter = "A-";
        }elseif($sum >= 87){
            $letter = "B+";
        }elseif($sum >= 83){
            $letter = "B";
        }elseif($sum >= 80){
            $letter = "B-";
        }elseif($sum >= 77){
            $letter = "C+";
        }elseif($sum >= 73){
            $letter = "C";
        }elseif($sum >= 70){
            $letter = "C-";
        }elseif($sum >= 67){
            $letter = "D+";
        }elseif($sum >= 63){
            $letter = "D";
        }elseif($sum >= 60){
            $letter = "D-";
        }elseif($sum < 60 and $sum > 0){
            $letter = "F";
        } else {
            $letter = "-";
        }
        return $letter;
    }

}




