<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Displays information about all the assignment modules in the requested course
 *
 * @package   local_quicklinks
 * @copyright 2017 ISF
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once("../../config.php");
require_once("lib.php");
require_once("locallib.php");
require_once("classes/forms.php");

require_login();

$id         = optional_param('id', 0, PARAM_INT);
$courseid   = optional_param('courseid', 0, PARAM_INT);
$action     = optional_param('action', '', PARAM_NOTAGS);
$confirm    = optional_param('confirm', 0, PARAM_BOOL);

$course = null;
$category = null;
$params = array();

// check params
if ($courseid) {
    $course = $DB->get_record('course', array('id'=>$courseid));
}
if ($id) {
    $category = $DB->get_record('local_quicklinks_categories', array('id'=>$id));
    if (isset($category->courseid) and $category->courseid > 0 and $category->type == LOCAL_QUICKLINKS_COURSETYPE) {
        $course = $DB->get_record('course', array('id'=>$courseid));
    }

    $title = get_string('updatecategory', 'local_quicklinks');
} else {
    $title = get_string('createnewcategory', 'local_quicklinks');
}

if (isset($course->id)){
    require_login($course);
    $context = context_course::instance($course->id);
    $params['courseid'] = $course->id;
} else {
    $context = context_system::instance();
}

// user need to has permissions to edit category
require_capability('local/quicklinks:editcategory', $context);
$PAGE->set_context($context);
$PAGE->set_url(new moodle_url("/local/quicklinks/editcategory.php", $params));

// categories actions
if ($action == 'show' and $id) {
    $category->state = 1;
    local_quicklinks_update_category($category, $context);
    redirect(new moodle_url("/local/quicklinks/index.php", $params), get_string('categoryupdated', 'local_quicklinks'));
} else if ($action == 'hide' and $id) {
    $category->state = 0;
    local_quicklinks_update_category($category, $context);
    redirect(new moodle_url("/local/quicklinks/index.php", $params), get_string('categoryupdated', 'local_quicklinks'));
} else if ($action == 'delete' and $id) {
    if ($confirm and confirm_sesskey()) {
        local_quicklinks_delete_category($category);
        redirect(new moodle_url("/local/quicklinks/index.php", $params), get_string('categorydeleted', 'local_quicklinks'));
    }
    $strheading = get_string('deletecategory', 'local_quicklinks');
    $PAGE->navbar->add(get_string('pluginname', 'local_quicklinks'), new moodle_url('/local/quicklinks/index.php', $params));
    $PAGE->navbar->add($strheading);
    $PAGE->set_title($strheading);
    $PAGE->set_heading($strheading);

    echo $OUTPUT->header();
    echo $OUTPUT->heading($strheading);
    $yesurl = new moodle_url($CFG->wwwroot.'/local/quicklinks/editcategory.php', array('id' => $category->id, 'action' => 'delete', 'confirm' => 1, 'sesskey' => sesskey(), 'returnurl' => new moodle_url('/local/quicklinks/index.php', $params)));
    $message = get_string('confirmcategorydelete', 'local_quicklinks', format_string($category->name));
    echo $OUTPUT->confirm($message, $yesurl, new moodle_url('/local/quicklinks/index.php', $params));
    echo $OUTPUT->footer();
    die;
}

// set moodle required params
$PAGE->navbar->add(get_string('pluginname', 'local_quicklinks'), new moodle_url('/local/quicklinks/index.php', $params));
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_pagelayout((isset($course->id)) ? 'course' : 'standard');
$PAGE->set_heading($title);

// create form
$editform = new category_edit_form(NULL, array('data'=>$category, 'context'=>$context));

// process form data
if ($editform->is_cancelled()) {
    redirect(new moodle_url("/local/quicklinks/index.php", $params));
} else if ($data = $editform->get_data()) {

    if ($data->id > 0) {
        local_quicklinks_update_category($data, $context);
        redirect(new moodle_url("/local/quicklinks/index.php", $params), get_string('categoryupdated', 'local_quicklinks'));
    } else {
        $data->id = local_quicklinks_insert_category($data, $context);
        redirect(new moodle_url("/local/quicklinks/view.php", array('cid'=>$data->id)), get_string('categoryupdated', 'local_quicklinks'));
    }
}

local_quicklinks_get_last_category($context);

// load js
$PAGE->requires->js_call_amd('local_quicklinks/qlinks', 'editcategory', array('coursetype'=>LOCAL_QUICKLINKS_COURSETYPE));

// set plugin renderer
$renderer = $PAGE->get_renderer('local_quicklinks');

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo $renderer->startbox('quicklinks-categories');

// display form
$editform->display();

echo $renderer->endbox();

echo $OUTPUT->footer();
