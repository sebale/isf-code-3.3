<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Displays information about all the assignment modules in the requested course
 *
 * @package   local_quicklinks
 * @copyright 2017 ISF
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once("../../config.php");
require_once("lib.php");
require_once("locallib.php");
require_once("classes/forms.php");

require_login();

$id         = optional_param('id', 0, PARAM_INT);
$cid        = required_param('cid', PARAM_INT);
$courseid   = optional_param('courseid', 0, PARAM_INT);
$action     = optional_param('action', '', PARAM_NOTAGS);
$confirm    = optional_param('confirm', 0, PARAM_BOOL);

$category = $course = $qlink = null;
$params = array('cid'=>$cid);

// check params
if ($courseid) {
    $course = $DB->get_record('course', array('id'=>$courseid));
}
if ($cid) {
    $category = $DB->get_record('local_quicklinks_categories', array('id'=>$cid));
    if (isset($category->courseid) and $category->courseid > 0 and $category->type == LOCAL_QUICKLINKS_COURSETYPE) {
        $course = $DB->get_record('course', array('id'=>$courseid));
    }
}

if ($id) {
    $qlink = $DB->get_record('local_quicklinks', array('id'=>$id));
    $title = get_string('updateqlink', 'local_quicklinks');
} else {
    $title = get_string('createnewlink', 'local_quicklinks');
}

if (isset($course->id)){
    require_login($course);
    $context = context_course::instance($course->id);
    $params['courseid'] = $course->id;
} else {
    $context = context_system::instance();
}

// user need to has permissions to edit category
require_capability('local/quicklinks:editquicklink', $context);
$PAGE->set_context($context);
$PAGE->set_url(new moodle_url("/local/quicklinks/editquicklink.php", $params));

// categories actions
if ($action == 'show' and $id) {
    $qlink->state = 1;
    local_quicklinks_update($qlink);
    redirect(new moodle_url("/local/quicklinks/view.php", $params), get_string('qlinkupdated', 'local_quicklinks'));
} else if ($action == 'hide' and $id) {
    $qlink->state = 0;
    local_quicklinks_update($qlink);
    redirect(new moodle_url("/local/quicklinks/view.php", $params), get_string('qlinkupdated', 'local_quicklinks'));
} else if ($action == 'delete' and $id) {
    if ($confirm and confirm_sesskey()) {
        local_quicklinks_delete($qlink);
        redirect(new moodle_url("/local/quicklinks/view.php", $params), get_string('qlinkdeleted', 'local_quicklinks'));
    }
    $strheading = get_string('deleteqlink', 'local_quicklinks');
    $PAGE->navbar->add(get_string('pluginname', 'local_quicklinks'), new moodle_url('/local/quicklinks/index.php', $params));
    $PAGE->navbar->add($category->name, new moodle_url('/local/quicklinks/view.php', $params));
    $PAGE->navbar->add($strheading);
    $PAGE->set_title($strheading);
    $PAGE->set_heading($strheading);

    echo $OUTPUT->header();
    echo $OUTPUT->heading($strheading);
    $yesurl = new moodle_url($CFG->wwwroot.'/local/quicklinks/editquicklink.php', array('id' => $qlink->id, 'cid' => $cid, 'action' => 'delete', 'confirm' => 1, 'sesskey' => sesskey(), 'returnurl' => new moodle_url('/local/quicklinks/index.php', $params)));
    $message = get_string('confirmcdeleteqlink', 'local_quicklinks', format_string($qlink->title));
    echo $OUTPUT->confirm($message, $yesurl, new moodle_url('/local/quicklinks/view.php', $params));
    echo $OUTPUT->footer();
    die;
}

// set moodle required params
$PAGE->navbar->add(get_string('pluginname', 'local_quicklinks'), new moodle_url('/local/quicklinks/index.php', $params));
$PAGE->navbar->add($category->name, new moodle_url('/local/quicklinks/view.php', $params));
$PAGE->navbar->add($title);
$PAGE->set_title($title);
$PAGE->set_pagelayout((isset($course->id)) ? 'course' : 'standard');
$PAGE->set_heading($title);

// create form
$imagefilesoptions = array(
    'maxfiles' => 1,
    'maxbytes' => $CFG->maxbytes,
    'subdirs' => 0,
    'accepted_types' => 'image',
    'context' => $context
);

if (isset($qlink->id)){
    $qlink = file_prepare_standard_filemanager($qlink, 'image', $imagefilesoptions, $context,
                                           'local_quicklinks', 'image', $qlink->id);
}

$editform = new quicklink_edit_form(null, array('data'=>$qlink, 'category'=>$category, 'context'=>$context, 'imagefilesoptions'=>$imagefilesoptions));

// process form data
if ($editform->is_cancelled()) {
    redirect(new moodle_url("/local/quicklinks/view.php", $params));
} else if ($data = $editform->get_data()) {

    if ($data->id > 0) {
        local_quicklinks_update($data);
        file_save_draft_area_files($data->image_filemanager, $context->id, 'local_quicklinks', 'image',
                   $data->id, $imagefilesoptions);
        redirect(new moodle_url("/local/quicklinks/view.php", $params), get_string('qlinkupdated', 'local_quicklinks'));
    } else {
        $data->id = local_quicklinks_insert($data);
        file_save_draft_area_files($data->image_filemanager, $context->id, 'local_quicklinks', 'image',
                   $data->id, $imagefilesoptions);
        redirect(new moodle_url("/local/quicklinks/view.php", array('cid'=>$cid)), get_string('qlinkcreated', 'local_quicklinks'));
    }
}

local_quicklinks_get_last_category($context);

// set plugin renderer
$renderer = $PAGE->get_renderer('local_quicklinks');

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo $renderer->startbox('quicklinks-list');

// display form
$editform->display();

echo $renderer->endbox();

echo $OUTPUT->footer();
