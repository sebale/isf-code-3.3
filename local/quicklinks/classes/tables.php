<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Displays information about all the assignment modules in the requested course
 *
 * @package   local_quicklinks
 * @copyright 2017 ISF
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class categories_table extends table_sql {

    protected $_types = null;
    protected $_context = null;

    function __construct($uniqueid, $context, $search, $courseid = 0) {
        global $CFG, $USER;

        parent::__construct($uniqueid);
        $this->_types = local_quicklinks_get_category_types();
        $this->_context = $context;

        $columns = array('id', 'sortorder', 'name', 'creator', 'type', 'course', 'links');
        $headers = array(
            'ID',
            '#',
            get_string('title', 'local_quicklinks'),
            get_string('creator', 'local_quicklinks'),
            get_string('type', 'local_quicklinks'),
            get_string('course', 'local_quicklinks'),
            get_string('links', 'local_quicklinks')
        );

        $this->no_sorting('name');
        $this->no_sorting('creator');
        $this->no_sorting('type');
        $this->no_sorting('course');
        $this->no_sorting('links');

        if (has_capability('local/quicklinks:editcategory', $context)){
            $columns[] = 'actions';
            $headers[] = get_string('actions', 'local_quicklinks');

            $this->no_sorting('actions');
        }

        $this->define_columns($columns);
        $this->define_headers($headers);

        $sql_search = ($search) ? " AND (qlc.name LIKE '%$search%' OR c.fullname LIKE '%$search%' OR u.firstname LIKE '%$search%' OR u.lastname LIKE '%$search%')" : "";

        $fields = "qlc.*, c.fullname as course, CONCAT(u.firstname, ' ', u.lastname) as creator, l.links";
        $from = "{local_quicklinks_categories} qlc
                    LEFT JOIN {course} c ON c.id = qlc.courseid
                    LEFT JOIN {user} u ON u.id = qlc.userid AND u.deleted = 0
                    LEFT JOIN (SELECT COUNT(id) as links, category FROM {local_quicklinks} GROUP BY category) l ON l.category = qlc.id";
        $where = "qlc.id > 0".$sql_search;

        if ($courseid > 0){
            $where .= " AND qlc.courseid = $courseid";
        }

        $types = array();
        $params = array();

        if (has_capability('local/quicklinks:editsystemtype', $context)){
            $types[] = " qlc.type = '".LOCAL_QUICKLINKS_SYSTEMTYPE."' ";
        }
        if (has_capability('local/quicklinks:editprivatetype', $context)){
            $types[] = " (qlc.type = '".LOCAL_QUICKLINKS_PRIVATETYPE."' AND qlc.userid = :userid) ";
            $params['userid'] = $USER->id;
        }
        if (has_capability('local/quicklinks:editcoursetype', $context)){
            $courses = 0;
            $mycourses = local_quicklinks_get_courses($context);
            if (count($mycourses)){
                $coursekeys = array_keys($mycourses);
                $courses = implode(',', $coursekeys);
            }
            $types[] = " (qlc.type = '".LOCAL_QUICKLINKS_COURSETYPE."' AND qlc.courseid IN (:courses)) ";
            $params['courses'] = $courses;
        }
        if (count($types)){
            $where .= " AND (".implode(' OR ', $types).")";
        }

        $this->set_sql($fields, $from, $where, $params);
        $this->define_baseurl("$CFG->wwwroot/local/quicklinks/index.php?courseid=".$courseid."&search=".$search);
    }

    function col_name($values) {
        global $CFG;

        return html_writer::link(new moodle_url($CFG->wwwroot.'/local/quicklinks/view.php', array('cid'=>$values->id)), $values->name, array('title' => $values->name));
    }

    function col_creator($values) {
        global $CFG;

        return html_writer::link(new moodle_url($CFG->wwwroot.'/user/profile.php', array('id'=>$values->userid)), $values->creator, array('title' => $values->creator));
    }

    function col_type($values) {
        return $this->_types[$values->type];
    }

    function col_course($values) {
        global $CFG;

        if ($values->courseid > 0 and $values->course){
            return html_writer::link(new moodle_url($CFG->wwwroot.'/course/view.php', array('id'=>$values->courseid)), $values->course, array('title' => $values->course));
        }
        return '';
    }

    function col_links($values) {
        return ($values->links) ? $values->links : 0;
    }

    function col_actions($values) {
        global $CFG, $OUTPUT, $USER;

        $buttons = array();

        if (!has_capability('local/quicklinks:editcategory', $this->_context)){
            return '';
        }

        if ($values->type == LOCAL_QUICKLINKS_SYSTEMTYPE and !has_capability('local/quicklinks:editsystemtype', $this->_context)){
            return '';
        }
        if ($values->type == LOCAL_QUICKLINKS_COURSETYPE and !has_capability('local/quicklinks:editcoursetype', $this->_context)){
            return '';
        }
        if ($values->type == LOCAL_QUICKLINKS_PRIVATETYPE and !has_capability('local/quicklinks:editsystemtype', $this->_context)){
            return '';
        }

        $urlparams = array('id' => $values->id);

        // edit link
        $buttons[] = html_writer::link(
            new moodle_url($CFG->wwwroot.'/local/quicklinks/editcategory.php', $urlparams), html_writer::tag('i', '', array('class' => 'fa fa-pencil iconsmall', 'alt' => get_string('edit'))),
            array('title' => get_string('edit'))
        );

        // delete link
        $buttons[] = html_writer::link(
            new moodle_url($CFG->wwwroot.'/local/quicklinks/editcategory.php', $urlparams + array('action' => 'delete')),
            html_writer::tag('i', '', array('alt' => get_string('delete'), 'class' => 'iconsmall fa fa-trash')),
            array('title' => get_string('delete')));

        // show/hide

        if ($values->state > 0){
            $buttons[] = html_writer::link(
                new moodle_url($CFG->wwwroot.'/local/quicklinks/editcategory.php', $urlparams + array('action' => 'hide')),
                html_writer::tag('i', '', array('alt' => get_string('hide'), 'class' => 'iconsmall fa fa-eye')),
                array('title' => get_string('hide')));
        } else {
            $buttons[] = html_writer::link(
                new moodle_url($CFG->wwwroot.'/local/quicklinks/editcategory.php', $urlparams + array('action' => 'show')),
                html_writer::tag('i', '', array('alt' => get_string('show'), 'class' => 'fa fa-eye-slash iconsmall')),
                array('title' => get_string('show')));
        }

        $buttons[] = html_writer::link(
                'javascript:void(0)',
                html_writer::tag('i', '', array('alt' => get_string('move'), 'class' => 'fa fa-arrows iconsmall move-action')),
                array('title' => get_string('move')));

        return implode(' ', $buttons);
    }

    function print_row($row, $classname = '') {
        echo $this->get_row_html($row, $classname.(($classname != '') ? ' ' : '').((isset($row[0])) ? $row[0] : ''));
    }
}


class quicklinks_table extends table_sql {

    protected $_context = null;

    function __construct($uniqueid, $context, $search = '', $category) {
        global $CFG, $USER;

        parent::__construct($uniqueid);
        $this->_context = $context;

        $columns = array('id', 'sortorder', 'title', 'creator');
        $headers = array(
            'ID',
            '#',
            get_string('title', 'local_quicklinks'),
            get_string('creator', 'local_quicklinks')
        );

        $this->no_sorting('title');
        $this->no_sorting('creator');

        if (has_capability('local/quicklinks:editquicklink', $context)){
            $columns[] = 'actions';
            $headers[] = get_string('actions', 'local_quicklinks');

            $this->no_sorting('actions');
        }

        $this->define_columns($columns);
        $this->define_headers($headers);

        $sql_search = ($search) ? " AND (ql.title LIKE '%$search%' OR u.firstname LIKE '%$search%' OR u.lastname LIKE '%$search%')" : "";

        $fields = "ql.*, CONCAT(u.firstname, ' ', u.lastname) as creator";
        $from = "{local_quicklinks} ql
                    LEFT JOIN {user} u ON u.id = ql.userid AND u.deleted = 0";
        $where = "ql.id > 0 AND category = :category ".$sql_search;

        $this->set_sql($fields, $from, $where, array('category'=>$category->id));
        $this->define_baseurl("$CFG->wwwroot/local/quicklinks/view.php?cid=".$category->id."&search=".$search);
    }

    function col_creator($values) {
        global $CFG;

        return html_writer::link(new moodle_url($CFG->wwwroot.'/user/profile.php', array('id'=>$values->userid)), $values->creator, array('title' => $values->creator));
    }

    function col_actions($values) {
        global $CFG, $OUTPUT, $USER;

        $buttons = array();

        if (!has_capability('local/quicklinks:editquicklink', $this->_context)){
            return '';
        }

        $urlparams = array('cid'=>$values->category, 'id' => $values->id);

        // edit link
        $buttons[] = html_writer::link(
            new moodle_url($CFG->wwwroot.'/local/quicklinks/editquicklink.php', $urlparams), html_writer::tag('i', '', array('alt' => get_string('edit'), 'class' => 'fa fa-pencil iconsmall')),
            array('title' => get_string('edit'))
        );

        // delete link
        $buttons[] = html_writer::link(
            new moodle_url($CFG->wwwroot.'/local/quicklinks/editquicklink.php', $urlparams + array('action' => 'delete')),
            html_writer::tag('i', '', array('alt' => get_string('delete'), 'class' => 'fa fa-trash iconsmall')),
            array('title' => get_string('delete')));

        // show/hide

        if ($values->state > 0){
            $buttons[] = html_writer::link(
                new moodle_url($CFG->wwwroot.'/local/quicklinks/editquicklink.php', $urlparams + array('action' => 'hide')),
                html_writer::tag('i', '', array('alt' => get_string('hide'), 'class' => 'fa fa-eye iconsmall')),
                array('title' => get_string('hide')));
        } else {
            $buttons[] = html_writer::link(
                new moodle_url($CFG->wwwroot.'/local/quicklinks/editquicklink.php', $urlparams + array('action' => 'show')),
                html_writer::tag('i', '', array('alt' => get_string('show'), 'class' => 'fa fa-eye-slash iconsmall')),
                array('title' => get_string('show')));
        }

        $buttons[] = html_writer::link(
                'javascript:void(0)',
                html_writer::tag('i', '', array('alt' => get_string('move'), 'class' => 'fa fa-arrows iconsmall move-action')),
                array('title' => get_string('move')));

        return implode(' ', $buttons);
    }

    function print_row($row, $classname = '') {
        echo $this->get_row_html($row, $classname.(($classname != '') ? ' ' : '').((isset($row[0])) ? $row[0] : ''));
    }
}
