<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Displays information about all the assignment modules in the requested course
 *
 * @package   local_quicklinks
 * @copyright 2017 ISF
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/formslib.php');

/**
 * The form for handling editing a category.
 */
class category_edit_form extends moodleform {

    /**
     * Form definition.
     */
    function definition() {
        global $CFG, $PAGE, $USER;

        $mform    = $this->_form;

        $data     = $this->_customdata['data'];
        $context  = $this->_customdata['context'];

        $strrequired = get_string('requiredfield', 'local_quicklinks');

        $mform->addElement('hidden', 'id', 0);
        $mform->setType('id', PARAM_INT);

        $mform->addElement('text', 'name', get_string('title', 'local_quicklinks'));
        $mform->addRule('name', $strrequired, 'required', null, 'client');
        $mform->setType('name', PARAM_NOTAGS);

        $options = local_quicklinks_get_formtypes($context);
        $mform->addElement('select', 'type', get_string('type', 'local_quicklinks'), $options);
        $mform->addRule('type', $strrequired, 'required', null, 'client');
        $mform->setType('type', PARAM_INT);

        $courses = array(0=>get_string('selectcourse', 'local_quicklinks')) + local_quicklinks_get_courses($context);
        $options = array(
            'multiple' => false,
            'noselectionstring' => get_string('selectcourse', 'local_quicklinks'),
        );
        $mform->addElement('autocomplete', 'courseid', get_string('course', 'local_quicklinks'), $courses, $options);

        $this->add_action_buttons('true', ((isset($data->id) and $data->id) ? get_string('save', 'local_quicklinks') : get_string('create', 'local_quicklinks')));

        $this->set_data($data);
    }

}


/**
 * The form for handling editing a category.
 */
class quicklink_edit_form extends moodleform {

    /**
     * Form definition.
     */
    function definition() {
        global $CFG, $PAGE, $USER;

        $mform    = $this->_form;

        $data               = $this->_customdata['data'];
        $category           = $this->_customdata['category'];
        $context            = $this->_customdata['context'];
        $imagefilesoptions  = $this->_customdata['imagefilesoptions'];

        $strrequired = get_string('requiredfield', 'local_quicklinks');

        $mform->addElement('hidden', 'id', 0);
        $mform->setType('id', PARAM_INT);

        $mform->addElement('hidden', 'cid', $category->id);
        $mform->setType('cid', PARAM_INT);

        $mform->addElement('text', 'title', get_string('title', 'local_quicklinks'));
        $mform->addRule('title', $strrequired, 'required', null, 'client');
        $mform->setType('title', PARAM_NOTAGS);

        $mform->addElement('text', 'link', get_string('link', 'local_quicklinks'));
        $mform->addRule('link', $strrequired, 'required', null, 'client');
        $mform->setType('link', PARAM_NOTAGS);

        $mform->addElement('text', 'icon', get_string('selecticon', 'local_quicklinks'));
        $mform->setType('icon', PARAM_TEXT);

        $mform->addElement('html', '<div class="form-group row fitem"><div class="col-md-3"></div><div class="col-md-9 form-inline felement"><span class="course-res-info">'.get_string('fa_info', 'local_quicklinks').'</span></div></div>');

        $mform->addElement('filemanager', 'image_filemanager', get_string('optionalimage', 'local_quicklinks'), null, $imagefilesoptions);

        $this->add_action_buttons('true', ((isset($data->id) and $data->id) ? get_string('save', 'local_quicklinks') : get_string('create', 'local_quicklinks')));

        $this->set_data($data);
    }

}

