// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is an empty module, that is required before all other modules.
 * Because every module is returned from a request for any other module, this
 * forces the loading of all modules with a single request.
 *
 * This function also sets up the listeners for ajax requests so we can tell
 * if any requests are still in progress.
 *
 * @package    local_quicklinks
 * @module     local_quicklinks/qlinks
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define(['jquery', 'core/ajax', 'core/log', 'local_quicklinks/sortable'], function($, ajax, log, Sortable) {

    Sortable.init($, window, 'sortable', undefined);

    var QLinks = {
        
        methodName: 'local_quicklinks_save_cats_ordering',

        defaultProps: [],

        save: function(){
            log.debug('save new ordering');
            var list = [];
            $('.sorting > tbody > tr').each(function(){
                var id = $(this).attr('class');
                if(parseInt(id, 10) > 0){
                    list.push({
                        value: parseInt(id, 10)
                    });
                }
            });
            this.defaultProps.list = list;
            ajax.call([{
                methodname: QLinks.methodName, args: QLinks.defaultProps
            }])[0]
            .done(function(response) {
              try {
                response = JSON.parse(response);
                // maybe add here notifications
              } catch (Error){
                log.debug(Error.message);
              }
            }).fail(function(ex) {
              log.debug(ex.message);
            });
        },

        init: function(methodName, props) {
            this.methodName = methodName;
            this.defaultProps = typeof props === 'undefined' ? [] : props;
            var group = $('.sorting > tbody').sortable({
              containerSelector: '.sorting > tbody',
              itemSelector: 'tr',
              group: 'sort_1',
              handle: 'i.move-action',
              onDrop: function ($item, container, _super) {
                QLinks.save();
                _super($item, container);
              },
            });

        },

        // category edit
        editcategory: function(coursetype) {
            var type = $('#id_type').val();
            _editcategory_process_courses(type, coursetype);

            $('#id_type').change(function(){
                var type = $('#id_type').val();
                _editcategory_process_courses(type, coursetype);
            });
        },

    };

    function _editcategory_process_courses(type, coursetype){
        if (type == coursetype){
            $('#id_courseid').parents('.fitem').show();
        } else {
            $('#id_courseid').parents('.fitem').hide();
        }
    }

    /**
    * @alias module:local_quicklinks/qlinks
    */
    return QLinks;

});

