<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * External Web Service quicklinks plugin
 *
 * @package    local_quicklinks
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once($CFG->libdir . "/externallib.php");

class local_quicklinks_external extends external_api {

    /*-------------------SAVE CATEGORY ORDERING----------------------*/
    /**
    * Returns description of method parameters
    * @return external_function_parameters
    */
    public static function save_cats_ordering_parameters() {
        return new external_function_parameters(
            array(
                'list' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'value' => new external_value(PARAM_INT, 'category id')
                        )
                    )
                )
            )
        );
    }

    public static function save_cats_ordering($list) {

        global $DB;
        
        $params = self::validate_parameters(
            self::save_cats_ordering_parameters(),
            array(
                'list' => $list
            )
        );

        for($i=0, $c=count($list); $i<$c; $i++) {
            if($link = $DB->get_record('local_quicklinks_categories', array('id' => $list[$i]['value']))){
                $link->sortorder = $i;
                $DB->update_record('local_quicklinks_categories', $link);
            } else {
                return array(
                    'status' => 'can`t find category '. $list[$i]['value']
                );
            }
        }

        return array('status' => 'success');
    }

    /**
    * Returns description of method result value
    * @return external_description
    */
    public static function save_cats_ordering_returns() {
        return new external_single_structure(
            array (
                'status' => new external_value(PARAM_NOTAGS, 'Status of categories ordering update')
            )
        );
    }

}