<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * PowerSchool
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require('connections_classes.php');
require('locallib.php');

$id = optional_param('id',0, PARAM_INT);

require_login();
$context = context_system::instance();
require_capability('local/powerschool:manage', $context);

$heading = ($id>0)?get_string('edit_connection', 'local_powerschool'):get_string('create_connection', 'local_powerschool');

$PAGE->set_url(new moodle_url("/local/powerschool/edit-connection.php"));
$PAGE->set_pagelayout('admin');
$PAGE->set_context($context);
$PAGE->set_title(get_string('powerschoolroot', 'local_powerschool').': '.$heading);
$PAGE->set_heading(get_string('powerschoolroot', 'local_powerschool').': '.$heading);

$form = new edit_connection_form($PAGE->url,$id);
if ($data = $form->get_data()) {
    if(isset($data->id) && $record = $DB->get_record('powerschool_connections',array('id'=>$data->id))){
        if(empty($data->course_fullname_group))
            $data->course_fullname_group = "[[section_number]] / [[section_expression]] [[terms_abbreviation]]";
        if(empty($data->course_fullname))
            $data->course_fullname = "[[course_name]] [[section_number]] / [[section_expression]]";

        $record->name = $data->name;
        $record->url = trim($data->url,' /');
        $record->client_id = trim($data->client_id);
        $record->client_secret = trim($data->client_secret);
        $record->school_numbers = $data->school_numbers;
        $record->timecreate = time();
        $record->token_expires_in = 1;
        $record->school_ids = '';
        $record->access_token = '';
        $record->token_type = '';
        $record->server_primary_ip = '';
        $record->username_prefix = $data->username_prefix;
        $record->course_prefix = $data->course_prefix;
        $record->course_fullname = $data->course_fullname;
        $record->course_fullname_group = $data->course_fullname_group;

        $DB->update_record('powerschool_connections',$record);
    }else{
        if(empty($data->course_fullname_group))
            $data->course_fullname_group = "[[section_number]] / [[section_expression]] [[terms_abbreviation]]";
        if(empty($data->course_fullname))
            $data->course_fullname = "[[course_name]] [[section_number]] / [[section_expression]]";

        $record = new stdClass();
        $record->name = $data->name;
        $record->url = trim($data->url,' /');
        $record->client_id = trim($data->client_id);
        $record->client_secret = trim($data->client_secret);
        $record->school_numbers = $data->school_numbers;
        $record->timecreate = time();
        $record->username_prefix = $data->username_prefix;
        $record->course_prefix = $data->course_prefix;
        $record->course_fullname = $data->course_fullname;
        $record->course_fullname_group = $data->course_fullname_group;

        $record->id = $DB->insert_record('powerschool_connections',$record);
    }

    new PowerSchool($record->id);
    redirect(new moodle_url("/local/powerschool/manage-connections.php"));
}elseif($form->is_cancelled()){
    redirect(new moodle_url("/local/powerschool/manage-connections.php"));
}

echo $OUTPUT->header();
echo $OUTPUT->heading($heading);

$form->display();

echo $OUTPUT->footer();
