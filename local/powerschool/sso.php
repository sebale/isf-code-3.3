<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * PowerSchool Settings
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require('locallib.php');
require_once($CFG->dirroot.'/login/lib.php');

$openid_identifier = required_param('openid_identifier',PARAM_RAW);
$openid_identifier = explode('/',$openid_identifier);
$master_domen = implode('/',array($openid_identifier[0], $openid_identifier[1], $openid_identifier[2]));


$connection = $DB->get_record('powerschool_connections',array('url'=>$master_domen));
if(!isset($connection->id))
    redirect($CFG->wwwroot.'/login/');


$plugin = new PowerSchool($connection->id);

if(!isset($_SERVER['HTTP_REFERER']))
    redirect($CFG->wwwroot.'/login/');

$userlogin = $connection->username_prefix.array_pop($openid_identifier);
$referer = clean_param($_SERVER['HTTP_REFERER'],PARAM_RAW);
$master_domen = explode('.',$plugin->get_master_url(),2)[1];


if(strpos($referer,$master_domen) === false){
    redirect($CFG->wwwroot.'/login/');
}

$user = $DB->get_record('user',array('username'=>$userlogin));

if(isset($user->id)){
    complete_user_login($user);
    redirect($CFG->wwwroot);
}else{
    redirect($CFG->wwwroot.'/login/');
}
