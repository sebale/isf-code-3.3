<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * PowerSchool Settings
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

function xmldb_local_powerschool_upgrade($oldversion) {
    global $CFG, $DB;

    $dbman = $DB->get_manager();
    if($oldversion < 2016071800){
        $table = new xmldb_table('powerschool_assignment_cat');
        $field = new xmldb_field('grade_cat');
        $dbman->drop_field($table, $field);

        $field = new xmldb_field('section_id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $dbman->add_field($table, $field);
        $field = new xmldb_field('moodle_mod', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $dbman->add_field($table, $field);
        $field = new xmldb_field('timecreate', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $dbman->add_field($table, $field);

        $table = new xmldb_table('powerschool_course_fields');
        $field = new xmldb_field('gradebooktype', XMLDB_TYPE_INTEGER, '10', null, null, null, 1);
        $dbman->add_field($table, $field);
        $field = new xmldb_field('section_dcid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $dbman->add_field($table, $field);
        $field = new xmldb_field('section_id', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $dbman->add_field($table, $field);
        $field = new xmldb_field('term_id', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $dbman->add_field($table, $field);
    }
    if($oldversion < 2016072700){
        if(!$dbman->field_exists('powerschool_course_fields', 'gradescaleitem')){
            $table = new xmldb_table('powerschool_course_fields');
            $field = new xmldb_field('gradescaleitem', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
            $dbman->add_field($table, $field);
        }

        if(!$dbman->field_exists('powerschool_assignment', 'assignmentsectionid')){
            $table = new xmldb_table('powerschool_assignment');
            $field = new xmldb_field('assignmentsectionid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
            $dbman->add_field($table, $field);
        }
    }
    if($oldversion < 2016081000){
        $table = new xmldb_table('powerschool_course_fields');
        $field = new xmldb_field('section_wheretaught', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $dbman->change_field_type($table, $field);
        $field = new xmldb_field('section_wheretaughtdistrict', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $dbman->change_field_type($table, $field);

        $table = new xmldb_table('powerschool_grade_cat');
        $field = new xmldb_field('id', XMLDB_TYPE_INTEGER, '10',null,null,true,null);
        $table->addField($field);
        $field = new xmldb_field('section_id', XMLDB_TYPE_INTEGER, '10',null,null,null,null);
        $table->addField($field);
        $field = new xmldb_field('power_cat', XMLDB_TYPE_INTEGER, '10',null,null,null,null);
        $table->addField($field);
        $field = new xmldb_field('moodle_cat', XMLDB_TYPE_INTEGER, '10',null,null,null,null);
        $table->addField($field);
        $field = new xmldb_field('timecreate', XMLDB_TYPE_INTEGER, '10',null,null,null,null);
        $table->addField($field);

        $key = new xmldb_key('primary');
        $key->setType(XMLDB_KEY_PRIMARY);
        $key->setFields(array('id'));
        $key->setLoaded(true);
        $key->setChanged(true);
        $table->addKey($key);

        $dbman->create_table($table);
    }
    if($oldversion < 2016090500){
        $field = new xmldb_field('connection_id', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table = new xmldb_table('powerschool_course_fields');
        $dbman->add_field($table, $field);

        $field = new xmldb_field('courseid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table = new xmldb_table('powerschool_grade_cat');
        $dbman->add_field($table, $field);

        $field = new xmldb_field('section_id', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table = new xmldb_table('powerschool_assignment');
        $dbman->add_field($table, $field);

        $table = new xmldb_table('powerschool_connections');
        $field = new xmldb_field('id', XMLDB_TYPE_INTEGER, '10',null,null,true,null);
        $table->addField($field);
        $field = new xmldb_field('name', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->addField($field);
        $field = new xmldb_field('url', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->addField($field);
        $field = new xmldb_field('client_id', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->addField($field);
        $field = new xmldb_field('client_secret', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->addField($field);
        $field = new xmldb_field('school_numbers', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->addField($field);
        $field = new xmldb_field('course_cat', XMLDB_TYPE_INTEGER, '10',null,null,null,null);
        $table->addField($field);
        $field = new xmldb_field('school_ids', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->addField($field);
        $field = new xmldb_field('access_token', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->addField($field);
        $field = new xmldb_field('token_type', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->addField($field);
        $field = new xmldb_field('token_expires_in', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->addField($field);
        $field = new xmldb_field('server_primary_ip', XMLDB_TYPE_CHAR, '25', null, null, null, null);
        $table->addField($field);
        $field = new xmldb_field('sync_params', XMLDB_TYPE_TEXT, '255', null, null, null, null);
        $table->addField($field);
        $field = new xmldb_field('section_type', XMLDB_TYPE_CHAR, '50', null, null, null, null);
        $table->addField($field);
        $field = new xmldb_field('course_prefix', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->addField($field);
        $field = new xmldb_field('username_prefix', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $table->addField($field);
        $field = new xmldb_field('timecreate', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
        $table->addField($field);

        $key = new xmldb_key('primary');
        $key->setType(XMLDB_KEY_PRIMARY);
        $key->setFields(array('id'));
        $key->setLoaded(true);
        $key->setChanged(true);
        $table->addKey($key);

        $dbman->create_table($table);

        $sync_params = json_decode(get_config('local_powerschool', 'sync_params'));

        $first_connection = new stdClass();
        $first_connection->name = get_string('first_server','local_powerschool');
        $first_connection->url = get_config('local_powerschool', 'master_url');
        $first_connection->client_id = get_config('local_powerschool', 'client_id');
        $first_connection->client_secret = get_config('local_powerschool', 'client_secret');
        $first_connection->school_numbers = get_config('local_powerschool', 'school_numbers');
        $first_connection->course_cat = $DB->get_record_sql('SELECT id FROM {course_categories} WHERE depth=1 LIMIT 1')->id;
        $first_connection->school_ids = '';
        $first_connection->access_token = '';
        $first_connection->token_type = '';
        $first_connection->token_expires_in = '';
        $first_connection->server_primary_ip = '';
        $first_connection->sync_params = get_config('local_powerschool', 'sync_params');
        $first_connection->section_type = $sync_params->section_type;
        $first_connection->course_prefix = '';
        $first_connection->username_prefix = '';
        $first_connection->timecreate = time();
        $DB->insert_record('powerschool_connections',$first_connection);
    }
    if($oldversion < 2016110801){
        $table = new xmldb_table('powerschool_connections');
        $field = new xmldb_field('course_fullname', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $dbman->add_field($table, $field);

        $DB->execute('UPDATE {powerschool_connections} SET course_fullname="[[section_number]] / [[section_expression]] [[terms_abbreviation]]" WHERE section_type="groups" ');
        $DB->execute('UPDATE {powerschool_connections} SET course_fullname="[[course_name]] [[section_number]] / [[section_expression]]" WHERE section_type="course" ');
    }
    if($oldversion < 2016111400){
        $table = new xmldb_table('powerschool_connections');
        $field = new xmldb_field('course_fullname', XMLDB_TYPE_CHAR, '255', null, null, null, null);
        $dbman->change_field_type($table, $field);
    }
    if($oldversion < 2016112700){
        if(!$dbman->field_exists('powerschool_course_fields', 'course_type')){
            $table = new xmldb_table('powerschool_course_fields');
            $field = new xmldb_field('course_type', XMLDB_TYPE_CHAR, '255', null, null, null, null);
            $dbman->add_field($table, $field);
        }

        $connections = $DB->get_records('powerschool_connections');
        foreach($connections as $connection){
            $DB->execute('UPDATE {powerschool_course_fields} SET course_type=:course_type WHERE connection_id=:connection_id',array('course_type'=>$connection->section_type, 'connection_id'=>$connection->id));

            $connection->sync_params = json_decode($connection->sync_params);
            $sync_courses = (array)$connection->sync_params->sync_courses;
            $connection->sync_params->sync_courses = array();
            $synced_courses = $DB->get_records('powerschool_course_fields',array('connection_id'=>$connection->id),'','id,courseid,course_number');
            foreach($synced_courses as $course){
                $course_number = str_replace(' ', '', $course->course_number);
                $connection->sync_params->sync_courses[$course_number] = array('enable'=>((isset($sync_courses[$course_number]))?$sync_courses[$course_number]:0),'type'=>$connection->section_type);
            }
            $connection->sync_params = json_encode($connection->sync_params);

            $DB->update_record('powerschool_connections',$connection);
        }

        $table = new xmldb_table('powerschool_connections');
        $field = new xmldb_field('section_type');
        $dbman->drop_field($table, $field);

        if(!$dbman->field_exists('powerschool_connections', 'course_fullname_group')){
            $table = new xmldb_table('powerschool_connections');
            $field = new xmldb_field('course_fullname_group', XMLDB_TYPE_CHAR, '255', null, null, null, null);
            $dbman->add_field($table, $field);
        }

        $DB->execute('UPDATE {powerschool_connections} SET course_fullname_group="[[section_number]] / [[section_expression]] [[terms_abbreviation]]"');
        $DB->execute('UPDATE {powerschool_connections} SET course_fullname="[[course_name]] [[section_number]] / [[section_expression]]"');

        set_config('available_modules_to_create',implode(',', array_keys(get_module_types_names())),'local_powerschool');
    }
    if($oldversion < 2017021500){
        if(!$dbman->field_exists('powerschool_assignment_sync', 'standard_sync')){
            $table = new xmldb_table('powerschool_assignment_sync');
            $field = new xmldb_field('standard_sync', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
            $dbman->add_field($table, $field);
        }

        if(!$dbman->field_exists('powerschool_assignment_sync', 'count_in_final_grade')){
            $table = new xmldb_table('powerschool_assignment_sync');
            $field = new xmldb_field('count_in_final_grade', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
            $dbman->add_field($table, $field);
        }

        if(!$dbman->table_exists('powerschool_gradescale')){
            $table = new xmldb_table('powerschool_gradescale');
            $field = new xmldb_field('id', XMLDB_TYPE_INTEGER, '10', null, null, true, null);
            $table->addField($field);
            $field = new xmldb_field('ps_gradescale_id', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
            $table->addField($field);
            $field = new xmldb_field('mdl_gradescale_id', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
            $table->addField($field);

            $key = new xmldb_key('primary');
            $key->setType(XMLDB_KEY_PRIMARY);
            $key->setFields(array('id'));
            $key->setLoaded(true);
            $key->setChanged(true);
            $table->addKey($key);
            $dbman->create_table($table);
        }

        if(!$dbman->table_exists('powerschool_standard_assoc')){
            $table = new xmldb_table('powerschool_standard_assoc');
            $field = new xmldb_field('id', XMLDB_TYPE_INTEGER, '10', null, null, true, null);
            $table->addField($field);
            $field = new xmldb_field('assignmentid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
            $table->addField($field);
            $field = new xmldb_field('assignmentstandardassocid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
            $table->addField($field);
            $field = new xmldb_field('standardid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
            $table->addField($field);

            $key = new xmldb_key('primary');
            $key->setType(XMLDB_KEY_PRIMARY);
            $key->setFields(array('id'));
            $key->setLoaded(true);
            $key->setChanged(true);
            $table->addKey($key);
            $dbman->create_table($table);
        }

        if(!$dbman->table_exists('powerschool_standards')){
            $table = new xmldb_table('powerschool_standards');
            $field = new xmldb_field('id', XMLDB_TYPE_INTEGER, '10', null, null, true, null);
            $table->addField($field);
            $field = new xmldb_field('outcomeid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
            $table->addField($field);
            $field = new xmldb_field('standardid', XMLDB_TYPE_INTEGER, '10', null, null, null, null);
            $table->addField($field);

            $key = new xmldb_key('primary');
            $key->setType(XMLDB_KEY_PRIMARY);
            $key->setFields(array('id'));
            $key->setLoaded(true);
            $key->setChanged(true);
            $table->addKey($key);
            $dbman->create_table($table);
        }
    }
    if($oldversion < 2017071300){
        $table = new xmldb_table('powerschool_connections');
        $field = new xmldb_field('course_cat');
        $dbman->drop_field($table, $field);
    }

    return true;
}