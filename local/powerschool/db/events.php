<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * PowerSchool Settings
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$observers = array(
    array(
        'eventname'   => '\core\event\user_graded',
        'callback'    => 'events_handler::moodle_user_graded',
        'includefile' => '/local/powerschool/classes/Events.php'
    ),
    array(
        'eventname'   => 'core\event\user_updated',
        'callback'    => 'events_handler::moodle_user_updated',
        'includefile' => '/local/powerschool/classes/Events.php'
    ),
    array(
        'eventname'   => 'core\event\course_deleted',
        'callback'    => 'events_handler::moodle_course_deleted',
        'includefile' => '/local/powerschool/classes/Events.php'
    ),
    array(
        'eventname'   => 'core\event\course_module_created',
        'callback'    => 'events_handler::moodle_mod_created',
        'includefile' => '/local/powerschool/classes/Events.php'
    ),
    array(
        'eventname'   => 'core\event\course_module_updated',
        'callback'    => 'events_handler::moodle_mod_updated',
        'includefile' => '/local/powerschool/classes/Events.php'
    ),
    array(
        'eventname'   => 'core\event\course_module_deleted',
        'callback'    => 'events_handler::moodle_mod_deleted',
        'includefile' => '/local/powerschool/classes/Events.php'
    ),
    array(
        'eventname'   => 'core\event\user_enrolment_deleted',
        'callback'    => 'events_handler::moodle_user_unenrolled',
        'includefile' => '/local/powerschool/classes/Events.php'
    ),
    array(
        'eventname'   => 'core\event\course_category_deleted',
        'callback'    => 'events_handler::moodle_course_category_deleted',
        'includefile' => '/local/powerschool/classes/Events.php'
    ),
    array(
        'eventname'   => 'core\event\group_deleted',
        'callback'    => 'events_handler::moodle_groups_group_deleted',
        'includefile' => '/local/powerschool/classes/Events.php'
    ),
);
