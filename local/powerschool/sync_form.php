<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * PowerSchool Settings
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    //  It must be included from a Moodle page.
}

require_once($CFG->libdir.'/formslib.php');
require_once($CFG->libdir.'/coursecatlib.php');
require_once('classes/Users.php');

class sync_users_form extends moodleform {

    /**
     * Define the form.
     */
    public function definition () {
		global $plugin,$CFG,$DB;
		$params = json_decode($plugin->connection->sync_params);

		if(isset($params->field_for_cohort_student)){
            $params->field_for_cohort_student = (is_array($params->field_for_cohort_student))?$params->field_for_cohort_student:array($params->field_for_cohort_student);
        }else{
            $params->field_for_cohort_student = array();
        }
		if(isset($params->field_for_cohort_staff)){
            $params->field_for_cohort_staff = (is_array($params->field_for_cohort_staff))?$params->field_for_cohort_staff:array($params->field_for_cohort_staff);
        }else{
            $params->field_for_cohort_staff = array();
        }

        $course_types = array('groups' => get_string('groups', 'local_powerschool'), 'course' => get_string('course', 'local_powerschool'));
        $course_categories_list = coursecat::make_categories_list();


        get_enabled_auth_plugins(true); // fix the list of enabled auths
        $authsenabled = array('manual');
        if (!empty($CFG->auth))
            $authsenabled = array_merge($authsenabled, explode(',', $CFG->auth));

        foreach ($authsenabled as $auth) {
            $authplugin = get_auth_plugin($auth);
            $displayauths[$auth] = $authplugin->get_title();
        }

        $expansions_staff = $extensions_student = $extensions_staff = array();
        $expansions_student = array('contact_info', 'demographics', 'addresses', 'alerts', 'phones', 'school_enrollment', 'contact', 'initial_enrollment', 'schedule_setup');
        foreach($plugin->school_ids as $school_id){
            $data = (array)$plugin->request('get', "/ws/v1/school/{$school_id}/student?page=1&pagesize=1")->students;
            $expansions_arr = (!empty($data['@expansions']))?explode(',', str_replace(' ', '', $data['@expansions'])):array();
            $expansions_student = array_merge($expansions_student,$expansions_arr);
            $extensions_arr = (!empty($data['@extensions']))?explode(',', str_replace(' ', '', $data['@extensions'])):array();
            $extensions_student = array_merge($extensions_student,$extensions_arr);

            $data = (array)$plugin->request('get', "/ws/v1/school/{$school_id}/staff?page=1&pagesize=1")->staffs;
            $expansions_arr = (!empty($data['@expansions']))?explode(',', str_replace(' ', '', $data['@expansions'])):array();
            $expansions_staff = array_merge($expansions_staff,$expansions_arr);
            $extensions_arr = (!empty($data['@extensions']))?explode(',', str_replace(' ', '', $data['@extensions'])):array();
            $extensions_staff = array_merge($extensions_staff,$extensions_arr);
        }
        $expansions_student = array_unique($expansions_student);
        $expansions_student = array_flip($expansions_student);
        unset($expansions_student['lunch']);
        unset($expansions_student['fees']);
        unset($expansions_student['ethnicity_race']);
        $expansions_student = array_flip($expansions_student);

        $expansions_staff = array_unique($expansions_staff);
        $expansions_staff = array_flip($expansions_staff);
        unset($expansions_staff['school_affiliations']);
        $expansions_staff = array_flip($expansions_staff);

        $extensions_student = array_unique($extensions_student);
        $extensions_staff = array_unique($extensions_staff);

        $profile_fields = $DB->get_records('user_info_field',array('categoryid'=>$plugin->profile_category),'','id,name');
        $fields = array();
        if(!empty($profile_fields)){
            foreach($profile_fields as $item){
                $fields[$item->id] = $item->name;
            }
        }

        $mform = $this->_form;

        $mform->addElement('header', 'moodle', get_string('students', 'local_powerschool'));
		$mform->addElement('checkbox', 'students', get_string('include'));
		foreach($expansions_student as $item){
			$mform->addElement('checkbox', 'student_expansions_'.$item, get_string($item, 'local_powerschool'));
		}

        $mform->addElement('textarea', 'student_custom_fields', get_string('custom_fields', 'local_powerschool'));
        $mform->setType('student_custom_fields', PARAM_RAW);
        $mform->addHelpButton('student_custom_fields', 'student_custom_fields_desc', 'local_powerschool');

        $auth_plugin = $mform->addElement('select', 'student_auth_plugin', get_string('chooseauthmethod','auth'), $displayauths);
        $auth_plugin->setSelected('manual');

        if(!empty($extensions_student)){
            $mform->addElement('html', '<div class="extensions alert"><h5>' . get_string('extensions', 'local_powerschool') . '</h5>');
            foreach($extensions_student as $item){
                $mform->addElement('checkbox', 'student_extensions_' . $item, $item);
            }
            $mform->addElement('html', '</div>');
        }

        if(!empty($fields)){
            $mform->addElement('header', 'moodle', get_string('field_for_student_cohort','local_powerschool'));
            foreach($fields as $id=>$name){
                $mform->addElement('checkbox', 'field_for_cohort_student['.$id.']', $name);
                if(in_array($id,$params->field_for_cohort_student))
                    $mform->setDefault('field_for_cohort_student['.$id.']', 1);
            }
            unset($params->field_for_cohort_student);
        }
		
        $mform->addElement('header', 'moodle', get_string('staffs', 'local_powerschool'));
		$mform->addElement('checkbox', 'staffs', get_string('include'));
		foreach($expansions_staff as $item){
			$mform->addElement('checkbox', 'staff_expansions_'.$item, get_string($item, 'local_powerschool'));
		}
        $auth_plugin = $mform->addElement('select', 'staff_auth_plugin', get_string('chooseauthmethod','auth'), $displayauths);
        $auth_plugin->setSelected('manual');

        if(!empty($extensions_staff)){
            $mform->addElement('html', '<div class="extensions alert"><h5>' . get_string('extensions', 'local_powerschool') . '</h5>');
            foreach($extensions_staff as $item){
                $mform->addElement('checkbox', 'staff_extensions_' . $item, $item);
            }
            $mform->addElement('html', '</div>');
        }

        if(!empty($fields)){
            $mform->addElement('header', 'moodle', get_string('field_for_staff_cohort','local_powerschool'));
            foreach($fields as $id=>$name){
                $mform->addElement('checkbox', 'field_for_cohort_staff['.$id.']', $name);
                if(in_array($id,$params->field_for_cohort_staff))
                    $mform->setDefault('field_for_cohort_staff['.$id.']', 1);
            }
            unset($params->field_for_cohort_staff);
        }

        $roles_user = $DB->get_records_sql("SELECT r.* 
                                        FROM {role} r 
                                          LEFT JOIN {role_context_levels} rcl ON rcl.roleid=r.id
                                        WHERE rcl.contextlevel=:contextlevel GROUP BY r.id",array('contextlevel'=>CONTEXT_USER));
        $roles_user = role_fix_names($roles_user);
        $roles_user_arr = array('0'=>get_string('not_assign', 'local_powerschool'));
        foreach($roles_user as $role){
            $roles_user_arr[$role->id] = $role->localname;
        }
        $roles_course = $DB->get_records_sql("SELECT r.* 
                                        FROM {role} r 
                                          LEFT JOIN {role_context_levels} rcl ON rcl.roleid=r.id
                                        WHERE rcl.contextlevel=:contextlevel GROUP BY r.id",array('contextlevel'=>CONTEXT_COURSE));
        $roles_course = role_fix_names($roles_course);
        $roles_course_arr = array('0'=>get_string('not_assign', 'local_powerschool'));
        foreach($roles_course as $role){
            $roles_course_arr[$role->id] = $role->localname;
        }
        $roles_system = $DB->get_records_sql("SELECT r.* 
                                        FROM {role} r 
                                          LEFT JOIN {role_context_levels} rcl ON rcl.roleid=r.id
                                        WHERE rcl.contextlevel=:contextlevel GROUP BY r.id",array('contextlevel'=>CONTEXT_SYSTEM));
        $roles_system = role_fix_names($roles_system);
        $roles_system_arr = array('0'=>get_string('not_assign', 'local_powerschool'));
        foreach($roles_system as $role){
            $roles_system_arr[$role->id] = $role->localname;
        }

        $mform->addElement('header', 'moodle', get_string('parents','local_powerschool'));
        $mform->addElement('checkbox', 'parents', get_string('include'));
        $mform->addElement('select', 'parent_role', get_string('parent_role', 'local_powerschool'), $roles_user_arr);
        $mform->addElement('select', 'parent_course_role', get_string('parent_course_role', 'local_powerschool'), $roles_course_arr);

        $roles_course_arr = array();
        foreach($roles_course as $role){
            $roles_course_arr[$role->id] = $role->localname;
        }
		$mform->addElement('header', 'moodle', get_string('users_role', 'local_powerschool'));
		$student = $mform->addElement('select', 'student_role', get_string('student_role', 'local_powerschool'), $roles_course_arr);
		$student->setSelected(5);

		$teacher = $mform->addElement('select', 'teacher_role', get_string('co_teacher_role', 'local_powerschool'), $roles_course_arr);
		$teacher->setSelected(4);

		$teacher = $mform->addElement('select', 'teacher_lead_role', get_string('lead_teacher_role', 'local_powerschool'), $roles_course_arr);
		$teacher->setSelected(3);

		$mform->addElement('header', 'moodle', get_string('users_system_role', 'local_powerschool'));
		$student = $mform->addElement('select', 'system_student_role', get_string('student_role', 'local_powerschool'), $roles_system_arr);
		$student->setSelected(0);

		$teacher = $mform->addElement('select', 'system_guardian_role', get_string('guardian_role', 'local_powerschool'), $roles_system_arr);
		$teacher->setSelected(0);

		$teacher = $mform->addElement('select', 'system_staff_role', get_string('staff_role', 'local_powerschool'), $roles_system_arr);
		$teacher->setSelected(0);

        $one_section = $plugin->request('get', "/ws/schema/table/sections?page=1&pagesize=1&projection=id");
        $one_sectionid = $one_section->record[0]->tables->sections->id;
        if($one_sectionid > 0){
            $section = $plugin->request('get', "/ws/v1/section/".$one_sectionid);
            $section_extensions = explode(',', $section->section->{'@extensions'});
            $extensions = array(''=>'Not selected');
            foreach($section_extensions as $section_extension){
                $extensions[$section_extension] = $section_extension;
            }

            $mform->addElement('header', 'moodle', get_string('migrate_sections', 'local_powerschool'));
            $mform->addElement('html', '<div class="extensions alert"><h5>' . get_string('alert_migrate_sections', 'local_powerschool') . '</h5></div>');
            $mform->addElement('select', 'migrate_sections_extension', get_string('migrate_sections_extension', 'local_powerschool'), $extensions);
            $mform->addElement('text', 'migrate_sections_field', get_string('migrate_sections_field', 'local_powerschool'));
            $mform->setType('migrate_sections_field', PARAM_RAW);
        }

        if(!empty($CFG->enableoutcomes)){
            $mform->addElement('header', 'moodle', get_string('standards', 'local_powerschool'));
            $mform->addElement('checkbox', 'import_standards', get_string('import_standards', 'local_powerschool'));
            if(isset($params->import_standards))
                $mform->setDefault('import_standards', 1);
        }

        foreach($plugin->school_ids as $school_id){
            $response = $plugin->request('get','/ws/v1/school/'.$school_id);
            $school_name = $response->school->name;
            $mform->addElement('header', 'moodle', get_string('terms_sync_per_school', 'local_powerschool',$school_name));

            $school_ids = array_flip($plugin->school_ids);
            $data = new stdClass();
            $data->schoolid = $school_ids[$school_id];
            $data = json_encode($data);

            $count = $plugin->request('post',"/ws/schema/query/get.school.terms/count",array(),$data);
            $pages = (isset($count->count))?ceil($count->count/$plugin->metadata->metadata->schema_table_query_max_page_size):0;
            for($i=1;$i<=$pages;$i++){
                $request = $plugin->request('post',"/ws/schema/query/get.school.terms?page={$i}&pagesize=" . $plugin->metadata->metadata->schema_table_query_max_page_size,array(),$data);

                if(!is_array($request->record))
                    $request->record = array($request->record);

                foreach($request->record as $record){
                    $record = $record->tables->terms;
                    $mform->addElement('checkbox', 'terms['.$school_id.']['.$record->term_id.']', $record->terms_abbreviation, '('.$record->terms_firstday.' - '.$record->terms_lastday.')');

                    if(isset($params->sync_terms->{$school_id}->{$record->term_id}))
                        $mform->setDefault('terms['.$school_id.']['.$record->term_id.']', 1);
                }
            }


            $mform->addElement('header', 'moodle', get_string('courses_sync_per_school', 'local_powerschool',$school_name));

            $links = html_writer::link('javascript:select_all_in(\'DIV\', \'course_wrap'.$school_id.'\', null);',get_string('selectall', 'scorm')).' / ';
            $links .= html_writer::link('javascript:deselect_all_in(\'DIV\', \'course_wrap'.$school_id.'\', null);',get_string('selectnone', 'scorm'));

            $mform->addElement('html', "<div class='fitem'><div class='felement'>$links</div></div>");
            $mform->addElement('html', '<div class="course_wrap'.$school_id.'">');

            $mform->addElement('advcheckbox', 'school_update_course_cat['.$school_id.']', get_string('update_course_cat', 'local_powerschool'));
            if(isset($params->school_update_course_cat->{$school_id}) && $params->school_update_course_cat->{$school_id} == 0){
                $mform->setDefault('school_update_course_cat['.$school_id.']', 0);
            }else{
                $mform->setDefault('school_update_course_cat['.$school_id.']', 1);
            }

            $count = $plugin->request('get',"/ws/v1/school/{$school_id}/course/count");
            $pages = (isset($count->resource->count))?ceil($count->resource->count/$plugin->metadata->metadata->course_max_page_size):0;
            for($i=1;$i<=$pages;$i++){
                $request = $plugin->request('get', "/ws/v1/school/{$school_id}/course?page={$i}&pagesize=" . $plugin->metadata->metadata->course_max_page_size);

                if(!is_array($request->courses->course))
                    $request->courses->course = array($request->courses->course);

                foreach($request->courses->course as $course){
                    $clean_course_number = str_replace(' ', '', $course->course_number);
                    $chekbox_str = '('.$course->course_number.')';
                    if(isset($params->sync_courses->{$clean_course_number}) && isset($params->sync_courses->{$clean_course_number}->type)){
                        $chekbox_str .= ' (synced as ' . $course_types[$params->sync_courses->{$clean_course_number}->type] . ')';
                    }

                    $elements = array();
                    $elements[] =& $mform->createElement('advcheckbox', 'courses['.$clean_course_number.'][enable]', $course->course_name, $chekbox_str);
                    if(!isset($params->sync_courses->{$clean_course_number}) || !isset($params->sync_courses->{$clean_course_number}->type)){
                        $elements[] =& $mform->createElement('select', 'courses[' . $clean_course_number . '][type]', $course->course_name, $course_types);
                    }

                    $elements[] =& $mform->createElement('select', 'courses['.$clean_course_number.'][category]', $course->course_name, $course_categories_list);
                    $mform->addGroup($elements, 'courses', $course->course_name, array(' '), false);

                    if(isset($params->sync_courses->{$clean_course_number}) && $params->sync_courses->{$clean_course_number}->enable == 1){
                        $mform->setDefault('courses[' . $clean_course_number . '][enable]', 1);
                    }

                    $curent_course_cat = $this->get_course_cat($course,$clean_course_number,$params);
                    if($curent_course_cat>0){
                        $mform->setDefault('courses[' . $clean_course_number . '][category]', $curent_course_cat);
                    }
                }
            }
            $mform->addElement('html', '</div>');
        }

        //$this->add_action_buttons(false, get_string('sync', 'local_powerschool'));
        $buttonarray=array();
        $buttonarray[] = &$mform->createElement('submit', 'save', get_string('save', 'local_powerschool'));
        $buttonarray[] = &$mform->createElement('submit', 'save_sync_all', get_string('save_sync_all', 'local_powerschool'));
        $buttonarray[] = &$mform->createElement('submit', 'save_sync_users', get_string('save_sync_users', 'local_powerschool'));
        $buttonarray[] = &$mform->createElement('submit', 'save_sync_courses', get_string('save_sync_courses', 'local_powerschool'));

        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        $mform->closeHeaderBefore('buttonar');

        unset($params->school_update_course_cat);
		$this->set_data($params);
    }


    public function validation($params, $files) {
		$errors = parent::validation($params, $files);
		if(!isset($params['students']) && !isset($params['staffs'])){
			$errors['students'] = get_string('must_exist_students_or_staffs', 'local_powerschool');
			$errors['staffs'] = get_string('must_exist_students_or_staffs', 'local_powerschool');
		}
		
        return $errors;
    }


    public function get_course_cat($course, $clean_course_number, $params) {
        global $DB;

        if(isset($params->sync_courses->{$clean_course_number}) && isset($params->sync_courses->{$clean_course_number}->category) && $params->sync_courses->{$clean_course_number}->category > 0){
            return $params->sync_courses->{$clean_course_number}->category;
        }

        $record = $DB->get_record_sql('
                        SELECT c.category
                        FROM mdl_powerschool_course_fields pcf
                          LEFT JOIN mdl_course c ON c.id=pcf.courseid
                        WHERE pcf.course_number=:course_number
                        LIMIT 1',array('course_number'=>$course->course_number));
        if(isset($record->category) && $record->category > 0){
            return $record->category;
        }
        return 0;
    }
}


