<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/local/powerschool/locallib.php');
require_once($CFG->dirroot.'/course/lib.php');
require_once($CFG->dirroot.'/course/modlib.php');

class Courses extends PowerSchool{
    public $categories;
    public $sync_params;
    public $gradescales;

    protected $assignment_updated;
    protected $standard_updated;

    public function __construct($connection_id){
        global $DB;
        parent::__construct($connection_id);

        $this->categories = $DB->get_records_sql('SELECT CONCAT_WS(\'\',cc.id,pcc.courseid) as dcid,cc.id,cc.name,cc.parent,pcc.courseid as pcourse,cc.idnumber 
                                            FROM {course_categories} cc
                                              LEFT JOIN {powerschool_course_cat} pcc ON pcc.catid=cc.id');

        $this->sync_params = json_decode($this->connection->sync_params);
        $this->sync_params->sync_courses = (array)$this->sync_params->sync_courses;

        $this->standard_updated = array();
    }

    public function sync_courses($page, $pagesize = 0){
        $pagesize = ($pagesize == 0)?$this->metadata->metadata->schema_table_query_max_page_size:$pagesize;

        foreach($this->school_numbers as $school_number){
            $data = new stdClass();
            $data->schoolid = $school_number;
            $data = json_encode($data);

            $data = $this->request('post', '/ws/schema/query/get.school.courses?page=' . $page . '&pagesize=' . $pagesize, array(), $data);
            if(!isset($data->record))
                continue;

            foreach($data->record as $course){
                $this->school_ids = array_flip(array_flip($this->school_ids));
                $clean_course_number = str_replace(' ', '', $course->tables->sections->course_number);
                if(isset($this->sync_params->sync_courses[$clean_course_number])
                    && $this->sync_params->sync_courses[$clean_course_number]->enable == 1
                    && isset($this->sync_params->sync_courses[$clean_course_number]->type)
                    && isset($this->sync_params->sync_terms->{$this->school_ids[$school_number]}->{$course->tables->sections->term_id}))
                {
                    $course->tables->sections->school_number = $school_number;
                    $course->tables->sections->course_type = $this->sync_params->sync_courses[$clean_course_number]->type;
                    $course->tables->sections->course_category = $this->sync_params->sync_courses[$clean_course_number]->category;
                    $this->execute_course($course->tables->sections);
                }
            }
        }
    }
    public function execute_course($course){
        global $DB;
        $course->original_course_number = $course->course_number;
        $course->course_number = $this->connection->course_prefix.$course->course_number;
        $course->moving = false;

        $this->moving_section($course);
        $cat = $this->get_course_cat($course);
        $course->terms_firstday = strtotime($course->terms_firstday);
        if($course->course_type == 'groups'){
            return $this->execute_course_with_groups($course,$cat);
        }

        $old_course = $this->course_exist($course);
        if(isset($old_course->id)){
            $new_course = clone $old_course;
            if(!$course->moving){
                $new_course = new stdClass();
                $new_course->id = $old_course->id;
                if(isset($this->sync_params->school_update_course_cat->{$this->school_ids[$course->school_number]}) && $this->sync_params->school_update_course_cat->{$this->school_ids[$course->school_number]} == 1)
                    $new_course->category = $cat;
                $new_course->shortname = $course->course_name . ' ' . $course->section_number . ' / ' . $course->section_expression . ' ' . $course->terms_abbreviation;
                $new_course->idnumber = $course->course_number . '.' . $course->section_number;
                $new_course->fullname = $this->get_course_fullname($course, $this->connection->course_fullname);
                $new_course->startdate = $course->terms_firstday;
                $new_course->summary = $course->description;
                if($new_course != $old_course)
                    update_course($new_course);
            }
        }else{
            $new_course = new stdClass();
            $new_course->category = $cat;
            $new_course->shortname = $course->course_name.' '.$course->section_number.' / '.$course->section_expression.' '.$course->terms_abbreviation;
            $new_course->idnumber = $course->course_number.'.'.$course->section_number;
            $new_course->fullname = $this->get_course_fullname($course, $this->connection->course_fullname);
            $new_course->startdate = $course->terms_firstday;
            $new_course->summary = $course->description;
            $new_course->enablecompletion = 1;

            $new_course = create_course($new_course);
            $record = new stdClass();
            $record->mcourse = $new_course->id;
            $record->psection = $course->section_id;
            $DB->insert_record('powerschool_courses', $record, false);
        }
        $this->execute_course_fields($course,$new_course->id);
        $this->enroll_users($new_course,$course);
        $this->execute_grade_categories($new_course,$course);
        $this->execute_section_modules($course,$new_course);
        $this->execute_course_gradescale($new_course,$course);
        $this->execute_course_standards($new_course,$course);

        return true;
    }

    private function execute_course_with_groups($course,$cat){
        global $DB;

        $old_course = $this->course_exist($course);
        if(isset($old_course->id)){
            $new_course = clone $old_course;
            if(!$course->moving){
                $new_course = new stdClass();
                $new_course->id = $old_course->id;
                if(isset($this->sync_params->school_update_course_cat->{$this->school_ids[$course->school_number]}) && $this->sync_params->school_update_course_cat->{$this->school_ids[$course->school_number]} == 1)
                    $new_course->category = $cat;
                $new_course->shortname = $course->course_name . ' (' . $course->course_number . ')';
                $new_course->idnumber = $course->course_number;
                $new_course->fullname = $course->course_name;
                $new_course->startdate = $course->terms_firstday;
                $new_course->summary = $course->description;
                if($new_course != $old_course)
                    update_course($new_course);
            }
        }else{
            $new_course = new stdClass();
            $new_course->category = $cat;
            $new_course->shortname = $course->course_name. ' ('.$course->course_number.')';
            $new_course->idnumber = $course->course_number;
            $new_course->fullname = $course->course_name;
            $new_course->startdate = $course->terms_firstday;
            $new_course->summary = $course->description;
            $new_course->enablecompletion = 1;

            $new_course = create_course($new_course);
            $record = new stdClass();
            $record->mcourse = $new_course->id;
            $record->psection = $course->course_id;
            $DB->insert_record('powerschool_courses', $record, false);
        }
        $this->execute_course_fields($course,$new_course->id);
        $this->execute_course_group($new_course,$course);
        $this->enroll_users($new_course,$course);
        $this->execute_grade_categories($new_course,$course);
        $this->execute_section_modules($course,$new_course);
        $this->execute_course_gradescale($new_course,$course);
        $this->execute_course_standards($new_course,$course);
    }

    private function moving_section(&$section){
        $sync_params = json_decode($this->connection->sync_params);

        if(!isset($sync_params->migrate_sections_extension) || empty($sync_params->migrate_sections_extension) || !isset($sync_params->migrate_sections_field) || empty($sync_params->migrate_sections_field))
            return true;

        $responce = $this->request('get', "/ws/v1/section/".$section->section_id.'?extensions='.$sync_params->migrate_sections_extension);

        if(!isset($responce->section->_extension_data))
            return true;

        if(is_object($responce->section->_extension_data->_table_extension))
            $responce->section->_extension_data->_table_extension = array($responce->section->_extension_data->_table_extension);

        foreach($responce->section->_extension_data->_table_extension as $table_extension){
            if(is_object($table_extension->_field))
                $table_extension->_field = array($table_extension->_field);

            foreach($table_extension->_field as $field){
                if(strnatcasecmp($field->name, $sync_params->migrate_sections_field) == 0){
                    $value = explode('.',$field->value);
                    if(isset($value[0]) && !empty($value[0])){
                        $section->course_number = $this->connection->course_prefix.$value[0];
                        $section->moving = true;
                    }
                    if(isset($value[1]) && !empty($value[1])){
                        $section->section_number = $value[1];
                        $section->moving = true;
                    }
                }
            }
        }
        return true;
    }

    private function course_exist($course){
        global $DB;
        if($course->course_type == 'groups') {
            $id = $course->course_id;
            $idnumber = $course->course_number;
        }else {
            $id = $course->section_id;
            $idnumber = $course->course_number.'.'.$course->section_number;
        }

        /*$old_course = $DB->get_record_sql('SELECT  c.id,c.category,c.shortname,c.idnumber,c.fullname,c.startdate,c.summary
                                            FROM {powerschool_courses} pc
                                              LEFT JOIN {course} c ON pc.mcourse=c.id
                                              LEFT JOIN {powerschool_course_fields} cf ON cf.courseid=c.id
                                            WHERE pc.psection=:psection AND cf.connection_id=:connection_id LIMIT 1',array('psection'=> $id, 'connection_id'=>$this->connection->id));
        if(isset($old_course->id))
            return $old_course;*/

        $old_course = $DB->get_record_sql('SELECT  c.id,c.category,c.shortname,c.idnumber,c.fullname,c.startdate,c.summary
                                            FROM {course} c
                                            WHERE c.idnumber=:idnumber ',array('idnumber'=> $idnumber));
        if(isset($old_course->id)){
            $record = new stdClass();
            $record->mcourse = $old_course->id;
            $record->psection = ($course->course_type == 'groups')?$course->course_id:$course->section_id;
            if(!$DB->record_exists('powerschool_courses',(array)$record))
                $DB->insert_record('powerschool_courses', $record, false);

            return $old_course;
        }else
            return null;
    }

    private function get_course_fullname($course, $name){
        $name = str_replace('[[course_name]]',$course->course_name,$name);
        $name = str_replace('[[course_number]]',$course->course_number,$name);

        $name = str_replace('[[section_number]]',$course->section_number,$name);
        $name = str_replace('[[section_expression]]',$course->section_expression,$name);
        $name = str_replace('[[terms_abbreviation]]',$course->terms_abbreviation,$name);

        return $name;
    }

    private function execute_course_fields($course, $courseid){
        global $DB;

        $course_field = $DB->get_record('powerschool_course_fields',array('courseid'=>$courseid,'section_dcid'=>$course->section_dcid),'id');

        $additional_fields = new stdClass();

        if(isset($course_field->id))
            $additional_fields->id = $course_field->id;
        else
            $additional_fields->courseid = $courseid;

        $additional_fields->program_name = $course->program_name;
        $additional_fields->section_gradescaleid = $course->section_gradescaleid;
        $additional_fields->section_maxenrollment = $course->section_maxenrollment;
        $additional_fields->section_maxcut = $course->section_maxcut;
        $additional_fields->section_wheretaught = $course->section_wheretaught;
        $additional_fields->section_excludefromhonorroll = $course->section_excludefromhonorroll;
        $additional_fields->section_exclude_ada = $course->section_exclude_ada;
        $additional_fields->section_excludefromclassrank = $course->section_excludefromclassrank;
        $additional_fields->section_excludefromgpa = $course->section_excludefromgpa;
        $additional_fields->section_wheretaughtdistrict = $course->section_wheretaughtdistrict;
        $additional_fields->sec_excludefromstoredgrades = $course->section_excludefromstoredgrades;
        $additional_fields->section_number = $course->section_number;
        $additional_fields->section_house = $course->section_house;
        $additional_fields->section_section_type = $course->section_section_type;
        $additional_fields->section_att_mode_code = $course->section_att_mode_code;
        $additional_fields->section_room = $course->section_room;
        $additional_fields->section_grade_level = $course->section_grade_level;
        $additional_fields->section_team = $course->section_team;
        $additional_fields->section_max_load_status = $course->section_max_load_status;
        $additional_fields->section_attendance_type_code = $course->section_attendance_type_code;
        $additional_fields->teacher_user_dcid = $course->teacher_user_dcid;
        $additional_fields->course_number = $course->original_course_number;
        $additional_fields->gradebooktype = (isset($course->gradebooktype) && $course->gradebooktype>0)?$course->gradebooktype:1;
        $additional_fields->section_dcid = $course->section_dcid;
        $additional_fields->section_id = $course->section_id;
        $additional_fields->term_id = $course->term_id;
        $additional_fields->school_number = $course->school_number;
        $additional_fields->gradescaleitem = $course->gradescaleitem;
        $additional_fields->connection_id = $this->connection->id;
        $additional_fields->course_type = $course->course_type;

        if(isset($course_field->id))
            return $DB->update_record('powerschool_course_fields', $additional_fields);
        else
            return $DB->insert_record('powerschool_course_fields', $additional_fields, false);
    }

    private function get_course_cat($course){
        global $CFG,$DB;
        require_once("$CFG->libdir/coursecatlib.php");

        $parent = coursecat::get($course->course_category);
        if(!$parent){
            $parent = (object) array('id'=>1,'parent'=>0);
        }

        if($course->course_type == 'groups')
            return $parent->id;

        $category = array();
        foreach($this->categories as $cat){
            if(isset($cat->pcourse) && $cat->pcourse == $course->course_id){
                $category = $cat;
                break;
            }elseif($cat->parent>0 && $cat->idnumber == $course->course_number){
                $category = $cat;

                if(!$DB->record_exists('powerschool_course_cat', array('courseid'=>$course->course_id,'catid'=>$cat->id))){
                    $record = new stdClass();
                    $record->courseid = $course->course_id;
                    $record->catid = $cat->id;
                    $DB->insert_record('powerschool_course_cat',$record,false);
                }

                break;
            }
        }

        if(empty($category)){
            $record = array();
            $record['name'] = $course->course_name;
            $record['description'] = '';
            $record['idnumber'] = $course->course_number;
            $record['parent'] = $parent->id;
            $cat_id = coursecat::create($record)->id;
            $this->categories[] = (object) array('id'=>$cat_id, 'parent'=>$parent->id,'pcourse'=>$course->course_id, 'idnumber'=>$record['idnumber'], 'name'=>$record['name']);

            $record = new stdClass();
            $record->courseid = $course->course_id;
            $record->catid = $cat_id;
            $DB->insert_record('powerschool_course_cat',$record,false);
            return $cat_id;
        }elseif($category->idnumber == $course->course_number && $category->name == $course->course_name && $category->parent == $parent->id){
            return $category->id;
        }else{
            $cat = coursecat::get($category->id);
            $cat->update(array('parent'=>$parent->id,'idnumber'=>$course->course_number, 'name'=>$course->course_name));
            return $category->id;
        }
    }

    private function execute_course_gradescale($course,$section){
        global $DB,$CFG,$USER;
        require_once($CFG->libdir.'/grade/grade_scale.php');


        $context = context_course::instance($course->id);
        if(!isset($this->gradescales[$section->section_gradescaleid]))
            $this->gradescales[$section->section_gradescaleid] = $this->request('get',"/ws/schema/table/gradescaleitem?q=gradescaleid=={$section->section_gradescaleid}&projection=name,cutoffpercentage");

        $grade_scale = $this->gradescales[$section->section_gradescaleid];
        $DB->delete_records('grade_letters',array('contextid'=>$context->id));

        $scales = array();
        if(!isset($grade_scale->record) || empty($grade_scale->record))
            return;

        foreach($grade_scale->record as $scale){
            $scale = $scale->tables->gradescaleitem;
            $record = new stdClass();
            $record->contextid = $context->id;
            $record->lowerboundary = $scale->cutoffpercentage;
            $record->letter = $scale->name;
            $DB->insert_record('grade_letters',$record,false);
            $scales[] = $scale->name;
        }

        $moodle_grade_scale_record = $DB->get_record('scale',array('courseid'=>$course->id,'name'=>get_string('ps_grade_scale','local_powerschool')));
        $moodle_grade_scale = new grade_scale();
        if(isset($moodle_grade_scale_record->id)){
            $moodle_grade_scale = new grade_scale(array('id'=>$moodle_grade_scale_record->id));
        }
        $moodle_grade_scale->userid = $USER->id;
        $moodle_grade_scale->courseid = $course->id;
        $moodle_grade_scale->name = get_string('ps_grade_scale','local_powerschool');
        $moodle_grade_scale->description = get_string('ps_grade_scale','local_powerschool');
        $moodle_grade_scale->timemodified = time();
        $moodle_grade_scale->scale = implode(',', $scales);

        if($moodle_grade_scale->id)
            $moodle_grade_scale->update();
        else
            $moodle_grade_scale->insert();
        return;
    }

    private function execute_course_group($course,$section){
        global $CFG,$DB;
        require_once($CFG->dirroot.'/group/lib.php');

        $old_group = $this->group_exist($course,$section);
        $group_name = $this->get_course_fullname($section, $this->connection->course_fullname_group);
        $group_idnumber = $section->course_number.'.'.$section->section_number;
        if(isset($old_group->id)){
            $group_id = $old_group->id;

            $new_group = new stdClass();
            $new_group->id = $old_group->id;
            $new_group->name = $group_name;
            $new_group->courseid = $old_group->courseid;
            $new_group->idnumber = $group_idnumber;
            groups_update_group($new_group);
        }else{
            $new_group = new stdClass();
            $new_group->courseid = $course->id;
            $new_group->name = $group_name;
            $new_group->idnumber = $group_idnumber;
            $group_id = groups_create_group($new_group);
        }


        $record = new stdClass();
        $record->courseid = $section->section_id;
        $record->catid = $group_id;
        $record->courseid_group = $course->id;
        if(!$DB->record_exists('powerschool_course_cat',(array)$record))
            return $DB->insert_record('powerschool_course_cat', $record, false);
    }

    private function group_exist($course,$section){
        global $DB;

        $old_group = $DB->get_record_sql('SELECT g.id,g.name,g.courseid, g.idnumber
                                            FROM {powerschool_course_cat} pcc
                                              LEFT JOIN {powerschool_course_fields} cf ON cf.courseid=pcc.courseid_group
                                              LEFT JOIN {groups} g ON g.id = pcc.catid
                                            WHERE pcc.courseid=:courseid AND cf.connection_id=:connection_id LIMIT 1',
            array('courseid'=>$section->section_id,'connection_id'=>$this->connection->id));

        if(isset($old_group->id))
            return $old_group;

        $group_name = $this->get_course_fullname($section, $this->connection->course_fullname_group);
        $group_idnumber = $section->course_number.'.'.$section->section_number;
        $old_group = $DB->get_record_sql('SELECT g.id,g.name,g.courseid,g.idnumber
                                            FROM {groups} g
                                            WHERE (g.name=:group_name OR g.idnumber=:group_idnumber) AND g.courseid=:courseid',
            array('group_name'=>$group_name, 'group_idnumber'=>$group_idnumber, 'courseid'=>$course->id));
        if(isset($old_group->id)){
            $new_record = new stdClass();
            $new_record->courseid = $section->section_id;
            $new_record->catid = $old_group->id;
            $new_record->courseid_group = $course->id;
            $DB->insert_record('powerschool_course_cat', $new_record, false);
            return $old_group;
        }else
            return null;
    }

    private function execute_grade_categories($mcourse, $section){
        global $CFG,$DB;
        require_once ($CFG->dirroot.'/lib/grade/grade_item.php');
        require_once ($CFG->dirroot.'/lib/grade/grade_category.php');
        require_once ($CFG->dirroot.'/lib/grade/constants.php');

        $response = $this->request('get', '/ws/xte/teacher_category?users_dcid='.$section->teacher_user_dcid.'&term_id='.$section->term_id);
        foreach($response as $power_cat){
            $record = $DB->get_record_sql('SELECT pgc.* 
                                            FROM {powerschool_grade_cat} pgc
                                              LEFT JOIN {powerschool_course_fields} pcf ON pcf.courseid=pgc.courseid
                                              LEFT JOIN {grade_categories} gc ON gc.id=pgc.moodle_cat AND gc.courseid=pcf.courseid
                                            WHERE pgc.courseid=:courseid AND pgc.power_cat=:power_cat AND pcf.connection_id=:connection_id AND pgc.section_id=:section_id AND gc.id IS NOT NULL LIMIT 1',
                                        array('courseid'=>$mcourse->id, 'power_cat'=>$power_cat->teachercategoryid, 'connection_id'=>$this->connection->id, 'section_id'=>$section->section_id));
            if(isset($record->id)){
                $grade_category = grade_category::fetch(array('id'=>$record->moodle_cat, 'courseid'=>$mcourse->id));

                $data = new stdClass();
                $data->fullname = $power_cat->name;
                grade_category::set_properties($grade_category, $data);
                $grade_category->update();
            }else{
                $old_cat = $DB->get_record_sql('SELECT gc.* 
                                            FROM {powerschool_course_fields} pcf 
                                              LEFT JOIN {grade_categories} gc ON gc.courseid=pcf.courseid
                                            WHERE pcf.courseid=:courseid AND gc.fullname=:power_cat AND pcf.connection_id=:connection_id AND gc.id IS NOT NULL LIMIT 1',
                    array('courseid'=>$mcourse->id, 'power_cat'=>$power_cat->name, 'connection_id'=>$this->connection->id));

                if(!isset($old_cat->id)){
                    $grade_category = new grade_category(array('courseid' => $mcourse->id), false);
                    $grade_category->apply_default_settings();
                    $grade_category->apply_forced_settings();

                    $data = new stdClass();
                    $data->fullname = $power_cat->name;
                    $data->grade_item = new grade_item(array('courseid' => $mcourse->id, 'itemtype' => 'manual'), false);

                    grade_category::set_properties($grade_category, $data);
                    $grade_category->insert();
                }

                if($record = $DB->get_record('powerschool_grade_cat',array('section_id'=>$section->section_id,'power_cat'=>$power_cat->teachercategoryid,'courseid'=>$mcourse->id))){
                    $record->moodle_cat = (!isset($old_cat->id))?$grade_category->id:$old_cat->id;
                    $DB->update_record('powerschool_grade_cat',$record);
                }else{
                    $record = new stdClass();
                    $record->section_id = $section->section_id;
                    $record->power_cat = $power_cat->teachercategoryid;
                    $record->moodle_cat = (!isset($old_cat->id))?$grade_category->id:$old_cat->id;
                    $record->courseid = $mcourse->id;
                    $record->timecreate = time();
                    $DB->insert_record('powerschool_grade_cat', $record);
                }
            }
        }
        return;
    }
    private function get_grade_category($section_id,$teachercategoryid,$courseid){
        global $DB;
        $record = $DB->get_record_sql('SELECT pgc.* 
                                            FROM {powerschool_grade_cat} pgc
                                              LEFT JOIN {powerschool_course_fields} pcf ON pcf.section_id=pgc.section_id AND pcf.courseid=pgc.courseid
                                            WHERE pgc.courseid=:courseid AND pgc.power_cat=:power_cat AND pcf.connection_id=:connection_id LIMIT 1',
            array('courseid'=>$courseid, 'power_cat'=>$teachercategoryid, 'connection_id'=>$this->connection->id));

        if(isset($record->id))
            return $record->moodle_cat;
        else
            return null;
    }

    private function enroll_users($course,$section){
        global $DB,$CFG;
        require_once ($CFG->dirroot.'/enrol/manual/lib.php');
        require_once ($CFG->dirroot.'/group/lib.php');

        if(!class_exists ('enrol_manual_plugin')) {
            echo 'Enrollment module "manual" must be installed and enabled';
            return false;
        }

        $enroll_plug = new enrol_manual_plugin();
        $instance = $DB->get_record('enrol', array('courseid'=>$course->id, 'enrol'=>'manual'));

        $count = $this->request('get','/ws/v1/section/'.$section->section_id.'/section_enrollment/count');
        $pages = (isset($count->resource->count))?ceil($count->resource->count/$this->metadata->metadata->section_enrollment_max_page_size):0;


        for($i=1;$i<=$pages;$i++){
            $request = $this->request('get','/ws/v1/section/'.$section->section_id."/section_enrollment?page=$i&pagesize=".$this->metadata->metadata->section_enrollment_max_page_size);

            if(!isset($request->section_enrollments)){
                return false;
            }

            if(!is_array($request->section_enrollments->section_enrollment))
                $request->section_enrollments->section_enrollment = array('0'=>$request->section_enrollments->section_enrollment);


            foreach ($request->section_enrollments->section_enrollment as $sectin_enrollment) {
                if($section->course_type == 'groups') {
                    $user_record = $DB->get_record_sql('SELECT pcc.id, d.userid, pcc.catid as groupid
                                                FROM {user_info_field} f
                                                  JOIN {user_info_data} d ON d.fieldid=f.id
                                                  JOIN {user_info_field} fs ON fs.shortname = \'powerschool777usertype\'
                                                  JOIN {user_info_data} ds ON ds.fieldid=fs.id AND d.userid=ds.userid
                                                  JOIN {user_info_field} fc ON fc.shortname = \'powerschool777connection_id\'
                                                  JOIN {user_info_data} dc ON dc.fieldid=fc.id AND d.userid=dc.userid
                                                  JOIN {powerschool_course_cat} pcc ON pcc.courseid=:section_id 
                                                  JOIN {powerschool_course_fields} pcf ON pcf.courseid=pcc.courseid_group
                                                WHERE f.shortname = \'powerschool777id\' AND d.data=:id AND ds.data=\'student\' AND dc.data=:connection_id AND pcf.connection_id=:connection_id2 LIMIT 1
                        ', array('section_id' => $section->section_id, 'id' => $sectin_enrollment->student_id,'connection_id'=>$this->connection->id,'connection_id2'=>$this->connection->id));
                }else{
                    $user_record = $DB->get_record_sql('SELECT d.userid, 0 as groupid
                                                FROM {user_info_field} f
                                                  JOIN {user_info_data} d ON d.fieldid=f.id
                                                  JOIN {user_info_field} fs ON fs.shortname = \'powerschool777usertype\'
                                                  JOIN {user_info_data} ds ON ds.fieldid=fs.id AND d.userid=ds.userid
                                                  JOIN {user_info_field} fc ON fc.shortname = \'powerschool777connection_id\'
                                                  JOIN {user_info_data} dc ON dc.fieldid=fc.id AND d.userid=dc.userid
                                                WHERE f.shortname = \'powerschool777id\' AND d.data=:id AND ds.data=\'student\' AND dc.data=:connection_id
                        ', array('id' => $sectin_enrollment->student_id,'connection_id'=>$this->connection->id));
                }

                if(isset($user_record->userid) && $user_record->userid > 0){
                    $this->enroll_student($enroll_plug, $instance, $user_record->userid, $course->id, $sectin_enrollment->id, $user_record->groupid, $sectin_enrollment->entry_date, $sectin_enrollment->exit_date);
                }
            }

        }

        /* for ps > 9.2 */
        $params = new stdClass();
        $params->section_ids = array($section->section_id);

        list($count,$header) = $this->request('post','/ws/schema/query/com.powerschool.core.users.coteacher_access_roles/count',array(),json_encode($params),true);
        $pages = (isset($count->count))?ceil($count->count/$this->metadata->metadata->schema_table_query_max_page_size):0;

        for($i=1;$i<=$pages;$i++){
            list($request,$header) = $this->request('post','/ws/schema/query/com.powerschool.core.users.coteacher_access_roles?page='.$i.'&pagesize='.$this->metadata->metadata->schema_table_query_max_page_size,array(),json_encode($params),true);

            if(!isset($request->record)){
                return false;
            }

            foreach ($request->record as $sectin_enrollment) {
                $sectin_enrollment = $sectin_enrollment->tables->schoolstaff;
                if(isset($sectin_enrollment->teachernumber) && $sectin_enrollment->teachernumber == $sectin_enrollment->dcid)
                    continue;

                if($section->course_type == 'groups'){
                    $user_record = $DB->get_record_sql('SELECT d.userid, pcc.catid as groupid
                                                FROM {user_info_field} f
                                                  JOIN {user_info_data} d ON d.fieldid=f.id
                                                  JOIN {user_info_field} fs ON fs.shortname = \'powerschool777usertype\'
                                                  JOIN {user_info_data} ds ON ds.fieldid=fs.id AND d.userid=ds.userid
                                                  JOIN {user_info_field} fc ON fc.shortname = \'powerschool777connection_id\'
                                                  JOIN {user_info_data} dc ON dc.fieldid=fc.id AND d.userid=dc.userid
                                                  JOIN {powerschool_course_cat} pcc ON pcc.courseid=:section_id 
                                                  JOIN {powerschool_course_fields} pcf ON pcf.courseid=pcc.courseid_group
                                                WHERE f.shortname = \'powerschool777id\' AND d.data=:id AND ds.data=\'staff\' AND dc.data=:connection_id AND pcf.connection_id=:connection_id2 LIMIT 1
                        ', array('section_id' => $section->section_id, 'id' => $sectin_enrollment->dcid,'connection_id'=>$this->connection->id,'connection_id2'=>$this->connection->id));
                }else{
                    $user_record = $DB->get_record_sql('SELECT d.userid, 0 as groupid
                                                FROM {user_info_field} f
                                                  JOIN {user_info_data} d ON d.fieldid=f.id
                                                  JOIN {user_info_field} fs ON fs.shortname = \'powerschool777usertype\'
                                                  JOIN {user_info_data} ds ON ds.fieldid=fs.id AND d.userid=ds.userid
                                                  JOIN {user_info_field} fc ON fc.shortname = \'powerschool777connection_id\'
                                                  JOIN {user_info_data} dc ON dc.fieldid=fc.id AND d.userid=dc.userid
                                                WHERE f.shortname = \'powerschool777id\' AND d.data=:id AND ds.data=\'staff\' AND dc.data=:connection_id
                        ', array('id' => $sectin_enrollment->dcid,'connection_id'=>$this->connection->id));
                }


                if(isset($user_record->userid) && $user_record->userid>0){
                    $enroll_plug->enrol_user($instance, $user_record->userid, $this->sync_params->teacher_role);
                    if($user_record->groupid > 0)
                        groups_add_member($user_record->groupid,$user_record->userid);
                }
            }
        }
        /* */

        $section_id = ($this->metadata->metadata->powerschool_version >= 10)?$section->section_dcid:$section->section_id;
        $count = $this->request('get','/ws/schema/table/sectionteacher/count/?q=sectionid=='.$section_id);
        $pages = (isset($count->count))?ceil($count->count/$this->metadata->metadata->schema_table_query_max_page_size):0;

        for($i=1;$i<=$pages;$i++){
            $data = new stdClass();
            $data->sectionid = $section_id;
            $data = json_encode($data);

            $request = $this->request('post','/ws/schema/query/get.section.teachers?page='.$i.'&pagesize='.$this->metadata->metadata->schema_table_query_max_page_size,array(),$data);

            if(!isset($request->record)){
                return false;
            }

            foreach ($request->record as $sectin_enrollment) {
                $sectin_enrollment = $sectin_enrollment->tables->sectionteacher;
                if($section->teachernumber == $sectin_enrollment->teachernumber)
                    continue;

                if($section->course_type == 'groups'){
                    $user_record = $DB->get_record_sql('SELECT d.userid, pcc.catid as groupid
                                                FROM {user_info_field} f
                                                  JOIN {user_info_data} d ON d.fieldid=f.id
                                                  JOIN {user_info_field} fs ON fs.shortname = \'powerschool777usertype\'
                                                  JOIN {user_info_data} ds ON ds.fieldid=fs.id AND d.userid=ds.userid
                                                  JOIN {user_info_field} fc ON fc.shortname = \'powerschool777connection_id\'
                                                  JOIN {user_info_data} dc ON dc.fieldid=fc.id AND d.userid=dc.userid
                                                  JOIN {powerschool_course_cat} pcc ON pcc.courseid=:section_id 
                                                  JOIN {powerschool_course_fields} pcf ON pcf.courseid=pcc.courseid_group
                                                WHERE f.shortname = \'powerschool777local_id\' AND d.data=:id AND ds.data=\'staff\' AND dc.data=:connection_id AND pcf.connection_id=:connection_id2 LIMIT 1
                        ', array('section_id' => $section->section_id, 'id' => $sectin_enrollment->teachernumber,'connection_id'=>$this->connection->id,'connection_id2'=>$this->connection->id));
                }else{
                    $user_record = $DB->get_record_sql('SELECT d.userid, 0 as groupid
                                                FROM {user_info_field} f
                                                  JOIN {user_info_data} d ON d.fieldid=f.id
                                                  JOIN {user_info_field} fs ON fs.shortname = \'powerschool777usertype\'
                                                  JOIN {user_info_data} ds ON ds.fieldid=fs.id AND d.userid=ds.userid
                                                  JOIN {user_info_field} fc ON fc.shortname = \'powerschool777connection_id\'
                                                  JOIN {user_info_data} dc ON dc.fieldid=fc.id AND d.userid=dc.userid
                                                WHERE f.shortname = \'powerschool777local_id\' AND d.data=:id AND ds.data=\'staff\' AND dc.data=:connection_id
                        ', array('id' => $sectin_enrollment->teachernumber,'connection_id'=>$this->connection->id));
                }


                if(isset($user_record->userid) && $user_record->userid>0){
                    $enroll_plug->enrol_user($instance, $user_record->userid, $this->sync_params->teacher_role, strtotime($sectin_enrollment->start_date), strtotime($sectin_enrollment->end_date));
                    if($user_record->groupid > 0)
                        groups_add_member($user_record->groupid,$user_record->userid);
                }
            }
        }

        if($section->course_type == 'groups'){
            $user_record = $DB->get_record_sql('SELECT d.userid, pcc.catid as groupid
                                                FROM {user_info_field} f
                                                  JOIN {user_info_data} d ON d.fieldid=f.id
                                                  JOIN {user_info_field} fs ON fs.shortname = \'powerschool777usertype\'
                                                  JOIN {user_info_data} ds ON ds.fieldid=fs.id AND d.userid=ds.userid
                                                  JOIN {user_info_field} fc ON fc.shortname = \'powerschool777connection_id\'
                                                  JOIN {user_info_data} dc ON dc.fieldid=fc.id AND d.userid=dc.userid
                                                  JOIN {powerschool_course_cat} pcc ON pcc.courseid=:section_id 
                                                  JOIN {powerschool_course_fields} pcf ON pcf.courseid=pcc.courseid_group
                                                WHERE f.shortname = \'powerschool777local_id\' AND d.data=:id AND ds.data=\'staff\' AND dc.data=:connection_id AND pcf.connection_id=:connection_id2 LIMIT 1
                        ', array('section_id' => $section->section_id, 'id' => $section->teachernumber,'connection_id'=>$this->connection->id,'connection_id2'=>$this->connection->id));
        }else{
            $user_record = $DB->get_record_sql('SELECT d.userid, 0 as groupid
                                                FROM {user_info_field} f
                                                  JOIN {user_info_data} d ON d.fieldid=f.id
                                                  JOIN {user_info_field} fs ON fs.shortname = \'powerschool777usertype\'
                                                  JOIN {user_info_data} ds ON ds.fieldid=fs.id AND d.userid=ds.userid
                                                  JOIN {user_info_field} fc ON fc.shortname = \'powerschool777connection_id\'
                                                  JOIN {user_info_data} dc ON dc.fieldid=fc.id AND d.userid=dc.userid
                                                WHERE f.shortname = \'powerschool777local_id\' AND d.data=:id AND ds.data=\'staff\' AND dc.data=:connection_id
                        ', array('id' => $section->teachernumber,'connection_id'=>$this->connection->id));
        }

        if(isset($user_record->userid) && $user_record->userid>0){
            $enroll_plug->enrol_user($instance, $user_record->userid, $this->sync_params->teacher_lead_role);
            if($DB->record_exists('groups', array('id'=>$user_record->groupid)))
                groups_add_member($user_record->groupid,$user_record->userid);
        }
    }

    public function enroll_student($enroll_plug, $instance, $userid, $course_id, $sectin_enrollment_id, $groupid, $entry_date, $exit_date){
        global $DB,$CFG;

        if(self::get_coursetype_from_courseid($course_id) == 'groups'){
            require_once ($CFG->dirroot.'/group/lib.php');
            $enroll_plug->enrol_user($instance, $userid, $this->sync_params->student_role);

            if(!$DB->record_exists('powerschool_enrollments', array('courseid'=>$course_id,'userid'=>$userid,'penrollid'=>$sectin_enrollment_id, 'groupid'=>$groupid))){
                $new_record = new stdClass();
                $new_record->courseid = $course_id;
                $new_record->userid = $userid;
                $new_record->penrollid = $sectin_enrollment_id;
                $new_record->groupid = $groupid;
                $DB->insert_record('powerschool_enrollments',$new_record,false);
            }
            if($DB->record_exists('groups', array('id'=>$groupid)))
                groups_add_member($groupid,$userid);
        }else{
            $enroll_plug->enrol_user($instance, $userid, $this->sync_params->student_role, strtotime($entry_date), strtotime($exit_date));

            if(!$DB->record_exists('powerschool_enrollments', array('courseid'=>$course_id,'userid'=>$userid,'penrollid'=>$sectin_enrollment_id))){
                $new_record = new stdClass();
                $new_record->courseid = $course_id;
                $new_record->userid = $userid;
                $new_record->penrollid = $sectin_enrollment_id;
                $DB->insert_record('powerschool_enrollments',$new_record,false);
            }

        }

        if($this->sync_params->parent_role > 0 && $this->sync_params->parent_course_role > 0){
            $roleusers = get_role_users($this->sync_params->parent_role, context_user::instance($userid), false, 'u.id, u.lastname, u.firstname');

            foreach($roleusers as $user){
                $user_exist = $DB->record_exists_sql('SELECT u.id
                                                        FROM {user} u
                                                          JOIN {user_info_field} fs ON fs.shortname = \'powerschool777usertype\'
                                                          JOIN {user_info_data} ds ON ds.fieldid=fs.id AND u.id=ds.userid
                                                          JOIN {user_info_field} fc ON fc.shortname = \'powerschool777connection_id\'
                                                          JOIN {user_info_data} dc ON dc.fieldid=fc.id AND u.id=dc.userid
                                                        WHERE u.id=:userid AND ds.data=\'guardian\' AND dc.data=:connection_id
                        ', array('userid' => $user->id, 'connection_id' => $this->connection->id));
                if($user_exist){
                    if(self::get_coursetype_from_courseid($course_id) == 'groups'){
                        $enroll_plug->enrol_user($instance, $user->id, $this->sync_params->parent_course_role);
                        groups_add_member($groupid, $user->id);
                    }else{
                        $enroll_plug->enrol_user($instance, $user->id, $this->sync_params->parent_course_role, strtotime($entry_date), strtotime($exit_date));
                    }
                }
            }
        }
        return true;
    }

    public function unenroll_student($enroll_plug, $instance, $userid, $penrollid){
        global $DB,$CFG;
        $record = $DB->get_record('powerschool_enrollments',array('penrollid'=>$penrollid));

        if(self::get_coursetype_from_courseid($record->courseid) == 'groups'){
            require_once ($CFG->dirroot.'/group/lib.php');

            $DB->delete_records('powerschool_enrollments',array('penrollid'=>$penrollid));
            groups_remove_member($record->groupid, $record->userid);

            $all_user_groups = groups_get_user_groups($record->courseid, $record->userid);
            $count = count($all_user_groups[0]);
            if($count == 0)
                $enroll_plug->unenrol_user($instance, $userid);
        }else{
            $enroll_plug->unenrol_user($instance, $userid);
            $DB->delete_records('powerschool_enrollments',array('penrollid'=>$penrollid));
        }

        if($this->sync_params->parent_role > 0 && $this->sync_params->parent_course_role > 0){
            $roleusers = get_role_users($this->sync_params->parent_role, context_user::instance($userid), false, 'u.id, u.lastname, u.firstname');

            foreach($roleusers as $user){
                $user_exist = $DB->record_exists_sql('SELECT u.id
                                                        FROM {user} u
                                                          JOIN {user_info_field} fs ON fs.shortname = \'powerschool777usertype\'
                                                          JOIN {user_info_data} ds ON ds.fieldid=fs.id AND u.id=ds.userid
                                                          JOIN {user_info_field} fc ON fc.shortname = \'powerschool777connection_id\'
                                                          JOIN {user_info_data} dc ON dc.fieldid=fc.id AND u.id=dc.userid
                                                        WHERE u.id=:userid AND ds.data=\'guardian\' AND dc.data=:connection_id
                        ', array('userid' => $user->id, 'connection_id' => $this->connection->id));
                if($user_exist){
                    if(self::get_coursetype_from_courseid($record->courseid) == 'groups'){
                        groups_remove_member($record->groupid, $user->id);
                        if($count == 0)
                            $enroll_plug->unenrol_user($instance, $user->id);
                    }else{
                        $enroll_plug->unenrol_user($instance, $user->id);
                    }
                }
            }
        }
    }

    private function execute_section_modules($section, $course){
        global $DB;
        $terms_firstday = date('Y-m-d',$section->terms_firstday);
        if($section->gradebooktype == 1){
            $pages = ceil($this->request('get', '/ws/schema/table/pgassignments/count?q=sectionid==' . $section->section_dcid . ';datedue=ge=' . $terms_firstday)->count / $this->metadata->metadata->schema_table_query_max_page_size);

            for($i = 1; $i <= $pages; $i++){
                $assignments = $this->request('get', "/ws/schema/table/pgassignments/?q=sectionid==$section->section_dcid;datedue=ge=$terms_firstday&page=$i&pagesize=" . $this->metadata->metadata->schema_table_query_max_page_size . "&projection=ID,AssignmentID,Name,Abbreviation,PGCategoriesID,DateDue,PointsPossible,Weight,Description");

                foreach($assignments->record as $assignment){
                    $assignment = $assignment->tables->pgassignments;

                    $data = new stdClass();
                    $data->assignment_id = $assignment->dcid;
                    $data = json_encode($data);

                    $item = $this->request('post', '/ws/schema/query/get.section.assignment', array(), $data);
                    $item = array_shift($item->record);
                    $item = $item->tables->sync_pgassignmentsmap;

                    if($this->assignment_exist($item))
                        $this->execute_ps_assignment($item, $assignment);
                }
            }
        }elseif($section->gradebooktype == 2){
            $response = $this->request('get',"/ws/schema/table/assignmentsection/?q=duedate=ge=$terms_firstday;sectionsdcid==$section->section_id&page=1&projection=assignmentid,sectionsdcid&pagesize=".$this->metadata->metadata->course_max_page_size);
            foreach($response->record as $record){
                $assignmentid = $record->tables->assignmentsection->assignmentid;
                $assignmentcategoryassoc = $this->request('get',"/ws/schema/table/assignmentcategoryassoc/?q=assignmentsectionid==$assignmentid&page=1&projection=teachercategoryid&pagesize=1");

                if(!empty($assignmentcategoryassoc->record)){
                    $item = new stdClass();
                    $item->assignment_id = $assignmentid;
                    $item->section_id = $record->tables->assignmentsection->sectionsdcid;
                    $item->categoryid = $assignmentcategoryassoc->record[0]->tables->assignmentcategoryassoc->teachercategoryid;

                    if($this->assignment_exist($item))
                        $this->execute_ps_assignment($item, null, true);
                }
            }
        }

        $modules = $DB->get_records_sql("SELECT cm.id as cmid, cm.course as courseid
                                         FROM {course_modules} cm
                                            LEFT JOIN {powerschool_assignment_sync} pas ON pas.courseid=cm.course AND pas.cmid=cm.id
                                         WHERE pas.id IS NULL AND cm.course=:courseid"
                                        , array('courseid'=>$course->id));
        foreach($modules as $module){
            $this->create_assignment_sync($module);
        }
    }

    public function create_assignment_sync($module){
        global $DB;

        if($DB->record_exists('powerschool_assignment_sync',array('cmid'=>$module->cmid, 'courseid'=>$module->courseid)))
            return true;

        $record = new stdClass();
        $record->cmid = $module->cmid;
        $record->courseid = $module->courseid;
        $record->sync = 0;
        return $DB->insert_record('powerschool_assignment_sync',$record,false);
    }

    public function create_assignment_from_sync($recordid){
        global $DB, $CFG;
        require_once ($CFG->dirroot.'/lib/grade/grade_item.php');
        require_once ($CFG->dirroot.'/lib/gradelib.php');
        require_once ($CFG->dirroot.'/lib/grade/constants.php');
        $records = $DB->get_records_sql('SELECT pa.id, pas.cmid, pas.courseid, pa.assignmentid, pas.sync, pa.assignmentsectionid, cat.section_id, cat.power_cat
                                        FROM {powerschool_assignment_sync} pas
                                          LEFT JOIN {powerschool_assignment} pa ON pa.cmid=pas.cmid AND pa.courseid=pas.courseid
                                          LEFT JOIN {powerschool_assignment_cat} cat ON cat.moodle_mod=pas.cmid AND cat.section_id=pa.section_id
                                        WHERE pas.id=:id',array('id'=>$recordid));
        
        foreach ($records as $record) {
            if ($record->sync && empty($record->assignmentid)) {
                $this->create_assignment((object)array('courseid' => $record->courseid, 'cmid' => $record->cmid));
            } elseif ($record->sync) {
                $this->update_assignment((object)array('courseid' => $record->courseid, 'cmid' => $record->cmid));
            }

            $modinfo = get_fast_modinfo($record->courseid);
            $cm = $modinfo->get_cm($record->cmid);

            if($item = $DB->get_record($cm->modname, array('id'=>$cm->instance))){
                $gradecat = $this->get_grade_category($record->section_id,$record->power_cat,$record->courseid);
                if($gradecat>0){
                    $grade_item = new grade_item(array('courseid'=>$record->courseid, 'itemtype'=>'mod', 'itemmodule'=>$cm->modname,'iteminstance'=>$cm->instance, 'outcomeid'=>null));
                    $grade_item->categoryid = $gradecat;
                    $grade_item->update();
                }
            }
        }
    }

    public function create_assignment($module,$groups = array()){
        global $DB,$CFG;

        if(!$this->cm_assignment_sync($module))
            return true;

        $modinfo = get_fast_modinfo($module->courseid);
        $cm = $modinfo->get_cm($module->cmid);
        $assignment_sync = $DB->get_record('powerschool_assignment_sync', array('cmid'=>$module->cmid, 'courseid'=>$module->courseid));

        $instance = $DB->get_record($cm->modname,array('id'=>$cm->instance));
        $desc = substr(clean_param($instance->intro,PARAM_NOTAGS), 0, 4000);

        $sections = array();
        if(self::get_coursetype_from_courseid($module->courseid) == 'groups'){
            if(empty($groups))
                $groups = $this->get_module_groups(json_decode($cm->availability),$cm);
            if(!empty($groups))
                $sections = $DB->get_records_sql('SELECT cat.courseid as psection, cat.catid as groupid, cf.gradebooktype, cf.section_dcid, cf.section_id, cf.teacher_user_dcid, cf.term_id, cf.course_type
                                                    FROM {powerschool_course_cat} cat
                                                      LEFT JOIN {powerschool_course_fields} cf ON cf.courseid=cat.courseid_group AND cf.section_id=cat.courseid
                                                  WHERE cat.catid IN ('.implode(',',$groups).') AND cf.connection_id=:connection_id GROUP BY cat.courseid',array('connection_id'=>$this->connection->id));
        }else{
            $sections[] = $DB->get_record_sql('SELECT c.psection, cf.gradebooktype, cf.section_dcid, cf.section_id, cf.teacher_user_dcid, cf.term_id, cf.course_type
                                                FROM {powerschool_courses} c
                                                  LEFT JOIN {powerschool_course_fields} cf ON cf.courseid=c.mcourse
                                                WHERE c.mcourse=:mcourse AND cf.connection_id=:connection_id',array('mcourse'=>$module->courseid,'connection_id'=>$this->connection->id));
        }

        foreach($sections as $section){
            if($section->gradebooktype == 1){
                if (!$grade_item = grade_item::fetch(array('itemtype'=>'mod', 'itemmodule'=>$cm->modname, 'iteminstance'=>$instance->id, 'courseid'=>$module->courseid, 'outcomeid'=>null)))
                    continue;

                $assignment = new stdClass();
                $assignment->name = $cm->name;
                $assignment->abbreviation = $cm->name;
                $assignment->description = (!empty($desc))?$desc:$cm->name;
                $assignment->date_assignment_due = $this->get_due_date($instance,$cm);
                $assignment->include_final_grades = ($assignment_sync->count_in_final_grade == 1)?true:false;

                if($grade_item->gradetype == 2){
                    $scale = $DB->get_record('scale', array('id' => $grade_item->scaleid));
                    $scales = count(explode(",", $scale->scale));

                    $assignment->scoring_type = 'LETTERGRADE';
                    $assignment->points_possible = $scales;
                }else{
                    $assignment->points_possible = $grade_item->grademax;
                }

                $data = new stdClass();
                $data->assignment = $assignment;
                $data = json_encode($data);

                list($response, $header) = $this->request('post', '/powerschool-ptg-api/v2/section/' . $section->psection . '/assignment', array(), $data, true);
                if(empty($response)){
                    $location = explode('/', explode("\r\n", $header)[3]);
                    $id = array_pop($location);
                }else{
                    throw new moodle_exception($response->errorMessage->message, 'local_powerschool', new moodle_url('/admin/'), null, print_r($response, true));
                }
            }elseif($section->gradebooktype == 2){
                if (!$grade_item = grade_item::fetch(array('itemtype'=>'mod', 'itemmodule'=>$cm->modname, 'iteminstance'=>$instance->id, 'courseid'=>$module->courseid, 'outcomeid'=>null)))
                    continue;

                if(!empty($CFG->enableoutcomes) and plugin_supports('mod', $cm->modname, FEATURE_GRADE_OUTCOMES, true)){
                    $items = grade_item::fetch_all(array('itemtype' => 'mod', 'itemmodule' => $cm->modname, 'iteminstance' => $instance->id, 'courseid' => $module->courseid));
                    $_assignmentstandardassociations = array();

                    if(!empty($items)){
                        foreach($items as $item){
                            if(!empty($item->outcomeid)){
                                $outcome_record = $DB->get_record('powerschool_standards', array('outcomeid' => $item->outcomeid));

                                $assignmentstandardassociations = new stdClass();
                                $assignmentstandardassociations->standardid = $outcome_record->standardid;
                                $_assignmentstandardassociations[] = $assignmentstandardassociations;
                            }
                        }
                    }
                }

                $this->update_assignment_power_cat($section->psection,$cm,$section);

                $assignment = new stdClass();
                $assignment->name = $cm->name;
                $assignment->sectionsdcid = $section->psection;
                $assignment->abbreviation = $cm->name;
                $assignment->description = (!empty($desc))?$desc:$cm->name;
                $assignment->duedate = $this->get_due_date($instance,$cm);
                $assignment->isscorespublish = true;
                $assignment->iscountedinfinalgrade = ($assignment_sync->count_in_final_grade == 1)?true:false;

                if($grade_item->gradetype == 2){
                    $scale = $DB->get_record('scale', array('id' => $grade_item->scaleid));
                    $scales = count(explode(",", $scale->scale));

                    $assignment->scoretype = 'GRADESCALE';
                    $assignment->scoreentrypoints = $scales;
                    $assignment->totalpointvalue = $scales;
                }else{
                    $assignment->scoretype = 'POINTS';
                    $assignment->scoreentrypoints = $grade_item->grademax;
                    $assignment->totalpointvalue = $grade_item->grademax;
                }

                $_assignmentcategoryassociations = new stdClass();
                $_assignmentcategoryassociations->teachercategoryid = $this->get_assignment_power_cat($section->psection,$cm->id,$section);
                $_assignmentcategoryassociations->isprimary = true;
                $assignment->_assignmentcategoryassociations = array($_assignmentcategoryassociations);

                $data = new stdClass();
                $data->_name =  $cm->name;
                $data->_assignmentsections = array($assignment);
                $data->standardscoringmethod = 'GradeScale';

                if(!empty($_assignmentstandardassociations))
                    $data->_assignmentstandardassociations = $_assignmentstandardassociations;
                $data = json_encode($data);

                list($response, $header) = $this->request('post', '/ws/xte/section/assignment?users_dcid='.$section->teacher_user_dcid, array(), $data, true);

                if($this->curl->info['http_code'] == 200 || $this->curl->info['http_code'] == 201){
                    $location = explode('/', explode("\r\n", $header)[6]);
                    if(empty($location[0]))
                        $location = explode('/', explode("\r\n", $response)[6]);
                    $id = array_pop($location);
                }else{
                    throw new moodle_exception($response->errorMessage->message, 'local_powerschool', new moodle_url('/admin/'), null, print_r($response, true));
                }

                list($response, $header) = $this->request('get', '/ws/xte/section/assignment/'.$id.'?users_dcid='.$section->teacher_user_dcid, array(), '', true);
                $this->create_assignment_standardassoc($response);
                $assid = $response->_assignmentsections[0]->assignmentsectionid;
            }



            $record = new stdClass();
            $record->cmid = $module->cmid;
            $record->courseid = $module->courseid;
            $record->assignmentid = $id;
            $record->assignmentsectionid = (isset($assid))?$assid:0;
            $record->section_id = $section->section_id;
            if($section->course_type == 'groups'){
                $record->groupid = $section->groupid;
            }
            $this->assignment_updated[$id] = true;

            $DB->insert_record('powerschool_assignment', $record, false);

            //$this->export_grades($cm);
        }
    }

    public function update_assignment($module){
        global $DB,$CFG;

        if(!$this->cm_assignment_sync($module))
            return true;

        $modinfo = get_fast_modinfo($module->courseid);
        $cm = $modinfo->get_cm($module->cmid);
        $assignment_sync = $DB->get_record('powerschool_assignment_sync', array('cmid'=>$module->cmid, 'courseid'=>$module->courseid));
        $course_as_group = (self::get_coursetype_from_courseid($module->courseid) == 'groups')?true:false;

        if($course_as_group){
            $this->check_availability($cm,$module);
            $assignment_record = $DB->get_records_sql('SELECT ps.*, pcf.gradebooktype, pcf.teacher_user_dcid, ps.section_id as psection, pcf.term_id
                                                       FROM {powerschool_assignment} ps 
                                                        LEFT JOIN {powerschool_course_cat} pcc ON pcc.catid=ps.groupid
                                                        LEFT JOIN {powerschool_course_fields} pcf ON pcf.section_id=pcc.courseid
                                                       WHERE ps.cmid=:cmid AND pcf.connection_id=:connection_id GROUP BY ps.id',array('cmid'=>$cm->id,'connection_id'=>$this->connection->id));
        }else{
            $this->check_availability($cm,$module);
            $assignment_record = $DB->get_records_sql('SELECT ps.*, pcf.gradebooktype, pcf.teacher_user_dcid, pcf.section_id as psection, pcf.term_id
                                                       FROM {powerschool_assignment} ps 
                                                        LEFT JOIN {powerschool_course_fields} pcf ON pcf.courseid=ps.courseid
                                                       WHERE ps.cmid=:cmid AND pcf.connection_id=:connection_id',array('cmid'=>$cm->id,'connection_id'=>$this->connection->id));
        }

        if(empty($assignment_record) && $course_as_group){
            $this->check_availability($cm,$module);
        }elseif(empty($assignment_record)){
            return true;
        }

        $instance = $DB->get_record($cm->modname,array('id'=>$cm->instance));
        $intro = optional_param_array('introeditor',array(),PARAM_RAW);
        if(!empty($intro)){
            $desc = (isset($intro['text']))?clean_param($intro['text'],PARAM_NOTAGS):'';
        }else{
            $desc = clean_param($instance->intro,PARAM_NOTAGS);
        }
        $desc = substr($desc, 0, 4000);
        $due_date = $this->get_due_date($instance,$cm,false);

        foreach($assignment_record as $record) {
            if(isset($this->assignment_updated[(int)$record->assignmentid]))
                continue;

            if($record->gradebooktype == 1){
                if (!$grade_item = grade_item::fetch(array('itemtype'=>'mod', 'itemmodule'=>$cm->modname, 'iteminstance'=>$instance->id, 'courseid'=>$module->courseid, 'outcomeid'=>null)))
                    continue;

                $assignment = new stdClass();
                $assignment->name = $cm->name;
                if(!empty($desc))
                    $assignment->description = $desc;
                if($due_date > 0)
                    $assignment->date_assignment_due = $due_date;
                $assignment->include_final_grades = ($assignment_sync->count_in_final_grade == 1)?true:false;

                if($grade_item->gradetype == 2){
                    $scale = $DB->get_record('scale', array('id' => $grade_item->scaleid));
                    $scales = count(explode(",", $scale->scale));

                    $assignment->scoring_type = 'LETTERGRADE';
                    $assignment->points_possible = $scales;
                }else{
                    $assignment->points_possible = $grade_item->grademax;
                }

                $data = new stdClass();
                $data->assignment = $assignment;

                list($response, $header) = $this->request('put', '/powerschool-ptg-api/v2/assignment/' . $record->assignmentid, array(), json_encode($data), true, false);

                if($this->curl->info['http_code'] == 404){
                    $DB->delete_records('powerschool_assignment',array('assignmentid'=>$record->assignmentid));

                    $sync = $DB->get_record('powerschool_assignment_sync',array('cmid'=>$module->cmid, 'courseid'=>$module->courseid));
                    $sync->sync = 0;
                    $DB->update_record('powerschool_assignment_sync',$sync);
                }elseif(!empty($response)){
                    throw new moodle_exception('test', 'local_powerschool', new moodle_url('/admin/'), null, print_r($header, true));
                }
            }elseif($record->gradebooktype == 2){
                $this->update_assignment_power_cat($record->psection,$cm,$record);
                $grade_item = grade_item::fetch(array('itemtype'=>'mod', 'itemmodule'=>$cm->modname, 'iteminstance'=>$cm->instance, 'courseid'=>$cm->course, 'outcomeid'=>null));
                if(empty($grade_item))
                    continue;

                if(!empty($CFG->enableoutcomes) and plugin_supports('mod', $cm->modname, FEATURE_GRADE_OUTCOMES, true)){
                    $items = grade_item::fetch_all(array('itemtype' => 'mod', 'itemmodule' => $cm->modname, 'iteminstance' => $instance->id, 'courseid' => $module->courseid));
                    $_assignmentstandardassociations = array();

                    if(!empty($items)){
                        foreach($items as $item){
                            if(!empty($item->outcomeid)){
                                $outcome_record = $DB->get_record('powerschool_standards', array('outcomeid' => $item->outcomeid));

                                $assignmentstandardassociations = new stdClass();
                                $assignmentstandardassociations->standardid = $outcome_record->standardid;
                                $_assignmentstandardassociations[] = $assignmentstandardassociations;
                            }
                        }
                    }
                }

                $assignment = new stdClass();
                $assignment->assignmentsectionid = (int)$record->assignmentsectionid;
                $assignment->name = $cm->name;
                $assignment->scoretype = ($grade_item->gradetype == 2)?'GRADESCALE':'POINTS';
                $assignment->scoreentrypoints = $grade_item->grademax;
                $assignment->totalpointvalue = $grade_item->grademax;
                $assignment->iscountedinfinalgrade = ($assignment_sync->count_in_final_grade == 1)?true:false;

                if(!empty($desc))
                    $assignment->description = $desc;
                if($due_date > 0)
                    $assignment->duedate = $due_date;

                $_assignmentcategoryassociations = new stdClass();
                $_assignmentcategoryassociations->teachercategoryid = $this->get_assignment_power_cat($record->psection,$cm->id,$record);
                $_assignmentcategoryassociations->isprimary = true;
                $assignment->_assignmentcategoryassociations = array($_assignmentcategoryassociations);

                $data = new stdClass();
                $data->_id =  (int)$record->assignmentid;
                $data->_name =  $cm->name;
                $data->_assignmentsections = array($assignment);
                $data->standardscoringmethod = 'GradeScale';
                if(!empty($_assignmentstandardassociations))
                    $data->_assignmentstandardassociations = $_assignmentstandardassociations;
                $data = json_encode($data);

                list($response, $header) = $this->request('put', '/ws/xte/section/assignment/'.$record->assignmentid.'?update_scores=true&users_dcid='.$record->teacher_user_dcid, array(), $data, true, false);

                if($this->curl->info['http_code'] == 400){
                    $DB->delete_records('powerschool_assignment',array('assignmentid'=>$record->assignmentid));

                    $sync = $DB->get_record('powerschool_assignment_sync',array('cmid'=>$module->cmid, 'courseid'=>$module->courseid));
                    $sync->sync = 0;
                    $DB->update_record('powerschool_assignment_sync',$sync);
                }elseif(!empty($response) && $this->curl->info['http_code'] != 200 && $this->curl->info['http_code'] != 100){
                    throw new moodle_exception(print_r($response, true), 'local_powerschool', new moodle_url('/admin/'), null, print_r($header, true));
                }

                $assignment = $this->request('get', '/ws/xte/section/assignment/' . $record->assignmentid.'?users_dcid='.$record->teacher_user_dcid);
                $this->create_assignment_standardassoc($assignment);
            }
            $this->assignment_updated[(int)$record->assignmentid] = true;

            //$this->export_grades($cm);
        }
        return true;
    }

    public function delete_assignment($module,$groups = array()){
        global $DB;

        if(!$this->cm_assignment_sync($module) && empty($groups)){
            $DB->delete_records('powerschool_assignment', array('cmid' => $module->cmid));
            $DB->delete_records('powerschool_assignment_sync',array('cmid'=>$module->cmid));
            $DB->delete_records('powerschool_assignment_cat',array('moodle_mod'=>$module->cmid));
            return true;
        }elseif(!$this->cm_assignment_sync($module)){
            return true;
        }

        if(empty($groups)){
            if(self::get_coursetype_from_courseid($module->courseid) == 'groups'){
                $assignment_record = $DB->get_records_sql('SELECT ps.*, pcf.gradebooktype, pcf.teacher_user_dcid
                                                           FROM {powerschool_assignment} ps 
                                                            LEFT JOIN {powerschool_course_cat} pcc ON pcc.catid=ps.groupid
                                                            LEFT JOIN {powerschool_course_fields} pcf ON pcf.section_id=pcc.courseid
                                                           WHERE ps.cmid=:cmid AND pcf.connection_id=:connection_id', array('cmid' => $module->cmid,'connection_id'=>$this->connection->id));
            }else{
                $assignment_record = $DB->get_records_sql('SELECT ps.*, pcf.gradebooktype, pcf.teacher_user_dcid
                                                           FROM {powerschool_assignment} ps 
                                                            LEFT JOIN {powerschool_course_fields} pcf ON pcf.courseid=ps.courseid
                                                           WHERE ps.cmid=:cmid AND pcf.connection_id=:connection_id', array('cmid' => $module->cmid,'connection_id'=>$this->connection->id));
            }

        }else{
            $assignment_record = $DB->get_records_sql('SELECT ps.*, pcf.gradebooktype, pcf.teacher_user_dcid
                                                       FROM {powerschool_assignment} ps 
                                                        LEFT JOIN {powerschool_course_cat} pcc ON pcc.catid=ps.groupid
                                                        LEFT JOIN {powerschool_course_fields} pcf ON pcf.section_id=pcc.courseid
                                                       WHERE ps.cmid=:cmid AND ps.groupid IN ('.implode(',',$groups).') AND pcf.connection_id=:connection_id',array('cmid'=>$module->cmid,'connection_id'=>$this->connection->id));
        }

        if(empty($assignment_record))
            return;

        foreach($assignment_record as $record){
            if($record->gradebooktype == 1){
                list($response, $header) = $this->request('delete', '/powerschool-ptg-api/v2/assignment/' . $record->assignmentid, array(), '', true);
            }elseif($record->gradebooktype == 2){
                list($response, $header) = $this->request('delete', '/ws/xte/section/assignment/'.$record->assignmentid.'?force=true&users_dcid='.$record->teacher_user_dcid, array(), '', true);
            }


            if(empty($groups)) {
                $DB->delete_records('powerschool_assignment', array('cmid' => $module->cmid));
                $DB->delete_records('powerschool_assignment_sync',array('cmid'=>$module->cmid));
                $DB->delete_records('powerschool_assignment_cat',array('moodle_mod'=>$module->cmid));
            }else{
                $DB->execute('DELETE FROM {powerschool_assignment} WHERE groupid IN ('.implode(',',$groups).') AND cmid=:cmid',array('cmid'=>$module->cmid));
            }

            if(!empty($response))
                throw new moodle_exception('test', 'local_powerschool', new moodle_url('/admin/'), null, print_r($response,true));
        }

        return true;
    }

    private function create_assignment_standardassoc($responce){
        global $DB;

        if(!isset($responce->_assignmentstandardassociations) || empty($responce->_assignmentstandardassociations))
            return true;

        foreach($responce->_assignmentstandardassociations as $assignmentstandardassociation){
            if($record = $DB->get_record('powerschool_standard_assoc',array('assignmentid'=>$assignmentstandardassociation->assignmentid, 'standardid'=>$assignmentstandardassociation->standardid))){
                $record->assignmentstandardassocid = $assignmentstandardassociation->assignmentstandardassocid;
                $DB->update_record('powerschool_standard_assoc',$record);
            }else{
                $record = new stdClass();
                $record->assignmentid = $assignmentstandardassociation->assignmentid;
                $record->standardid = $assignmentstandardassociation->standardid;
                $record->assignmentstandardassocid = $assignmentstandardassociation->assignmentstandardassocid;
                $DB->insert_record('powerschool_standard_assoc',$record);
            }
        }

        return true;
    }

    private function get_due_date($instance, $cm, $default_time = true){
        if(isset($instance->duedate) && $instance->duedate>0)
            return date('Y-m-d',$instance->duedate);
        elseif(isset($instance->timeclose) && $instance->timeclose > 0)
            return date('Y-m-d',$instance->timeclose);
        elseif(isset($cm->completionexpected) && $cm->completionexpected > 0)
            return date('Y-m-d',$cm->completionexpected);
        elseif($default_time)
            return date('Y-m-d');
        else
            return 0;
    }

    private function get_assignment_power_cat($section_id, $cm_id,$section){
        global $DB;
        $record = $DB->get_record('powerschool_assignment_cat',array('section_id'=>$section_id,'moodle_mod'=>$cm_id),'power_cat');
        if(isset($record->power_cat))
            return $record->power_cat;
        else{
            $response = $this->request('get', '/ws/xte/teacher_category?users_dcid='.$section->teacher_user_dcid.'&term_id='.$section->term_id);
            $first_cat = array_shift($response);
            return (isset($first_cat->_id))?$first_cat->_id:null;
        }
    }

    private function update_assignment_power_cat($section_id, $cm, $section){
        global $DB;
        $new_cat = optional_param('gradecat','0',PARAM_INT);

        if($new_cat>0){
            $assignment_record = $DB->get_record('powerschool_assignment_cat', array('section_id'=>$section_id,'moodle_mod'=>$cm->id));
            $cat_record = $DB->get_record('powerschool_grade_cat',array('section_id'=>$section_id, 'moodle_cat'=>$new_cat, 'courseid'=>$cm->course));
            if(isset($assignment_record->id) && isset($cat_record->id)){
                $assignment_record->power_cat = $cat_record->power_cat;
                return $DB->update_record('powerschool_assignment_cat',$assignment_record);
            }elseif(isset($cat_record->id)){
                $assignment_record = new stdClass();
                $assignment_record->power_cat = $cat_record->power_cat;
                $assignment_record->section_id = $section_id;
                $assignment_record->moodle_mod = $cm->id;
                $assignment_record->timecreate = time();
                return $DB->insert_record('powerschool_assignment_cat',$assignment_record);
            }
        }
        return true;
    }

    protected function export_grades($cm){
        global $CFG;
        require_once("$CFG->libdir/gradelib.php");
        $users = get_enrolled_users(context_module::instance($cm->id),'', 0, 'u.id');
        $grade_items = grade_item::fetch_all(array('itemtype'=>'mod', 'itemmodule'=>$cm->modname, 'iteminstance'=>$cm->instance, 'courseid'=>$cm->course));

        foreach($grade_items as $grade_item){
            $grade_grades = grade_grade::fetch_users_grades($grade_item, array_keys($users), true);
            foreach($grade_grades as $grade_grade){
                $grade_grade->grade_item = $grade_grade->load_grade_item();
                $this->graded_assignments(null,$grade_grade);
            }
        }
    }

    public function get_module_groups($availability, $cm){
        $groups = array();
        if(empty($availability))
            return $groups;

        $all_groups = array_keys(groups_get_activity_allowed_groups($cm));

        if(strpos($availability->op,'!') !== false)
            $groups = $all_groups;

        foreach($availability->c as $child){
            if(isset($child->type) && $child->type == 'group'){
                if(strpos($availability->op,'!') === false) {
                    if (isset($child->id))
                        $groups[] = $child->id;
                    else
                        $groups = array_merge($groups, $all_groups);
                }else{
                    if (isset($child->id))
                        unset($groups[array_search($child->id,$groups)]);
                    else
                        $groups = array();
                }
            }
        }

        if(empty($groups)){
            foreach($availability->c as $child){
                if(isset($child->op)){
                    $groups = $this->get_module_groups($child, $cm);
                    if(!empty($groups)){
                        break;
                    }
                }
            }
        }
        $groups = array_unique($groups);

        return $groups;
    }

    private function check_availability($cm,$module){
        global $DB;
        if(!$this->cm_assignment_sync($module))
            return true;

        $availabilityconditionsjson = optional_param('availabilityconditionsjson','',PARAM_RAW);
        if(empty($availabilityconditionsjson)){
            $data = $DB->get_records('powerschool_assignment',array('cmid'=>$module->cmid, 'courseid'=>$module->courseid));
            $old_groups = array();
            foreach ($data as $item){
                $old_groups[] = $item->groupid;
            }

            $new_groups = $this->get_module_groups(json_decode($cm->availability),$cm);
        }else{
            $old_groups = $this->get_module_groups(json_decode($cm->availability),$cm);
            $new_groups = $this->get_module_groups(json_decode($availabilityconditionsjson),$cm);
        }

        $need_delete = array_diff($old_groups,$new_groups);
        $need_create = array_diff($new_groups,$old_groups);

        if(!empty($need_create))
            $this->create_assignment($module,$need_create);

        if(!empty($need_delete))
            $this->delete_assignment($module,$need_delete);

        return true;

    }

    public function cm_assignment_sync($module){
        global $DB;
        $record = $DB->get_record('powerschool_assignment_sync',array('cmid'=>$module->cmid,'courseid'=>$module->courseid));
        return (isset($record->sync))?$record->sync:false;
    }

    public function sync_assignment_from_ps($section_id, $items){
        global $DB;
        $section = $DB->get_record('powerschool_course_fields',array('section_id'=>$section_id,'connection_id'=>$this->connection->id));
        $power_pro = ($section->gradebooktype == 2)?true:false;
        foreach($items as $assignmentid=>$mod_data){
            if(empty($mod_data['module']))
                continue;

            $item = new stdClass();
            $item->assignment_id = $assignmentid;
            $item->section_id = $section_id;
            $item->mod_name = $mod_data['module'];
            $item->mod_topic = $mod_data['topic'];
            $this->execute_ps_assignment($item,null,$power_pro);
        }
    }

    private function execute_ps_assignment($item,$assignment = null,$power_pro = false){
        global $DB,$PAGE, $CFG;

        if(!$assignment && !$power_pro){
            $assignment = $this->request('get', '/powerschool-ptg-api/v2/assignment/' . $item->assignment_id)->assignment;
            $item->assignmentsectionid = $item->assignment_id;
        }elseif(!$assignment && $power_pro){
            $teacher_id = $DB->get_record('powerschool_course_fields',array('section_id'=>$item->section_id,'connection_id'=>$this->connection->id),'teacher_user_dcid')->teacher_user_dcid;
            $assignment = $this->request('get', '/ws/xte/section/assignment/' . $item->assignment_id.'?users_dcid='.$teacher_id)->_assignmentsections[0];
            $assignment->datedue = $assignment->duedate;
            $item->assignmentsectionid = $assignment->assignmentsectionid;
            $item->teachercategoryid = $assignment->_assignmentcategoryassociations[0]->teachercategoryid;
        }

        $course_type = $DB->get_record('powerschool_course_fields',array('section_id'=>$item->section_id,'connection_id'=>$this->connection->id),'course_type')->course_type;
        if($course_type == 'groups'){
            $info = $DB->get_record_sql('SELECT pcc.catid as groupid, pcc.courseid_group as courseid
                                          FROM {powerschool_course_cat} pcc 
                                            LEFT JOIN {powerschool_course_fields} cf ON cf.courseid=pcc.courseid_group
                                          WHERE pcc.courseid=:courseid AND cf.connection_id=:connection_id LIMIT 1',array('courseid'=>$item->section_id,'connection_id'=>$this->connection->id));
            if(empty($info))
                return true;

            $availability = new stdClass();
            $availability->op = '&';
            $availability->c = array();
            $availability->c[0] = new stdClass();
            $availability->c[0]->type = 'group';
            $availability->c[0]->id = (int)$info->groupid;
            $availability->showc = array();
            $availability->showc[0] = true;
            $availability = json_encode($availability);

            $passignment = $this->assignment_exist($item,$assignment,$info->courseid,$info->groupid);
        }else{
            $info = $DB->get_record_sql('SELECT pc.id, pc.mcourse as courseid
                                          FROM {powerschool_courses} pc 
                                            LEFT JOIN {powerschool_course_fields} cf ON cf.courseid=pc.mcourse
                                          WHERE pc.psection=:psection AND cf.connection_id=:connection_id',array('psection'=>$item->section_id,'connection_id'=>$this->connection->id));
            if(empty($info))
                return true;
            $passignment = $this->assignment_exist($item,$assignment,$info->courseid);
        }

        $course = get_course($info->courseid);
        $PAGE->set_context(context_course::instance($info->courseid));

        if(isset($passignment->cmid)){
            $cm = get_coursemodule_from_id('', $passignment->cmid, 0, true, MUST_EXIST);
            $mod = $DB->get_record('modules', array('id'=>$cm->module), '*', MUST_EXIST);

            if($mod->name == 'hvp'){ // currently unsupported module H5P, sorry
                return true;
            }

            if($record = $DB->get_record($cm->modname, array('id'=>$cm->instance))){
                $module = $record;
            }else{
                $module = new stdClass();
            }

            $module->modulename = $cm->modname;
            $module->course = $course->id;
            $module->module = $mod->id;
            $module->coursemodule = $passignment->cmid;
            $module->name = $assignment->name;
            $module->introeditor = array();
            $module->introeditor['text'] = (isset($assignment->description))?$assignment->description:'';
            $module->introeditor['format'] = 0;
            $module->introeditor['itemid'] = 0;
            $module->quizpassword = '';
            $module->visible = $cm->visible;
            $module->cmidnumber = $cm->idnumber;
            $module->completionexpected = (isset($assignment->date_assignment_due))?strtotime($assignment->date_assignment_due):strtotime($assignment->datedue);
            if(empty($cm->availability) && isset($availability))
                $module->availability = $availability;
            elseif(!empty($cm->availability) && isset($availability))
                $module->availability = $this->get_availability_from_instance($cm->availability, (int)$info->groupid);
            if(isset($item->teachercategoryid))
                $module->gradecat     = $this->get_grade_category($item->section_id,$item->teachercategoryid,$course->id);

            if(isset($item->teachercategoryid) && $record = $DB->get_record('powerschool_assignment_cat',array('section_id'=>$item->section_id,'moodle_mod'=>$cm->id))){
                $record->power_cat = $item->teachercategoryid;
                $DB->update_record('powerschool_assignment_cat', $record);
            }
            if((isset($assignment->iscountedinfinalgrade) && $assignment->iscountedinfinalgrade == 1) || (isset($assignment->include_final_grades) && $assignment->include_final_grades == 1))
                $DB->execute('UPDATE {powerschool_assignment_sync} SET count_in_final_grade=1 WHERE cmid=:cmid AND courseid=:courseid',array('cmid'=>$cm->id,'courseid'=>$course->id));

            return update_moduleinfo($cm, $module, $course);
        }else{
            if(isset($item->mod_name) && !empty($item->mod_name))
                $mod_name = $item->mod_name;
            else
                return;

            $mod = $DB->get_record('modules', array('name'=>$mod_name), 'id', MUST_EXIST);

            $module = new stdClass();
            $module->modulename = $mod_name;
            $module->course = $course->id;
            $module->module = $mod->id;
            $module->section = (isset($item->mod_topic) && !empty($item->mod_topic))?$item->mod_topic:1;
            $module->name = $assignment->name;
            $module->introeditor = array();
            $module->introeditor['text'] = (isset($assignment->description))?$assignment->description:'';
            $module->introeditor['format'] = 0;
            $module->introeditor['itemid'] = 0;
            $module->quizpassword = '';
            $module->visible = 1;
            $module->completionexpected = (isset($assignment->date_assignment_due))?strtotime($assignment->date_assignment_due):strtotime($assignment->datedue);
            $module->instance         = '';
            $module->coursemodule     = '';
            $module->cmidnumber     = null;
            $module->availability     = (isset($availability))?$availability:null;
            $module->visibleoncoursepage     = 1;
            if(isset($item->teachercategoryid))
                $module->gradecat     = $this->get_grade_category($item->section_id,$item->teachercategoryid,$course->id);
            if(isset($assignment->points_possible))
                $module->grade = $assignment->points_possible;
            if(isset($assignment->totalpointvalue))
                $module->grade = $assignment->totalpointvalue;

            require_once($CFG->libdir."/gradelib.php");
            require_once($CFG->libdir."/plagiarismlib.php");
            require_once($CFG->dirroot."/mod/$mod_name/mod_form.php");
            $form_class = "mod_".$mod_name."_mod_form";
            $form = new $form_class($module,1,null,$course);


            $sweetsThief = Closure::bind(function ($form) {
                return $form->_form;
            }, null, $form);

            $data = $sweetsThief($form)->exportValues();
            $module = (object)array_merge($data,(array)$module);
            unset($module->feedbacktext);

            $instance = add_moduleinfo($module, $course);

            $record = new stdClass();
            $record->cmid = $instance->coursemodule;
            $record->courseid = $instance->course;
            $record->assignmentid = $item->assignment_id;
            $record->assignmentsectionid = $item->assignmentsectionid;
            $record->section_id = $item->section_id;
            if($course_type == 'groups'){
                $record->groupid = $info->groupid;
            }
            $DB->insert_record('powerschool_assignment',$record,false);

            if(isset($item->teachercategoryid)){
                $record = new stdClass();
                $record->section_id = $item->section_id;
                $record->moodle_mod = $instance->coursemodule;
                $record->power_cat = $item->teachercategoryid;
                $record->timecreate = time();
                $DB->insert_record('powerschool_assignment_cat', $record);
            }

            if((isset($assignment->iscountedinfinalgrade) && $assignment->iscountedinfinalgrade == 1) || (isset($assignment->include_final_grades) && $assignment->include_final_grades == 1))
                $DB->execute('UPDATE {powerschool_assignment_sync} SET count_in_final_grade=1 WHERE cmid=:cmid AND courseid=:courseid',array('cmid'=>$instance->coursemodule,'courseid'=>$instance->course));

            return $DB->execute('UPDATE {powerschool_assignment_sync} SET sync=1 WHERE cmid=:cmid AND courseid=:courseid',array('cmid'=>$instance->coursemodule,'courseid'=>$instance->course));
        }
    }

    private function get_availability_from_instance($availability, $groupid){
        $availability = json_decode($availability);
        foreach($availability->c as $item){
            if(($item->type == 'group' && !isset($item->id)) || ($item->type == 'group' && $item->id == $groupid))
                return json_encode($availability);
        }
        $instance = new stdClass();
        $instance->type = 'group';
        $instance->id = $groupid;
        $availability->c[] = $instance;
        $availability->showc[] = true;

        return json_encode($availability);
    }

    private function assignment_exist($item, $assignment = null, $courseid = null, $groupid = null){
        global $DB;

        $passignment = $DB->get_record_sql('SELECT pa.* 
                                                  FROM {powerschool_assignment} pa 
                                                    LEFT JOIN {powerschool_course_fields} pcf ON pcf.courseid=pa.courseid
                                                  WHERE pa.assignmentid=:assignmentid AND pcf.connection_id=:connectionn_id GROUP BY pa.id',array('assignmentid'=>$item->assignment_id,'connectionn_id'=>$this->connection->id));
        if(isset($passignment->id))
            return $passignment;

        return null;
    }

    public function import_gradescale($gradescaleid){
        global $DB,$CFG,$USER;
        require_once($CFG->libdir.'/grade/grade_scale.php');

        $mdl_gradescaleid = $DB->get_record_sql('SELECT s.id AS mdl_gradescale_id
                                                  FROM {powerschool_gradescale} pg
                                                    LEFT JOIN {scale} s ON s.id=pg.mdl_gradescale_id
                                                  WHERE pg.ps_gradescale_id=:ps_gradescale_id',array('ps_gradescale_id'=>$gradescaleid));

        $grade_scale = $this->request('get',"/ws/schema/table/gradescaleitem?q=DCID==$gradescaleid&projection=name,cutoffpercentage,DCID,ID,gradescaleid,description");
        $grade_scale = $grade_scale->record[0]->tables->gradescaleitem;

        $grade_scale_items = $this->request('get',"/ws/schema/table/gradescaleitem?q=gradescaleid=={$grade_scale->id}&projection=name,cutoffpercentage");
        $scales = array();
        if(!isset($grade_scale_items->record) || empty($grade_scale_items->record))
            return;

        foreach($grade_scale_items->record as $scale){
            $scales[] = $scale->tables->gradescaleitem->name;
        }

        $moodle_grade_scale = new grade_scale();
        if(isset($mdl_gradescaleid->mdl_gradescale_id) && !empty($mdl_gradescaleid->mdl_gradescale_id)){
            $moodle_grade_scale = new grade_scale(array('id'=>$mdl_gradescaleid->mdl_gradescale_id));
        }

        $moodle_grade_scale->userid = $USER->id;
        $moodle_grade_scale->courseid = 0;
        $moodle_grade_scale->name = $grade_scale->name;
        $moodle_grade_scale->description = $grade_scale->description;
        $moodle_grade_scale->timemodified = time();
        $moodle_grade_scale->scale = implode(',', $scales);

        if($moodle_grade_scale->id){
            $moodle_grade_scale->update();
        }else{
            $moodle_grade_scale->id = $moodle_grade_scale->insert();

            if($record = $DB->get_record('powerschool_gradescale',array('ps_gradescale_id'=>$gradescaleid))){
                $record->mdl_gradescale_id = $moodle_grade_scale->id;
                $DB->update_record('powerschool_gradescale',$record);
            }else{
                $mdl_gradescale_record = new stdClass();
                $mdl_gradescale_record->ps_gradescale_id = $gradescaleid;
                $mdl_gradescale_record->mdl_gradescale_id = $moodle_grade_scale->id;
                $DB->insert_record('powerschool_gradescale',$mdl_gradescale_record);
            }
        }

        return $moodle_grade_scale->id;
    }

    public function execute_course_standards($moodle_course,$section){
        global $DB,$CFG;

        if(empty($CFG->enableoutcomes) || !isset($this->sync_params->import_standards))
            return true;

        $data = new stdClass();
        $data->courses = array($section->original_course_number);
        $data->terms = array($section->term_id);
        $data->schoolid = $section->school_number;

        $response = $this->request('post', '/ws/schema/query/get.course_standard', array(), json_encode($data));
        if(!isset($response->record) || empty($response->record))
            return true;

        foreach($response->record as $standard){
            $standard = $standard->tables->standardcourseassoc;
            if(empty($standard->gradescaleitemdcid))
                continue;

            $outcomeid = $this->execute_standard($standard);

            if(!$DB->record_exists('grade_outcomes_courses', array('courseid' => $moodle_course->id, 'outcomeid' => $outcomeid))){
                $record = new stdClass();
                $record->courseid = $moodle_course->id;
                $record->outcomeid = $outcomeid;
                $DB->insert_record('grade_outcomes_courses', $record);
            }

        }
        return true;
    }

    public function import_standards(array $assignment_ids){
        global $DB;

        $data = new stdClass();
        $data->assignments = $assignment_ids;

        $response = $this->request('post', '/ws/schema/query/get.assignments_standard', array(), json_encode($data));
        foreach($response->record as $standard){
            $standard = $standard->tables->assignmentstandardassoc;
            $outcomeid = $this->execute_standard($standard);

            if($record = $DB->get_record('powerschool_standard_assoc',array('assignmentid'=>$standard->assignmentid, 'standardid'=>$standard->standardid))){
                $record->assignmentstandardassocid = $standard->assignmentstandardassocid;
                $DB->update_record('powerschool_standard_assoc',$record);
            }else{
                $record = new stdClass();
                $record->assignmentid = $standard->assignmentid;
                $record->standardid = $standard->standardid;
                $record->assignmentstandardassocid = $standard->assignmentstandardassocid;
                $DB->insert_record('powerschool_standard_assoc',$record);
            }

            $this->assign_asignment_outcome($standard->assignmentid, $outcomeid);
        }
        return;
    }

    private function execute_standard($standard){
        global $DB,$CFG,$USER;
        require_once $CFG->dirroot.'/grade/lib.php';

        $exist_standard = $DB->get_record_sql('SELECT ou.id AS outcomeid
                                                FROM {powerschool_standards} ps 
                                                  LEFT JOIN {grade_outcomes} ou ON ps.outcomeid=ou.id
                                                WHERE ps.standardid=:standardid',array('standardid'=>$standard->standardid));
        if(!empty($exist_standard->outcomeid) && in_array($standard->standardid,$this->standard_updated))
            return $exist_standard->outcomeid;

        $outcomeid = (isset($exist_standard->outcomeid) && !empty($exist_standard->outcomeid))?$exist_standard->outcomeid:0;
        $outcome = new grade_outcome(array('id'=>$outcomeid));
        $data = new stdClass();
        $data->usermodified = $USER->id;
        $data->description = $standard->description;
        $data->fullname = $standard->identifier.' - '.$standard->name;
        $data->shortname = $standard->identifier;
        $data->standard = 1;
        $data->scaleid = $this->import_gradescale($standard->gradescaleitemdcid);
        grade_outcome::set_properties($outcome, $data);

        if (empty($outcome->id)) {
            $outcomeid = $outcome->insert();

            if($record = $DB->get_record('powerschool_standards',array('standardid'=>$standard->standardid))){
                $record->outcomeid = $outcomeid;
                $DB->update_record('powerschool_standards',$record);
            }else{
                $record = new stdClass();
                $record->outcomeid = $outcomeid;
                $record->standardid = $standard->standardid;
                $DB->insert_record('powerschool_standards',$record);
            }
        } else {
            $outcome->update();
        }
        $this->standard_updated[] = $standard->standardid;

        return $outcomeid;
    }

    public function update_standards_from_sync(array $sync_records){
        global $DB;

        list($sql, $params) = $DB->get_in_or_equal($sync_records, SQL_PARAMS_NAMED, 'pas_id');
        $data = $DB->get_records_sql("SELECT pa.assignmentid
                                      FROM {powerschool_assignment_sync} pas
                                        LEFT JOIN {powerschool_assignment} pa ON pa.cmid=pas.cmid
                                      WHERE pas.id $sql", $params);
        $assignments = array();
        foreach($data as $datum){
            $assignments[] = $datum->assignmentid;
        }

        return $this->import_standards($assignments);
    }

    public function assign_asignment_outcome($assignmentid, $outcomeid){
        global $DB,$CFG,$USER;
        require_once($CFG->libdir.'/grade/grade_outcome.php');

        $passignment = $DB->get_record('powerschool_assignment',array('assignmentid'=>$assignmentid));
        if(!isset($passignment->cmid))
            return;

        $cm = get_coursemodule_from_id('', $passignment->cmid, 0, true, MUST_EXIST);

        $module = $DB->get_record($cm->modname, array('id'=>$cm->instance));
        if(empty($module) || !plugin_supports('mod', $cm->modname, FEATURE_GRADE_OUTCOMES, true))
            return;

        if(!$DB->record_exists('grade_outcomes_courses',array('courseid'=>$passignment->courseid, 'outcomeid'=>$outcomeid))){
            $record = new stdClass();
            $record->courseid = $passignment->courseid;
            $record->outcomeid = $outcomeid;
            $DB->insert_record('grade_outcomes_courses', $record);
        }

        $item = grade_item::fetch_all(array('itemtype'=>'mod', 'itemmodule'=>$cm->modname,'iteminstance'=>$cm->instance, 'courseid'=>$passignment->courseid, 'outcomeid'=>$outcomeid));
        if(!empty($item))
            return true;
        
        $outcome = new grade_outcome(array('id'=>$outcomeid));

        $outcome_item = new grade_item();
        $outcome_item->courseid     = $passignment->courseid;
        $outcome_item->itemtype     = 'mod';
        $outcome_item->itemmodule   = $cm->modname;
        $outcome_item->iteminstance = $cm->instance;
        $outcome_item->itemnumber   = 999;
        $outcome_item->itemname     = $outcome->fullname;
        $outcome_item->outcomeid    = $outcome->id;
        $outcome_item->gradetype    = GRADE_TYPE_SCALE;
        $outcome_item->scaleid      = $outcome->scaleid;
        $outcome_item->insert();

        // Move the new outcome into correct category and fix sortorder if needed.
        $grade_item = grade_item::fetch(array('itemtype'=>'mod', 'itemmodule'=>$cm->modname,'iteminstance'=>$cm->instance, 'itemnumber'=>0, 'courseid'=>$passignment->courseid));
        if ($grade_item) {
            $outcome_item->set_parent($grade_item->categoryid);
            $outcome_item->move_after_sortorder($grade_item->sortorder);
        }

        return true;
    }

    public function graded_assignments(\core\event\user_graded $instant = null, $grade = null){
        global $DB;
        if($instant == null && $grade == null)
            throw new moodle_exception('error_grade', 'local_powerschool', new moodle_url('/admin/'), null, 'Give me please \core\event\user_graded instant or grade instant');

        if($grade == null)
            $grade = $instant->get_grade();

        if($grade->grade_item->itemtype == 'mod') {
            if($grade->grade_item->outcomeid > 0)
                return true;

            $cm = get_coursemodule_from_instance($grade->grade_item->itemmodule,$grade->grade_item->iteminstance,$grade->grade_item->courseid,false,MUST_EXIST);

            if(!$this->cm_assignment_sync((object)array('cmid'=>$cm->id,'courseid'=>$grade->grade_item->courseid )))
                return true;

            if(self::get_coursetype_from_courseid($grade->grade_item->courseid) == 'groups'){
                $groups = groups_get_user_groups($grade->grade_item->courseid,$grade->userid);
                $groups = array_pop($groups);
                if(!empty($groups)){
                    $info_records = $DB->get_records_sql('SELECT a.id, d.data as userid, a.assignmentid, cf.gradebooktype, cf.teacher_user_dcid, cf.gradescaleitem, cf.term_id, a.assignmentsectionid
                                                    FROM {user_info_field} f
                                                       LEFT JOIN {user_info_data} d ON d.fieldid=f.id AND d.userid=:userid
                                                       JOIN {user_info_field} fs ON fs.shortname = \'powerschool777usertype\'
                                                       JOIN {user_info_data} ds ON ds.fieldid=fs.id AND d.userid=ds.userid
                                                       JOIN {user_info_field} fc ON fc.shortname = \'powerschool777connection_id\'
                                                       JOIN {user_info_data} dc ON dc.fieldid=fc.id AND d.userid=dc.userid
                                                       JOIN {powerschool_assignment} a ON a.cmid=:cmid AND a.courseid=:courseid AND a.groupid IN ('.implode(',',$groups).')
                                                       LEFT JOIN {powerschool_course_cat} cc ON cc.catid=a.groupid
                                                       LEFT JOIN {powerschool_course_fields} cf ON cf.section_id=cc.courseid
                                                    WHERE f.shortname = "powerschool777id" AND ds.data=\'student\' AND dc.data=:connection_id AND cf.connection_id=:connection_id2 GROUP BY a.id'
                            ,array('userid'=>$grade->userid,'cmid'=>$cm->id,'courseid'=>$grade->grade_item->courseid,'connection_id'=>$this->connection->id,'connection_id2'=>$this->connection->id));
                }else{
                    $info_records = array();
                }

            }else{
                $info_records = $DB->get_records_sql('SELECT a.id, d.data as userid, a.assignmentid, cf.gradebooktype, cf.teacher_user_dcid, cf.gradescaleitem, cf.term_id, a.assignmentsectionid
                                                FROM {user_info_field} f
                                                   LEFT JOIN {user_info_data} d ON d.fieldid=f.id AND d.userid=:userid
                                                    JOIN {user_info_field} fs ON fs.shortname = \'powerschool777usertype\'
                                                    JOIN {user_info_data} ds ON ds.fieldid=fs.id AND d.userid=ds.userid
                                                    JOIN {user_info_field} fc ON fc.shortname = \'powerschool777connection_id\'
                                                    JOIN {user_info_data} dc ON dc.fieldid=fc.id AND d.userid=dc.userid
                                                   JOIN {powerschool_assignment} a ON a.cmid=:cmid AND a.courseid=:courseid
                                                   LEFT JOIN {powerschool_course_fields} cf ON cf.courseid=a.courseid
                                                WHERE f.shortname = "powerschool777id" AND ds.data=\'student\' AND dc.data=:connection_id AND cf.connection_id=:connection_id2'
                            ,array('userid'=>$grade->userid,'cmid'=>$cm->id,'courseid'=>$grade->grade_item->courseid,'connection_id'=>$this->connection->id,'connection_id2'=>$this->connection->id));
            }

            foreach($info_records as $info) {
                if($info->gradebooktype == 1){
                    if(empty($grade->finalgrade)){
                        $this->request('delete', "/powerschool-ptg-api/v2/assignment/{$info->assignmentid}/student/{$info->userid}/score");
                    }else{
                        $score = new stdClass();
                        $score->assignment_score = new stdClass();
                        $score->assignment_score->score_entered = $grade->finalgrade - $grade->get_grade_min();
                        $score->assignment_score->points_possible = $grade->get_grade_max() - $grade->get_grade_min();
                        $score->assignment_score->exempt = ($grade->is_excluded())?true:false;

                        if($grade->rawscaleid > 0){
                            $scale = $DB->get_record('scale', array('id' => $grade->rawscaleid));
                            $scales = explode(",", $scale->scale);

                            $score->assignment_score->score_entered = $scales[$score->assignment_score->score_entered];
                        }

                        $grade->feedback = str_replace('</p><p>', chr(13), $grade->feedback);
                        $grade->feedback = str_replace('<br>', chr(13), $grade->feedback);
                        $comment = clean_param($grade->feedback, PARAM_NOTAGS);
                        if($comment){
                            $score->assignment_score->comment = substr($comment, 0, 4000);
                        }

                        $this->request('put', "/powerschool-ptg-api/v2/assignment/{$info->assignmentid}/student/{$info->userid}/score", array(), json_encode($score), true, false);
                    }
                }elseif($info->gradebooktype == 2){
                    if(empty($grade->finalgrade)){
                        $actualscoreentered = '';
                        $scorenumericgrade = 0;
                    }else{
                        $actualscoreentered = max($grade->finalgrade - $grade->get_grade_min(), 0);
                        $scorenumericgrade = max($grade->finalgrade - $grade->get_grade_min(), 0);
                    }
                    $score = new stdClass();
                    $score->isexempt = ($grade->is_excluded())?'true':'false';
                    $score->isExempt = ($grade->is_excluded())?'true':'false';
                    $score->studentsdcid = (int)$info->userid;
                    $score->_assignmentsection = new stdClass();
                    $score->_assignmentsection->assignmentsectionid = (int)$info->assignmentsectionid;
                    $score->actualscoreentered = $actualscoreentered;
                    $score->scorepoints = $grade->get_grade_max() - $grade->get_grade_min();
                    $score->actualscorekind = "REAL_SCORE";
                    $score->actualscoregradescaledcid = $info->gradescaleitem;
                    $score->scorenumericgrade = $scorenumericgrade;

                    if($grade->rawscaleid > 0){
                        $scale = $DB->get_record('scale', array('id' => $grade->rawscaleid));
                        $scales = explode(",", $scale->scale);

                        $score->actualscorekind = "RELATED_SCALE";
                        $score->actualscoreentered = $scales[$score->actualscoreentered];
                    }

                    $responce = $this->request('put', "/ws/xte/score?users_dcid=" . $info->teacher_user_dcid, array(), json_encode(array('assignment_scores' => array($score))), true, false);

                    /* hack for PS score comment, plugin cannot overwrite comment only delete and save new */
                    $grade->feedback = str_replace('</p><p>', chr(13), $grade->feedback);
                    $grade->feedback = str_replace('<br>', chr(13), $grade->feedback);
                    $comment = clean_param($grade->feedback, PARAM_NOTAGS);
                    if($comment){
                        $score->_assignmentscorecomment = new stdClass();
                        $score->_assignmentscorecomment->commentvalue = substr($comment, 0, 4000);
                        $responce = $this->request('put', "/ws/xte/score?users_dcid=" . $info->teacher_user_dcid, array(), json_encode(array('assignment_scores' => array($score))), true, false);
                    }
                    /* hack */

                }
            }
            return true;
        }
    }

}