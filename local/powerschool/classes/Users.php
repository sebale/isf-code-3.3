<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/local/powerschool/locallib.php');
require_once($CFG->dirroot.'/user/profile/definelib.php');
require_once($CFG->dirroot.'/user/lib.php');

class Users extends PowerSchool{
    private $exist_users = array();
    private $fieldname_separator = '777';
    public $profile_category;
    private $field_sortorder = 0;
    private $field_need_reorder = 0;
    private $temp_pass = 0;

    public $sync_params;
    private $profile_fields = array();

    public function __construct($connection_id){
		parent::__construct($connection_id);

		global $DB;

		$data = $DB->get_records('user_info_field',null,'','id,shortname');
		foreach($data as $item){
			$this->profile_fields[$item->shortname] = $item->id;
		}
		$this->field_sortorder = count($data)+1;

        if($DB->record_exists('user_info_category',array('id'=>get_config('local_powerschool', 'profile_category')))){
            $this->profile_category = get_config('local_powerschool', 'profile_category');
        }else{
            $new_categoty = new stdClass();
            $new_categoty->name = get_string('powerschool_category_name', 'local_powerschool');
            $new_categoty->sortorder = 99999;
            $this->profile_category = $DB->insert_record('user_info_category', $new_categoty);
            set_config('profile_category',$this->profile_category,'local_powerschool');
            profile_reorder_categories();
        }

		$this->exist_users = $DB->get_records_sql("SELECT u.id, CONCAT(d.data,dl.data) as powerid, u.username, u.email, dc.data as connection_id
										FROM {user} u
										  LEFT JOIN {user_info_field} f ON f.categoryid={$this->profile_category} AND f.shortname='powerschool{$this->fieldname_separator}usertype'
										  LEFT JOIN {user_info_data} d ON d.userid=u.id AND d.fieldid=f.id
										  LEFT JOIN {user_info_field} fl ON fl.categoryid={$this->profile_category} AND fl.shortname='powerschool{$this->fieldname_separator}local_id'
										  LEFT JOIN {user_info_data} dl ON dl.userid=u.id AND dl.fieldid=fl.id
										  LEFT JOIN {user_info_field} fc ON fl.categoryid={$this->profile_category} AND fc.shortname='powerschool{$this->fieldname_separator}connection_id'
										  LEFT JOIN {user_info_data} dc ON dc.userid=u.id AND dc.fieldid=fc.id
										WHERE u.deleted=0");

		$this->temp_pass = get_config('local_powerschool', 'temp_password');
        $this->sync_params = json_decode($this->connection->sync_params);

	}

	public function sync_users($page,$pagesize = 0){
		$params = $this->sync_params;
		if($pagesize == 0){
			$pagesize = min($this->metadata->metadata->student_max_page_size,$this->metadata->metadata->staff_max_page_size);
		}

		foreach($this->school_ids as $school_number=>$school_id){
            $expansions = $extensions = array();
            if(isset($params->students)){
                $student_ids_cf = array();
                foreach($params as $param => $value){
                    if(strpos($param, 'udent_expansions_') && $value == 1)
                        $expansions[] = str_replace('student_expansions_', '', $param);

                    if(strpos($param, 'udent_extensions_') && $value == 1)
                        $extensions[] = str_replace('student_extensions_', '', $param);

                }
                $expansions = (!empty($expansions))?'&expansions=' . implode(',', $expansions):'';
                $extensions = (!empty($extensions))?'&extensions=' . implode(',', $extensions):'';

                $data = $this->request('get', "/ws/v1/school/{$school_id}/student?page={$page}&pagesize=$pagesize" . $expansions . $extensions);

                if(isset($data->students->student)){
                    if(is_array($data->students->student)){
                        foreach($data->students->student as $item){
                            $item->usertype = 'student';
                            $item->school_id = $school_id;
                            $item->connection_id = $this->connection->id;
                            $this->execute_user($item);
                            $student_ids_cf[] = (int)$item->id;
                        }
                    }else{
                        $item = $data->students->student;
                        $item->usertype = 'student';
                        $item->school_id = $school_id;
                        $item->connection_id = $this->connection->id;
                        $this->execute_user($item);
                        $student_ids_cf[] = (int)$item->id;
                    }
                }
                $this->import_students_custom_fields($student_ids_cf);
            }

            if(isset($params->staffs)){
                $expansions = $extensions = array();
                foreach($params as $param => $value){
                    if(strpos($param, 'taff_expansions_') && $value == 1)
                        $expansions[] = str_replace('staff_expansions_', '', $param);

                    if(strpos($param, 'taff_extensions_') && $value == 1)
                        $extensions[] = str_replace('staff_extensions_', '', $param);

                }
                $expansions[] = 'school_affiliations';
                $expansions = (!empty($expansions))?'&expansions=' . implode(',', $expansions):'';
                $extensions = (!empty($extensions))?'&extensions=' . implode(',', $extensions):'';

                $data = $this->request('get', "/ws/v1/school/{$school_id}/staff?page={$page}&pagesize=$pagesize" . $expansions . $extensions);

                $staff = array();
                if(isset($data->staffs->staff)){
                    if(is_array($data->staffs->staff)){
                        foreach($data->staffs->staff as $item){
                            if(is_array($item->school_affiliations->school_affiliation)){
                                foreach($item->school_affiliations->school_affiliation as $school_affiliation){
                                    if(in_array($school_affiliation->school_id, $this->school_ids) && (!empty($item->teacher_username) || !empty($item->admin_username))){
                                        unset($item->school_affiliations);
                                        $staff[] = $item;
                                    }
                                }
                            }else{
                                if(in_array($item->school_affiliations->school_affiliation->school_id,$this->school_ids) && (!empty($item->teacher_username) || !empty($item->admin_username))){
                                    unset($item->school_affiliations);
                                    $staff[] = $item;
                                }
                            }
                        }

                        foreach($staff as $item){
                            $item->usertype = 'staff';
                            $item->school_id = $school_id;
                            $item->connection_id = $this->connection->id;
                            $this->execute_user($item);
                        }
                    }else{
                        $item = $data->staffs->staff;
                        if(is_array($item->school_affiliations->school_affiliation)){
                            foreach($item->school_affiliations->school_affiliation as $school_affiliation){
                                if(in_array($school_affiliation->school_id,$this->school_ids) && (!empty($item->teacher_username) || !empty($item->admin_username))){
                                    unset($item->school_affiliations);
                                    $staff = $item;
                                }
                            }
                        }else{
                            if(in_array($item->school_affiliations->school_affiliation->school_id,$this->school_ids) && (!empty($item->teacher_username) || !empty($item->admin_username))){
                                unset($item->school_affiliations);
                                $staff = $item;
                            }
                        }
                        $staff->usertype = 'staff';
                        $staff->school_id = $school_id;
                        $item->connection_id = $this->connection->id;
                        $this->execute_user($staff);
                    }
                }
            }
        }
		
	}
	
	public function execute_user($item){
		$username = $this->get_power_username($item);
		$email = $this->get_power_email($item);
        $sync_on = get_config('local_powerschool','associate_moodle_user');
        $auth_plugin = ($item->usertype == 'student')?$this->sync_params->student_auth_plugin:$this->sync_params->staff_auth_plugin;
        if(isset($item->_extension_data)){
            $extension_data = $item->_extension_data;
            unset($item->_extension_data);
        }

        if($sync_on == 'email' && $email == 'not_exist@example.com')
            return;

		if(empty($username) || $username == $this->connection->username_prefix)
			return;

		$id = $this->exist_user($item);
		if($id){
			$student = new stdClass();
			$student->id = $id;
			//$student->username = ($sync_on == 'email')?$email:$username;
			$student->firstname = (!empty($item->name->first_name))?$item->name->first_name:'';
			$student->lastname = (!empty($item->name->last_name))?$item->name->last_name:'';
			$student->email = $email;
            $student->auth = $auth_plugin;
			user_update_user($student, false, false);
			$this->execute_user_profile($student->id,$item);
		}else{
			$student = new stdClass();
			$student->username = ($sync_on == 'email')?$email:$username;
			$student->firstname = (!empty($item->name->first_name))?$item->name->first_name:'';
			$student->lastname = (!empty($item->name->last_name))?$item->name->last_name:'';
			$student->password = $this->temp_pass;
			$student->confirmed = 1;
			$student->mnethostid = 1;
			$student->email = $email;
			$student->auth = $auth_plugin;
			$id = user_create_user($student);

            if(get_config('local_powerschool', 'forcepasswordchange'))
                set_user_preference('auth_forcepasswordchange', true, $id);

			$this->execute_user_profile($id,$item);

            $exist_user = new stdClass();
            $exist_user->id = $id;
            $exist_user->powerid = $item->usertype.$item->local_id;
            $exist_user->username = $username;
            $exist_user->email = $this->get_power_email($item);
            $exist_user->connection_id = $this->connection->id;
            $this->exist_users[] = $exist_user;
		}
		if($this->field_need_reorder)
			profile_reorder_fields();

		$this->assign_system_role($id,$item);


		if(isset($extension_data))
            $this->execute_user_profile_extension($id,$extension_data,$item->usertype);

		if($item->usertype == 'student')
		    $this->import_student_guardian($item);
	}

	private function exist_user($power_user){
	    $sync_on = get_config('local_powerschool','associate_moodle_user');
		$powerid = $power_user->usertype.$power_user->local_id;

        foreach($this->exist_users as $exist_user){
			if($exist_user->powerid == $powerid && $exist_user->connection_id == $this->connection->id){
                return $exist_user->id;
            }
		}
		if($sync_on == 'email'){
            foreach($this->exist_users as $exist_user){
                if($exist_user->email == $this->get_power_email($power_user) && $exist_user->email != 'not_exist@example.com'){
                    return $exist_user->id;
                }
            }
        }elseif($sync_on == 'username'){
            $username = $this->get_power_username($power_user);
            foreach($this->exist_users as $exist_user){
                if($exist_user->username == $username && $username != $this->connection->username_prefix){
                    return $exist_user->id;
                }
            }
        }

		return false;
	}

	private function get_power_username($power_user){
		$username = (isset($power_user->student_username))?core_text::strtolower($power_user->student_username):'';
		$username = (isset($power_user->guardian_username))?core_text::strtolower($power_user->guardian_username):$username;
		$username = (isset($power_user->teacher_username))?core_text::strtolower($power_user->teacher_username):$username;
		$username = (isset($power_user->admin_username))?core_text::strtolower($power_user->admin_username):$username;
        $username = clean_param($username,PARAM_USERNAME);

		return $this->connection->username_prefix.$username;
	}

	private function get_power_email($power_user){
		$email = 'not_exist@example.com';
		if(!empty($power_user->contact_info->email)){
			return $power_user->contact_info->email;
		}elseif(!empty($power_user->contact->guardian_email)){
			return $power_user->contact->guardian_email;
		}elseif(!empty($power_user->emails->work_email)){
			return $power_user->emails->work_email;
		}elseif($power_user->usertype == 'student'){
            $data = new stdClass();
            $data->student_number = $power_user->local_id;
            $data = json_encode($data);

            $data = $this->request('post', "/ws/schema/query/get.student.email",array(),$data);

            if(isset($data->record[0]->tables->students->email) && !empty($data->record[0]->tables->students->email)){
                return $data->record[0]->tables->students->email;
            }
        }

		return $email;
	}
	
	private function execute_user_profile($user_id, $fields, $field_shortname = 'powerschool', $usertype = null){
		global $DB;

        if(!$usertype)
            $usertype = $fields->usertype;
		
		foreach($fields as $name=>$item){
			if(in_array($name,array('teacher_username','student_username','name','admin_username','emails','guardian_username')) && $field_shortname == 'powerschool')
				continue;
			if(empty($item) && $item !== 0) 
				continue;
			
			if(is_object($item) || is_array($item)){
				$this->execute_user_profile($user_id, $item, $field_shortname.$this->fieldname_separator.$name, $usertype);
				continue;
			}
			
			if(isset($this->profile_fields[$field_shortname.$this->fieldname_separator.$name])){
				if($id = $DB->get_record('user_info_data',array('userid'=>$user_id,'fieldid'=>$this->profile_fields[$field_shortname.$this->fieldname_separator.$name]),'id')){
					$field_data = new stdClass();
					$field_data->id = $id->id;
					$field_data->data = $item;
					$DB->update_record('user_info_data',$field_data);
				}else{
					$field_data = new stdClass();
					$field_data->userid = $user_id;
					$field_data->fieldid = $this->profile_fields[$field_shortname.$this->fieldname_separator.$name];
					$field_data->data = $item;
					$DB->insert_record('user_info_data',$field_data);
				}
			}else{
				$new_field = new stdClass();
				$new_field->shortname = $field_shortname.$this->fieldname_separator.$name;
				$new_field->name = get_string($field_shortname.$this->fieldname_separator.$name, 'local_powerschool');
				$new_field->datatype = 'text';
				$new_field->descriptionformat = 1;
				$new_field->categoryid = $this->profile_category;
				$new_field->sortorder = $this->field_sortorder++;
				$new_field->locked = 1;
				$new_field->visible = (in_array($field_shortname.$this->fieldname_separator.$name,array('powerschool777id','powerschool777local_id','powerschool777usertype','powerschool777school_id','powerschool777connection_id')))?0:1;
				$new_field->forceunique = 0;
				$new_field->signup = 0;
				$new_field->defaultdataformat = 0;
				$new_field->param1 = 30; //??
				$new_field->param2 = 2048;
				$new_field->param3 = 0;
				$this->profile_fields[$field_shortname.$this->fieldname_separator.$name] = $DB->insert_record('user_info_field',$new_field);	
				$this->field_need_reorder = 1;
				
				$field_data = new stdClass();
				$field_data->userid = $user_id;
				$field_data->fieldid = $this->profile_fields[$field_shortname.$this->fieldname_separator.$name];
				$field_data->data = $item;
				$DB->insert_record('user_info_data',$field_data);
			}

			if($usertype == 'student' && in_array($this->profile_fields[$field_shortname.$this->fieldname_separator.$name], $this->sync_params->field_for_cohort_student)
                || $usertype == 'staff' && in_array($this->profile_fields[$field_shortname.$this->fieldname_separator.$name], $this->sync_params->field_for_cohort_staff)){
                $this->execute_cohorts($this->profile_fields[$field_shortname.$this->fieldname_separator.$name], $item, $user_id);
            }
		}
		
	}

	private function execute_user_profile_extension($user_id, $extension_data, $usertype){
		global $DB;
        $field_shortname = 'powerschool';

        if(is_object($extension_data->_table_extension))
            $extension_data->_table_extension = array($extension_data->_table_extension);

        foreach($extension_data->_table_extension as $table_extension){
            if(is_object($table_extension->_field))
                $table_extension->_field = array($table_extension->_field);

            foreach($table_extension->_field as $field){
                if(isset($this->profile_fields[$field_shortname.$this->fieldname_separator.$field->name])){
                    if($id = $DB->get_record('user_info_data',array('userid'=>$user_id,'fieldid'=>$this->profile_fields[$field_shortname.$this->fieldname_separator.$field->name]),'id')){
                        $field_data = new stdClass();
                        $field_data->id = $id->id;
                        $field_data->data = $field->value;
                        $DB->update_record('user_info_data',$field_data);
                    }else{
                        $field_data = new stdClass();
                        $field_data->userid = $user_id;
                        $field_data->fieldid = $this->profile_fields[$field_shortname.$this->fieldname_separator.$field->name];
                        $field_data->data = $field->value;
                        $DB->insert_record('user_info_data',$field_data);
                    }
                    if($usertype == 'student' && $this->profile_fields[$field_shortname.$this->fieldname_separator.$field->name] == $this->sync_params->field_for_cohort_student
                        || $usertype == 'staff' && $this->profile_fields[$field_shortname.$this->fieldname_separator.$field->name] == $this->sync_params->field_for_cohort_staff){
                        $this->execute_cohorts($this->profile_fields[$field_shortname.$this->fieldname_separator.$field->name], $field->value, $user_id);
                    }
                }else{
                    $new_field = new stdClass();
                    $new_field->shortname = $field_shortname.$this->fieldname_separator.$field->name;
                    $new_field->name = ucwords(str_replace('_',' ',$field->name));
                    $new_field->datatype = 'text';
                    $new_field->descriptionformat = 1;
                    $new_field->categoryid = $this->profile_category;
                    $new_field->sortorder = $this->field_sortorder++;
                    $new_field->locked = 1;
                    $new_field->visible = 1;
                    $new_field->forceunique = 0;
                    $new_field->signup = 0;
                    $new_field->defaultdataformat = 0;
                    $new_field->param1 = 30; //??
                    $new_field->param2 = 2048;
                    $new_field->param3 = 0;
                    $this->profile_fields[$field_shortname.$this->fieldname_separator.$field->name] = $DB->insert_record('user_info_field',$new_field);
                    $this->field_need_reorder = 1;

                    $field_data = new stdClass();
                    $field_data->userid = $user_id;
                    $field_data->fieldid = $this->profile_fields[$field_shortname.$this->fieldname_separator.$field->name];
                    $field_data->data = $field->value;
                    $DB->insert_record('user_info_data',$field_data);
                }
            }
        }
	}

	private function execute_cohorts($field_id, $item_value, $user_id){
        global $DB,$CFG;
        require_once($CFG->dirroot.'/cohort/lib.php');
        $field = $DB->get_record('user_info_field',array('id'=>$field_id));
        $context = context_system::instance();
        $cohort_name = $field->name.': '.$item_value;
        $cohort = $DB->get_record('cohort',array('idnumber'=>$cohort_name));

        if(isset($cohort->id)){
            $like_cohorts_records = $DB->get_records_sql('SELECT id FROM {cohort} c WHERE c.idnumber LIKE "'.$field->name.'%" AND c.id <> :id',array('id'=>$cohort->id));
            $like_cohorts = array();
            foreach($like_cohorts_records as $like_cohort){
                $like_cohorts[] = $like_cohort->id;
            }
            if(!empty($like_cohorts))
                $DB->execute('DELETE FROM {cohort_members} WHERE cohortid IN ('.implode(',',$like_cohorts).') AND userid=:user_id',array('user_id'=>$user_id));

            cohort_add_member($cohort->id, $user_id);
        }else{
            $cohort = new stdClass();
            $cohort->name = $cohort_name;
            $cohort->idnumber = $cohort_name;
            $cohort->contextid = $context->id;
            $cohort->id = cohort_add_cohort($cohort);
            cohort_add_member($cohort->id, $user_id);
        }
        return true;
    }

	public function import_students_custom_fields($ids){
        global $DB;
        if(empty($ids))
            return;

        $users = $DB->get_records_sql("SELECT u.id, dl.data as power_id
										FROM {user} u
										  LEFT JOIN {user_info_field} f ON f.categoryid={$this->profile_category} AND f.shortname='powerschool{$this->fieldname_separator}usertype'
										  LEFT JOIN {user_info_data} d ON d.userid=u.id AND d.fieldid=f.id
										  LEFT JOIN {user_info_field} fl ON fl.categoryid={$this->profile_category} AND fl.shortname='powerschool{$this->fieldname_separator}id'
										  LEFT JOIN {user_info_data} dl ON dl.userid=u.id AND dl.fieldid=fl.id
										  LEFT JOIN {user_info_field} fc ON fl.categoryid={$this->profile_category} AND fc.shortname='powerschool{$this->fieldname_separator}connection_id'
										  LEFT JOIN {user_info_data} dc ON dc.userid=u.id AND dc.fieldid=fc.id
										WHERE d.data=:user_type AND dl.data IN(".implode(',',$ids).") AND dc.data=:connection_id",array('user_type'=>'student','connection_id'=>$this->connection->id));

        $custom_fields = explode(',',$this->sync_params->student_custom_fields);
        if(empty($custom_fields))
            return;

        foreach($custom_fields as $custom_field){
            if(empty($custom_field))
                continue;
            
            list($label,$name) = explode('|',$custom_field);
            $field_shortname = 'powerschool'. $this->fieldname_separator . $name;

            if(empty($name))
                continue;

            $data = new stdClass();
            $data->students_dcid = $ids;
            $data->field_name = $name;

            $data = json_encode($data);
            $data = $this->request('post', "/ws/schema/query/get.student.custom.fields",array(),$data);

            if(!isset($data->record))
                continue;

            foreach($data->record as $record){
                $record = $record->tables->students;
                $record->data = (string)$record->data;
                $user_id = null;
                foreach($users as $user){
                    if($user->power_id == $record->id)
                        $user_id = $user->id;
                }
                if(empty($user_id))
                    continue;

                if(isset($this->profile_fields[$field_shortname])){
                    if($id = $DB->get_record('user_info_data', array('userid' => $user_id, 'fieldid' => $this->profile_fields[$field_shortname]), 'id')){
                        $field_data = new stdClass();
                        $field_data->id = $id->id;
                        $field_data->data = $record->data;
                        $DB->update_record('user_info_data', $field_data);
                    }else{
                        $field_data = new stdClass();
                        $field_data->userid = $user_id;
                        $field_data->fieldid = $this->profile_fields[$field_shortname];
                        $field_data->data = $record->data;
                        $DB->insert_record('user_info_data', $field_data);
                    }
                }else{
                    $new_field = new stdClass();
                    $new_field->shortname = $field_shortname;
                    $new_field->name = $label;
                    $new_field->datatype = 'text';
                    $new_field->descriptionformat = 1;
                    $new_field->categoryid = $this->profile_category;
                    $new_field->sortorder = $this->field_sortorder++;
                    $new_field->locked = 1;
                    $new_field->visible = 1;
                    $new_field->forceunique = 0;
                    $new_field->signup = 0;
                    $new_field->defaultdataformat = 0;
                    $new_field->param1 = 30; //??
                    $new_field->param2 = 2048;
                    $new_field->param3 = 0;
                    $this->profile_fields[$field_shortname] = $DB->insert_record('user_info_field', $new_field);
                    $this->field_need_reorder = 1;

                    $field_data = new stdClass();
                    $field_data->userid = $user_id;
                    $field_data->fieldid = $this->profile_fields[$field_shortname];
                    $field_data->data = $record->data;
                    $DB->insert_record('user_info_data', $field_data);
                }
            }
        }
    }

    private function import_student_guardian($student, $student_user_id = null){
        $params = $this->sync_params;
        if(!isset($params->parents))
            return true;

        $data = new stdClass();
        $data->students_dcid = array($student->id);
        $data = json_encode($data);

        $count = $this->request('post', '/ws/schema/query/com.pearson.core.guardian.student_guardian_detail/count',array(),$data);
        $pages = ceil($count->count/$this->metadata->metadata->schema_table_query_max_page_size);

        for($page=1;$page<=$pages;$page++){
            $data = $this->request('post', "/ws/schema/query/com.pearson.core.guardian.student_guardian_detail?page=$page&pagesize=" . $this->metadata->metadata->schema_table_query_max_page_size, array(), $data);

            if(!isset($data->record))
                continue;

            $usercontext = ($student_user_id)?context_user::instance($student_user_id):context_user::instance($this->exist_user($student));
            foreach($data->record as $item){
                $record = $item->tables;
                if(empty($record->guardian->guardianid))
                    continue;

                $user = new stdClass();
                $user->usertype = 'guardian';
                $user->local_id = $record->guardian->guardianid;
                $user->connection_id = $this->connection->id;
                $user->guardian_username = $record->pcas_account->username;
                $user->name = new stdClass();
                $user->name->first_name = $record->guardian->firstname;
                $user->name->last_name = $record->guardian->lastname;
                $user->emails = new stdClass();
                $user->emails->work_email = (filter_var($record->pcas_emailcontact->emailaddress, FILTER_VALIDATE_EMAIL))?$record->pcas_emailcontact->emailaddress:'not_exist@example.com';

                $this->execute_user($user);

                if($params->parent_role > 0)
                    role_assign($params->parent_role, $this->exist_user($user), $usercontext->id);
            }
        }
        return true;
    }
    public function auto_import_guardians(){
        global $DB;
        $params = $this->sync_params;
        if(!isset($params->parents))
            return true;

        $students = $DB->get_records_sql("SELECT dl.data AS id, u.id AS moodle_id
                                            FROM {user} u
                                              LEFT JOIN {user_info_field} f ON f.categoryid={$this->profile_category} AND f.shortname='powerschool{$this->fieldname_separator}usertype'
                                              LEFT JOIN {user_info_data} d ON d.userid=u.id AND d.fieldid=f.id
                                              LEFT JOIN {user_info_field} fl ON fl.categoryid={$this->profile_category} AND fl.shortname='powerschool{$this->fieldname_separator}id'
                                              LEFT JOIN {user_info_data} dl ON dl.userid=u.id AND dl.fieldid=fl.id
                                              LEFT JOIN {user_info_field} fc ON fc.categoryid={$this->profile_category} AND fc.shortname='powerschool{$this->fieldname_separator}connection_id'
                                              LEFT JOIN {user_info_data} dc ON dc.userid=u.id AND dc.fieldid=fc.id
                                            WHERE d.data=:user_type AND dc.data=:connection_id",
                                array('user_type' => 'student', 'connection_id' => $this->connection->id));

        $data = new stdClass();
        $data->students_dcid = array();
        $student_ids = array();
        foreach($students as $student){
            $data->students_dcid[] = $student->id;
            $student_ids[$student->id] = $student->moodle_id;
        }
        $data = json_encode($data);

        $count = $this->request('post', '/ws/schema/query/com.pearson.core.guardian.student_guardian_detail/count',array(),$data);
        $pages = ceil($count->count/$this->metadata->metadata->schema_table_query_max_page_size);

        for($page=1;$page<=$pages;$page++){
            $request = $this->request('post', "/ws/schema/query/com.pearson.core.guardian.student_guardian_detail?page=$page&pagesize=" . $this->metadata->metadata->schema_table_query_max_page_size, array(), $data);
            if(!isset($request->record))
                continue;

            foreach($request->record as $item){
                $record = $item->tables;

                if(empty($record->guardian->guardianid))
                    continue;

                $user = new stdClass();
                $user->usertype = 'guardian';
                $user->local_id = $record->guardian->guardianid;
                $user->connection_id = $this->connection->id;
                $user->guardian_username = $record->pcas_account->username;
                $user->name = new stdClass();
                $user->name->first_name = $record->guardian->firstname;
                $user->name->last_name = $record->guardian->lastname;
                $user->emails = new stdClass();
                $user->emails->work_email = (filter_var($record->pcas_emailcontact->emailaddress, FILTER_VALIDATE_EMAIL))?$record->pcas_emailcontact->emailaddress:'not_exist@example.com';

                $this->execute_user($user);

                if($params->parent_role > 0)
                    role_assign($params->parent_role, $this->exist_user($user), context_user::instance($student_ids[$record->students->dcid]));
            }
        }

        return true;
    }

	public function sync_moodle_user($user){
		global $DB;

		$profile_fields = $DB->get_records_sql('SELECT f.id, f.shortname, d.data
												FROM {user_info_data} d
													JOIN {user_info_field} f ON f.id=d.fieldid AND f.categoryid = :categoryid
												WHERE d.userid=:userid
											',array('categoryid'=>$this->profile_category,'userid'=>$user->id));

		$field_obj = new stdClass();
		foreach($profile_fields as $field){
			if(($field->shortname == 'powerschool'.$this->fieldname_separator.'usertype' && $field->data != 'student') || ($field->shortname == 'powerschool'.$this->fieldname_separator.'connection_id' && $field->data != $this->connection->id))
				return;

			$this->get_object_from_path(explode($this->fieldname_separator, $field->shortname),$field_obj, $field->data);
		}

		if(!isset($field_obj->powerschool->local_id))
			return;

		@$field_obj->powerschool->name->first_name = $user->firstname;
		$field_obj->powerschool->name->last_name = $user->lastname;
		$field_obj = $this->clear_profile($field_obj,$user);

		$request = new stdClass();
		@$request->students->student = $field_obj;
		$request->students->student->action = 'UPDATE';
		$request->students->student->client_uid = $field_obj->id;

		$response = $this->request('post','/ws/v1/student', array(),json_encode($request));

		if($response->results->result->status == 'SUCCESS'){
			return true;
		}else{
            throw new moodle_exception($response->results->result->status, 'local_powerschool', new moodle_url('/admin/'), null, print_r($response->results->result->error_message,true));
		}
	}

	protected function assign_system_role($user_id,$item){

		if($item->usertype == 'student'){
            $roleid = $this->sync_params->system_student_role;
        }elseif($item->usertype == 'guardian'){
            $roleid = $this->sync_params->system_guardian_role;
        }elseif($item->usertype == 'staff'){
            $roleid = $this->sync_params->system_staff_role;
        }

        if($roleid > 0){
            return role_assign($roleid, $user_id, SYSCONTEXTID);
        }

        return true;
	}

	private function get_object_from_path($path, &$obj, $data){
		if(empty($path)) {
			$obj = $data;
		}elseif(is_array($path)) {
			$item = array_shift($path);
			$this->get_object_from_path($path,$obj->$item,$data);
		}
	}

	private function clear_profile($field_obj,$user){
		$field_obj = $field_obj->powerschool;

		if(count($field_obj->addresses->physical)<4){
			unset($field_obj->addresses->physical);
		}
		if(count($field_obj->addresses->home)<4){
			unset($field_obj->addresses->home);
		}
		if(count($field_obj->addresses->mailing)<4){
			unset($field_obj->addresses->mailing);
		}
		if(count($field_obj->schedule_setup)<2){
			unset($field_obj->schedule_setup);
		}
        if(!empty($field_obj->contact->emergency_phone2) && empty($field_obj->contact->emergency_contact_name2)){
		    unset($field_obj->contact->emergency_phone2);
        }
		if(empty($field_obj->contact->emergency_phone2) && !empty($field_obj->contact->emergency_contact_name2)){
		    unset($field_obj->contact->emergency_contact_name2);
        }
		if(!empty($field_obj->contact->emergency_phone1) && empty($field_obj->contact->emergency_contact_name1)){
		    unset($field_obj->contact->emergency_phone1);
        }
		if(empty($field_obj->contact->emergency_phone1) && !empty($field_obj->contact->emergency_contact_name1)){
		    unset($field_obj->contact->emergency_contact_name1);
        }
		if(empty($field_obj->contact->doctor_name) && !empty($field_obj->contact->doctor_phone)){
		    unset($field_obj->contact->doctor_phone);
        }
		if(!empty($field_obj->contact->doctor_name) && empty($field_obj->contact->doctor_phone)){
		    unset($field_obj->contact->doctor_name);
        }

		unset($field_obj->lunch);
		unset($field_obj->local_id);
		unset($field_obj->school_enrollment);
		unset($field_obj->usertype);
		unset($field_obj->connect_id);

		if(empty($field_obj->contact_info->email)){
			$field_obj->contact_info->email = $user->email;
		}

		return $field_obj;
	}
}
