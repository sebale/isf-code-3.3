<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/local/powerschool/locallib.php');
require_once($CFG->dirroot.'/local/powerschool/classes/Users.php');
require_once($CFG->dirroot.'/local/powerschool/classes/Courses.php');


class events_handler {

    public static function moodle_user_updated(core\event\user_updated $instant){
        if(!get_config('local_powerschool', 'sync_moodle_user_change')){
            return true;
        }

        $user = core_user::get_user($instant->objectid);
        profile_load_data($user);

        if(isset($user->profile_field_powerschool777connection_id) && $user->profile_field_powerschool777connection_id>0){
            $plugin = new Users($user->profile_field_powerschool777connection_id);
            return $plugin->sync_moodle_user($user);
        }else
            return true;

    }
    public static function moodle_course_deleted(core\event\course_deleted $instant){
        global $DB;

        $DB->delete_records('powerschool_courses',array('mcourse'=>$instant->courseid));
        $DB->delete_records('powerschool_course_fields',array('courseid'=>$instant->courseid));
        $DB->delete_records('powerschool_enrollments',array('courseid'=>$instant->courseid));
        $DB->delete_records('powerschool_assignment',array('courseid'=>$instant->courseid));
        $DB->delete_records('powerschool_assignment_sync',array('courseid'=>$instant->courseid));
        $DB->delete_records('powerschool_grade_cat',array('courseid'=>$instant->courseid));

        if(PowerSchool::get_coursetype_from_courseid($instant->courseid) == 'groups')
            $DB->delete_records('powerschool_course_cat', array('courseid_group' => $instant->courseid));

        return true;
    }
    public static function moodle_mod_created(core\event\course_module_created $instant){
        $module = new stdClass();
        $module->cmid = $instant->objectid;
        $module->courseid = $instant->courseid;

        $connection_id = PowerSchool::get_connection_from_courseid($module->courseid);
        if(!empty($connection_id)){
            $plugin = new Courses($connection_id);
            return $plugin->create_assignment_sync($module);
        }else
            return true;
    }
    public static function moodle_mod_updated(core\event\course_module_updated $instant){
        $module = new stdClass();
        $module->courseid = $instant->courseid;
        $module->cmid = $instant->objectid;

        $connection_id = PowerSchool::get_connection_from_courseid($module->courseid);
        if(!empty($connection_id)){
            $plugin = new Courses($connection_id);
            return $plugin->update_assignment($module);
        }else
            return true;
    }
    public static function moodle_mod_deleted($instant){
        global $DB;

        $module = new stdClass();
        $module->courseid = $instant->courseid;
        $module->cmid = $instant->objectid;

        $connection_id = PowerSchool::get_connection_from_courseid($module->courseid);
        if(!empty($connection_id)){
            $plugin = new Courses($connection_id);
            $plugin->delete_assignment($module);
        }

        $DB->delete_records('powerschool_assignment', array('cmid' => $module->cmid));
        $DB->delete_records('powerschool_assignment_sync',array('cmid'=>$module->cmid));
        $DB->delete_records('powerschool_assignment_cat',array('moodle_mod'=>$module->cmid));
        return true;
    }
    public static function moodle_user_unenrolled($instant){
        global $DB;
        $DB->delete_records('powerschool_enrollments',array('userid'=>$instant->relateduserid, 'courseid'=>$instant->courseid));
        return true;
    }
    public static function moodle_user_graded(\core\event\user_graded $instant){
        $connection_id = PowerSchool::get_connection_from_courseid($instant->courseid);
        if(!empty($connection_id)){
            $plugin = new Courses($connection_id);
            $plugin->graded_assignments($instant);
        }
        return true;
    }
    public static function moodle_course_category_deleted (core\event\course_category_deleted $instant){
        global $DB;
        $DB->delete_records('powerschool_course_cat',array('catid'=>$instant->objectid,'courseid_group'=>0));

        return true;
    }
    public static function moodle_groups_group_deleted($instant){
        global $DB;

        $cats = $DB->get_records('powerschool_course_cat',array('catid'=>$instant->objectid,'courseid_group'=>$instant->courseid));
        $DB->delete_records('powerschool_course_cat',array('catid'=>$instant->objectid,'courseid_group'=>$instant->courseid));
        foreach($cats as $cat){
            if(isset($cat->courseid)){
                $DB->delete_records('powerschool_course_fields', array('section_id' => $cat->courseid, 'courseid' => $instant->courseid));
                $DB->delete_records('powerschool_assignment', array('section_id' => $cat->courseid, 'courseid' => $instant->courseid));
            }
        }

        return true;
    }
}















