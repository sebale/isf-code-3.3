<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * PowerSchool
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');
require_once($CFG->libdir.'/formslib.php');
require_once($CFG->libdir.'/coursecatlib.php');

class connections_table extends table_sql {

    function __construct($uniqueid) {
        global $CFG;

        parent::__construct($uniqueid);

        $columns = array('id','name','url','school_numbers', 'timecreate', 'actions');
        $header = array('ID','Name','Powerschool url', 'Schools', 'Created', 'Actions');

        $this->define_columns($columns);
        $this->define_headers($header);


        $fields = "con.id, con.url, con.name, con.school_numbers, con.timecreate, '' as actions";
        $from = "{powerschool_connections} con";

        $this->set_sql($fields, $from, 'con.id>0', array());
        $this->define_baseurl($CFG->wwwroot.$_SERVER['REQUEST_URI']);
    }
    function col_timecreate($values) {
        return  date('m/d/Y', $values->timecreate);
    }

    function col_actions($values) {
        global $OUTPUT;

        if ($this->is_downloading()){
            return '';
        }

        $strdelete  = get_string('delete');
        $stredit  = get_string('edit');

        $edit = array();

        $aurl = new moodle_url('/local/powerschool/edit-connection.php', array('id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/edit', $stredit, 'core', array('class' => 'iconsmall')));

        $aurl = new moodle_url('/local/powerschool/manage-connections.php', array('action'=>'delete', 'id'=>$values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/delete', $strdelete, 'core', array('class' => 'iconsmall')), null, array('onclick'=>"if (!confirm('Are you sure want to delete this redord?')) return false;"));

        return implode('', $edit);
    }
}

class edit_connection_form extends moodleform {

    /**
     * Define the form.
     */
    public function definition () {
        global $DB;
        $id = $this->_customdata;
        $mform = $this->_form;

        $data = $DB->get_record('powerschool_connections',array('id'=>$id));

        $mform->addElement('text', 'name', get_string('name'));
        $mform->setType('name', PARAM_RAW);
        $mform->addRule('name', get_string('required'), 'required', null, 'client');

        $mform->addElement('text', 'url', get_string('master_url', 'local_powerschool'));
        $mform->setType('url', PARAM_RAW);
        $mform->addRule('url', get_string('required'), 'required', null, 'client');
        $mform->addHelpButton('url', 'master_url_desc', 'local_powerschool');

        $mform->addElement('text', 'client_id', get_string('client_id', 'local_powerschool'));
        $mform->setType('client_id', PARAM_RAW);
        $mform->addRule('client_id', get_string('required'), 'required', null, 'client');
        $mform->addHelpButton('client_id', 'client_id_desc', 'local_powerschool');

        $mform->addElement('text', 'client_secret', get_string('client_secret', 'local_powerschool'));
        $mform->setType('client_secret', PARAM_RAW);
        $mform->addRule('client_secret', get_string('required'), 'required', null, 'client');
        $mform->addHelpButton('client_secret', 'client_secret_desc', 'local_powerschool');

        $mform->addElement('text', 'school_numbers', get_string('school_numbers', 'local_powerschool'));
        $mform->setType('school_numbers', PARAM_RAW);
        $mform->addRule('school_numbers', get_string('required'), 'required', null, 'client');
        $mform->addHelpButton('school_numbers', 'school_numbers_desc', 'local_powerschool');


        $mform->addElement('text', 'username_prefix', get_string('username_prefix', 'local_powerschool'));
        $mform->setType('username_prefix', PARAM_RAW);
        $mform->addHelpButton('username_prefix', 'username_prefix_desc', 'local_powerschool');

        $mform->addElement('text', 'course_prefix', get_string('course_prefix', 'local_powerschool'));
        $mform->setType('course_prefix', PARAM_RAW);
        $mform->addHelpButton('course_prefix', 'course_prefix_desc', 'local_powerschool');

        $mform->addElement('text', 'course_fullname', get_string('section_fullname', 'local_powerschool'));
        $mform->setType('course_fullname', PARAM_RAW);
        $mform->addHelpButton('course_fullname', 'section_fullname_desc', 'local_powerschool');

        $mform->addElement('text', 'course_fullname_group', get_string('section_fullname_group', 'local_powerschool'));
        $mform->setType('course_fullname_group', PARAM_RAW);
        $mform->addHelpButton('course_fullname_group', 'section_fullname_desc', 'local_powerschool');

        if(isset($data->id)){
            $mform->addElement('hidden', 'id', $data->id);
            $mform->setType('id', PARAM_INT);

            $mform->setDefault('name', $data->name);
            $mform->setDefault('url', $data->url);
            $mform->setDefault('client_id', $data->client_id);
            $mform->setDefault('client_secret', $data->client_secret);
            $mform->setDefault('school_numbers', $data->school_numbers);
            $mform->setDefault('username_prefix', $data->username_prefix);
            $mform->setDefault('course_prefix', $data->course_prefix);
            $mform->setDefault('course_fullname', $data->course_fullname);
            $mform->setDefault('course_fullname_group', $data->course_fullname_group);
        }

        $this->add_action_buttons(true, get_string('submit'));
    }

    function validation($data, $files) {
        global $DB;

        $errors = parent::validation($data, $files);

        if(!isset($data['id']) && $DB->record_exists('powerschool_connections',array('url'=>trim($data['url'],' /')))){
            $errors['url'] = get_string('server_connected','local_powerschool');
        }

        if(!isset($data['id']) && !empty($data['username_prefix']) && $DB->record_exists_sql('SELECT * FROM {powerschool_connections} WHERE username_prefix=:username_prefix',array('username_prefix'=>$data['username_prefix']))){
            $errors['username_prefix'] = get_string('must_be_unique','local_powerschool');
        }

        if(isset($data['id']) && !empty($data['username_prefix']) && $DB->record_exists_sql('SELECT * FROM {powerschool_connections} WHERE username_prefix=:username_prefix AND id<>:id',array('username_prefix'=>$data['username_prefix'], 'id'=>$data['id']))){
            $errors['username_prefix'] = get_string('must_be_unique','local_powerschool');
        }

        if(!empty($data['username_prefix']) && $data['username_prefix'] != clean_param($data['username_prefix'], PARAM_USERNAME)){
            $errors['username_prefix'] = get_string('invalidusername');
        }
        if(!isset($data['id']) && !empty($data['course_prefix']) && $DB->record_exists_sql('SELECT * FROM {powerschool_connections} WHERE course_prefix=:course_prefix',array('course_prefix'=>$data['course_prefix']))){
            $errors['course_prefix'] = get_string('must_be_unique','local_powerschool');
        }
        if(isset($data['id']) && !empty($data['course_prefix']) && $DB->record_exists_sql('SELECT * FROM {powerschool_connections} WHERE course_prefix=:course_prefix AND id<>:id',array('course_prefix'=>$data['course_prefix'], 'id'=>$data['id']))){
            $errors['course_prefix'] = get_string('must_be_unique','local_powerschool');
        }

        return $errors;
    }
}