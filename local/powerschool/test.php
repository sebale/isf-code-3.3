<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * PowerSchool Settings
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require_once('classes/Courses.php');

require_login();
require_capability('local/powerschool:view', context_system::instance());

$connection = $DB->get_record('powerschool_connections',array());

$plugin = new Courses($connection->id);

$PAGE->set_url(new moodle_url("/local/powerschool/sync-courses.php"));
$PAGE->set_pagelayout('admin');
$PAGE->set_context(context_system::instance());
$PAGE->set_title(get_string('powerschoolroot', 'local_powerschool'));
$PAGE->set_heading(get_string('powerschoolroot', 'local_powerschool'));
$PAGE->requires->js('/local/powerschool/javascript/jquery-2.2.0.min.js',true);

echo $OUTPUT->header();
echo $OUTPUT->heading('Test page');


$data = new stdClass();
$data->courses = array('OPstudy');
$data->terms = array(2602);
$data->schoolid = 100;

$response = $plugin->request('post', '/ws/schema/query/get.course_standard', array(), json_encode($data));
print_object($response);

echo $OUTPUT->footer();

