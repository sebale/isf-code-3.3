<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * External Web Service quickadd plugin
 *
 * @package    local_quickadd
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once($CFG->libdir . "/externallib.php");

class local_quickadd_external extends external_api {

    /**
    * Returns description of method parameters
    * @return external_function_parameters
    */
    public static function save_ordering_parameters() {
        return new external_function_parameters(
            array(
                'move' => new external_value(PARAM_INT, 'item id'),
                'moveafter' => new external_value(PARAM_INT, 'moveafter id')
            )
        );
    }

    public static function save_ordering($move = 0, $moveafter = 0) {
        global $DB, $CFG, $USER;

        $params = self::validate_parameters(
            self::save_ordering_parameters(),
            array(
                'move' => $move,
                'moveafter' => $moveafter,
            )
        );

        $moving_item = $DB->get_record("local_quickmodules", array('id'=>$move));

        $params = array('move'=>$move); $where = ' id <> :move ';

        if (!has_capability('local/quickadd:seeallmodules', context_system::instance())){
            $managers = get_users_by_capability(context_system::instance(), 'local/quickadd:seeallmodules', 'u.id');

            if (count($managers)){
                $users = array_keys($managers) + array($USER->id);
            } else {
                $users = array($USER->id);
            }

            list($sql_in, $params) = $DB->get_in_or_equal($users, SQL_PARAMS_NAMED);
            $where .= " AND userid $sql_in ";
        }

        if ($moveafter == 0){

            $moving_item->sortorder = 0;
            $DB->update_record("local_quickmodules", $moving_item);

            $items = $DB->get_records_sql("SELECT id, sortorder
                                                 FROM {local_quickmodules}
                                                WHERE $where
                                             ORDER BY sortorder", $params);

            $i = 1;
            foreach ($items as $item){
                $item->sortorder = $i++;
                $DB->update_record("local_quickmodules", $item);
            }
        } else {
            $moveafter_item = $DB->get_record("local_quickmodules", array('id'=>$moveafter));

            $moving_item->sortorder = ++$moveafter_item->sortorder;
            $DB->update_record("local_quickmodules", $moving_item);

            $items = $DB->get_records_sql("SELECT id, sortorder
                                                 FROM {local_quickmodules}
                                                WHERE $where
                                             ORDER BY sortorder", $params);

            $i = 0;
            foreach ($items as $item){
                $item->sortorder = $i++;
                $DB->update_record("local_quickmodules", $item);
                if ($item->id == $moveafter_item->id) $i++;
            }
        }

        return array('status' => 'success');
    }

    /**
    * Returns description of method result value
    * @return external_description
    */
    public static function save_ordering_returns() {
        return new external_single_structure(
            array (
                'status' => new external_value(PARAM_NOTAGS, 'Status of ordering update')
            )
        );
    }

}
