// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is an empty module, that is required before all other modules.
 * Because every module is returned from a request for any other module, this
 * forces the loading of all modules with a single request.
 *
 * This function also sets up the listeners for ajax requests so we can tell
 * if any requests are still in progress.
 *
 * @package    local_quickadd
 * @module     local_quickadd/quickadd_modal
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define(['jquery', 'core/ajax', 'core/log', 'core/str', 'core/config', 'core/modal_factory', 'core/modal_events', 'core/templates', 'theme_primaryisf/classyeditor'], function($, ajax, log, str, mdlcfg, ModalFactory, ModalEvents, Templates, editor) {

    editor.init($);

    var QuickAddModal = {
        init: function(courseid, eid) {
            var currentSectionID = 0;
            var currentModuleID = 0;
            $('.quickmodule-chooser').each(function(){
              $(this).click(function(e){
                currentSectionID = $(this).attr('data-section-id');
                currentModuleID = $(this).attr('data-module-id');
              });
            });

            var quickadd_trigger = $('.quickmodule-chooser');
            str.get_strings([
                { key: 'createnew', component: 'local_quickadd' },
            ]).then(function(add) {
                var body = $('#'+eid).html();
                $('#'+eid).remove();
                ModalFactory.create({
                    type: ModalFactory.types.SAVE_CANCEL,
                    title: add,
                    body: body,
                  }, quickadd_trigger)
                  .done(function(modal) {

                    var listbox = modal.getBody().find('.quickadd-formlist');
                    var formbox = modal.getBody().find('#quickadd_form');
                    var savebtn = modal.getFooter().find('[data-action="save"]');

                    modal.getRoot().on(ModalEvents.shown, function(e) {
                        $(savebtn).prop('disabled', true);
                        $(listbox).show();
                        $(formbox).html('');
                    });

                    $(listbox).find('li').click(function(e){
                        var id = $(this).attr('data-module-id');
                        $(listbox).hide();
                        $(formbox).html('<span class="loader"><i class="fa fa-spin fa-spinner"></i></span>').load( mdlcfg.wwwroot+"/local/quickadd/ajax.php?action=get_module_form&courseid="+courseid+"&id="+id+"&sectionid="+currentSectionID+"&cmid="+currentModuleID, function() {
                            $(savebtn).prop('disabled', false);

                            $("#quickadd_form textarea.form-control").each(function(e){
                                $(this).Editor();
                                $(this).Editor("setText", $(this).val());
                            });
                        });
                    });

                    modal.getRoot().on(ModalEvents.save, function(e) {
                        $("#quickadd_form textarea.form-control").each(function(e){
                            var text = $(this).Editor("getText");
                            $(this).val(text);
                        });
                        $(formbox).submit();
                    });


                  });
            });

        }
    };

    /**
    * @alias module:local_quickadd/quickadd_modal
    */
    return QuickAddModal;

});
