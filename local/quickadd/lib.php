<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Displays information about all the assignment modules in the requested course
 *
 * @package   local_quickadd
 * @copyright 2017 ISF
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

function local_quickadd_create($data) {
    global $DB, $USER;

    $last_module = $DB->get_record_sql("SELECT MAX(sortorder) as last FROM {local_quickmodules}");

    $module = new stdClass();
    $module->userid = $USER->id;
    $module->cmid = $data->coursemodule;
    $module->state = 1;
    $module->timemodified = time();
    $module->sortorder = ($last_module->last) ? $last_module->last+1 : 1;
    $module->id = $DB->insert_record("local_quickmodules", $module);

    return $module->id;
}

function local_quickadd_update($data) {
    global $DB;

    $module = $DB->get_record('local_quickmodules', array('cmid'=>$data->coursemodule));

    if (isset($module->id)) {
        $module->timemodified = time();
        $DB->update_record("local_quickmodules", $module);
    } else {
        local_quickadd_create($data);
    }
}

function local_quickadd_delete_module($item, $cm) {
    global $DB;
    $DB->delete_records('local_quickmodules', array('id'=>$item->id));
    course_delete_module($cm->id, true);
}

function local_quickadd_get_modules() {
    global $DB, $USER;

    $where = ''; $params = array();
    if (!has_capability('local/quickadd:seeallmodules', context_system::instance())){
        $managers = get_users_by_capability(context_system::instance(), 'local/quickadd:seeallmodules', 'u.id');

        if (count($managers)){
            $users = array_keys($managers) + array($USER->id);
        } else {
            $users = array($USER->id);
        }

        list($sql_in, $params) = $DB->get_in_or_equal($users, SQL_PARAMS_NAMED);
        $where .= " AND q.userid $sql_in ";
    }

    $sql = "SELECT q.*, m.name as modname, cm.instance
              FROM {local_quickmodules} q
         LEFT JOIN {course_modules} cm ON cm.id = q.cmid
         LEFT JOIN {modules} m ON m.id = cm.module
             WHERE q.id > 0 AND q.state > 0 AND m.visible > 0 $where
          ORDER BY q.sortorder";

    return $DB->get_records_sql($sql, $params);
}

function local_quickadd_get_fields($module = '') {
    $fields = array('name' => array('type'=>'text', 'title'=>get_string('name')),
                    'intro' => array('type'=>'textarea', 'title'=>get_string('moduleintro'))
                   );

    switch ($module) {
        case 'assign':
            $fields['duedate'] = array('type'=>'time', 'title'=>get_string('duedate', 'mod_assign'));
        case 'lti':
            $fields['toolurl'] = array('type'=>'url', 'title'=>get_string('toolurl', 'mod_lti'));
        case 'page':
            $fields['text'] = array('type'=>'textarea', 'title'=>get_string('content', 'page'));
        case 'url':
            $fields['externalurl'] = array('type'=>'url', 'title'=>get_string('externalurl', 'mod_url'));
        case 'label':
            $fields['intro']['title'] = get_string('labeltext', 'label');
        case 'resource':
            //$fields['packagefile'] = array('type'=>'file', 'title'=>get_string('packagefile', 'mod_resource'));
        case 'scorm':
            //$fields['files'] = array('type'=>'file', 'title'=>get_string('file', 'mod_scorm'));
    }

    return $fields;
}


