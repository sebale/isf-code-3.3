<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
* Adds or updates modules in a local_quickadd using new formslib
*
* @package    local_quickadd
* @copyright  2017 ISF
* @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
*/

require_once("../../config.php");
require_once('lib.php');
require_once($CFG->libdir.'/filelib.php');
require_once($CFG->libdir.'/gradelib.php');
require_once($CFG->libdir.'/completionlib.php');
require_once($CFG->libdir.'/plagiarismlib.php');
require_once($CFG->dirroot . "/course/lib.php");
require_once($CFG->dirroot . '/course/modlib.php');

$add        = optional_param('add', '', PARAM_ALPHA);     // module name
$id         = optional_param('id', 0, PARAM_INT);
$update     = optional_param('update', 0, PARAM_INT);
$action     = optional_param('action', '', PARAM_NOTAGS);
$confirm    = optional_param('confirm', 0, PARAM_BOOL);

$url = new moodle_url('/local/quickadd/modedit.php');

if ($action == 'create_quickmodule' and $id > 0){
    require_once($CFG->dirroot.'/course/lib.php');
    require_once($CFG->dirroot.'/course/format/isf/lib.php');

    $PAGE->set_context(context_system::instance());

    $form       = (object)optional_param_array('form', array(), PARAM_RAW);
    $section    = optional_param('section', 0, PARAM_INT);
    $courseid   = optional_param('courseid', 0, PARAM_INT);
    $cmid       = optional_param('cmid', 0, PARAM_INT);

    $cm = course_copy_mod($courseid, $section, $id, $cmid);

    if (isset($cm->id) and count($form)) {
        $fields = local_quickadd_get_fields($cm->modname);
        $instance = $DB->get_record($cm->modname, array('id'=>$cm->instance));

        foreach ($form as $fieldname=>$fieldvalue) {
            if ($fieldname == 'name'){
                $instance->name    = (!empty($form->name) and $form->name != $instance->name) ? format_string($form->name) : $instance->name;
            } elseif (isset($fields[$fieldname]) and ($fields[$fieldname]['type'] == 'time' or $fields[$fieldname]['type'] == 'date')) {
                $instance->{$fieldname}   = (!empty($form->{$fieldname})) ? strtotime($form->{$fieldname}) : $instance->{$fieldname};
            } else {
                $instance->{$fieldname}   = (!empty($form->{$fieldname}) and $form->{$fieldname} != $instance->{$fieldname}) ? $form->{$fieldname} : $instance->{$fieldname};
            }
        }

        $DB->update_record($cm->modname, $instance);
        rebuild_course_cache($cm->course);
    }

    redirect(new moodle_url("/course/view.php", array('id'=>$cm->course)));
    exit;
}


if (!empty($add)) {
    $url->param('add', $add);
}
if (!empty($update)) {
    $url->param('update', $update);
}
if ($id) {
    $url->param('id', $id);
    $item = $DB->get_record('local_quickmodules', array('id'=>$id));
    $cm = get_coursemodule_from_id('', $item->cmid, 0, false, MUST_EXIST);

}
$PAGE->set_url($url);

// actions
if ($action == 'show' and $id) {
    $item->state = 1;
    $DB->update_record('local_quickmodules', $item);
    redirect(new moodle_url("/local/quickadd/index.php"), get_string('quickaddupdated', 'local_quickadd'));
} else if ($action == 'hide' and $id) {
    $item->state = 0;
    $DB->update_record('local_quickmodules', $item);
    redirect(new moodle_url("/local/quickadd/index.php"), get_string('quickaddupdated', 'local_quickadd'));
} else if ($action == 'delete' and $id) {
    if ($confirm and confirm_sesskey()) {
        local_quickadd_delete_module($item, $cm);
        redirect(new moodle_url("/local/quickadd/index.php"), get_string('deleted', 'local_quickadd'));
    }
    $strheading = get_string('deletemodule', 'local_quickadd');
    $PAGE->set_context(context_system::instance());
    $PAGE->navbar->add(get_string('pluginname', 'local_quickadd'), new moodle_url('/local/quickadd/index.php'));
    $PAGE->navbar->add($strheading);
    $PAGE->set_title($strheading);
    $PAGE->set_heading($strheading);

    $course = $DB->get_record('course', array('id'=>$cm->course), '*', MUST_EXIST);
    $modinfo = get_fast_modinfo($course);
    $mod = $modinfo->cms[$cm->id];

    echo $OUTPUT->header();
    echo $OUTPUT->heading($strheading);
    $yesurl = new moodle_url($CFG->wwwroot.'/local/quickadd/modedit.php', array('id' => $item->id, 'action' => 'delete', 'confirm' => 1, 'sesskey' => sesskey(), 'returnurl' => new moodle_url('/local/quickadd/index.php')));
    $message = get_string('confirmdelete', 'local_quickadd', $mod->get_formatted_name());
    echo $OUTPUT->confirm($message, $yesurl, new moodle_url('/local/quickadd/index.php'));
    echo $OUTPUT->footer();
    die;
}

if (!empty($add)) {
    $section = 0;
    $course = $DB->get_record('course', array('id'=>1), '*', MUST_EXIST);
    require_login($course);

    list($module, $context, $cw, $cm, $data) = prepare_new_moduleinfo_data($course, $add, $section);
    $data->add = $add;

} else if (!empty($update)) {

    // Check the course module exists.
    $cm = get_coursemodule_from_id('', $update, 0, false, MUST_EXIST);

    // Check the course exists.
    $course = $DB->get_record('course', array('id'=>$cm->course), '*', MUST_EXIST);

    // require_login
    require_login($course);

    list($cm, $context, $module, $data, $cw) = get_moduleinfo_data($cm, $course);
    $data->update = $update;

    // SEBALE
    $filesoptions = course_overviewfiles_options($course);
    $filesoptions['accepted_types'] = array('.jpg', '.gif', '.png');
    file_prepare_standard_filemanager($data, 'activityimage', $filesoptions, $context, 'format_isf', 'activityimage', $cm->id);
    // SEBALE

} else {
    require_login();
    print_error('invalidaction');
}

$PAGE->set_pagetype('local-quick-modules-form');
$PAGE->set_pagelayout('admin');

$modmoodleform = "$CFG->dirroot/mod/$module->name/mod_form.php";
if (file_exists($modmoodleform)) {
    require_once($modmoodleform);
} else {
    print_error('noformdesc');
}

$mformclassname = 'mod_'.$module->name.'_mod_form';
$mform = new $mformclassname($data, $cw->section, $cm, $course);
$mform->set_data($data);

if ($mform->is_cancelled()) {
    redirect("$CFG->wwwroot/local/quickadd/index.php");
} else if ($fromform = $mform->get_data()) {
    // Convert the grade pass value - we may be using a language which uses commas,
    // rather than decimal points, in numbers. These need to be converted so that
    // they can be added to the DB.
    if (isset($fromform->gradepass)) {
        $fromform->gradepass = unformat_float($fromform->gradepass);
    }

    if (!empty($fromform->update)) {
        list($cm, $fromform) = update_moduleinfo($cm, $fromform, $course, $mform);
    } else if (!empty($fromform->add)) {
        $fromform = add_moduleinfo($fromform, $course, $mform);
    } else {
        print_error('invaliddata');
    }

    // SEBALE
    if ($course->format == 'isf') {
        $course = course_get_format($course)->get_course();
        if (isset($course->coursetype) and $course->coursetype) {
            course_get_format($course)->process_activityimages_chooser($course, $fromform, $mform->get_data());
        }
    }
    // SEBALE

    if (!empty($add)){
        local_quickadd_create($fromform);
        redirect("$CFG->wwwroot/local/quickadd/index.php", get_string('quickaddcreated', 'local_quickadd'));
    } else {
        local_quickadd_update($fromform);
        redirect("$CFG->wwwroot/local/quickadd/index.php", get_string('quickaddupdated', 'local_quickadd'));
    }

    exit;

} else {

    $fullmodulename = get_string('modulename', $module->name);
    if (!empty($add)){
        $pageheading = get_string('creatingnew', 'local_quickadd', $fullmodulename);
    } else {
        $pageheading = get_string('updating', 'local_quickadd', $fullmodulename);
    }

    $PAGE->set_heading($pageheading);
    $PAGE->set_title($pageheading);
    $PAGE->set_cacheable(false);

    $PAGE->navbar->add(get_string('pluginname', 'local_quickadd'), new moodle_url('/local/quickadd/index.php'));
    $PAGE->navbar->add($pageheading);

    echo $OUTPUT->header();
    echo $OUTPUT->heading($pageheading);

    $mform->display();
    $PAGE->requires->js_call_amd('local_quickadd/quickadd', 'edit', array('str'=>get_string('save', 'local_quickadd')));

    echo $OUTPUT->footer();
}
