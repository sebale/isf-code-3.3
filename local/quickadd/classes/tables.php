<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Displays information about all the assignment modules in the requested course
 *
 * @package   local_quickadd
 * @copyright 2017 ISF
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class modules_table extends table_sql {

    public $_modinfo = null;

    function __construct($uniqueid, $search) {
        global $CFG, $USER, $DB;

        parent::__construct($uniqueid);

        $course = $DB->get_record('course', array('id'=>1));
        $this->_modinfo = get_fast_modinfo($course);

        $columns = array('id', 'sortorder', 'modname', 'title', 'creator');
        $headers = array(
            'ID',
            '#',
            get_string('module', 'local_quickadd'),
            get_string('title', 'local_quickadd'),
            get_string('creator', 'local_quickadd'),
        );

        $this->no_sorting('title');
        $this->no_sorting('modname');
        $this->no_sorting('creator');

        if (has_capability('local/quickadd:add', context_system::instance())){
            $columns[] = 'actions';
            $headers[] = get_string('actions', 'local_quickadd');

            $this->no_sorting('actions');
        }

        $this->define_columns($columns);
        $this->define_headers($headers);
        $params = array();

        $fields = "q.*, m.name as modname, cm.instance, CONCAT(u.firstname, ' ', u.lastname) as creator";
        $from = "{local_quickmodules} q
                    LEFT JOIN {course_modules} cm ON cm.id = q.cmid
                    LEFT JOIN {modules} m ON m.id = cm.module
                    LEFT JOIN {user} u ON u.id = q.userid AND u.deleted = 0
                    ";
        $where = "q.id > 0";

        if (!has_capability('local/quickadd:seeallmodules', context_system::instance())) {
            $where .= " AND q.userid = :userid ";
            $params['userid'] = $USER->id;
        }

        $this->set_sql($fields, $from, $where, $params);
        $this->define_baseurl("$CFG->wwwroot/local/quickadd/index.php");
    }

    function col_title($values) {
        $mod = $this->_modinfo->cms[$values->cmid];
        return $mod->get_formatted_name();
    }

    function col_modname($values) {
        return get_string('modulename', $values->modname);
    }

    function col_creator($values) {
        global $CFG;

        return html_writer::link(new moodle_url($CFG->wwwroot.'/user/profile.php', array('id'=>$values->userid)), $values->creator, array('title' => $values->creator));
    }

    function col_actions($values) {
        global $CFG, $OUTPUT, $USER;

        $buttons = array();

        if (!has_capability('local/quickadd:add', context_system::instance())){
            return '';
        }

        $urlparams = array('id' => $values->id);

        // edit link
        $editparams = array('update' => $values->cmid);
        $buttons[] = html_writer::link(
            new moodle_url($CFG->wwwroot.'/local/quickadd/modedit.php', $editparams), html_writer::tag('i', '', array('class' => 'fa fa-pencil iconsmall', 'alt' => get_string('edit'))),
            array('title' => get_string('edit'))
        );

        // delete link
        $buttons[] = html_writer::link(
            new moodle_url($CFG->wwwroot.'/local/quickadd/modedit.php', $urlparams + array('action' => 'delete')),
            html_writer::tag('i', '', array('alt' => get_string('delete'), 'class' => 'iconsmall fa fa-trash')),
            array('title' => get_string('delete')));

        // show/hide

        if ($values->state > 0){
            $buttons[] = html_writer::link(
                new moodle_url($CFG->wwwroot.'/local/quickadd/modedit.php', $urlparams + array('action' => 'hide')),
                html_writer::tag('i', '', array('alt' => get_string('hide'), 'class' => 'iconsmall fa fa-eye')),
                array('title' => get_string('hide')));
        } else {
            $buttons[] = html_writer::link(
                new moodle_url($CFG->wwwroot.'/local/quickadd/modedit.php', $urlparams + array('action' => 'show')),
                html_writer::tag('i', '', array('alt' => get_string('show'), 'class' => 'fa fa-eye-slash iconsmall')),
                array('title' => get_string('show')));
        }

        $buttons[] = html_writer::link(
                'javascript:void(0)',
                html_writer::tag('i', '', array('alt' => get_string('move'), 'class' => 'fa fa-arrows iconsmall move-action')),
                array('title' => get_string('move')));

        return implode(' ', $buttons);
    }

    function print_row($row, $classname = '') {
        echo $this->get_row_html($row, $classname.(($classname != '') ? ' ' : '').((isset($row[0])) ? $row[0] : ''));
    }
}

