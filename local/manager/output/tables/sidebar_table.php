<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Displays information about all the assignment modules in the requested course
 *
 * @package   local_quicklinks
 * @copyright 2017 ISF
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/tablelib.php');

class sidebar_table extends table_sql {

    protected $_types = null;
    protected $_context = null;

    function __construct($uniqueid, $context, $search, $theme) {
        global $CFG, $USER;

        parent::__construct($uniqueid);
        $this->_types = local_manager_get_usertypes();
        $this->_context = $context;

        $columns = array('id', 'sortorder', 'title', 'title_ch', 'link', 'types', 'actions');
        $headers = array(
            'ID',
            '#',
            get_string('shortcuttitleen', 'local_manager'),
            get_string('shortcuttitlech', 'local_manager'),
            get_string('link', 'local_manager'),
            get_string('usertypes', 'local_manager'),
            get_string('actions', 'local_manager'),
        );

        $this->no_sorting('title');
        $this->no_sorting('title_ch');
        $this->no_sorting('link');
        $this->no_sorting('types');
        $this->no_sorting('actions');

        $this->define_columns($columns);
        $this->define_headers($headers);

        $sql_search = ($search) ? " AND (s.title LIKE '%$search%' OR s.title_ch LIKE '%$search%')" : "";

        $fields = "s.*, s.id as actions";
        $from = "{local_manager_sidebar} s";
        $where = "s.theme = :theme AND core=0".$sql_search;

        $params = array('theme'=>$theme);

        $this->set_sql($fields, $from, $where, $params);
        $this->define_baseurl("$CFG->wwwroot/local/manager/settings/sidebar.php?theme=".$theme."&search=".$search);
    }

    function col_types($values) {
        $usertypes = array();

        if (!empty($values->types)) {
            $types = explode(',', $values->types);
            if (count($types)) {
                foreach ($types as $type) {
                    $usertypes[] = $this->_types[$type];
                }
            }
        }
        return implode(', ', $usertypes);
    }

    function col_actions($values) {
        global $CFG, $OUTPUT, $USER;

        $buttons = array();

        $urlparams = array('id' => $values->id, 'theme' => $values->theme);

        // edit link
        $buttons[] = html_writer::link(
            new moodle_url($CFG->wwwroot.'/local/manager/settings/sidebar_edit.php', $urlparams), html_writer::tag('i', '', array('class' => 'fa fa-pencil iconsmall', 'alt' => get_string('edit'))),
            array('title' => get_string('edit'))
        );

        // delete link
        $buttons[] = html_writer::link(
            new moodle_url($CFG->wwwroot.'/local/manager/settings/sidebar_edit.php', $urlparams + array('action' => 'delete')),
            html_writer::tag('i', '', array('alt' => get_string('delete'), 'class' => 'iconsmall fa fa-trash')),
            array('title' => get_string('delete')));

        // show/hide

        if ($values->state > 0){
            $buttons[] = html_writer::link(
                new moodle_url($CFG->wwwroot.'/local/manager/settings/sidebar_edit.php', $urlparams + array('action' => 'hide')),
                html_writer::tag('i', '', array('alt' => get_string('hide'), 'class' => 'iconsmall fa fa-eye')),
                array('title' => get_string('hide')));
        } else {
            $buttons[] = html_writer::link(
                new moodle_url($CFG->wwwroot.'/local/manager/settings/sidebar_edit.php', $urlparams + array('action' => 'show')),
                html_writer::tag('i', '', array('alt' => get_string('show'), 'class' => 'fa fa-eye-slash iconsmall')),
                array('title' => get_string('show')));
        }

        $buttons[] = html_writer::link(
                'javascript:void(0)',
                html_writer::tag('i', '', array('alt' => get_string('move'), 'class' => 'fa fa-arrows iconsmall move-action')),
                array('title' => get_string('move')));

        return implode(' ', $buttons);
    }

    function print_row($row, $classname = '') {
        echo $this->get_row_html($row, $classname.(($classname != '') ? ' ' : '').((isset($row[0])) ? $row[0] : ''));
    }
}

class sidebar_table_core extends table_sql {

    protected $_types = null;
    protected $_context = null;

    function __construct($uniqueid, $context, $search, $theme) {
        global $CFG, $USER;

        parent::__construct($uniqueid);
        $this->_types = local_manager_get_usertypes();
        $this->_context = $context;

        $columns = array('id', 'sortorder', 'title', 'title_ch', 'icon', 'actions');
        $headers = array(
            'ID',
            '#',
            get_string('shortcuttitleen', 'local_manager'),
            get_string('shortcuttitlech', 'local_manager'),
            get_string('icon', 'local_manager'),
            get_string('actions', 'local_manager'),
        );

        $this->no_sorting('title');
        $this->no_sorting('title_ch');
        $this->no_sorting('icon');
        $this->no_sorting('actions');

        $this->define_columns($columns);
        $this->define_headers($headers);

        $sql_search = ($search) ? " AND (s.title LIKE '%$search%' OR s.title_ch LIKE '%$search%')" : "";

        $fields = "s.*, s.id as actions";
        $from = "{local_manager_sidebar} s";
        $where = "s.theme = :theme AND core=1 ".$sql_search;

        $params = array('theme'=>$theme);

        $this->set_sql($fields, $from, $where, $params);
        $this->define_baseurl("$CFG->wwwroot/local/manager/settings/sidebar.php?theme=".$theme."&search=".$search);
    }

    function col_actions($values) {
        global $CFG, $OUTPUT, $USER;

        $buttons = array();

        $urlparams = array('id' => $values->id, 'theme' => $values->theme);

        // edit link
        $buttons[] = html_writer::link(
            new moodle_url($CFG->wwwroot.'/local/manager/settings/sidebar_edit.php', $urlparams), html_writer::tag('i', '', array('class' => 'fa fa-pencil iconsmall', 'alt' => get_string('edit'))),
            array('title' => get_string('edit'))
        );
        // show/hide

        if ($values->state > 0){
            $buttons[] = html_writer::link(
                new moodle_url($CFG->wwwroot.'/local/manager/settings/sidebar_edit.php', $urlparams + array('action' => 'hide')),
                html_writer::tag('i', '', array('alt' => get_string('hide'), 'class' => 'iconsmall fa fa-eye')),
                array('title' => get_string('hide')));
        } else {
            $buttons[] = html_writer::link(
                new moodle_url($CFG->wwwroot.'/local/manager/settings/sidebar_edit.php', $urlparams + array('action' => 'show')),
                html_writer::tag('i', '', array('alt' => get_string('show'), 'class' => 'fa fa-eye-slash iconsmall')),
                array('title' => get_string('show')));
        }

        return implode(' ', $buttons);
    }
}
