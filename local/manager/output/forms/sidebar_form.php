<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once($CFG->dirroot.'/lib/formslib.php');

/**
 * The form for handling editing sidebar item.
 */
class sidebar_edit_form extends moodleform {

    /**
     * Form definition.
     */
    function definition() {
        global $CFG, $PAGE, $USER;

        $mform    = $this->_form;

        $data               = $this->_customdata['data'];
        $theme               = $this->_customdata['theme'];
        $context            = $this->_customdata['context'];
        $imagefilesoptions  = $this->_customdata['imagefilesoptions'];

        $strrequired = get_string('requiredfield', 'local_manager');

        $mform->addElement('hidden', 'id', 0);
        $mform->setType('id', PARAM_INT);

        $mform->addElement('hidden', 'theme', $theme);
        $mform->setType('theme', PARAM_RAW);

        $mform->addElement('text', 'title', get_string('shortcuttitleen', 'local_manager'), array('size'=>'30'));
        $mform->addRule('title', $strrequired, 'required', null, 'client');
        $mform->setType('title', PARAM_NOTAGS);

        $mform->addElement('text', 'title_ch', get_string('shortcuttitlech', 'local_manager'), array('size'=>'30'));
        $mform->setType('title_ch', PARAM_NOTAGS);

        if(!isset($data->core) || $data->core == 0){
            $options = array(
                'multiple' => true,
                'noselectionstring' => get_string('selectusertype', 'local_manager')
            );

            $select_roles = $mform->addElement('autocomplete', 'types', get_string('usertypes', 'local_manager'), local_manager_get_usertypes(), $options);
            $mform->addRule('types', $strrequired, 'required', null, 'client');

            $mform->addElement('text', 'link', get_string('link', 'local_manager'), array('size' => '30'));
            $mform->addRule('link', $strrequired, 'required', null, 'client');
            $mform->setType('link', PARAM_NOTAGS);

            $mform->addElement('select', 'display', get_string('display', 'local_manager'), array(0 => get_string('samepage', 'local_manager'), 1 => get_string('newpage', 'local_manager')), array('size' => '30'));
            $mform->setType('type', PARAM_INT);
        }

        $mform->addElement('text', 'icon', get_string('selecticon', 'local_manager'));
        $mform->setType('icon', PARAM_TEXT);

        $mform->addElement('html', '<div class="form-group row fitem"><div class="col-md-3"></div><div class="col-md-9 form-inline felement"><span class="course-res-info">'.get_string('fa_info', 'local_manager').'</span></div></div>');

        $mform->addElement('filemanager', 'shortcutimage_filemanager', get_string('shortcutimage', 'local_manager'), null, $imagefilesoptions);

        $mform->addElement('textarea', 'svg', get_string('svgimage', 'local_manager'));
        $mform->setType('svg', PARAM_RAW);

        $this->add_action_buttons('true', ((isset($data->id) and $data->id) ? get_string('save', 'local_manager') : get_string('create', 'local_manager')));

        $this->set_data($data);
    }

}

