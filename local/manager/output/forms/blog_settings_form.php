<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Manage course images
 * @package format_talentquest
 * @copyright 2016 SEBALE
 * @author sebale.net
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once($CFG->libdir.'/formslib.php');

class blog_settings_form extends moodleform {

    /**
     * Form definition.
     */
    public function definition() {
        $mform =& $this->_form;

        $mform->addElement('text', 'color', get_string('blogbackgroundcolor', 'local_manager'), 'class="ColorPicker"');
        $mform->setType('color', PARAM_TEXT);

        $options = array('subdirs' => 0, 'maxfiles' => 1, 'accepted_types' => array('.jpg', '.gif', '.png'));

        $mform->addElement('filemanager', 'blogbg_filemanager', get_string('blogbackgroundimage', 'local_manager'), null, $options);

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);

        $this->add_action_buttons();
        $this->set_data($this->_customdata['data']);
    }
}
