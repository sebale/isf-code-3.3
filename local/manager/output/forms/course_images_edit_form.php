<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Manage course images
 * @package format_talentquest
 * @copyright 2016 SEBALE
 * @author sebale.net
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once($CFG->libdir.'/formslib.php');

class course_images_edit_form extends moodleform {

    /**
     * Form definition.
     */
    public function definition() {
        $mform =& $this->_form;
        $course = $this->_customdata['course'];
        $modules = $this->_customdata['modules'];

        $options = array('subdirs' => 0, 'maxfiles' => -1, 'accepted_types' => array('.jpg', '.gif', '.png'), 'return_types' => FILE_INTERNAL | FILE_REFERENCE);

        $mform->addElement('header', 'carouselimageshdr', get_string('carouselimages', 'local_manager'));
        $mform->setExpanded('carouselimageshdr', true);

        $mform->addElement('filemanager', 'courseimages_filemanager', get_string('files'), null, $options);

        $mform->addElement('header', 'thumbimageshdr', get_string('thumbimages', 'local_manager'));
        $mform->setExpanded('thumbimageshdr', false);

        $options = array('subdirs' => 0, 'maxfiles' => 1, 'accepted_types' => array('.jpg', '.gif', '.png'), 'return_types' => FILE_INTERNAL | FILE_REFERENCE);
        $mform->addElement('filemanager', 'defaultimage_filemanager', get_string('defaultimage', 'local_manager'), null, $options);

        if (count($modules)) {
            foreach ($modules as $modname=>$mod) {
                $mform->addElement('filemanager', 'defaultimage_'.$modname.'_filemanager', $mod->title, null, $options);
            }
        }

        $mform->addElement('hidden', 'courseid', $course->id);
        $mform->setType('courseid', PARAM_INT);

        $this->add_action_buttons(false, get_string('savechanges'));
        $this->set_data($this->_customdata['data']);
    }
}
