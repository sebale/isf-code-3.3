// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is an empty module, that is required before all other modules.
 * Because every module is returned from a request for any other module, this
 * forces the loading of all modules with a single request.
 *
 * This function also sets up the listeners for ajax requests so we can tell
 * if any requests are still in progress.
 *
 * @package    local_manager
 * @module     local_manager/course
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define(['jquery', 'core/str', 'core/config', 'core/notification', 'core/log', 'local_manager/datatable'], function($, str, config, notification, log, Datatable) {

    Datatable.init($, window, document, undefined);    

    var Course = {
        
        init: function(capability) {

            log.debug('init');
            log.debug(capability);

            str.get_strings([
                {'key' : 'search', component : 'local_manager'},
                {'key' : 'show_all', component: 'local_manager'},
                {'key' : 'active', component : 'local_manager'},
                {'key' : 'inactive', component : 'local_manager'}
            ]).done(function(Loc) {

                var courses = $('#datatable_courses').dataTable({

                    "bAutoWidth": false,
                    "bProcessing": false,
                    "bServerSide": true,
                    "sAjaxSource": config.wwwroot + "/local/manager/courses/courses_datatables.php",
                    "sDom": 'T<"clear">lfrtip',
                    "sPaginationType": "full_numbers",
                    "order": [[ 1, "asc" ]],
                    "oLanguage": {
                        "sSearch": "_INPUT_",
                        "sLengthMenu": "<span>_MENU_</span>",
                        "oPaginate": { "sFirst": "First", "sLast": "Last" }
                    },
                    "aoColumnDefs": [ 
                        { "bSearchable": true, "bVisible": false, "aTargets": [ 0 ] },
                        { "bSortable": false, "aTargets": [ -1 ] },
                    ],
                    "oTableTools": {
                        "aButtons": []
                    }
                });
                
                $('.dataTables_filter input').attr('placeholder', Loc[0]);
                var filter = '1';

                if (courses.fnSettings().aoPreSearchCols[0].sSearch.length){
                    filter = courses.fnSettings().aoPreSearchCols[0].sSearch;
                }
                
                $('.dataTables_filter input')
                    .before('<select id="table_filter" class="form-control" style="margin-right: 7px;"><option value="-1" '+((filter=='-1') ? "selected" : "")+'>' + Loc[1] + '</option><option value="1" '+((filter=='1') ? "selected" : "")+'>' + Loc[2] + '</option><option value="0" '+((filter=='0') ? "selected" : "")+'>' + Loc[3] + '</option></select>');
                
                $('#table_filter').change(function(){
                    courses.fnFilter( $(this).val(), 0 );
                });

                if (capability) {
                    $('.dataTables_filter').addClass('relative');
                }

                window.toggleCourse = function(id, action){
                    if (action == 'hidecourse'){
                        $('.hideitem_'+id).addClass('hidden');
                        $('.showitem_'+id).removeClass('hidden');
                    } else {
                        $('.hideitem_'+id).removeClass('hidden');
                        $('.showitem_'+id).addClass('hidden');
                    }
                    $.get(config.wwwroot + '/course/ajax/management.php?courseid='+id+'&action='+action+'&ajax=1&sesskey=' + config.sesskey, function(data){ });
                }

            }).fail(function(err){
                log.debug(err);
            });
        }
    };

    /**
    * @alias module:local_manager/course
    */
    return Course;

});
