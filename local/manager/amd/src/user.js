// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is an empty module, that is required before all other modules.
 * Because every module is returned from a request for any other module, this
 * forces the loading of all modules with a single request.
 *
 * This function also sets up the listeners for ajax requests so we can tell
 * if any requests are still in progress.
 *
 * @package    local_manager
 * @module     local_manager/user
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define(['jquery', 'core/str', 'core/config', 'core/notification', 'core/log', 'local_manager/datatable'], function($, str, config, notification, log, Datatable) {

    Datatable.init($, window, document, undefined);

    var User = {

        init: function(firstRole) {

            $('.users-tabs-header li').click(function(event){
                var role_id = $(this).attr('id');
                var index = $(".users-tabs-header li").index(this);
                $('.users-tabs .users-tabs-item').eq(index).addClass('load');
                if (role_id != 'search-tab'){
                    var roleid = role_id.replace("role_", "");
                    $('.users-tabs .users-tabs-item').eq(index).html('<i class="fa fa-spin fa-spinner"></i>').load(config.wwwroot + "/local/manager/ajax.php?action=load_users&roleid=" + roleid,  function() {
                        $('.users-tabs .users-tabs-item').eq(index).removeClass('load');
                    });
                }
            });

            $('#all-search').keyup( function () {
                var filter = $(this).val().toLowerCase();
                var input = $(this);

                if (filter) {
                    $('#search-tab').removeClass('hidden');
                    $(".users-tabs-header .nav-link").removeClass('active');
                    $('#search-tab .nav-link').addClass('active');
                    $(".users-tabs-item").removeClass('active');
                    $('#roleid_search').addClass('active');
                    $('.search-users-tab').addClass('load');
                    $('.search-users-tab').html('<i class="fa fa-spin fa-spinner"></i>').load(config.wwwroot + "/local/manager/ajax.php?action=search_users", { search: filter }, function() {
                        $('.search-users-tab').removeClass('load');
                        $('#all-search').focus();
                    });
                } else {
                    $(".users-tabs-header .nav-link").removeClass('active');
                    $('.users-tabs-header li').eq(0).find('.nav-link').addClass('active');
                    $(".users-tabs-item").removeClass('active');
                    $('.users-tabs .users-tabs-item').eq(0).addClass('active');
                    $('#search-tab').addClass('hidden');
                }

            });

            $('.search-allusers-box i').click( function () {
                $('#all-search').trigger('keyup');
            });

            str.get_strings([
                {'key' : 'userdeletemsg', component : 'local_manager'}
            ]).done(function(Loc) {
                window.delete_user = function(id){
                    if (confirm(Loc[0])){
                        $.get(config.wwwroot + "/local/manager/ajax.php?action=user_delete&id="+id, function(data){
                            $('#user_'+id).parent().parent().remove();
                            $('.user-manager.manager-'+id).remove();
                        });
                    }
                }
            });

            $(document).ready(function(){
                $('.users-tabs .users-tabs-item#roleid_' + firstRole).addClass('load').html('<i class="fa fa-spin fa-spinner"></i>').load(config.wwwroot + "/local/manager/ajax.php?action=load_users&roleid=" + firstRole,  function() {
                    $('.users-tabs .users-tabs-item#roleid_' + firstRole).removeClass('load');
                });
            });

        }
    };

    /**
    * @alias module:local_manager/user
    */
    return User;

});
