<?php

define('AJAX_SCRIPT', true);

require_once('../../config.php');

$id        = optional_param('id', 0, PARAM_INT);
$type    = optional_param('type', '', PARAM_RAW);
$action = optional_param('action', '', PARAM_RAW);
$search = optional_param('search', '', PARAM_RAW);
$form    = (object)optional_param_array('form', array(), PARAM_RAW);
$msg    = optional_param_array('msg', array(), PARAM_RAW);
$start    = optional_param('start', 0, PARAM_INT);

if ($action == 'user_delete' and $id > 0){
    $user = $DB->get_record('user', array('id'=>$id));
    if ($user){
         delete_user($user);
         \core\session\manager::gc(); // Remove stale sessions.
    }
    echo time();
}else if ($action == 'delete_users'){
    $items = optional_param('items', '', PARAM_RAW);

    if (!empty($items)){
        $items = json_decode($items);
        if (count($items) > 0){
            foreach ($items as $item){
                if (strstr($item, 'row_')){
                    $userid = str_replace('row_', '', $item);
                    $user = $DB->get_record('user', ['id'=>$userid]);
                    if ($user) {
                        delete_user($user);
                        \core\session\manager::gc(); // Remove stale sessions.
                        $result = 'success';
                    }
                }
            }
        }
    }
    echo json_encode(array('result'=>$result));

} elseif ($action == 'load_users'){

    $roleid = optional_param('roleid', 0, PARAM_INT);

    $systemcontext = context_system::instance();
    list($assignableroles, $assigncounts, $nameswithcounts) = get_assignable_roles($systemcontext, ROLENAME_BOTH, true);
    $role = $DB->get_record("role", array('id'=>$roleid));
    ?>
    <?php if ($roleid > 0) : ?>
        <div class="users-create-panel clearfix">
            <?php if (((has_capability('moodle/role:assign', $systemcontext) and isset($assignableroles[$role->id])) || is_siteadmin()) and has_capability('moodle/user:create', $systemcontext)) : ?>
                <button class="btn btn-primary" onclick="location = '<?php echo $CFG->wwwroot;?>/admin/roles/assign.php?contextid=1&roleid=<?php echo $role->id; ?>'"><i class="fa fa-plus"></i> <?php echo get_string('assignrole', 'local_manager');?></button>
            <?php endif;?>
            <?php if (has_capability('moodle/user:create', $systemcontext)) : ?>
                    <button class="btn btn-primary" onclick="location = '<?php echo $CFG->wwwroot;?>/user/editadvanced.php?id=-1'"><i class="fa fa-pencil-square-o"></i> <?php echo get_string('createnew', 'local_manager');?> <?php echo $role->name; ?></button>
            <?php endif; ?>
            </div>
            <table title="<?php echo $role->name; ?>s" id="datatable_<?php echo $role->id; ?>" class="generaltable sorting data-table datatable_<?php echo $role->id; ?> selecting" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th align="center" value="id"><span><?php echo get_string('ID', 'local_manager');?></span></th>
                        <th align="center" value="name" class="active asc"><span><?php echo get_string('name', 'local_manager');?></span></th>
                        <th align="center" value="username"><span><?php echo get_string('username', 'local_manager');?></span></th>
                        <th align="center" value="email"><span><?php echo get_string('email', 'local_manager');?></span></th>
                        <th align="center" value="idnumber"><span><?php echo get_string('user_ID', 'local_manager');?></span></th>
                        <th align="center" value="status"><span><?php echo get_string('status', 'local_manager');?></span></th>
                        <th align="center" class="nosort"><span><?php echo get_string('actions', 'local_manager');?></span></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        <script>
            var role_<?php echo $role->id; ?> = jQuery('#datatable_<?php echo $role->id; ?>').dataTable( {
            "bAutoWidth": false,
            "bServerSide": true,
            "sAjaxSource": "<?php echo $CFG->wwwroot.'/local/manager/users/users_datatables.php';?>?role=<?php echo $role->id; ?>",
            "sDom": 'T<"clear">lfrtip',
            "sPaginationType": "full_numbers",
            "aLengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
            "iDisplayLength": 50,
            "order": [[ 1, "asc" ]],
            "oLanguage": {
                "sSearch": "_INPUT_",
                "sLengthMenu": "<span>_MENU_</span>",
                "oPaginate": { "sFirst": "First", "sLast": "Last" }
            },
            "aoColumnDefs": [
                        { "bSearchable": true, "bVisible": false, "aTargets": [ 0 ] },
                        { "bSortable": false, "aTargets": [ -1 ] },
                    ],
                    "oTableTools": {
                        "aButtons": []
                    },
            });
            <?php if (((!has_capability('moodle/role:assign', $systemcontext) and isset($assignableroles[$role->id])) and is_siteadmin()) or $role->id == 4) : ?>
                jQuery('#datatable_<?php echo $role->id; ?>_wrapper').addClass('withoutbuttons');
            <?php endif; ?>

            jQuery('#datatable_<?php echo $role->id; ?>_wrapper .dataTables_filter input').attr('placeholder', 'Search...');

        </script>
    <?php elseif ($roleid == 0) : ?>
        <table title="<?php echo get_string('notassignedusers', 'local_manager');?>" id="datatable-users" class="generaltable sorting data-table datatable-users selecting" cellpadding="0" cellspacing="0">
            <thead>
            <tr>
                <th align="center" value="id"><span><?php echo get_string('ID', 'local_manager');?></span></th>
                <th align="center" value="name" class="active asc"><span><?php echo get_string('name', 'local_manager');?></span></th>
                <th align="center" value="username"><span><?php echo get_string('username', 'local_manager');?></span></th>
                <th align="center" value="email"><span><?php echo get_string('email', 'local_manager');?></span></th>
                <th align="center" value="idnumber"><span><?php echo get_string('user_ID', 'local_manager');?></span></th>
                <th align="center" value="status"><span><?php echo get_string('status', 'local_manager');?></span></th>
                <th align="center" class="nosort"><span><?php echo get_string('actions', 'local_manager');?></span></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <script>
            var users = jQuery('.datatable-users').dataTable( {
                "bAutoWidth": false,
                "bServerSide": true,
                "sAjaxSource": "<?php echo $CFG->wwwroot.'/local/manager/users/users_datatables.php';?>",
                "sDom": 'T<"clear">lfrtip',
                "sPaginationType": "full_numbers",
                "aLengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                "iDisplayLength": 50,
                "order": [[ 1, "asc" ]],
                "oLanguage": {
                    "sSearch": "_INPUT_",
                    "sLengthMenu": "<span>_MENU_</span>",
                    "oPaginate": { "sFirst": "First", "sLast": "Last" }
                },

                "aoColumnDefs": [
                            { "bSearchable": true, "bVisible": false, "aTargets": [ 0 ] },
                            { "bSortable": false, "aTargets": [ -1 ] },
                        ],
                        "oTableTools": {
                            "aButtons": [
                            ]
                        },
            });

            jQuery('#datatable-users_wrapper .dataTables_filter input').attr('placeholder', '<?php echo get_string('search', 'local_manager'); ?>');

            if (jQuery('input[type="search"]').length){
                jQuery('input[type="search"]').each(function(e){
                    var input = jQuery(this);
                    var el = '<span class="search-actions"><i class="fa fa-times" style="'+((input.val().length)?"":"display:none;")+'" onclick="searchProcess(this, 0);"></i><i class="fa fa-search" style="'+((input.val().length)?"display:none;":"")+'" onclick="searchProcess(this, 1);"></i></span>';

                    input.after(el);

                    input.keyup(function(e){
                        if (input.val().length){
                            input.parent().find('.search-actions .fa-times').show();
                            input.parent().find('.search-actions .fa-search').hide();
                        } else {
                            input.parent().find('.search-actions .fa-times').hide();
                            input.parent().find('.search-actions .fa-search').show();
                        }
                    });
                });
            }
        </script>
    <?php endif; ?>
    <?php

} elseif ($action == 'search_users'){

    $search = optional_param('search', 0, PARAM_RAW);
    ?>
        <table title="<?php echo get_string('searchusersresults', 'local_manager'); ?>" id="datatable-search" class="generaltable sorting data-table datatable-search selecting" cellpadding="0" cellspacing="0">
            <thead>
            <tr>
                <th align="center" value="id"><span><?php echo get_string('ID', 'local_manager');?></span></th>
                <th align="center" value="name" class="active asc"><span><?php echo get_string('name', 'local_manager');?></span></th>
                <th align="center" value="username"><span><?php echo get_string('username', 'local_manager');?></span></th>
                <th align="center" value="email"><span><?php echo get_string('email', 'local_manager');?></span></th>
                <th align="center" value="idnumber"><span><?php echo get_string('user_ID', 'local_manager');?></span></th>
                <th align="center" value="role"><span><?php echo get_string('role', 'local_manager');?></span></th>
                <th align="center" value="status"><span><?php echo get_string('status', 'local_manager');?></span></th>
                <th align="center" class="nosort"><span><?php echo get_string('actions', 'local_manager');?></span></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
        <script>
            var search_users = jQuery('.datatable-search').dataTable( {
                "bAutoWidth": false,
                "bServerSide": true,
                "sAjaxSource": "<?php echo $CFG->wwwroot;?>/local/manager/users/users_search.php?search=<?php echo $search; ?>",
                "sDom": 'T<"clear">lfrtip',
                "sPaginationType": "full_numbers",
                "aLengthMenu": [[-1, 10, 25, 50, 100], ["All", 10, 25, 50, 100]],
                "iDisplayLength": 50,
                "order": [[ 1, "asc" ]],
                "oLanguage": {
                    "sSearch": "_INPUT_",
                    "sLengthMenu": "<span>_MENU_</span>",
                    "oPaginate": { "sFirst": "First", "sLast": "Last" }
                },
                "aoColumnDefs": [
                            { "bSearchable": true, "bVisible": false, "aTargets": [ 0 ] },
                            { "bSortable": false, "aTargets": [ -1 ] },
                        ],
                "oTableTools": {
                        "aButtons": []
                    },
            });

            jQuery('#datatable-search_filter').hide();
        </script>
    <?php
}else if ($action == 'set_lang'){
    $lang    = optional_param('name', '', PARAM_RAW);
    $state    = optional_param('state', 0, PARAM_INT);

    if (!empty($lang)){
        $lang_setting = $DB->get_record('local_manager_langs', array('name'=>$lang));
        if ($lang_setting){
            $lang_setting->state = $state;
            $DB->update_record('local_manager_langs', $lang_setting);
        } else {
            $lang_setting = new stdClass();
            $lang_setting->name = $lang;
            $lang_setting->state = $state;
            $DB->insert_record('local_manager_langs', $lang_setting);
        }
    }
    echo time();
}else if ($action == 'set_config'){
    $config       = optional_param('config', '', PARAM_RAW);
    $plugin       = optional_param('plugin', '', PARAM_RAW);
    $state        = optional_param('state', 0, PARAM_INT);

    set_config($config, $state, $plugin);

    echo '1';
    exit;
}else if ($action == 'search_select_users'){
    $q      = optional_param('q', '', PARAM_RAW);
    $page      = optional_param('page', 1, PARAM_RAW);
    $limit = 30; $start = $limit * ($page-1);

    $results = array(); $items = array(); $where = '';

    $users = $DB->get_records_sql("SELECT * FROM {user} WHERE id > 1 AND deleted = 0 AND confirmed = 1 AND suspended = 0 AND (firstname LIKE '%$q%' OR lastname LIKE '%$q%' OR email LIKE '%$q%') $where ORDER BY firstname ASC LIMIT $start, $limit");
    $users_count = $DB->get_record_sql("SELECT COUNT(id) as users FROM {user} WHERE id > 1 AND deleted = 0 AND confirmed = 1 AND suspended = 0 AND (firstname LIKE '%$q%' OR lastname LIKE '%$q%' OR email LIKE '%$q%') $where ORDER BY firstname ASC");

    if (count($users)){
        foreach($users as $user){
            $items[] = array('id'=>$user->id, 'text'=>fullname($user));
        }
    }
    $results['total_count'] = ($users_count->users) ? $users_count->users : 0;
    $results['incomplete_results'] = false;
    $results['results'] = $items;

    echo json_encode($results);
    exit;

} elseif ($action == 'blog_set_like') {
    $state    = optional_param('state', 0, PARAM_INT);

    if ($state > 0){
        $like = new stdClass();
        $like->postid = $id;
        $like->userid = $USER->id;
        $like->type = 'like';
        $like->value = 1;
        $like->timemodified = time();
        $DB->insert_record('local_manager_blog_activity', $like);
    } else {
        $DB->delete_records('local_manager_blog_activity', array('postid'=>$id, 'userid'=>$USER->id, 'type'=>'like'));
    }

    echo time();

} elseif($action == 'blog_set_helpful'){
    $state    = optional_param('state', 0, PARAM_INT);

    if ($state > 0){
        $helpful = new stdClass();
        $helpful->postid = $id;
        $helpful->userid = $USER->id;
        $helpful->type = 'helpful';
        $helpful->value = 1;
        $helpful->timemodified = time();
        $DB->insert_record('local_manager_blog_activity', $helpful);

        $entry = $DB->get_record('post', array('id'=>$id));
    } else {
        $DB->delete_records('local_manager_blog_activity', array('postid'=>$id, 'userid'=>$USER->id, 'type'=>'helpful'));
    }

    echo time();

} elseif($action == 'blog_set_rating'){

    $rate              = optional_param('rate', 0, PARAM_INT);

    if ($rate > 0){
        $rating = $DB->get_record('local_manager_blog_activity', array('postid'=>$id, 'userid'=>$USER->id, 'type'=>'rate'));
        if ($rating){
            $rating->value = $rate;
            $rating->timemodified = time();
            $DB->update_record('local_manager_blog_activity', $rating);
        } else {
            $rating = new stdClass();
            $rating->postid = $id;
            $rating->userid = $USER->id;
            $rating->type = 'rate';
            $rating->value = $rate;
            $rating->timemodified = time();
            $DB->insert_record('local_manager_blog_activity', $rating);
        }
    }

    echo time();
}elseif($action == 'like-course'){

    $like = optional_param('liked', 'false', PARAM_RAW);

    set_user_preference('liked_course_'.$id, (int)($like == "true"));
}elseif($action == 'hide-course-carousel'){

    $opened = optional_param('opened', 'false', PARAM_RAW);

    set_user_preference('hide-course-carousel', (int)($opened == "true"));
}


exit;
