<?php

// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * External Web Service local_manager plugin
 *
 * @package    local_manager
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once($CFG->libdir . "/externallib.php");

class local_manager_external extends external_api {

    /**
    * Returns description of method parameters
    * @return external_function_parameters
    */
    public static function save_shortcuts_ordering_parameters() {
        return new external_function_parameters(
            array(
                'move' => new external_value(PARAM_INT, 'item id'),
                'moveafter' => new external_value(PARAM_INT, 'moveafter id'),
                'theme' => new external_value(PARAM_RAW, 'moveafter id')
            )
        );
    }

    public static function save_shortcuts_ordering($move = 0, $moveafter = 0, $theme = 'primary') {

        global $DB, $CFG;

        $params = self::validate_parameters(
            self::save_shortcuts_ordering_parameters(),
            array(
                'move' => $move,
                'moveafter' => $moveafter,
                'theme' => $theme
            )
        );

        $moving_item = $DB->get_record("local_manager_sidebar", array('id'=>$move));

        if ($moveafter == 0){

            $moving_item->sortorder = 0;
            $DB->update_record("local_manager_sidebar", $moving_item);

            $shortcuts = $DB->get_records_sql("SELECT id, sortorder
                                                 FROM {local_manager_sidebar}
                                                WHERE id <> :move AND theme = :theme
                                             ORDER BY sortorder", array('move'=>$move, 'theme'=>$theme));

            $i = 1;
            foreach ($shortcuts as $item){
                $item->sortorder = $i++;
                $DB->update_record("local_manager_sidebar", $item);
            }
        } else {
            $moveafter_item = $DB->get_record("local_manager_sidebar", array('id'=>$moveafter));

            $moving_item->sortorder = ++$moveafter_item->sortorder;
            $DB->update_record("local_manager_sidebar", $moving_item);

            $shortcuts = $DB->get_records_sql("SELECT id, sortorder
                                                 FROM {local_manager_sidebar}
                                                WHERE id <> :move AND theme = :theme
                                             ORDER BY sortorder", array('move'=>$move, 'theme'=>$theme));

            $i = 0;
            foreach ($shortcuts as $item){
                $item->sortorder = $i++;
                $DB->update_record("local_manager_sidebar", $item);
                if ($item->id == $moveafter_item->id) $i++;
            }
        }

        return array('status' => 'success');
    }

    /**
    * Returns description of method result value
    * @return external_description
    */
    public static function save_shortcuts_ordering_returns() {
        return new external_single_structure(
            array (
                'status' => new external_value(PARAM_NOTAGS, 'Status of ordering update')
            )
        );
    }

}
