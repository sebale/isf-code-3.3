<?php
require_once('../../../config.php');
require_once($CFG->dirroot.'/local/manager/output/forms/appearance_form.php');
require_once('../lib.php');

$id = optional_param('id', $USER->id, PARAM_INT);
$type = optional_param('type', 'primary', PARAM_ALPHA);

require_login();
$systemcontext = context_system::instance();
require_capability('local/manager:appearance', $systemcontext);

$styles = $DB->get_records_sql("SELECT s.*, c.name as cat_name
                                  FROM {local_manager_styles} s
                             LEFT JOIN {local_manager_cats} c ON c.id = s.category
                                 WHERE s.state = 1 AND theme = :theme
                              ORDER BY s.category, s.sortorder, s.id", array('theme'=>$type));
$data = new StdClass();
foreach($styles as $style){
    $data->{$style->name} = $style->value;
}

$title = get_string('manageappearance', 'local_manager');
$PAGE->set_url('/local/manager/settings/appearance.php', array('type'=>$type));
$PAGE->set_context($systemcontext);
$PAGE->navbar->add($title);
$PAGE->set_pagelayout('admin');

$attachmentoptions = array('subdirs' => false, 'maxfiles' => 1, 'maxbytes' => $CFG->maxbytes, 'context' => $systemcontext);
$component = 'theme_isf'.$type;

$data->logo = appearance_file(1, 'logo', $component);
if (!empty($data->logo)){
    $data = file_prepare_standard_filemanager($data, 'logo', $attachmentoptions, $systemcontext, $component, 'logo', 1);
}
$data->favicon = appearance_file(2, 'favicon', $component);
if (!empty($data->favicon)){
    $data = file_prepare_standard_filemanager($data, 'favicon', $attachmentoptions, $systemcontext, $component, 'favicon', 2);
}
$data->faviconpng = appearance_file(3, 'faviconpng', $component);
if (!empty($data->faviconpng)){
    $data = file_prepare_standard_filemanager($data, 'faviconpng', $attachmentoptions, $systemcontext, $component, 'faviconpng', 3);
}
$data->loginbackground = appearance_file(4, 'loginbackground', $component);
if (!empty($data->loginbackground)){
    $data = file_prepare_standard_filemanager($data, 'loginbackground', $attachmentoptions, $systemcontext, $component, 'loginbackground', 4);
}

//create form
$mform = new edit_styles_form($CFG->wwwroot.'/local/manager/settings/appearance.php?type='.$type, array(
    'styles'=>$styles, 'data'=>$data));

if ($mform->is_cancelled()) {
} elseif ($stylesnew = $mform->get_data()) {

    if(isset($stylesnew->logo_filemanager) and $stylesnew->logo_filemanager){
        save_appearance_file('logo', $stylesnew, 1, $attachmentoptions, $systemcontext, $component);
    }
    if(isset($stylesnew->favicon_filemanager) and $stylesnew->favicon_filemanager){
        save_appearance_file('favicon', $stylesnew, 2, $attachmentoptions, $systemcontext, $component);
    }
    if(isset($stylesnew->faviconpng_filemanager) and $stylesnew->faviconpng_filemanager){
        save_appearance_file('faviconpng', $stylesnew, 3, $attachmentoptions, $systemcontext, $component);
    }
    if(isset($stylesnew->loginbackground_filemanager) and $stylesnew->loginbackground_filemanager){
        save_appearance_file('loginbackground', $stylesnew, 4, $attachmentoptions, $systemcontext, $component);
    }

    foreach ($stylesnew as $name=>$value){
        $db_style = $DB->get_record('local_manager_styles', array('name'=>$name,'theme'=>$type));
        if (isset($db_style->id)){
            $db_style->value = $value;
            $DB->update_record('local_manager_styles', $db_style);

            if ($db_style->name == 'system_name'){
                $system_course = new stdClass();
                $system_course->id = 1;
                $system_course->fullname = (!empty($db_style->value)) ? $db_style->value : $db_style->defaultvalue;
                $system_course->shortname = $system_course->fullname;
                $DB->update_record('course', $system_course);
            }
        }
    }
    theme_reset_all_caches();
    redirect($PAGE->url);
}

$PAGE->set_title($title);
$PAGE->set_heading($title);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$renderer = $PAGE->get_renderer('local_manager');
$renderer->manager_print_appearanse_tabs($type);

/// Finally display THE form
echo html_writer::start_tag('div', array('class'=>'styles-form'));
    $mform->display();
echo html_writer::end_tag('div');

/// and proper footer
echo $OUTPUT->footer();
