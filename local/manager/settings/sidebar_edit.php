<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Displays information about all the assignment modules in the requested course
 *
 * @package   local_quicklinks
 * @copyright 2017 ISF
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once("../../../config.php");
require_once("../lib.php");
require_once($CFG->dirroot."/local/manager/output/forms/sidebar_form.php");

require_login();

$id         = optional_param('id', 0, PARAM_INT);
$theme       = optional_param('theme', 'primary', PARAM_NOTAGS);
$action     = optional_param('action', '', PARAM_NOTAGS);
$confirm    = optional_param('confirm', 0, PARAM_BOOL);

$item = null;
$params = array('id'=>$id, 'theme'=>$theme);

// check params
if ($id) {
    $item = $DB->get_record('local_manager_sidebar', array('id'=>$id));
    $title = get_string('updateshortcut', 'local_manager');
} else {
    $title = get_string('createnewshortcut', 'local_manager');
}

$context = context_system::instance();

// user need to has permissions to edit category
require_capability('local/manager:managesidebar', $context);
$PAGE->set_context($context);
$PAGE->set_url(new moodle_url("/local/manager/settings/sidebar_edit.php", $params));

// actions
if ($action == 'show' and $id) {
    $item->state = 1;
    local_manager_sidebar_update($item);
    redirect(new moodle_url("/local/manager/settings/sidebar.php", $params), get_string('shortcutupdated', 'local_manager'));
} else if ($action == 'hide' and $id) {
    $item->state = 0;
    local_manager_sidebar_update($item);
    redirect(new moodle_url("/local/manager/settings/sidebar.php", $params), get_string('shortcutupdated', 'local_manager'));
} else if ($action == 'delete' and $id) {
    if ($confirm and confirm_sesskey()) {
        local_manager_sidebar_delete($item);
        redirect(new moodle_url("/local/manager/settings/sidebar.php", $params), get_string('shortcutdeleted', 'local_manager'));
    }
    $strheading = get_string('deleteshortcut', 'local_manager');
    $PAGE->navbar->add(get_string('sidebarsettings', 'local_manager'), new moodle_url('/local/manager/settings/sidebar.php', $params));
    $PAGE->navbar->add($strheading);
    $PAGE->set_title($strheading);
    $PAGE->set_heading($strheading);

    echo $OUTPUT->header();
    echo $OUTPUT->heading($strheading);
    $yesurl = new moodle_url($CFG->wwwroot.'/local/manager/settings/sidebar_edit.php', array('id' => $item->id, 'action' => 'delete', 'confirm' => 1, 'sesskey' => sesskey(), 'returnurl' => new moodle_url('/local/manager/settings/sidebar.php', $params)));
    $message = get_string('confirmcdeleteshortcut', 'local_manager', format_string($item->title));
    echo $OUTPUT->confirm($message, $yesurl, new moodle_url('/local/manager/settings/sidebar.php', $params));
    echo $OUTPUT->footer();
    die;
}

$PAGE->navbar->add(get_string('sidebarsettings', 'local_manager'), new moodle_url('/local/manager/settings/sidebar.php', $params));
$PAGE->navbar->add($title);

$PAGE->set_title($title);
$PAGE->set_pagelayout('standard');
$PAGE->set_heading($title);

// create form
$imagefilesoptions = array(
    'maxfiles' => 1,
    'maxbytes' => $CFG->maxbytes,
    'subdirs' => 0,
    'accepted_types' => 'image',
    'context' => $context
);

if (isset($item->id)){
    $item = file_prepare_standard_filemanager($item, 'shortcutimage', $imagefilesoptions, $context,
                                           'local_manager', 'shortcutimage', $item->id);
}

$editform = new sidebar_edit_form(null, array('data'=>$item, 'context'=>$context, 'theme'=> $theme, 'imagefilesoptions'=>$imagefilesoptions));

// process form data
if ($editform->is_cancelled()) {
    redirect(new moodle_url("/local/manager/settings/sidebar.php"));
} else if ($data = $editform->get_data()) {

    if ($data->id > 0) {
        local_manager_sidebar_update($data);
        file_save_draft_area_files($data->shortcutimage_filemanager, $context->id, 'local_manager', 'shortcutimage',
                   $data->id, $imagefilesoptions);
        redirect(new moodle_url("/local/manager/settings/sidebar.php"), get_string('shortcutupdated', 'local_manager'));
    } else {
        $data->id = local_manager_sidebar_insert($data);
        file_save_draft_area_files($data->shortcutimage_filemanager, $context->id, 'local_manager', 'shortcutimage',
                   $data->id, $imagefilesoptions);
        redirect(new moodle_url("/local/manager/settings/sidebar.php"), get_string('shortcutcreated', 'local_manager'));
    }
}

// set plugin renderer
$renderer = $PAGE->get_renderer('local_manager');

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo $renderer->startbox('sidebaritems-list');

// display form
$editform->display();

echo $renderer->endbox();

echo $OUTPUT->footer();
