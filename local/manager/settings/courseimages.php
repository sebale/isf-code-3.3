<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Course images library.
 *
 * @package local_manager
 * @copyright 2017 ISF
 * @author sebale.net
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once("../../../config.php");

require_once($CFG->dirroot."/local/manager/output/forms/course_images_edit_form.php");
require_once($CFG->dirroot . '/repository/lib.php');
require_once($CFG->dirroot . '/course/lib.php');

$courseid     = optional_param('courseid', 0, PARAM_INT);

// file parameters
$component  = 'local_manager';

if ($courseid > 1) {
    $course = $DB->get_record('course', array('id'=>$courseid));
    $context = context_course::instance($courseid);
} else {
    $course = $DB->get_record('course', array('id'=>1));
    $context = context_system::instance();
}

$filecontext = context::instance_by_id($context->id, IGNORE_MISSING);

$url = new moodle_url('/local/manager/settings/courseimages.php', array('courseid'=>$courseid));

require_login();

// Here cap for images
require_capability('local/manager:managecourseimages', $context);

$title = get_string('managecourseimages', 'local_manager');
$PAGE->set_url($url);
$PAGE->set_context($context);
$PAGE->set_title($title);
$PAGE->set_heading($title);
if ($courseid > 1) {
    $PAGE->set_course($course);
    $PAGE->set_pagelayout('course');
} else {
    $PAGE->set_pagelayout('admin');
    $PAGE->navbar->add($title);
}
$browser = get_file_browser();

$modnames = get_module_types_names();
$modules = get_module_metadata($course, $modnames, null);

$data = new stdClass();
// carousel images
$options = array('subdirs'=>0, 'maxfiles'=>-1, 'accepted_types'=>array('.jpg', '.gif', '.png'), 'return_types'=>FILE_INTERNAL);
file_prepare_standard_filemanager($data, 'courseimages', $options, $context, $component, 'courseimages', 0);

// default images
$options = array('subdirs'=>0, 'maxfiles'=>1, 'accepted_types'=>array('.jpg', '.gif', '.png'), 'return_types'=>FILE_INTERNAL);
file_prepare_standard_filemanager($data, 'defaultimage', $options, $context, $component, 'defaultimage', 0);
if (count($modules)) {
    foreach ($modules as $modname=>$mod) {
        file_prepare_standard_filemanager($data, 'defaultimage_'.$modname, $options, $context, $component, 'defaultimage_'.$modname, 0);
    }
}

$form = new course_images_edit_form(null, array('data'=>$data, 'contextid'=>$context->id,  'component'=>$component, 'course'=>$course, 'modules'=>$modules));


$data = $form->get_data();
if ($data) {
    // carousel images
    $options = array('subdirs'=>0, 'maxfiles'=>-1, 'accepted_types'=>array('.jpg', '.gif', '.png'), 'return_types'=>FILE_INTERNAL);
    $formdata = file_postupdate_standard_filemanager($data, 'courseimages', $options, $context, $component, 'courseimages', 0);

    // default images
    $options = array('subdirs'=>0, 'maxfiles'=>1, 'accepted_types'=>array('.jpg', '.gif', '.png'), 'return_types'=>FILE_INTERNAL);
    $formdata = file_postupdate_standard_filemanager($data, 'defaultimage', $options, $context, $component, 'defaultimage', 0);
    if (count($modules)) {
        foreach ($modules as $modname=>$mod) {
            $formdata = file_postupdate_standard_filemanager($data, 'defaultimage_'.$modname, $options, $context, $component, 'defaultimage_'.$modname, 0);
        }
    }

    redirect($url, get_string('settingsupdated', 'local_manager'));
}

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('managecourseimages', 'local_manager'));

echo $OUTPUT->container_start();
$form->display();
echo $OUTPUT->container_end();

echo $OUTPUT->footer();
