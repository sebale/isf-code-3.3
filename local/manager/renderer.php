<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderer file.
 *
 * @package    local_manager
 * @author     ISF
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

/**
 * Standard HTML output renderer for badges
 */
class local_manager_renderer extends plugin_renderer_base {

    // Prints tabs
    public function manager_print_appearanse_tabs($current = 'primary') {
        global $DB;

        $systemcontext   = context_system::instance();
        $row = array();

        $row[] = new tabobject('primary',
                    new moodle_url('/local/manager/settings/appearance.php', array('type'=>'primary')),
                    get_string('primary', 'local_manager')
                );

        $row[] = new tabobject('secondary',
                    new moodle_url('/local/manager/settings/appearance.php', array('type'=>'secondary')),
                    get_string('secondary', 'local_manager')
                );

        echo $this->tabtree($row, $current);
    }

    public function manager_print_approvalstabs($current = 'etraining') {
        global $DB, $CFG;

        require_once($CFG->dirroot.'/local/manager/lib.php');

        $systemcontext   = context_system::instance();
        $row = array();

        if (has_capability('enrol/approval:manage', $systemcontext)){
            $enrollment = get_approvals_count('enrollment');
            $row[] = new tabobject('enrollment',
                    new moodle_url('/local/manager/approvals/enrollments.php'),
                    get_string('tabrenrollments', 'local_manager').(($enrollment > 0) ? ' ('.$enrollment.')' : '')
                );
        }

        if (get_config('local_etraining', 'enabled')){

            if (has_capability('local/etraining:manage', $systemcontext) and get_config('local_etraining', 'confirm')){
                $etraining = get_approvals_count('etraining');
                $row[] = new tabobject('etraining',
                            new moodle_url('/local/manager/approvals/etraining.php'),
                            get_string('tabetraining', 'local_manager').(($etraining > 0) ? ' ('.$etraining.')' : '')
                        );
            }

            if (has_capability('local/etraining:managecourses', $systemcontext) and get_config('local_etraining', 'confirmcourses')){
                $ecourses = get_approvals_count('ecourses');
                $row[] = new tabobject('ecourses',
                            new moodle_url('/local/manager/approvals/ecourses.php'),
                            get_string('tabecourses', 'local_manager').(($ecourses > 0) ? ' ('.$ecourses.')' : '')
                        );
            }

            if (has_capability('local/etraining:manageinstitutes', $systemcontext) and get_config('local_etraining', 'confirminstitutes')){
                $einstitutes = get_approvals_count('einstitutes');
                $row[] = new tabobject('einstitutes',
                            new moodle_url('/local/manager/approvals/einstitutes.php'),
                            get_string('tabeinstitutes', 'local_manager').(($einstitutes > 0) ? ' ('.$einstitutes.')' : '')
                        );
            }

        }

        echo $this->tabtree($row, $current);
    }

    // Print box start
    public function startbox($boxclass = '') {
        return html_writer::start_tag('div', array('class'=>'content-wrapper '.$boxclass));
    }

    // Print box end
    public function endbox() {
        return html_writer::end_tag('div');
    }

    // Prints sidebar tabs
    public function manager_print_sidebar_tabs($current = 'primary') {
        global $DB;

        $systemcontext   = context_system::instance();
        $row = array();

        $row[] = new tabobject('primary',
                    new moodle_url('/local/manager/settings/sidebar.php', array('theme'=>'primary')),
                    get_string('primary', 'local_manager')
                );

        $row[] = new tabobject('secondary',
                    new moodle_url('/local/manager/settings/sidebar.php', array('theme'=>'secondary')),
                    get_string('secondary', 'local_manager')
                );

        echo $this->tabtree($row, $current);
    }

    public function sidebar_search_form($params, $context, $theme) {
        global $PAGE, $CFG;

        $output = '';

        $output .= html_writer::start_tag("form",  array("action"=> $PAGE->url, 'class'=>'ql-search-form mform clearfix'));

        $output .= html_writer::start_tag("label", array('class'=>'form-inline'));
        $output .= html_writer::empty_tag('input', array('type' => 'text', 'name' => 'search', 'placeholder' => get_string('search', 'local_manager'), 'value' => $params->search, 'class'=>'form-control'));
        $output .= html_writer::end_tag("label");

        $output .= html_writer::link(new moodle_url($CFG->wwwroot.'/local/manager/settings/sidebar_edit.php', array('theme'=>$theme)), html_writer::tag('i', '', array('class'=>'fa fa-pencil-square-o')).get_string('createnewshortcut', 'local_manager'), array('class'=>'btn btn-warning'));

        $output .= html_writer::end_tag("form");

        return $output;
    }

}
