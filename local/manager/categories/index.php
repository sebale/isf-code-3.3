<?php
/**
 * manager version file.
 *
 * @package    local_manager
 * @author     ISF
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

require_once('../../../config.php');

$id = optional_param('id', $USER->id, PARAM_INT);    // course id

require_login();
$systemcontext = context_system::instance();
require_capability('local/manager:managecategories', $systemcontext);
require_capability('moodle/category:manage', $systemcontext);

$USER->editing = 0;
$title = get_string('managecategories', 'local_manager');
$PAGE->set_url('/local/manager/categories/index.php');
$PAGE->set_context($systemcontext);
$PAGE->navbar->add($title);

$PAGE->set_pagelayout('admin');

$PAGE->set_title($title);
$PAGE->set_heading($title);
$PAGE->requires->css('/local/manager/assets/css/dataTable.css');

echo $OUTPUT->header();

echo $OUTPUT->heading($title);
echo html_writer::start_tag('div', array('class'=>'users-manage-box'));
?>
<div class="tq-users-tabs">
    <div class="users-tabs">
        <div class="users-tabs-item course-tab visible">
            <div class="users-create-panel clearfix">
                <button class="btn btn-primary" onclick="location = '<?php echo $CFG->wwwroot;?>/course/editcategory.php?parent=0'"><i class="fa fa-plus"></i> <?php echo get_string('createnewcategory', 'local_manager');?></button>
            </div>
            <table title="Courses" id="datatable_courses" class="generaltable sorting data-table" cellpadding="0" cellspacing="0">
                <thead>
                    <tr>
                        <th align="center" value="id" class="active asc"><span><?php echo get_string('ID', 'local_manager'); ?></span></th>
                        <th align="center" value="name"><span><?php echo get_string('name', 'local_manager'); ?></span></th>
                        <th align="center" value="parentcategory"><span><?php echo get_string('parentcategory', 'local_manager'); ?></span></th>
                        <th align="center" value="coursecount"><span><?php echo get_string('coursecount', 'local_manager'); ?></span></th>
                        <th align="center" class="nosort"><span><?php echo get_string('actions', 'local_manager'); ?></span></th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php
echo html_writer::end_tag('div');

$PAGE->requires->js_call_amd('local_manager/category', 'init', array());

/// and proper footer
echo $OUTPUT->footer();
