<?php
require_once('../../config.php');
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->dirroot.'/local/isfdup/locallib.php');
require_once($CFG->dirroot.'/local/isfdup/classes/forms.php');

admin_externalpage_setup('local_isfdup');

$url = new moodle_url('/local/isfdup/managesettings.php');
$strmanagesettings = get_string('manageactivitysettings', 'local_isfdup');
$PAGE->navbar->add($strmanagesettings, $url);

$deletesettingid = optional_param('d', 0, PARAM_INT);

if (!empty($deletesettingid) and confirm_sesskey()) {
    if ($DB->delete_records('local_isfdup_settings', array('id'=>$deletesettingid))) {
        redirect($url, get_string('settingdeleted', 'local_isfdup'));
    } else {
        redirect($url, get_string('settingnotdeleted', 'local_isfdup'));
    }
    exit;
}

$modulesettingsform = new local_isfdup_modulesettingsform();
if ($data = $modulesettingsform->get_data()) {
    $record = new stdClass;

    // We can assume the module name has already been checked by form validation.
    $record->moduleid = $DB->get_field('modules', 'id', array('name' => local_isfdup_get_modulename($data->name)));
    $record->name = $data->name;

    if ($DB->insert_record('local_isfdup_settings', $record)) {
        redirect($url, get_string('settingsaved', 'local_isfdup'));
    } else {
        redirect($url, get_string('settingsaveproblem', 'local_isfdup'));
    }
    exit;
}

$output = $PAGE->get_renderer('local_isfdup');

echo $output->print_module_settings($modulesettingsform);
