<?php
require_once('../../config.php');
require_once($CFG->libdir . '/adminlib.php');
require_once($CFG->dirroot.'/local/isfdup/locallib.php');

admin_externalpage_setup('local_isfdup');

$cid = optional_param('cid', 0, PARAM_INT);
$m   = optional_param('m',   0, PARAM_INT);

if (!empty($cid) and confirm_sesskey()) {
    local_isfdup_change_master_status($cid, $m);
}

$strmanagemastercourses = get_string('managemastercourses', 'local_isfdup');
$baseurl = new moodle_url('/local/isfdup/managemasters.php');
$PAGE->navbar->add($strmanagemastercourses, $baseurl);
$output = $PAGE->get_renderer('local_isfdup');

echo $output->print_master_course_selection();
