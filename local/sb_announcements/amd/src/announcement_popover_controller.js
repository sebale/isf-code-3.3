// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Controls the announcement popover in the nav bar.
 *
 * See template: local_sb_announcements/announcement_popover_controller
 *
 * @module     local_sb_announcements/announcement_popover_controller
 * @class      announcement_popover_controller
 * @package    local_sb_announcements
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
define(['jquery', 'core/ajax', 'core/templates', 'core/str', 'core/url',
            'core/notification', 'core/custom_interaction_events', 'core/popover_region_controller',
            'local_sb_announcements/announcement_repository', 'local_sb_announcements/announcement_area_events'],
        function($, Ajax, Templates, Str, URL, DebugNotification, CustomEvents,
            PopoverController, AnnouncementRepo, AnnouncementAreaEvents) {

    var SELECTORS = {
        MARK_ALL_READ_BUTTON: '[data-action="mark-all-read"]',
        ALL_ANNOUNCEMENTS_CONTAINER: '[data-region="all-announcements"]',
        ANNOUNCEMENT: '[data-region="announcement-content-item-container"]',
        UNREAD_ANNOUNCEMENT: '[data-region="announcement-content-item-container"].unread',
        ANNOUNCEMENT_LINK: '[data-action="content-item-link"]',
        EMPTY_MESSAGE: '[data-region="empty-message"]',
        COUNT_CONTAINER: '[data-region="count-container"]',
        ANNOUNCEMENT_DELETE_LINK: '[data-action="delete-item"]',
    };

    /**
     * Constructor for the AnnouncementPopoverController.
     * Extends PopoverRegionController.
     *
     * @param {object} element jQuery object root element of the popover
     */
    var AnnouncementPopoverController = function(element) {
        // Initialise base class.
        PopoverController.call(this, element);

        this.markAllReadButton = this.root.find(SELECTORS.MARK_ALL_READ_BUTTON);
        this.unreadCount = 0;
        this.userId = this.root.attr('data-userid');
        this.container = this.root.find(SELECTORS.ALL_ANNOUNCEMENTS_CONTAINER);
        this.limit = 20;
        this.offset = 0;
        this.loadedAll = false;
        this.initialLoad = false;

        // Let's find out how many unread announcements there are.
        this.loadUnreadAnnouncementCount();
    };

    /**
     * Clone the parent prototype.
     */
    AnnouncementPopoverController.prototype = Object.create(PopoverController.prototype);

    /**
     * Make sure the constructor is set correctly.
     */
    AnnouncementPopoverController.prototype.constructor = AnnouncementPopoverController;

    /**
     * Set the correct aria label on the menu toggle button to be read out by screen
     * readers. The message will indicate the state of the unread Announcement.
     *
     * @method updateButtonAriaLabel
     */
    AnnouncementPopoverController.prototype.updateButtonAriaLabel = function() {
        if (this.isMenuOpen()) {
            Str.get_string('hideannouncementwindow', 'message').done(function(string) {
                this.menuToggle.attr('aria-label', string);
            }.bind(this));
        } else {
            if (this.unreadCount) {
                Str.get_string('showannouncementwindowwithcount', 'message', this.unreadCount).done(function(string) {
                    this.menuToggle.attr('aria-label', string);
                }.bind(this));
            } else {
                Str.get_string('showannouncementwindownonew', 'message').done(function(string) {
                    this.menuToggle.attr('aria-label', string);
                }.bind(this));
            }
        }
    };

    /**
     * Return the jQuery element with the content. This will return either
     * the unread announcement container or the all announcement container
     * depending on which is currently visible.
     *
     * @method getContent
     * @return {object} jQuery object currently visible content contianer
     */
    AnnouncementPopoverController.prototype.getContent = function() {
        return this.container;
    };

    /**
     * Get the offset value for the current state of the popover in order
     * to sent to the backend to correctly paginate the announcements.
     *
     * @method getOffset
     * @return {int} current offset
     */
    AnnouncementPopoverController.prototype.getOffset = function() {
        return this.offset;
    };

    /**
     * Increment the offset for the current state, if required.
     *
     * @method incrementOffset
     */
    AnnouncementPopoverController.prototype.incrementOffset = function() {
        this.offset += this.limit;
    };

    /**
     * Check if the first load of announcement has been triggered for the current
     * state of the popover.
     *
     * @method hasDoneInitialLoad
     * @return {bool} true if first announcement loaded, false otherwise
     */
    AnnouncementPopoverController.prototype.hasDoneInitialLoad = function() {
        return this.initialLoad;
    };

    /**
     * Check if we've loaded all of the announcement for the current popover
     * state.
     *
     * @method hasLoadedAllContent
     * @return {bool} true if all announcement loaded, false otherwise
     */
    AnnouncementPopoverController.prototype.hasLoadedAllContent = function() {
        return this.loadedAll;
    };

    /**
     * Set the state of the loaded all content property for the current state
     * of the popover.
     *
     * @method setLoadedAllContent
     * @param {bool} val True if all content is loaded, false otherwise
     */
    AnnouncementPopoverController.prototype.setLoadedAllContent = function(val) {
        this.loadedAll = val;
    };

    /**
     * Show the unread announcement count badge on the menu toggle if there
     * are unread announcements, otherwise hide it.
     *
     * @method renderUnreadCount
     */
    AnnouncementPopoverController.prototype.renderUnreadCount = function() {
        var element = this.root.find(SELECTORS.COUNT_CONTAINER);

        if (this.unreadCount) {
            element.text(this.unreadCount);
            element.removeClass('hidden');
        } else {
            element.addClass('hidden');
        }
    };

    /**
     * Hide the unread announcement count badge on the menu toggle.
     *
     * @method hideUnreadCount
     */
    AnnouncementPopoverController.prototype.hideUnreadCount = function() {
        this.root.find(SELECTORS.COUNT_CONTAINER).addClass('hidden');
    };

    /**
     * Ask the server how many unread announcements are left, render the value
     * as a badge on the menu toggle and update the aria labels on the menu
     * toggle.
     *
     * @method loadUnreadAnnouncementCount
     */
    AnnouncementPopoverController.prototype.loadUnreadAnnouncementCount = function() {
        AnnouncementRepo.countUnread({useridto: this.userId}).then(function(count) {
            this.unreadCount = count;
            this.renderUnreadCount();
            this.updateButtonAriaLabel();
        }.bind(this));
    };

    /**
     * Find the announcement element for the given id.
     *
     * @param {int} id
     * @method getAnnouncementElement
     * @return {object|null} The announcement element
     */
    AnnouncementPopoverController.prototype.getAnnouncementElement = function(id) {
        var element = this.root.find(SELECTORS.ANNOUNCEMENT + '[data-id="' + id + '"]');
        return element.length == 1 ? element : null;
    };

    /**
     * Render the announcement data with the appropriate template and add it to the DOM.
     *
     * @method renderAnnouncements
     * @param {array} announcements announcement data
     * @param {object} container jQuery object the container to append the rendered announcements
     * @return {object} jQuery promise that is resolved when all announcements have been
     *                  rendered and added to the DOM
     */
    AnnouncementPopoverController.prototype.renderAnnouncement = function(announcements, container) {
        var promises = [];
        var allhtml = [];
        var alljs = [];

        if (announcements.length) {
            $.each(announcements, function(index, announcement) {
                // Determine what the offset was when loading this announcement.
                var offset = this.getOffset() - this.limit;
                // Update the view more url to contain the offset to allow the announcements
                // page to load to the correct position in the list of announcements.
                announcement.viewmoreurl = URL.relativeUrl('/local/sb_announcements/index.php', {
                    announcementid: announcement.id,
                    offset: offset,
                });

                var promise = Templates.render('local_sb_announcements/announcement_content_item', announcement);
                promises.push(promise);

                promise.then(function(html, js) {
                    allhtml[index] = html;
                    alljs[index] = js;
                })
                .fail(DebugNotification.exception);
            }.bind(this));
        }

        return $.when.apply($.when, promises).then(function() {
            if (announcements.length) {
                $.each(announcements, function(index) {
                    container.append(allhtml[index]);
                    Templates.runTemplateJS(alljs[index]);
                });
            }
        });
    };

    /**
     * Send a request for more announcements from the server, if we aren't already
     * loading some and haven't already loaded all of them.
     *
     * Takes into account the current mode of the popover and will request only
     * unread announcements if required.
     *
     * All announcements are marked as read by the server when they are returned.
     *
     * @method loadMoreAnnouncement
     * @return {object} jQuery promise that is resolved when announcements have been
     *                        retrieved and added to the DOM
     */
    AnnouncementPopoverController.prototype.loadMoreAnnouncement = function() {
        if (this.isLoading || this.hasLoadedAllContent()) {
            return $.Deferred().resolve();
        }

        this.startLoading();
        var request = {
            limit: this.limit,
            offset: this.getOffset(),
            useridto: this.userId,
        };

        var container = this.getContent();
        return AnnouncementRepo.query(request).then(function(result) {
            var announcements = result.announcements;
            this.unreadCount = result.unreadcount;
            this.setLoadedAllContent(!announcements.length || announcements.length < this.limit);
            this.initialLoad = true;
            this.updateButtonAriaLabel();

            if (announcements.length) {
                this.incrementOffset();
                return this.renderAnnouncement(announcements, container);
            }

            return false;
        }.bind(this))
        .always(function() {
            this.stopLoading();
        }.bind(this));
    };

    /**
     * Send a request to the server to mark all unread announcements as read and update
     * the unread count and unread announcements elements appropriately.
     *
     * @return {Promise}
     * @method markAllAsRead
     */
    AnnouncementPopoverController.prototype.markAllAsRead = function() {
        this.markAllReadButton.addClass('loading');

        return AnnouncementRepo.markAllAsRead({useridto: this.userId})
            .then(function() {
                this.unreadCount = 0;
                this.root.find(SELECTORS.ANNOUNCEMENT).each(function(i, el){
                    console.log($(el).find('.announcment-image'))
                    $(el).find('.announcment-image').addClass('announcment-with-check')
                    .append('<i class="fa fa-check"></i>');
                });
                this.root.find(SELECTORS.UNREAD_ANNOUNCEMENT).removeClass('unread');
            }.bind(this))
            .always(function() {
                this.markAllReadButton.removeClass('loading');
            }.bind(this));
    };

    /**
     * Send a request to the server to mark a single announcement as read and update
     * the unread count and unread announcement elements appropriately.
     *
     * @param {jQuery} element
     * @return {Promise|boolean}
     * @method markAllAsRead
     */
    AnnouncementPopoverController.prototype.markAnnouncementAsRead = function(element) {
        if (!element.hasClass('unread')) {
            return false;
        }

        return AnnouncementRepo.markAsRead(element.attr('data-id'))
            .then(function() {
                this.unreadCount--;
                element.removeClass('unread');
                element.find('.announcement-image')
                        .addClass('announcment-with-check')
                        .append('<i class="fa fa-check"></i>');
            }.bind(this));
    };


    /**
     * Send a request to the server to remove a single announcement and update
     * the unread count and unread announcement elements appropriately.
     *
     * @param {jQuery} element
     * @return {Promise|boolean}
     * @method deleteAnnouncement
     */
    AnnouncementPopoverController.prototype.deleteAnnouncement = function(element) {
        // must read announcement before remove
        if (element.hasClass('unread')) {
            return false;
        }

        return AnnouncementRepo.deleteItem(element.attr('data-id'))
            .then(function() {
                this.unreadCount--;
                element.remove();
            }.bind(this));
    };



    /**
     * Add all of the required event listeners for this announcement popover.
     *
     * @method registerEventListeners
     */
    AnnouncementPopoverController.prototype.registerEventListeners = function() {
        CustomEvents.define(this.root, [
            CustomEvents.events.activate,
        ]);

        // Mark all announcements read if the user activates the mark all as read button.
        this.root.on(CustomEvents.events.activate, SELECTORS.MARK_ALL_READ_BUTTON, function(e) {
            this.markAllAsRead();
            e.stopPropagation();
        }.bind(this));

        // Mark individual announcement read if the user activates it.
        this.root.on(CustomEvents.events.activate, SELECTORS.ACCOUNCEMENT_LINK, function(e) {
            var element = $(e.target).closest(SELECTORS.ANNOUNCEMENT);
            this.markAnnouncementAsRead(element);
            e.stopPropagation();
        }.bind(this));

        // Remove announcement
        this.root.on(CustomEvents.events.activate, SELECTORS.ANNOUNCEMENT_DELETE_LINK, function(e) {
            var element = $(e.target).closest(SELECTORS.ANNOUNCEMENT);
            this.deleteAnnouncement(element);
            e.stopPropagation();
        }.bind(this));

        // Update the announcement information when the menu is opened.
        this.root.on(this.events().menuOpened, function() {
            this.hideUnreadCount();
            this.updateButtonAriaLabel();

            if (!this.hasDoneInitialLoad()) {
                this.loadMoreAnnouncement();
            }
        }.bind(this));

        // Update the unread announcement count when the menu is closed.
        this.root.on(this.events().menuClosed, function() {
            this.renderUnreadCount();
            this.updateButtonAriaLabel();
        }.bind(this));

        // Set aria attributes when popover is loading.
        this.root.on(this.events().startLoading, function() {
            this.getContent().attr('aria-busy', 'true');
        }.bind(this));

        // Set aria attributes when popover is finished loading.
        this.root.on(this.events().stopLoading, function() {
            this.getContent().attr('aria-busy', 'false');
        }.bind(this));

        // Load more announcements if the user has scrolled to the end of content
        // item list.
        this.getContentContainer().on(CustomEvents.events.scrollBottom, function() {
            if (!this.isLoading && !this.hasLoadedAllContent()) {
                this.loadMoreAnnouncement();
            }
        }.bind(this));

        // Stop mouse scroll from propagating to the window element and
        // scrolling the page.
        CustomEvents.define(this.getContentContainer(), [
            CustomEvents.events.scrollLock
        ]);

        // Listen for when a announcement is shown in the announcements page and mark
        // it as read, if it's unread.
        $(document).on(AnnouncementAreaEvents.announcementShown, function(e, announcement) {
            if (!announcement.read) {
                var element = this.getAnnouncementElement(announcement.id);

                if (element) {
                    element.removeClass('unread');
                }

                this.unreadCount--;
                this.renderUnreadCount();
            }
        }.bind(this));
    };

    return AnnouncementPopoverController;
});
