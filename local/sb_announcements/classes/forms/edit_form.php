<?php

defined('MOODLE_INTERNAL') || die;

require_once($CFG->libdir . '/formslib.php');

/**
 * The form for handling editing a course.
 */
class edit_form extends moodleform
{
    protected $id;
    protected $announcement;
    protected $context;

    /**
     * Form definition.
     */
    function definition()
    {
        global $CFG, $PAGE;

        $mform = $this->_form;
        $id = $this->_customdata['id'];
        $announcement = $this->_customdata['announcement'];
        $editoroptions = $this->_customdata['editoroptions'];

        $systemcontext = context_system::instance();

        $this->id = $id;
        $this->context = $systemcontext;
        $this->announcement = $announcement;

        $mform->addElement('hidden', 'id', $id);
        $mform->setType('id', PARAM_INT);

        $mform->addElement('text', 'title', get_string('title', 'local_sb_announcements'), 'maxlength="254"  size="50"');
        $mform->addRule('title', get_string('required_field', 'local_sb_announcements'), 'required', null, 'client');
        $mform->setType('title', PARAM_TEXT);

        $types = local_announcemets_get_types();
        $mform->addElement('select', 'type', get_string('announcementtype', 'local_sb_announcements'), $types, ((isset($announcement->id) && $announcement->id > 0) ? 'disabled="disabled"' : ''));
        if (!isset($announcement->id) || $announcement->id == 0) {
            $mform->addRule('type', get_string('required_field', 'local_sb_announcements'), 'required', null, 'client');
        }
        $mform->setType('type', PARAM_RAW);

        $classes = local_announcemets_get_classes();
        $mform->addElement('select', 'class', get_string('announcementclass', 'local_sb_announcements'), $classes);
        $mform->setType('class', PARAM_RAW);

        $mform->addElement('date_time_selector', 'startdate', get_string('startdate', 'local_sb_announcements'),array('optional' => false));
        $mform->setDefault('startdate', time());

        $mform->addElement('date_time_selector', 'enddate', get_string('enddate', 'local_sb_announcements'), array('optional' => true));
        $mform->setDefault('enddate', time() + 3600 * 24);

        if (has_capability('local/sb_announcements:editsystemtype', context_system::instance())) {
            $options = array(
                'multiple' => true,
                'noselectionstring' => get_string('selectrole', 'local_sb_announcements')
            );

            $select_roles = $mform->addElement('autocomplete', 'roles', get_string('roles', 'local_sb_announcements'), local_announcemets_get_roles(), $options);

            if (isset($announcement->id) and $announcement->type and $announcement->type == LOCAL_ANNOUNCEMENTS_SYSTEMTYPE and $announcement->data != '') {
                $announcement->roles = explode(',',$announcement->data);
            }
        }

        if (has_capability('local/sb_announcements:editcoursetype', context_system::instance())) {
            $options = array(
                'multiple' => true,
                'noselectionstring' => get_string('selectcourse', 'local_sb_announcements')
            );

            $select_courses = $mform->addElement('autocomplete', 'courses', get_string('course', 'local_sb_announcements'), local_announcemets_get_courses(), $options);

            if (isset($announcement->id) and $announcement->type and $announcement->type == LOCAL_ANNOUNCEMENTS_COURSETYPE and $announcement->data != '') {
                $announcement->courses = explode(',',$announcement->data);
            }
        }

        if (has_capability('local/sb_announcements:editcohorttype', context_system::instance()) and has_capability('moodle/cohort:manage', context_system::instance())) {
            $options = array(
                'multiple' => true,
                'noselectionstring' => get_string('selectcohort', 'local_sb_announcements')
            );

            $select_cohorts = $mform->addElement('autocomplete', 'cohorts', get_string('cohort', 'local_sb_announcements'), local_announcemets_get_cohorts(), $options);

            if (isset($announcement->id) and $announcement->type and $announcement->type == LOCAL_ANNOUNCEMENTS_COHORTTYPE and $announcement->data != '') {
                $announcement->cohorts = explode(',',$announcement->data);
            }
        }

        $mform->addElement('editor', 'body_editor', get_string('body', 'local_sb_announcements'), null, $editoroptions);
        $mform->setType('body_editor', PARAM_RAW);
        $mform->addRule('body_editor', get_string('required_field', 'local_sb_announcements'), 'required', null, 'client');

        $mform->addElement('advcheckbox', 'state', get_string('visible', 'local_sb_announcements'));
        $mform->setDefault('state', 1);
        $mform->setType('state', PARAM_INT);

        $mform->addElement('advcheckbox', 'sendnotification', get_string('sendnotification', 'local_sb_announcements'));
        $mform->setDefault('sendnotification', 0);
        $mform->setType('sendnotification', PARAM_INT);

        $this->add_action_buttons(get_string('cancel'), (($id > 0) ? get_string('save', 'local_sb_announcements') : get_string('create', 'local_sb_announcements')));

        // Finally set the current form data
        $this->set_data($announcement);
    }

    /**
     * Fill in the current page data for this course.
     */
    function definition_after_data()
    {
        global $DB;

        $mform = $this->_form;

    }

    /**
     * Validation.
     *
     * @param array $data
     * @param array $files
     * @return array the errors that were found
     */
    function validation($data, $files)
    {
        global $DB;

        $errors = parent::validation($data, $files);

        return $errors;
    }

}

