<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Contains class used to return information to display for the message popup.
 *
 * @package    local_sb_announcements
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_sb_announcements;

defined('MOODLE_INTERNAL') || die();

/**
 * Class used to return information to display for the announcements popup.
 *
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class api {
    /**
     * Get popup announcements for the specified users. Nothing is returned if announcements are disabled.
     *
     * @param int $useridto the user id who received the announcements
     * @param string $sort the column name to order by including optionally direction
     * @param int $limit limit the number of result returned
     * @param int $offset offset the result set by this amount
     * @return array announcements records
     * @throws \moodle_exception
     * @since 3.2
     */
    public static function get_popup_announcements($useridto = 0, $sort = 'DESC', $limit = 0, $offset = 0) {
        global $DB, $USER;

        $sort = strtoupper($sort);
        if ($sort != 'DESC' && $sort != 'ASC') {
            throw new \moodle_exception('invalid parameter: sort: must be "DESC" or "ASC"');
        }

        if (empty($useridto)) {
            $useridto = $USER->id;
        }

        $params = [
            'useridto' => $useridto,
            'now1' => time(),
            'now2' => time(),
        ];

        $sql = "SELECT a.*, a.userid as useridfrom, au.new, au.userid as useridto
                  FROM {local_sb_announcements_user} au
             LEFT JOIN {local_sb_announcements} a ON au.announcementid = a.id
                WHERE a.startdate <= :now1 AND (a.enddate = 0 OR a.enddate >= :now2) AND au.userid = :useridto AND a.state > 0
                    ORDER BY a.startdate $sort, au.new $sort, a.id $sort";

        return array_values($DB->get_records_sql($sql, $params, $offset, $limit));
    }

    /**
     * Count the unread announcements for a user.
     *
     * @param int $useridto the user id who received the announcements
     * @return int count of the unread announcements
     * @since 3.2
     */
    public static function count_unread_popup_announcements($useridto = 0) {
        global $USER, $DB;

        if (empty($useridto)) {
            $useridto = $USER->id;
        }

        return $DB->count_records_sql(
            "SELECT COUNT(a.id)
                  FROM {local_sb_announcements_user} au
             LEFT JOIN {local_sb_announcements} a ON au.announcementid = a.id
                WHERE au.userid = ? AND a.startdate <= ? AND (a.enddate = 0 OR a.enddate >= ?) AND a.state > 0",
            [$useridto, time(), time()]
        );
    }

    /**
     * set read announcement for a user.
     *
     * @param int $useridto the user id who received the announcements
     * @return int count of the unread announcements
     * @since 3.2
     */
    public static function set_read_announcements($messageid) {
        global $USER, $DB;
        if($message = $DB->get_record('local_sb_announcements_user', array('announcementid' => $messageid, 'userid' => $USER->id))){
            $message->new = 1;
            $DB->update_record('local_sb_announcements_user', $message);
            return 1;
        }
    }

    /**
     * set read all announcements for a user.
     *
     * @return int count of the unread announcements
     * @since 3.2
     */
    public static function set_read_all_announcements() {
        global $USER, $DB;
        if($messages = $DB->get_records('local_sb_announcements_user', array('userid' => $USER->id))){
            foreach($messages as $message){
                $message->new = 1;
                $DB->update_record('local_sb_announcements_user', $message);
            }
            return 1;
        }
    }

    /**
     * Delete announcement
     *
     * @param int $announcementid the announcement id
     * @return bool status
     * @since 3.2
     */
    public static function delete_announcement($announcementid) {
        global $USER, $DB;
        // check what user remove own announcement
        if($announcement = $DB->get_record('local_sb_announcements_user', array('announcementid' => $announcementid, 'userid' => $USER->id))){
            $DB->delete_records('local_sb_announcements_user', array('id' => $announcement->id));
            $DB->delete_records('local_sb_announcements', array('id' => $announcementid));
            return 1;
        } else {
            return false;
        }
    }

}
