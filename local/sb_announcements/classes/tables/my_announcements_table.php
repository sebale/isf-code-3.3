<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Announcements version file.
 *
 * @package    local_sb_announcements
 * @author     SEBALE
 * @copyright  2016 sebale.net
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/tablelib.php');

class my_announcements_table extends table_sql
{
    function __construct($uniqueid, $search)
    {
        global $CFG, $USER, $PAGE;

        parent::__construct($uniqueid);


        $columns = array('type', 'title', 'courses', 'message', 'date', 'author', 'actions');

        $this->define_columns($columns);
        $headers = array(
            get_string('type', 'local_sb_announcements'),
            get_string('title', 'local_sb_announcements'),
            get_string('courses'),
            get_string('message', 'local_sb_announcements'),
            get_string('date', 'local_sb_announcements'),
            get_string('author', 'local_sb_announcements'),
            get_string('actions', 'local_sb_announcements'));

        $this->define_headers($headers);

        $now_time = time();

        $sql_search = ($search) ? " AND (a.title LIKE '%$search%' OR a.body LIKE '%$search%' OR a.type LIKE '%$search%')" : "";
        $fields = "a.id, a.type, a.title, a.body as message, a.class, a.startdate, CONCAT(u.firstname, ' ', u.lastname) as author, a.state, a.data, au.new as readed";
        $from = "{local_sb_announcements} a
                    LEFT JOIN {user} u ON u.id = a.userid
                    LEFT JOIN {local_sb_announcements_user} au ON au.announcementid = a.id";

        $where = 'a.id > 0
                    AND a.state = 1
                    AND a.startdate <= :now_time1
                    AND (a.enddate >= :now_time2 OR a.enddate = 0)
                    AND au.userid = :userid ' . $sql_search;
        $params = array('now_time1'=>$now_time, 'now_time2'=>$now_time, 'userid'=>$USER->id);
        $this->set_sql($fields, $from, $where, $params);

        $this->define_baseurl($PAGE->url);
    }


    function col_date($values)
    {
        $date = '-';
        if ($values->announcement_date)
            $date = date("g:i a, d M Y", $values->announcement_date);
        elseif ($values->startdate)
            $date = date("g:i a, d M Y", $values->startdate);

        return $date;
    }

    function col_courses($values)
    {
        global $DB;
        $data = '-';
        if ($values->type == 'course') {
            if ($values->data != '') {
                $data = '';
                $courses = unserialize($values->data);
                if ($courses and count($courses) > 0) {
                    $ccourses = $DB->get_records_sql('SELECT * FROM {course} WHERE id IN (' . implode(', ', $courses) . ')');
                    if (count($ccourses) > 0) {
                        $cn = array();
                        foreach ($ccourses as $course) {
                            $cn[] = $course->fullname;
                        }
                        $data = implode(', ', $cn);
                    }
                }
            }
        }

        return $data;
    }

    function col_actions($values)
    {
        global $OUTPUT, $PAGE;

        $strdelete = get_string('delete');
        $strshow = get_string('mark_as_unread', 'local_sb_announcements');
        $strhide = get_string('mark_as_read', 'local_sb_announcements');

        $aurl = new moodle_url('/local/sb_announcements/index.php', array('action' => 'delete', 'id' => $values->id));
        $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/delete', $strdelete, 'core', array('class' => 'iconsmall')));

        if ($values->readed == 1) {
            $aurl = new moodle_url('/local/sb_announcements/index.php', array('action' => 'read', 'id' => $values->id));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/markasread', $strhide, 'core', array('class' => 'iconsmall')));
        } else {
            $aurl = new moodle_url('/local/sb_announcements/index.php', array('action' => 'unread', 'id' => $values->id));
            $edit[] = $OUTPUT->action_icon($aurl, new pix_icon('t/show', $strshow, 'core', array('class' => 'iconsmall')));
        }

        $color = '';

        if ($values->color)
        {
            $color = '<span class="raw-color" style="display:none;">' . $values->color . '</span>';
        }

        return implode('', $edit) . $color;
    }
}
