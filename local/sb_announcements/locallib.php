<?php
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Announcements version file.
 *
 * @package    local_sb_announcements
 * @author     ISF
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 */


function local_announcemets_get_types() {
    $types = array(''=>get_string('selecttype', 'local_sb_announcements'));

    if (has_capability('local/sb_announcements:editsystemtype', context_system::instance())) {
        $types[LOCAL_ANNOUNCEMENTS_SYSTEMTYPE] = get_string('systemtype', 'local_sb_announcements');
    }
    if (has_capability('local/sb_announcements:editcoursetype', context_system::instance())) {
        $types[LOCAL_ANNOUNCEMENTS_COURSETYPE] = get_string('coursetype', 'local_sb_announcements');
    }
    if (has_capability('local/sb_announcements:editcohorttype', context_system::instance()) and has_capability('moodle/cohort:manage', context_system::instance())) {
        $types[LOCAL_ANNOUNCEMENTS_COHORTTYPE] = get_string('cohorttype', 'local_sb_announcements');
    }

    return $types;
}

function local_announcemets_get_classes() {
    return array('clean'=>get_string('cleanclass', 'local_sb_announcements'),
                 'info'=>get_string('infoclass', 'local_sb_announcements'),
                 'warning'=>get_string('warningclass', 'local_sb_announcements'),
                 'danger'=>get_string('dangerclass', 'local_sb_announcements'),
                 'success'=>get_string('successclass', 'local_sb_announcements')
                );
}


function local_announcemets_get_courses() {
    global $DB;
    $courses = array();

    if (has_capability('local/sb_announcements:canseeallcourses', context_system::instance())) {
        $courses = $DB->get_records_select_menu('course', 'id > 1', array(), 'fullname', 'id, fullname');
    } else {
        $mycourses = enrol_get_my_courses('id, fullname', 'fullname', 0);
        if (count($mycourses)){
            foreach ($mycourses as $course) {
                $courses[$course->id] = $course->fullname;
            }
        }
    }

    return $courses;
}

function local_announcemets_get_roles() {
    return get_assignable_roles(context_system::instance(), ROLENAME_BOTH);
}

function local_announcemets_get_cohorts() {
    global $DB, $USER;

    $cohorts = array();
    if (has_capability('local/sb_announcements:canseeallcohorts', context_system::instance())) {
        $cohorts = $DB->get_records_select_menu('cohort', 'visible > 0', array(), 'name', 'id, name');
    } else {
        $user_cohorts = $DB->get_records_sql(
            "SELECT c.id, c.name
               FROM {cohort} c
          LEFT JOIN {cohort_members} cm ON c.id = cm.cohortid
              WHERE c.visible > 0 AND cm.userid = :userid
              GROUP BY c.id ORDER BY c.name",
            array('userid'=>$USER->id));
        if (count($user_cohorts)) {
            foreach ($user_cohorts as $cohort) {
                $cohorts[$cohort->id] = $cohort->name;
            }
        }
    }

    return $cohorts;
}

function local_sb_announcements_send_announcements()
{
    global $DB, $CFG;

    $count = 0; $now = time();
    $sql = "SELECT * FROM {local_sb_announcements}
                WHERE sendnotification = 1 AND sent = 0 AND startdate <= " . $now . " AND (enddate = 0 OR enddate >= $now)";
    $announcements = $DB->get_records_sql($sql);

    if (count($announcements)){
        foreach ($announcements as $announcement){
            $count += local_sb_announcements_process_notification($announcement);
        }
    }

    return $count;
}


function local_sb_announcements_get_course_users($courses)
{
    global $CFG, $DB;

    list($sql_in, $params) = $DB->get_in_or_equal($courses, SQL_PARAMS_NAMED);
    $users = $DB->get_records_sql("SELECT DISTINCT(u.id)
                    FROM {$CFG->prefix}user_enrolments as ue
                        LEFT JOIN {$CFG->prefix}user as u ON u.id = ue.userid
                        LEFT JOIN {$CFG->prefix}enrol as e ON e.id = ue.enrolid
                        LEFT JOIN {$CFG->prefix}course as c ON c.id = e.courseid
                        LEFT JOIN {$CFG->prefix}context as cx ON cx.instanceid = e.courseid
                        LEFT JOIN {$CFG->prefix}role_assignments ra ON ra.contextid = cx.id AND ra.userid = ue.userid
                            WHERE e.courseid $sql_in AND u.id > 1 AND u.deleted = 0 GROUP BY u.id, e.courseid", $params);
    return $users;
}

function local_sb_announcements_send_mail($data) {
    global $DB;

    $announcement = $DB->get_record('local_sb_announcements', array('id'=>$data->id));

    if (!$announcement->sent and $data->startdate <= time() && ($data->enddate == 0 || $data->enddate >= time())) {
        local_sb_announcements_process_notification($announcement);
    }
}

function local_sb_announcements_get_mail_recipients($id)
{
    global $DB;

    $users = array();

    if ($id){
        $sql = "SELECT u.*
                       FROM {local_sb_announcements_user} au
                  LEFT JOIN {user} u ON u.id = au.userid
                      WHERE au.announcementid = $id
                        AND u.deleted = 0
                        AND u.suspended = 0
                        AND u.confirmed = 1";

        $users = $DB->get_records_sql($sql);
    }

    return $users;
}

function local_sb_announcements_process_notification($announcement) {
    global $DB;

    $count = 0;

    if ($announcement and !$announcement->sent and $announcement->sendnotification) {

        $users = local_sb_announcements_get_mail_recipients($announcement->id);
        $userfrom = core_user::get_noreply_user();
        $userfrom->maildisplay = true;

        if (count($users)){
            foreach ($users as $user){
                // Holds values for the essayemailsubject string for the email message
                $a = new stdClass;
                $a->text = $announcement->body;

                // Fetch message HTML and plain text formats
                $message  = get_string('announcementemail', 'local_sb_announcements', $a);
                $plaintext = format_text_email($message, FORMAT_HTML);

                // Subject
                $subject = $announcement->title;

                $eventdata = new \core\message\message();
                $eventdata->courseid         = SITEID;
                $eventdata->userfrom         = $userfrom;
                $eventdata->userto           = $user;
                $eventdata->subject          = $subject;
                $eventdata->fullmessage      = $plaintext;
                $eventdata->fullmessageformat = FORMAT_HTML;
                $eventdata->fullmessagehtml  = $message;
                $eventdata->smallmessage     = '';

                // Required for messaging framework
                $eventdata->component = 'local_sb_announcements';
                $eventdata->name = 'announcements_notify';

                message_send($eventdata);
            }
        }

        $count = count($users);
        $announcement->sent = 1;
        $DB->update_record('local_sb_announcements', $announcement);
    }

    return $count;
}


function local_sb_announcements_get_announcements()
{
    global $CFG, $DB, $USER;

    $limit = (int)get_config('local_sb_announcements', 'number');

    $announcements = $DB->get_records_sql(
        "SELECT a.*, f.filename
            FROM {local_sb_announcements} a
                LEFT JOIN {files} f ON f.itemid = a.id AND f.component = 'local_sb_announcements' AND f.filearea = 'imagefile' AND f.mimetype IS NOT NULL
                LEFT JOIN {local_kaltura_ann_view} av ON av.instanceid = a.id AND av.userid = $USER->id
            WHERE a.startdate <= " . time() . " AND (a.enddate = 0 OR a.enddate >= " . time() . ") AND a.state > 0 AND av.id IS NULL ORDER BY a.startdate ASC, a.id DESC LIMIT 0, $limit");

    return $announcements;
}

function local_sb_announcements_get_allannouncements_count()
{
    global $CFG, $DB, $USER;

    $announcements = $DB->get_record_sql(
        "SELECT COUNT(a.id) as count
            FROM {local_sb_announcements} a
            WHERE a.startdate <= " . time() . " AND (a.enddate = 0 OR a.enddate >= " . time() . ") AND a.state > 0");

    return ($announcements->count) ? $announcements->count : 0;
}

function local_sb_announcements_cleanup_announcements()
{
    global $DB, $CFG;

    $pluginconfig = get_config('local_sb_announcements', 'cleanup');

    if ((int)$pluginconfig > 0) {
        $now = time();
        $cleanuptime = $now + ((int)$pluginconfig * 86400);
        $announcements = $DB->get_records_select_menu('local_sb_announcements', "enddate < " . $cleanuptime . " AND enddate > 0 AND enddate < $now", array(), 'id', 'id, title');

        if (count($announcements)){
            $ids = array_keys($announcements);
            $sql_delete = "DELETE
                             FROM {local_sb_announcements_user}
                            WHERE announcementid IN (".implode(',', $ids).")";
            $DB->execute($sql_delete);
        }

        $sql_delete = "DELETE
                            FROM {local_sb_announcements}
                        WHERE enddate < " . $cleanuptime . " AND enddate > 0 AND enddate < $now";
        $DB->execute($sql_delete);
    }

    return (isset($announcements) and count($announcements)) ? count($announcements) : 0;
}

function local_sb_announcements_notifications()
{
    global $DB, $CFG;

    return ($count->count) ? $count->count : 0;
}

function local_sb_announcements_formatted_text($data, $options = array()) {
    global $CFG;

    require_once($CFG->libdir. '/filelib.php');
    if (!$data->body) {
        return '';
    }
    $options = (array)$options;
    $context = context_system::instance();

    $body = file_rewrite_pluginfile_urls($data->body, 'pluginfile.php', $context->id, 'local_sb_announcements', 'bodyfile', $data->id);

    return $body;
}
