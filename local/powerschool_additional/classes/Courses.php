<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * @package    local_powerschool
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/local/powerschool/classes/Courses.php');
require_once($CFG->dirroot.'/course/lib.php');
require_once($CFG->dirroot.'/course/modlib.php');

class AdditionalCourses extends Courses{

    public function __construct($connection_id){
        parent::__construct($connection_id);
    }

    public function graded_assignments(\core\event\user_graded $instant = null, $grade = null){
        global $DB, $USER;
        if($instant == null && $grade == null)
            throw new moodle_exception('error_grade', 'local_powerschool', new moodle_url('/admin/'), null, 'Give me please \core\event\user_graded instant or grade instant');

        if($grade == null)
            $grade = $instant->get_grade();

        if($grade->grade_item->itemtype == 'mod') {
            if(empty($grade->grade_item->outcomeid))
                return true;

            $url = get_config('local_powerschool_additional','web_service_url');
            if(empty($url))
                return true;

            $cm = get_coursemodule_from_instance($grade->grade_item->itemmodule,$grade->grade_item->iteminstance,$grade->grade_item->courseid,false,MUST_EXIST);

            if(!$this->cm_assignment_sync((object)array('cmid'=>$cm->id,'courseid'=>$grade->grade_item->courseid )))
                return true;

            if(self::get_coursetype_from_courseid($grade->grade_item->courseid) == 'groups'){
                $groups = groups_get_user_groups($grade->grade_item->courseid,$grade->userid);
                $groups = array_pop($groups);
                if(!empty($groups)){
                    $info_records = $DB->get_records_sql('SELECT a.id, d.data as userid, a.assignmentid, cf.gradebooktype, cf.teacher_user_dcid, cf.gradescaleitem, cf.term_id, a.assignmentsectionid, psa.assignmentstandardassocid
                                                    FROM {user_info_field} f
                                                       LEFT JOIN {user_info_data} d ON d.fieldid=f.id AND d.userid=:userid
                                                       JOIN {user_info_field} fs ON fs.shortname = \'powerschool777usertype\'
                                                       JOIN {user_info_data} ds ON ds.fieldid=fs.id AND d.userid=ds.userid
                                                       JOIN {user_info_field} fc ON fc.shortname = \'powerschool777connection_id\'
                                                       JOIN {user_info_data} dc ON dc.fieldid=fc.id AND d.userid=dc.userid
                                                       JOIN {powerschool_assignment} a ON a.cmid=:cmid AND a.courseid=:courseid AND a.groupid IN ('.implode(',',$groups).')
                                                       LEFT JOIN {powerschool_course_cat} cc ON cc.catid=a.groupid
                                                       LEFT JOIN {powerschool_course_fields} cf ON cf.section_id=cc.courseid
                                                       LEFT JOIN {powerschool_standards} ps ON ps.outcomeid=:outcomeid
                                                       LEFT JOIN {powerschool_standard_assoc} psa ON psa.assignmentid=a.assignmentid AND psa.standardid=ps.standardid
                                                    WHERE f.shortname = "powerschool777id" AND ds.data=\'student\' AND dc.data=:connection_id AND cf.connection_id=:connection_id2 GROUP BY a.id'
                            ,array('userid'=>$grade->userid,'cmid'=>$cm->id,'courseid'=>$grade->grade_item->courseid,'connection_id'=>$this->connection->id,'connection_id2'=>$this->connection->id, 'outcomeid'=>$grade->grade_item->outcomeid));
                }else{
                    $info_records = array();
                }

            }else{
                $info_records = $DB->get_records_sql('SELECT a.id, d.data as userid, a.assignmentid, cf.gradebooktype, cf.teacher_user_dcid, cf.gradescaleitem, cf.term_id, a.assignmentsectionid, psa.assignmentstandardassocid
                                                FROM {user_info_field} f
                                                   LEFT JOIN {user_info_data} d ON d.fieldid=f.id AND d.userid=:userid
                                                    JOIN {user_info_field} fs ON fs.shortname = \'powerschool777usertype\'
                                                    JOIN {user_info_data} ds ON ds.fieldid=fs.id AND d.userid=ds.userid
                                                    JOIN {user_info_field} fc ON fc.shortname = \'powerschool777connection_id\'
                                                    JOIN {user_info_data} dc ON dc.fieldid=fc.id AND d.userid=dc.userid
                                                   JOIN {powerschool_assignment} a ON a.cmid=:cmid AND a.courseid=:courseid
                                                   LEFT JOIN {powerschool_course_fields} cf ON cf.courseid=a.courseid
                                                   LEFT JOIN {powerschool_standards} ps ON ps.outcomeid=:outcomeid
                                                   LEFT JOIN {powerschool_standard_assoc} psa ON psa.assignmentid=a.assignmentid AND psa.standardid=ps.standardid
                                                WHERE f.shortname = "powerschool777id" AND ds.data=\'student\' AND dc.data=:connection_id AND cf.connection_id=:connection_id2'
                            ,array('userid'=>$grade->userid,'cmid'=>$cm->id,'courseid'=>$grade->grade_item->courseid,'connection_id'=>$this->connection->id,'connection_id2'=>$this->connection->id, 'outcomeid'=>$grade->grade_item->outcomeid));
            }

            foreach($info_records as $info) {
                if(empty($info->assignmentstandardassocid))
                    continue;

                $scale = $DB->get_record('scale', array('id' => $grade->rawscaleid));
                $scales = explode(",", $scale->scale);

                $json = new stdClass();
                $json->assignmentsectionid = $info->assignmentsectionid;
                $json->assignmentstandardassocid = $info->assignmentstandardassocid;
                $json->studentsdcid = (string)$info->userid;
                $json->scoreentered = $scales[max($grade->finalgrade - $grade->get_grade_min(),0)];
                $json->username = $USER->username;
                $json->delete = (empty($grade->finalgrade))?'1':'0';
                $json = json_encode($json);

                $this->curl->post($url, array('json'=>$json));
            }
            return true;
        }
    }

}