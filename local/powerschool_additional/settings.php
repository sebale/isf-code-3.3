<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * PowerSchool Settings
 *
 * @package    local_powerschool_additional
 * @copyright  IntelliBoard, Inc <https://intelliboard.net/>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

		
$settings = new admin_settingpage('local_powerschool_additional', get_string('pluginname', 'local_powerschool_additional'));
if (!$ADMIN->locate('powerschool')){
	$ADMIN->add('localplugins', $settings);
}

$name = 'local_powerschool_additional/web_service_url';
$title = get_string('web_service_url', 'local_powerschool_additional');
$description = 'Example: "https://psimage.isf.edu.hk/moodle/assignmentstandardscore.php"';
$setting = new admin_setting_configtext($name, $title, $description, null);
$settings->add($setting);

