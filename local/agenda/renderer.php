<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderer for outputting the topics course format.
 *
 * @package local_agenda
 * @copyright 2017 ISF
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


defined('MOODLE_INTERNAL') || die();

class local_agenda_renderer extends plugin_renderer_base {

    public $agenda_options_colors = array('#2EC6C8', '#B5A1DD', '#5AB0EE', '#F4984E', '#D77A80', '#DB4B39', '#2A394F', '#8CC63E', '#58585B', '#5B9BB4');
    public $agenda_icons = array('post'=>'fa fa-bars', 'question'=>'fa fa-question', 'link'=>'fa fa-link', 'files'=>'fa fa-file-o', 'poll'=>'fa fa-align-left');
    public $limit = 5;

    public function print_home($courseid) {

        $html = html_writer::start_tag('div', array('class' => 'cf-agenda'));
        $html .= html_writer::start_tag('div', array('class' => 'clearfix'));

        // posts
        $html .= html_writer::start_tag('div', array('class' => 'col-lg-8 agenda-box-inner'));
        $context = context_course::instance($courseid, MUST_EXIST);

        if (has_capability('local/agenda:post', $context)) {

            $html .= html_writer::link(new moodle_url('/local/agenda/post.php', array('courseid' => $courseid)),
                html_writer::tag('i', '', array('class' => 'fa fa-pencil-square-o')) .
                get_string('new_post', 'local_agenda') , array(
                    'class' => 'btn btn-primary btn-new-post',
                    'data-course-id' => $courseid
                ));
        }

        $html .= $this->print_agenda_posts($courseid);

        // agenda posts
        $html .= html_writer::end_tag('div');

        $html .= html_writer::start_tag('div', array('class' => 'col-lg-4 agenda-rightpanel'));
        $html .= html_writer::start_tag('div', array('class' => 'agenda-rightpanel-inner'));

        // assignments
        $html .= $this->print_upcoming_assignments($courseid);

        // calendar
        $html .= html_writer::start_tag('div', array('class' => 'item-box upcoming-deadline'));
        $html .= html_writer::tag('span', get_string('upcoming_deadlines', 'local_agenda'), array('class' => 'title'));
        $html .= html_writer::start_tag('div', array('class' => 'item-inner'));
        $html .= html_writer::tag('div', '', array('class' => 'course-calendar', 'id' => 'course_calendar'));
        $html .= html_writer::start_tag('div', array('class' => 'course-calendar-events'));

        $html .= html_writer::end_tag('div');

        $html .= html_writer::end_tag('div');

        $html .= html_writer::end_tag('div');

        $html .= html_writer::end_tag('div');
        $html .= html_writer::end_tag('div');
        $html .= html_writer::end_tag('div');
        $html .= html_writer::end_tag('div');

        echo $html;
    }

    public function print_agenda_posts($courseid, $start = 0) {
        global $CFG, $DB;
        require_once($CFG->dirroot.'/local/agenda/locallib.php');
        $html = '';

        $data = local_agenda_get_agenda_posts($courseid, $start, $this->limit);

        if (!count($data['agenda_posts'])){
            return html_writer::tag('div', get_string('nothingtodisplay', 'local_agenda'), array('class' => 'alert alert-success'));
        }

        foreach ($data['agenda_posts'] as $apostdate=>$apost) {
            $html .= html_writer::start_tag('div', array('class' => 'timeline-title'));
                if ($apostdate == userdate(time(), '%B %d, %Y')) {
                    $html .= get_string('today', 'local_agenda');
                } else if ($apostdate == userdate(mktime(0,0,0, date('n'), date('j')+1, date('Y')), '%B %d, %Y')) {
                    $html .= get_string('tomorrow', 'local_agenda');
                } else if ($apostdate == userdate(mktime(0,0,0, date('n'), date('j')-1, date('Y')), '%B %d, %Y')) {
                    $html .= get_string('yesterday', 'local_agenda');
                } else {
                    $html .= $apostdate;
                }
            $html .= html_writer::end_tag('div');
            $html .= html_writer::start_tag('ul', array('class' => 'items-list'.((strtotime($apostdate) > time()) ? ' notpubblished' : '')));
                foreach ($apost as $item){
                    $html .= $this->print_agenda_post($item, $data);
                }
            $html .= html_writer::end_tag('ul');
        }

        if ($start == 0 and isset($data['agenda_posts_count']->count) and $data['agenda_posts_count']->count > $this->limit) {
            $html .= html_writer::start_tag('div', array('class' => 'agenda-load-more-box'));
                $html .= html_writer::tag('button', get_string('loadmore', 'local_agenda'), array('class' => 'btn agenda-load-more', 'onclick'=>'agendaLoadMore(0, '.$data['agenda_posts_count']->count.', '.$courseid.')'));
            $html .= html_writer::end_tag('div');
        }

        $html .= "<script>$('.comments-form-textarea input').keypress(function (e) {
                      if (e.which == 13) {
                        var id = $(this).attr('data-id');
                        agendaSendComment(id);
                      }
                    });
                </script>";

        return $html;
    }

    public function print_agenda_post($item, $data) {
        global $CFG, $DB, $USER, $OUTPUT;
        $html = '';
        $item_icons = array();

        $html .= html_writer::start_tag('li', array('class' => 'clearfix alist-item'.(($item->visible > 0) ? '' : ' notvisible'), 'data-id'=>$item->id));
            $html .= html_writer::start_tag('div', array('class' => 'user-picture'));
                $userpicdata = (object)array('id'=>$item->userid, 'firstname'=>$item->firstname, 'lastname'=>$item->lastname, 'picture'=>$item->picture, 'imagealt'=>$item->imagealt, 'firstnamephonetic'=>$item->firstnamephonetic, 'lastnamephonetic'=>$item->lastnamephonetic, 'middlename'=>$item->middlename, 'alternatename'=>$item->alternatename, 'email'=>$item->email);
                $html .= $OUTPUT->user_picture($userpicdata, array('size'=>40));
            $html .= html_writer::end_tag('div');

            $html .= html_writer::start_tag('div', array('class' => 'post-body-box'));
                $html .= html_writer::start_tag('div', array('class' => 'post-author'));
                    $html .= html_writer::link(new moodle_url('/user/profile.php', array('id' => $item->userid)),
                        fullname($item));
                    $html .= html_writer::tag('span', $item->title, array('class' => 'post-title'));
                $html .= html_writer::end_tag('div');

                if (!empty($item->post)) {
                    $html .= html_writer::tag('div', local_agenda_get_formatted_text($item->post, $data['context'], $item->id, 'post'), array('class' => 'post-text'));
                    $item_icons['post'] = 'post';
                }
                if (!empty($item->question)) {
                    $html .= html_writer::start_tag('div', array('class' => 'post-question item-box'));
                        $html .= html_writer::tag('span', html_writer::tag('i', '', array('class'=>'fa fa-question')), array('class' => 'post-item-icon'));
                        $html .= html_writer::tag('div', local_agenda_get_formatted_text($item->question, $data['context'], $item->id, 'question'), array('class' => 'post-question item-text'));
                    $html .= html_writer::end_tag('div');
                    $item_icons['question'] = 'question';
                }

                if (!empty($item->link)) {
                    $html .= html_writer::start_tag('div', array('class' => 'post-link item-box'));
                        $html .= html_writer::tag('span', html_writer::tag('i', '', array('class'=>'fa fa-link')), array('class' => 'post-item-icon'));

                        $html .= html_writer::start_tag('div', array('class' => 'item-text'));
                            if (!empty($item->link_title)) {
                                $html .= html_writer::link($item->link, $item->link_title, array('target'=>'_blank'));
                            } else {
                                $html .= html_writer::link($item->link, $item->link, array('target'=>'_blank'));
                            }
                            if (!empty($item->link_info)) {
                                $html .= html_writer::tag('p', $item->link_info);
                            }
                        $html .= html_writer::end_tag('div');
                    $html .= html_writer::end_tag('div');
                    $item_icons['link'] = 'link';
                }

                if (isset($data['files'][$item->id]) and count($data['files'][$item->id]) > 0) {
                    $html .= html_writer::start_tag('div', array('class' => 'post-file item-box'));
                        $html .= html_writer::tag('span', html_writer::tag('i', '', array('class'=>'fa fa-file-o')), array('class' => 'post-item-icon'));
                        $html .= html_writer::start_tag('div', array('class' => 'item-text'));
                            foreach ($data['files'][$item->id] as $file) {
                                $html .= html_writer::link($file['url'], $file['filename']);
                            }
                        $html .= html_writer::end_tag('div');
                    $html .= html_writer::end_tag('div');
                    $item_icons['files'] = 'files';
                }

                $html .= $this->print_agenda_polls($item, $data);
                $html .= $this->print_agenda_actions($item, $data);

                if (count($data['courses_options'])){
                    $html .= $this->print_course_select($item, $data);
                }

                if ($item->allow_comments){
                    $html .= $this->print_agenda_comments($item, $data);
                }

            $html .= html_writer::end_tag('div');

            $html .= $this->print_agenda_icons($item, $data);

        $html .= html_writer::end_tag('li');

        return $html;
    }

    public function print_agenda_polls($item, $data) {
        global $CFG, $DB, $USER, $OUTPUT;
        $html = '';

        if (empty($item->poll)) {
            return $html;
        }

        $html .= html_writer::start_tag('div', array('class' => 'post-poll-box'.((isset($data['my_polls'][$item->id])) ? ' results' : ' quis')));
            if (isset($data['my_polls'][$item->id])) {
                $html .= html_writer::start_tag('div', array('class' => 'post-poll'));
                    $html .= (!empty($item->poll)) ? local_agenda_get_formatted_text($item->poll, $data['context'], $item->id, 'poll') : '';

                    $html .= html_writer::start_tag('span');
                        $html .= html_writer::tag('i', '', array('class'=>'fa fa-user'));
                        $html .= (isset($data['polls_count'][$item->id]) and $data['polls_count'][$item->id] > 0) ? number_format($data['polls_count'][$item->id]) : 0;
                    $html .= html_writer::end_tag('span');
                $html .= html_writer::end_tag('div');

                if(isset($data['agendas_options'][$item->id]) and count($data['agendas_options'][$item->id])){
                    $html .= html_writer::start_tag('table', array('class' => 'poll-list')); $c_f = 0;
                        foreach ($data['agendas_options'][$item->id] as $option){
                            $html .= html_writer::start_tag('tr');
                                $html .= html_writer::tag('td', $option->name, array('class'=>'cpo_1'));
                                $html .= html_writer::start_tag('td', array('class'=>'cpo_2'));
                                    $option_percents = round(($option->option_count / $option->agenda_count) * 100);
                                    $html .= html_writer::start_tag('div', array('class'=>'poll-result-box'));
                                        $html .= html_writer::tag('div', '', array('class'=>'poll-result-inner', "style"=>'width:'.$option_percents.'%; background-color:'.$this->agenda_options_colors[$c_f]));
                                    $html .= html_writer::end_tag('div');
                                    $html .= html_writer::tag('span', $option_percents.'%', array('class'=>'pol-result-num'));
                                $html .= html_writer::end_tag('td');
                                $html .= html_writer::tag('td', (($option->option_count) ? $option->option_count : 0), array('class'=>'cpo_3'));
                            $html .= html_writer::end_tag('tr');
                            $c_f = ($c_f <= 9) ? $c_f+1 : $c_f = 0;
                        }
                    $html .= html_writer::end_tag('table');
                }

            } else {

                $html .= html_writer::tag('div', local_agenda_get_formatted_text($item->poll, $data['context'], $item->id, 'poll'), array('class' => 'post-poll'));

                if(isset($data['agendas_options'][$item->id]) and count($data['agendas_options'][$item->id])){
                    $html .= html_writer::start_tag('ul', array('class'=>'poll-list'));
                        foreach ($data['agendas_options'][$item->id] as $option){
                            $html .= html_writer::start_tag('li', array('option-id'=>$option->id));
                            $html .= html_writer::start_tag('label', array('onclick'=>'agendaPollSave('.$option->instanceid.', '.$option->id.')'));
                                $html .= html_writer::empty_tag('input', array('name'=>'option_'.$option->instanceid.'[]', 'type'=>'radio', 'value'=>'1'));
                            $html .= $option->name;
                            $html .= html_writer::end_tag('label');
                            $html .= html_writer::end_tag('li');
                        }
                        $html .= html_writer::start_tag('li', array('class'=>'show-results'));
                            $html .= html_writer::tag('label', get_string('showresults', 'local_agenda'), array('onclick'=>'agendaPollShow('.$option->instanceid.');'));
                        $html .= html_writer::end_tag('li');
                    $html .= html_writer::end_tag('ul');
                }
            }

        $html .= html_writer::end_tag('div');

        return $html;
    }

    public function print_agenda_actions($item, $data) {
        global $CFG, $DB, $USER, $OUTPUT;
        $html = '';

        $html .= html_writer::start_tag('div', array('class'=>'post-actions'));
            $html .= html_writer::start_tag('ul', array('class'=>'clearfix'));

                if ($item->allow_likes > 0) {
                    $html .= html_writer::start_tag('li', array('class'=>'like'.(($item->currentuserliked > 0) ? '  likes' : ''), 'onclick'=>'agendaLike('.$item->id.')'));
                        $html .= html_writer::tag('i', '', array('class'=>'fa fa-thumbs-up'));
                        $html .= get_string('like', 'local_agenda');
                        $html .= html_writer::tag('span', (($item->likescount > 0) ? '('.$item->likescount.')' : ''), array('class'=>'count'));
                    $html .= html_writer::end_tag('li');
                }
                if ($item->allow_comments > 0) {
                    $html .= html_writer::start_tag('li', array('class'=>'comment', 'onclick'=>'agendaToggleComments('.$item->id.')'));
                        $html .= html_writer::tag('i', '', array('class'=>'fa fa-comments'));
                        $html .= get_string('comments', 'local_agenda');
                        $html .= html_writer::tag('span', (($item->commentscount > 0) ? '('.$item->commentscount.')' : ''), array('class'=>'count'));
                    $html .= html_writer::end_tag('li');
                }

                if ($data['canedit'] or $item->userid == $USER->id) {
                    // edit
                    $html .= html_writer::start_tag('li', array('class'=>'edit'));
                        $html .= html_writer::link(new moodle_url('/local/agenda/post.php', array('courseid' => $item->courseid, 'id' => $item->id)), html_writer::tag('i', '', array('class'=>'fa fa-pencil-square-o')) . get_string('edit', 'local_agenda'));
                    $html .= html_writer::end_tag('li');

                    // copy
                    if (count($data['courses_options']) > 0) {
                        $html .= html_writer::start_tag('li', array('class'=>'copy', 'onclick'=>'agendaToggleCopy('.$item->id.')'));
                        $html .= html_writer::tag('i', '', array('class'=>'fa fa-files-o')) . get_string('copy', 'local_agenda');
                        $html .= html_writer::end_tag('li');
                    }

                    // hide
                    $html .= html_writer::start_tag('li', array('class'=>'ahide'.(($item->visible > 0) ? '' : '  hidden'), 'onclick'=>'agendaHide('.$item->id.')'));
                    $html .= html_writer::tag('i', '', array('class'=>'fa fa-eye-slash')) . get_string('hide', 'local_agenda');
                    $html .= html_writer::end_tag('li');

                    // show
                    $html .= html_writer::start_tag('li', array('class'=>'ashow'.(($item->visible > 0) ? ' hidden' : ''), 'onclick'=>'agendaShow('.$item->id.')'));
                    $html .= html_writer::tag('i', '', array('class'=>'fa fa-eye')) . get_string('show', 'local_agenda');
                    $html .= html_writer::end_tag('li');

                    // delete
                    $html .= html_writer::start_tag('li', array('class'=>'delete', 'onclick'=>'agendaDelete('.$item->id.')'));
                    $html .= html_writer::tag('i', '', array('class'=>'fa fa-trash')) . get_string('delete', 'local_agenda');
                    $html .= html_writer::end_tag('li');
                }
            $html .= html_writer::end_tag('ul');
        $html .= html_writer::end_tag('div');

        return $html;
    }

    public function print_agenda_comments($item, $data) {
        global $CFG, $DB, $USER, $OUTPUT;
        $html = '';

        $html .= html_writer::start_tag('div', array('class'=>'post-comments'));
            $html .= html_writer::start_tag('ul');

                if (isset($data['comments'][$item->id]) and count($data['comments'][$item->id]) > 0) {
                    foreach ($data['comments'][$item->id] as $comment) {
                        $html .= html_writer::start_tag('li', array('class'=>'comment-item', 'comment-id'=>$comment->id));
                            $userpicdata = (object)array('id'=>$comment->userid, 'firstname'=>$comment->firstname, 'lastname'=>$comment->lastname, 'picture'=>$comment->picture, 'imagealt'=>$comment->imagealt, 'firstnamephonetic'=>$comment->firstnamephonetic, 'lastnamephonetic'=>$comment->lastnamephonetic, 'middlename'=>$comment->middlename, 'alternatename'=>$comment->alternatename, 'email'=>$comment->email);

                            $html .= html_writer::tag('div', $OUTPUT->user_picture($userpicdata, array('size'=>30)), array('class'=>'comments-form-picture'));

                            $html .= html_writer::start_tag('div', array('class'=>'comment-body'));

                                $url = new moodle_url('/user/profile.php',
                                                array('id' => $comment->userid));
                                $html .= html_writer::link($url, fullname($comment));
                                $html .= $comment->text;

                                $html .= html_writer::start_tag('span', array('class'=>'comment-actions'));
                                    if ($data['canedit'] or $item->userid == $USER->id) {
                                        $html .= html_writer::link('javascript:void(0);', html_writer::tag('i', '', array('class'=>'fa fa-trash')).get_string('delete', 'local_agenda'), array('onclick'=>'agendaDeleteComment('.$comment->id.', '.$item->id.')'));
                                    }
                                    $html .= html_writer::tag('span', userdate($comment->timecreated, '%B %d, %Y %I:%M %p'), array('class'=>'comment-date'));
                                $html .= html_writer::end_tag('span');

                            $html .= html_writer::end_tag('div');
                        $html .= html_writer::end_tag('li');
                    }
                }

                // comments form
                $html .= html_writer::start_tag('li', array('class'=>'comments-form'));
                    $html .= html_writer::start_tag('div', array('class'=>'comments-form-picture'));
                        $html .= $OUTPUT->user_picture($USER, array('size'=>30));
                    $html .= html_writer::end_tag('div');
                    $html .= html_writer::start_tag('div', array('class'=>'comments-form-textarea'));
                        $html .= html_writer::empty_tag('input', array('data-id'=>$item->id, 'type'=>'text', 'name'=>'comment_text', 'placeholder'=>get_string('typecomment', 'local_agenda')));
                        $html .= html_writer::tag('span', html_writer::tag('i', '', array('class'=>'fa fa-paper-plane')), array('class'=>'send', 'onclick'=>'agendaSendComment('.$item->id.')'));
                    $html .= html_writer::end_tag('div');
                $html .= html_writer::end_tag('li');

            $html .= html_writer::end_tag('ul');
        $html .= html_writer::end_tag('div');

        return $html;
    }

    public function print_course_select($item, $data) {
        global $CFG, $DB, $USER, $OUTPUT;
        $html = '';

        $html .= html_writer::start_tag('div', array('class'=>'post-acourses'));
            $html .= html_writer::start_tag('select', array('data-itemid' => $item->id, "name" => "courses", "class" => "acourses", "onchange"=>"agendaCopyPost(".$item->id.", this)"));
            $html .= html_writer::tag('option', get_string('selectcourse', 'local_agenda'), array('value' => 0));
            foreach ($data['courses_options'] as $cid => $cname) {
                $html .= html_writer::tag('option', $cname, array('value' => $cid));
            }
            $html .= html_writer::end_tag('select');
        $html .= html_writer::end_tag('div');

        return $html;
    }

    public function print_agenda_icons($item, $data) {
        global $CFG, $DB;
        $html = '';

        $html .= html_writer::start_tag('ul', array('class'=>'agenda-small-icons'));

        if (!empty($item->post)) {
            $html .= html_writer::start_tag('li');
            $html .= html_writer::tag('i', '', array('class'=>$this->agenda_icons['post']));
            $html .= html_writer::end_tag('li');
        }

        if (!empty($item->question)) {
            $html .= html_writer::start_tag('li');
            $html .= html_writer::tag('i', '', array('class'=>$this->agenda_icons['question']));
            $html .= html_writer::end_tag('li');
        }

        if (!empty($item->link)) {
            $html .= html_writer::start_tag('li');
            $html .= html_writer::tag('i', '', array('class'=>$this->agenda_icons['link']));
            $html .= html_writer::end_tag('li');
        }

        if (isset($data['files'][$item->id]) and count($data['files'][$item->id]) > 0) {
            $html .= html_writer::start_tag('li');
            $html .= html_writer::tag('i', '', array('class'=>$this->agenda_icons['files']));
            $html .= html_writer::end_tag('li');
        }

        if (!empty($item->poll)) {
            $html .= html_writer::start_tag('li');
            $html .= html_writer::tag('i', '', array('class'=>$this->agenda_icons['poll']));
            $html .= html_writer::end_tag('li');
        }


        $html .= html_writer::end_tag('ul');

        return $html;
    }

    public function print_upcoming_assignments($courseid) {
        global $CFG, $DB;
        require_once($CFG->dirroot.'/local/agenda/locallib.php');
        $html = '';

        $upcoming_assignments = local_agenda_get_upcoming_assignments($courseid);
        if (!$upcoming_assignments or !count($upcoming_assignments)){
            return $html;
        }

        $html .= html_writer::start_tag('div', array('class' => 'item-box upcoming-assignments'));
        $html .= html_writer::tag('span', get_string('upcoming_assignments', 'local_agenda'), array('class' => 'title'));

        $html .= html_writer::start_tag('div', array('class' => 'item-inner'));
            $html .= html_writer::start_tag('ul', array('class' => 'items-list'));

                foreach ($upcoming_assignments as $uassign) {
                    $lefttime = round(($uassign->duedate-time())/60);
                    if ($lefttime > 10080) {
                        $lefttime = round($lefttime/10080);
                        $lefttime = ($lefttime > 1) ? $lefttime.' '.get_string('weeks', 'local_agenda') : $lefttime.' '.get_string('week', 'local_agenda');
                    } elseif ($lefttime > 1440) {
                        $lefttime = round($lefttime/1440);
                        $lefttime = ($lefttime > 1) ? $lefttime.' '.get_string('days', 'local_agenda') : $lefttime.' '.get_string('day', 'local_agenda');
                    } elseif ($lefttime > 60) {
                        $lefttime = round($lefttime/60);
                        $lefttime = ($lefttime > 1) ? $lefttime.' '.get_string('hours', 'local_agenda') : $lefttime.' '.get_string('hour', 'local_agenda');
                    } else {
                        $lefttime = ($lefttime > 1) ? $lefttime.' '.get_string('minutes', 'local_agenda') : $lefttime.' '.get_string('minutes', 'local_agenda');
                    }

                    $html .= html_writer::start_tag('li');
                        $url = new moodle_url('/mod/assign/view.php',
                                                array('id' => $uassign->cmid));

                        $html .= html_writer::link($url, $uassign->name.html_writer::tag('span', 'in '.$lefttime),
                                                   array('class' => $class,
                                                            'data-toggle' => 'tab',
                                                            'role' => 'tab'
                                                        ));
                    $html .= html_writer::end_tag('li');
                }

            $html .= html_writer::end_tag('ul');
        $html .= html_writer::end_tag('div');

        $html .= html_writer::end_tag('div');

        return $html;
    }
}
