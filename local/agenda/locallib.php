<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Renderer for outputting the topics course format.
 *
 * @package local_agenda
 * @copyright 2017 ISF
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


function local_agenda_get_upcoming_assignments($courseid = 1) {
    global $CFG, $DB, $USER;


    return $DB->get_records_sql("SELECT a.*, cm.id as cmid FROM {assign} a
                              LEFT JOIN {modules} m ON m.name = 'assign'
                              LEFT JOIN {course_modules} cm ON cm.instance = a.id AND cm.module = m.id AND cm.course = :courseid1
                                  WHERE a.duedate >= :now AND a.course = :courseid2 ORDER BY a.duedate LIMIT 5", array('courseid1'=>$courseid, 'now'=>time(), 'courseid2' => $courseid));
}

function local_agenda_get_agenda_options($postid = 0){
    global $DB;
    $options = array();
    if ($postid > 0){
        $post_options = $DB->get_records('local_agenda_options', array('instanceid'=>$postid));
        if (count($post_options)){
            foreach($post_options as $option){
                if (!empty($option)){
                    $options[$option->id] = $option->name;
                }
            }
        }
    }
    return $options;
}

function local_agenda_get_agenda_posts($courseid = 0, $start = 0, $limit = 5){
    global $DB, $USER, $CFG;

    $course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
    $context = context_course::instance($course->id, MUST_EXIST);

    $can_edit = (has_capability('moodle/course:manageactivities', $context) and has_capability('local/agenda:post', $context));

    $agenda_posts_db = $DB->get_records_sql("
        SELECT ca.*, u.firstname, u.lastname, u.picture, u.imagealt, u.firstnamephonetic, u.lastnamephonetic, u.middlename, u.alternatename, u.email, cal.likescount, ccal.currentuserliked, com.commentscount FROM {local_agenda_posts} ca
            LEFT JOIN {user} u ON u.id = ca.userid
            LEFT JOIN (SELECT COUNT(id) as likescount, instanceid FROM {local_agenda_likes} GROUP BY instanceid) cal ON cal.instanceid = ca.id
            LEFT JOIN (SELECT userid as currentuserliked, instanceid FROM {local_agenda_likes} WHERE userid = :userid) ccal ON ccal.instanceid = ca.id
            LEFT JOIN (SELECT COUNT(id) as commentscount, instanceid FROM {local_agenda_comments} GROUP BY instanceid) com ON com.instanceid = ca.id
        WHERE ca.courseid = :courseid ".((!$can_edit) ? ' AND ca.timeopen <= '.time().' AND ca.visible > 0' : '')." AND u.deleted = 0 ORDER BY ca.timeopen DESC LIMIT $start, $limit",
        array('userid'=>$USER->id, 'courseid'=>$course->id));

    $agenda_posts_count = $DB->get_record_sql("
        SELECT COUNT(ca.id) as count FROM {local_agenda_posts} ca
            LEFT JOIN {user} u ON u.id = ca.userid
            LEFT JOIN (SELECT COUNT(id) as likescount, instanceid FROM {local_agenda_likes} GROUP BY instanceid) cal ON cal.instanceid = ca.id
            LEFT JOIN (SELECT userid as currentuserliked, instanceid FROM {local_agenda_likes} WHERE userid = :userid) ccal ON ccal.instanceid = ca.id
        WHERE ca.courseid = :courseid ".((!$can_edit) ? ' AND ca.timeopen <= '.time().' AND ca.visible > 0' : '')." AND u.deleted = 0 ORDER BY ca.timeopen DESC",
        array('userid'=>$USER->id, 'courseid'=>$course->id));

    $posts_ids = array();
    $agenda_posts = array();
    if (count($agenda_posts_db) > 0){
        foreach($agenda_posts_db as $ap){
            $agenda_posts[userdate($ap->timeopen, '%B %d, %Y')][$ap->id] = $ap;
            $posts_ids[] = $ap->id;
        }
    }

    $comments = local_agenda_get_agenda_comments($posts_ids);
    list($agendas_options, $my_polls, $polls_count) = local_agenda_get_agenda_polls($posts_ids);
    $files = local_agenda_get_agenda_files($context, $can_edit);
    $courses_options = local_agenda_get_available_courses($course->id);

    return array('agenda_posts'=>$agenda_posts,
                 'agenda_posts_count'=>$agenda_posts_count,
                 'posts_ids' => $posts_ids,
                 'comments' => $comments,
                 'agendas_options' => $agendas_options,
                 'my_polls' => $my_polls,
                 'polls_count' => $polls_count,
                 'files' => $files,
                 'context' => $context,
                 'courses_options' => $courses_options,
                 'canedit' => $can_edit);
}

function local_agenda_get_agenda_comments($posts_ids = array()) {
    global $DB, $USER, $CFG;

    $comments = array();

    if (!count($posts_ids)) {
        return $comments;
    }

    $comments_db = $DB->get_records_sql("SELECT com.*, u.firstname, u.lastname, u.picture, u.imagealt, u.firstnamephonetic, u.lastnamephonetic, u.middlename, u.alternatename, u.email FROM {local_agenda_comments} com LEFT JOIN {user} u ON u.id = com.userid WHERE com.instanceid IN (".implode(', ', $posts_ids).") AND u.deleted = 0 ORDER BY com.timecreated ASC");
    if (count($comments_db) > 0){
        foreach ($comments_db as $ci){
            $comments[$ci->instanceid][] = $ci;
        }
    }

    return $comments;
}

function local_agenda_get_agenda_polls($posts_ids = array()){
    global $DB, $USER, $CFG;

    $agendas_options = array(); $my_polls = array(); $polls_count = array();

    if (!count($posts_ids)){
        return array($agendas_options, $my_polls, $polls_count);
    }

    $options_db = $DB->get_records_sql("SELECT cao.*, oc.option_count, ac.agenda_count  FROM {local_agenda_options} cao
                                            LEFT JOIN (SELECT optionid, COUNT(id) as option_count FROM {local_agenda_poll_values} GROUP BY optionid) oc ON oc.optionid = cao.id
                                            LEFT JOIN (SELECT instanceid, COUNT(id) as agenda_count FROM {local_agenda_poll_values} GROUP BY instanceid) ac ON ac.instanceid = cao.instanceid
                                        WHERE cao.instanceid IN (".implode(', ', $posts_ids).") ORDER BY cao.id ASC");
    if (count($options_db) > 0){
        foreach ($options_db as $ao){
            $agendas_options[$ao->instanceid][] = $ao;
        }
    }
    $pollscount_db = $DB->get_records_sql("SELECT instanceid, COUNT(id) as agenda_count
                                                FROM {local_agenda_poll_values}
                                            WHERE instanceid IN (".implode(', ', $posts_ids).") GROUP BY instanceid");
    if (count($pollscount_db) > 0){
        foreach ($pollscount_db as $pc){
            $polls_count[$pc->instanceid] = $pc->agenda_count;
        }
    }

    $mypolls_db = $DB->get_records_sql("SELECT * FROM {local_agenda_poll_values} capv
                                            WHERE capv.instanceid IN (".implode(', ', $posts_ids).") AND capv.userid = $USER->id ORDER BY capv.instanceid ASC");
    if (count($mypolls_db) > 0){
        foreach ($mypolls_db as $pa){
            $my_polls[$pa->instanceid] = 1;
        }
    }

    return array($agendas_options, $my_polls, $polls_count);
}

function local_agenda_get_agenda_files($context) {
    global $DB, $USER, $CFG;

    $fs = get_file_storage(); $files = array();
    $afiles = $fs->get_area_files($context->id, 'local_agenda', 'files', false);
    foreach ($afiles as $file) {
        if ($file->get_filename() == '.') continue;
        $url = moodle_url::make_pluginfile_url($context->id, 'local_agenda', 'files', $file->get_itemid(), '/', $file->get_filename());

        $mime = (strlen($file->get_mimetype()) < 40) ? $file->get_mimetype() : get_string('file', 'local_agenda');
        $files[$file->get_itemid()][] = array('url'=>$url, 'filename'=>$file->get_filename(), 'mime'=>$mime);
    }

    return $files;
}

function local_agenda_get_formatted_text($text, $context, $itemid, $type = 'post') {
    global $CFG;

    require_once($CFG->libdir. '/filelib.php');
    if (!$text) {
        return '';
    }
    $options = array('overflowdiv' => false, 'noclean' => false, 'para' => false);

    $text = file_rewrite_pluginfile_urls($text, 'pluginfile.php', $context->id, 'local_agenda', $type.'file', $itemid);
    $text = format_text($text, 1, $options);

    return $text;
}

function local_announcemets_get_courses() {
    global $DB;
    $courses = array();

    if (has_capability('local/sb_announcements:canseeallcourses', context_system::instance())) {
        $courses = $DB->get_records_select_menu('course', 'id > 1', array(), 'fullname', 'id, fullname');
    } else {
        $mycourses = enrol_get_my_courses('id, fullname', 'fullname', 0);
        if (count($mycourses)){
            foreach ($mycourses as $course) {
                $courses[$course->id] = $course->fullname;
            }
        }
    }

    return $courses;
}

function local_agenda_get_available_courses($courseid = 0) {
    global $DB;
    $courses = array();

    if (has_capability('local/agenda:canseeallcourses', context_system::instance())) {
        $courses = $DB->get_records_select_menu('course', 'id > 1', array(), 'fullname', 'id, fullname');
        if ($courseid > 0 and isset($courses[$courseid])){
            unset($courses[$courseid]);
        }
    } else {
        $mycourses = enrol_get_my_courses('id, fullname', 'fullname', 0);
        if (count($mycourses)){
            foreach ($mycourses as $course) {
                if ($courseid > 0 and $course->id == $courseid) continue;
                $courses[$course->id] = $course->fullname;
            }
        }
    }

    return $courses;
}

function local_agenda_get_events($courseid = 0) {
    global $DB, $CFG;

    require_once($CFG->dirroot.'/calendar/lib.php');

    list($ecourses, $group, $user) = calendar_set_filters(array($course->id));
    $agenda_events = calendar_get_events(time()-31536000, time()+31536000, $user, $group, $course->id, true, true);
    $starttime = mktime (0, 0, 0, date("n"), date("j"), date("Y")); $endtime = $starttime+86399;
    $events_data = array(); $tevetns = array(); $events = array();
    foreach($agenda_events as $event){
        if ($event->eventtype == 'due') $event->timestart = strtotime(userdate($event->timestart));
        $events[date('Y-m-d', $event->timestart)] = $event;

        if ($event->timeduration > 0){
            if ($event->timeduration/86400 > 1){
                $days = round($event->timeduration/86400);
                for($i = 1; $i <= $days; $i++){
                    $event_period = clone($event);
                    $event_period->timestart = $event_period->timestart+(86400*$i);
                    $events[date('Y-m-d', $event_period->timestart)] = $event_period;
                }
            }
            $event_period = clone($event);
            $event_period->timestart = $event_period->timestart+$event_period->timeduration;
            $events[date('Y-m-d', $event_period->timestart)] = $event_period;
        }
    }

    foreach($events as $event){
        $events_data[] = array('id'=>$event->id, 'title'=>addslashes(trim($event->name)), 'start'=>date('Y-m-d', $event->timestart), 'allDay'=>true);
    }

    return $events_data;
}


