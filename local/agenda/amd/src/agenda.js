// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is an empty module, that is required before all other modules.
 * Because every module is returned from a request for any other module, this
 * forces the loading of all modules with a single request.
 *
 * This function also sets up the listeners for ajax requests so we can tell
 * if any requests are still in progress.
 *
 * @package    local_agenda
 * @module     local_agenda/agenda
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define(['jquery', 'core/ajax', 'core/str', 'core/config', 'core/log', 'theme_primaryisf/select2', 'theme_primaryisf/moment', 'theme_primaryisf/fullcalendar'], function($, ajax, str, config, log, select2, moment, calendar) {

    select2.init($);
    m = moment.init();
    calendar.init($, m);

    window.agendaHide = function(id){
        $('.items-list li[data-id="'+id+'"]').addClass('loading');
        $.get(config.wwwroot+"/local/agenda/ajax.php?action=agenda_hide&id="+id, function(data){
            $('.items-list li[data-id="'+id+'"]').addClass('notvisible');
            $('.items-list li[data-id="'+id+'"]').removeClass('loading');
            $('.items-list li[data-id="'+id+'"] .ahide').toggleClass('hidden');
            $('.items-list li[data-id="'+id+'"] .ashow').toggleClass('hidden');
        });
    }
    window.agendaShow = function(id){
        $('.items-list li[data-id="'+id+'"]').addClass('loading');
        $.get(config.wwwroot+"/local/agenda/ajax.php?action=agenda_show&id="+id, function(data){
            $('.items-list li[data-id="'+id+'"]').removeClass('notvisible');
            $('.items-list li[data-id="'+id+'"]').removeClass('loading');
            $('.items-list li[data-id="'+id+'"] .ahide').toggleClass('hidden');
            $('.items-list li[data-id="'+id+'"] .ashow').toggleClass('hidden');
        });
    }
    window.agendaToggleCopy = function(id){
        $('.items-list li[data-id="'+id+'"] .post-acourses').slideToggle('fast');
        agendaRightLeftPanel();
    }
    window.agendaDelete = function(id){
        str.get_string('deleteconfirmation', 'local_agenda').done(function(s) {
            if (confirm(s)){
                $('.items-list li[data-id="'+id+'"]').addClass('loading');
                $.get(config.wwwroot+"/local/agenda/ajax.php?action=agenda_delete&id="+id, function(data){
                    $('.items-list li[data-id="'+id+'"]').remove();
                });
                agendaRightLeftPanel();
            }
        });
    }
    window.agendaLike = function(id){
        $('.items-list li[data-id="'+id+'"]').addClass('loading');
        $.get(config.wwwroot+"/local/agenda/ajax.php?action=agenda_like&id="+id, function(result){
            if (result.type == 'like'){
                $('.items-list li[data-id="'+id+'"] .like').addClass('likes');
            } else {
                $('.items-list li[data-id="'+id+'"] .like').removeClass('likes');
            }
            if (result.count == '0'){
                $('.items-list li[data-id="'+id+'"] .like .count').text('');
            } else {
                $('.items-list li[data-id="'+id+'"] .like .count').text(' ('+result.count+')');
            }
            $('.items-list li[data-id="'+id+'"]').removeClass('loading');
        });
    }

    window.agendaCopyPost = function(id, obj){
        var courseid = $(obj).val();
        if (courseid != '0'){
            $('.items-list li[data-id="'+id+'"]').addClass('loading');
            $('.items-list li[data-id="'+id+'"] .post-acourses').append('<i class="fa fa-spin fa-spinner">');
            $.get(config.wwwroot+"/local/agenda/ajax.php?action=agenda_copy&id="+id+"&cid="+courseid, function(data){
                $('.items-list li[data-id="'+id+'"] .post-acourses .fa-spin').remove();
                $('.items-list li[data-id="'+id+'"] .post-acourses').append('<i class="fa fa-check-circle-o"></i>');
                $('.items-list li[data-id="'+id+'"]').removeClass('loading');
                setTimeout(function(){ $('.items-list li[data-id="'+id+'"] .fa-check-circle-o').remove(); }, 2000);
            });
        }
    }

    window.agendaSendComment = function(id){
        var text = $('.comments-form-textarea input[data-id="'+id+'"]').val();
        $('.comments-form-textarea input[data-id="'+id+'"]').val('');
        if (text.length > 0){
            $('.items-list li[data-id="'+id+'"]').addClass('loading');
            $.get(config.wwwroot+"/local/agenda/ajax.php?action=agenda_comment&id="+id+"&text="+text, function(result){
                $('.items-list li[data-id="'+id+'"] .post-comments .comments-form').before(result.html);
                var count = $('.items-list li[data-id="'+id+'"] .comment-item').length;
                if (count > 0){
                    $('.items-list li[data-id="'+id+'"] .comment .count').text(' ('+count+')');
                } else {
                    $('.items-list li[data-id="'+id+'"] .comment .count').text('');
                }
                $('.items-list li[data-id="'+id+'"]').removeClass('loading');
                agendaRightLeftPanel();
            });
        }
    }
    window.agendaDeleteComment = function(id, postid){
        str.get_string('deletecommentconfirmation', 'local_agenda').done(function(s) {
            if (confirm(s)){
                $.get(config.wwwroot+"/local/agenda/ajax.php?action=agenda_delete_comment&id="+postid+"&commentid="+id, function(result){
                    $('.comment-item[comment-id="'+id+'"]').remove();
                    if (result.count == '0'){
                        $('.items-list li[data-id="'+postid+'"] .comment .count').text('');
                    } else {
                        $('.items-list li[data-id="'+postid+'"] .comment .count').text(' ('+result.count+')');
                    }
                });
                agendaRightLeftPanel();
            }
        });
    }
    window.agendaToggleComments = function(id){
        $('.items-list li[data-id="'+id+'"] .post-comments').slideToggle('fast');
        agendaRightLeftPanel();
    }
    window.agendaLoadMore = function(start, postscount, courseid){
        $('.agenda-load-more-box').before('<span class="load-more-prt" load-id="'+start+'"><i class="fa fa-spin fa-spinner"></span>');
        $('.load-more-prt[load-id="'+start+'"]').load(config.wwwroot+'/local/agenda/ajax.php?action=agenda_load_more&courseid='+courseid+'&start='+(parseInt(start)+5), function(){
            $('.acourses').select2();
            agendaRightLeftPanel();
        });
        if (start+1 < postscount){
            $('.agenda-load-more').attr('onclick', 'agendaLoadMore('+(parseInt(start)+5)+', '+postscount+','+courseid+' );');
        } else {
            $('.agenda-load-more-box').hide();
        }
        agendaRightLeftPanel();
    }
    window.agendaRightLeftPanel = function(){
        if ($('.cf-agenda').height() > $('.agenda-rightpanel').height()){
            $('.agenda-rightpanel').css('min-height', $('.cf-agenda').height());
        }
    }
    window.agendaPollSave = function(agendaid, optionid, courseid){
        var box = $('.agenda-box-inner .items-list li[data-id="'+agendaid+'"] .post-poll-box');
        box.addClass('loading').html('<i class="fa fa-spin fa-spinner"></i>').load(config.wwwroot+'/local/agenda/ajax.php?action=agenda_poll_save&courseid='+courseid+'&id='+agendaid+'&optionid='+optionid, function(){
            box.removeClass('loading');
            box.removeClass('results');
        });
    }
    window.agendaPollShow = function(agendaid){
        var box = $('.agenda-box-inner .items-list li[data-id="'+agendaid+'"] .post-poll-box');
        box.addClass('loading').html('<i class="fa fa-spin fa-spinner"></i>').load(config.wwwroot+'/local/agenda/ajax.php?action=agenda_poll_show&id='+agendaid, function(){
            box.removeClass('loading');
            box.removeClass('results');
        });
    }

    var Agenda = {

        init: function(courseid) {
            $('.acourses').select2();
            agendaRightLeftPanel();

            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();
            var calendar = $("#course_calendar").fullCalendar({
                header: {
                    left: "title",
                    center: "",
                    right: "prev,next"
                },
                dayClick: function(date, jsEvent, view) {
                    $('.fc-day').removeClass('active');
                    $(this).addClass('active');
                    var day = $(this).attr('data-date');
                    $(".course-calendar-events").html("<div class='loading'><i class='fa fa-spin fa-spinner'></div>").load(config.wwwroot + "/local/agenda/ajax.php?action=get_day_events&courseid=" + courseid + "&date="+day);
                },
                firstDay: 0,
                editable: false,
                eventLimit: true,
                disableDragging: true,
                handleWindowResize: true,
                views: {
                    agenda: {
                        eventLimit: 1
                    }
                },
                events: config.wwwroot + "/local/agenda/ajax.php?action=get_events&courseid=" + courseid
            });
        },

        // edit
        edit: function(coursetype) {

            $('.agenda-buttons li').click(function(e){
                var type = $(this).attr('data-type');

                $('.agenda-buttons li').removeClass('active');
                $(this).addClass('active');

                $('#id_tab-general .collapsible').not('#id_tab-settings').addClass('collapsed');
                $('#id_tab-general .collapsible#id_'+type).removeClass('collapsed');
            });

            $('#agenda_add_more_options').click(function(event){
                event.preventDefault();
                var optionscount = parseInt($('input[name="options_count"]').val());

                $('#agenda_add_more_options').parent().parent().before('<div class="form-group row fitem"><div class="col-md-3"><span class="pull-xs-right text-nowrap"></span><label class="col-form-label d-inline " for="id_options_s_'+optionscount+'"></label></div><div class="col-md-9 form-inline felement" data-fieldtype="text"><input class="form-control" name="options[s_'+optionscount+']" id="id_options_s_'+optionscount+'" value="" size="" placeholder="Option" type="text"><div class="form-control-feedback" id="id_error_options_s_'+optionscount+'" style="display: none;"></div></div></div>');
                $('input[name="options_count"]').val(optionscount+1);
            });
        },

    };


    /**
    * @alias module:local_agenda/agenda
    */
    return Agenda;

});

