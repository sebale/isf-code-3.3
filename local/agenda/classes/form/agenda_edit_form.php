<?php

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/lib/formslib.php');
require_once($CFG->dirroot . '/local/agenda/lib.php');

class agenda_edit_form extends moodleform {

    /**
     * Define the field edit form
     */
    public function definition() {
        global $CFG, $DB, $USER;

        $mform = $this->_form;
        $id = $this->_customdata['id'];
        $courseid = $this->_customdata['courseid'];
        $agenda = $this->_customdata['agenda'];
        $post_options = $this->_customdata['post_options'];
        $editoroptions = $this->_customdata['editoroptions'];

        $mform->addElement('hidden', 'courseid');
        $mform->setDefault('courseid', $courseid);
        $mform->setType('courseid', PARAM_INT);

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);

        $mform->addElement('header', 'tab-general', get_string('general'));
        $mform->setExpanded('tab-general', true);
        
        $mform->addElement('text', 'title', get_string('title_placeholder', 'local_agenda'));
        $mform->addRule('title', get_string('required'), 'required', null, 'client');
        $mform->setType('title', PARAM_TEXT);

        $html = html_writer::start_tag('div', array('class' => 'agenda-buttons'));
        $html .= html_writer::start_tag('ul', array('class' => 'clearfix'));
        $html .= html_writer::tag('li',
                 html_writer::tag('i', '', array('class' => 'fa fa-comment-o')),
                 array('class' => 'tab-post active', 'title' => get_string('post', 'local_agenda'), 'data-type'=>'tab-post')
        );
        $html .= html_writer::tag('li',
            html_writer::tag('i', '', array('class' => 'fa fa-question-circle')),
            array('class' => 'tab-question', 'title' => get_string('question', 'local_agenda'), 'data-type'=>'tab-question')
        );
        $html .= html_writer::tag('li',
            html_writer::tag('i', '', array('class' => 'fa fa-link')),
            array('class' => 'tab-link', 'title' => get_string('link', 'local_agenda'), 'data-type'=>'tab-link')
        );
        $html .= html_writer::tag('li',
            html_writer::tag('i', '', array('class' => 'fa fa-file-text-o')),
            array('class' => 'tab-files', 'title' => get_string('files', 'local_agenda'), 'data-type'=>'tab-files')
        );
        $html .= html_writer::tag('li',
            html_writer::tag('i', '', array('class' => 'fa fa-align-left')),
            array('class' => 'tab-poll', 'title' => get_string('poll', 'local_agenda'), 'data-type'=>'tab-poll')
        );
        $html .= html_writer::end_tag('ul');
        $html .= html_writer::end_tag('div');
        $mform->addElement('html', $html);

        // type post
        $mform->addElement('html', '<div id="id_tab-post" class="collapsible clearfix">');
        $mform->addElement('html', '<div class="fcontainer">');
        $mform->addElement('editor', 'post_editor', get_string('post', 'local_agenda'), array('rows'=>'5'), $editoroptions, 'size="50"');
        $mform->setType('post_editor', PARAM_TEXT);
        $mform->addElement('html', '</div>');
        $mform->addElement('html', '</div>');

        // type question
        $mform->addElement('html', '<div id="id_tab-question" class="collapsible clearfix collapsed">');
        $mform->addElement('html', '<div class="fcontainer">');
        $mform->addElement('editor', 'question_editor', get_string('question', 'local_agenda'), array('rows'=>'5'), $editoroptions, 'size="50"');
        $mform->setType('question_editor', PARAM_TEXT);
        $mform->addElement('html', '</div>');
        $mform->addElement('html', '</div>');

        // type link
        $mform->addElement('html', '<div id="id_tab-link" class="collapsible clearfix collapsed">');
        $mform->addElement('html', '<div class="fcontainer">');
        $mform->addElement('text', 'link_title', get_string('link_title', 'local_agenda'));
        $mform->setType('link_title', PARAM_TEXT);

        $mform->addElement('text', 'link', get_string('link_url', 'local_agenda'));
        $mform->setType('link', PARAM_TEXT);

        $mform->addElement('textarea', 'link_info', get_string('link_description', 'local_agenda'));
        $mform->setType('link_info', PARAM_TEXT);
        $mform->addElement('html', '</div>');
        $mform->addElement('html', '</div>');

        // type files
        $mform->addElement('html', '<div id="id_tab-files" class="collapsible clearfix collapsed">');
        $mform->addElement('html', '<div class="fcontainer">');
        $mform->addElement('filemanager', 'files_filemanager', get_string('files', 'local_agenda'), null);
        $mform->addElement('html', '</div>');
        $mform->addElement('html', '</div>');


        // type poll
        $mform->addElement('html', '<div id="id_tab-poll" class="collapsible clearfix collapsed">');
        $mform->addElement('html', '<div class="fcontainer">');
        $mform->addElement('editor', 'poll_editor', get_string('poll', 'local_agenda'), array('rows'=>'5'), $editoroptions, 'size="50"');
        $mform->setType('poll_editor', PARAM_TEXT);

        $mform->addElement('hidden', 'options_count');
        $mform->setType('options_count', PARAM_INT);

        if (count($post_options) > 0){
            foreach ($post_options as $optionkey=>$option){
                $mform->addElement('text', 'options[u_'.$optionkey.']', '', array('placeholder'=>get_string('option', 'local_agenda')));
                $mform->setDefault('options[u_'.$optionkey.']', $option);
                $mform->setType('options[u_'.$optionkey.']', PARAM_RAW);
            }
            $mform->setDefault('options_count', count($post_options));
        } else {
            for($i = 0; $i < 3; $i++) {
                $mform->addElement('text', 'options[s_'.$i.']', '', array('placeholder' => get_string('option', 'local_agenda')));
                $mform->setType('options[s_'.$i.']', PARAM_RAW);
            }
            $mform->setDefault('options_count', 3);
        }

        $html = html_writer::start_tag('div', array("class"=>"form-group row fitem"));
            $html .= html_writer::start_tag('div', array("class"=>"col-md-3"));
                $html .= html_writer::tag('span', '', array("class"=>"pull-xs-right text-nowrap"));
                $html .= html_writer::tag('label', '', array("class"=>"col-form-label d-inline"));
            $html .= html_writer::end_tag('div');
            $html .= html_writer::start_tag('div', array("class"=>"col-md-9 form-inline felement"));
            $html .= html_writer::link('#', get_string('add_more_option', 'local_agenda'), array(
                'id' => 'agenda_add_more_options',
                'class' => 'btn btn-info'
            ));
            $html .= html_writer::end_tag('div');
        $html .= html_writer::end_tag('div');

        $mform->addElement('html', $html);

        $mform->addElement('html', '</div>');
        $mform->addElement('html', '</div>');

        // settings
        $mform->addElement('header', 'tab-settings', get_string('settings', 'local_agenda'));
        $mform->setExpanded('tab-settings', true);

        if (!isset($agenda->id)){
            $options = array(
                'multiple' => true,
                'noselectionstring' => get_string('selectcourse', 'local_agenda')
            );

            $select_courses = $mform->addElement('autocomplete', 'courses', get_string('courses', 'local_agenda'), local_agenda_get_available_courses($courseid), $options);
        }

        $mform->addElement('checkbox', 'allow_comments', get_string('allow_comments', 'local_agenda'));
        $mform->setDefault('allow_comments', 1);
        $mform->setType('allow_comments', PARAM_INT);

        $mform->addElement('checkbox', 'allow_likes', get_string('allow_likes', 'local_agenda'));
        $mform->setDefault('allow_likes', 1);
        $mform->setType('allow_likes', PARAM_INT);

        $mform->addElement('date_time_selector', 'timeopen', get_string('post_delay', 'local_agenda'), array('optional' => true));
        $mform->setDefault('timeopen', time());

        $this->set_data($agenda);
        $this->add_action_buttons();
    }

    public function validation($data, $files) {
        return parent::validation($data, $files);
    }
}
