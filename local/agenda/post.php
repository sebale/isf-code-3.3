<?php

require_once('../../config.php');
require_once('lib.php');
require_once('locallib.php');

require_once('classes/form/agenda_edit_form.php');

$courseid = required_param('courseid', PARAM_INT);
$id = optional_param('id', 0, PARAM_INT);

$course = $DB->get_record('course', array('id' => $courseid), '*', MUST_EXIST);
$context = context_course::instance($course->id, MUST_EXIST);

require_login();
require_capability('local/agenda:post', $context);

$editoroptions = array('maxfiles' => 1, 'maxbytes' => $CFG->maxbytes, 'trusttext' => false, 'noclean' => true, 'context' => $context, 'subdirs' => 0);

if ($id > 0){
    $post = $DB->get_record('local_agenda_posts', array('id'=>$id));
    $title = get_string('editing_agenda_post', 'local_agenda');

    $post->postformat = 1;
    $post = file_prepare_standard_editor($post, 'post', $editoroptions, $context, 'local_agenda', 'postfile', $post->id);
    $post->questionformat = 1;
    $post = file_prepare_standard_editor($post, 'question', $editoroptions, $context, 'local_agenda', 'questionfile', $post->id);
    $post->pollformat = 1;
    $post = file_prepare_standard_editor($post, 'poll', $editoroptions, $context, 'local_agenda', 'pollfile', $post->id);
    $post = file_prepare_standard_filemanager($post, 'files', $editoroptions, $context, 'local_agenda', 'files', $post->id);
} else {
    $post = new stdClass();
    $title = get_string('create_agenda_post', 'local_agenda');
}
$post_options = local_agenda_get_agenda_options((isset($post->id)) ? $post->id : 0);

$PAGE->set_url(new moodle_url('/local/agenda/post.php', array('courseid' => $courseid)));
$PAGE->set_pagelayout('course');
$PAGE->set_context($context);
$PAGE->set_course($course);
$PAGE->navbar->add($title);

$PAGE->set_title($title);
$PAGE->set_heading($title);

$editform = new agenda_edit_form($CFG->wwwroot.'/local/agenda/post.php', array(
    'courseid' => $courseid,
    'id' => $id,
    'agenda' => $post,
    'editoroptions' => $editoroptions,
    'post_options' => $post_options
));

if ($editform->is_cancelled()) {
    redirect(new moodle_url($CFG->wwwroot . '/course/view.php', array('id' => $course->id)));
} else if ($data = $editform->get_data()) {
    $data->userid = $USER->id;
    $data->allow_comments = isset($data->allow_comments) ? true : false;
    $data->allow_likes = isset($data->allow_likes) ? true : false;
    $data->timeopen = ($data->timeopen) ? $data->timeopen : time();

    if(!$data->id) {
        $data->timecreated = time();
        $data->visible = 1;
        $data->id = $DB->insert_record('local_agenda_posts', $data);
    } else {
        $DB->update_record('local_agenda_posts', $data);
    }

    // store or update files
    file_save_draft_area_files($data->files_filemanager, $context->id, 'local_agenda', 'files', $data->id, $editoroptions);

    $data = file_postupdate_standard_editor($data, 'post', $editoroptions, $context, 'local_agenda', 'postfile', $data->id);
    $data = file_postupdate_standard_editor($data, 'question', $editoroptions, $context, 'local_agenda', 'questionfile', $data->id);
    $data = file_postupdate_standard_editor($data, 'poll', $editoroptions, $context, 'local_agenda', 'pollfile', $data->id);

    $DB->update_record('local_agenda_posts', $data);

    $post_data = (object)$_POST;
    $agenda_options = local_agenda_get_agenda_options($data->id);
    if (count($post_data->options) > 0){
        foreach ($post_data->options as $optionkey=>$option){
            if (strpos($optionkey, 's_') !== false or strpos($optionkey, 'd_') !== false){
                if (!empty($option)){
                    $new_option = new stdClass();
                    $new_option->courseid = $course->id;
                    $new_option->instanceid = $data->id;
                    $new_option->name = $option;
                    $new_option->id = $DB->insert_record('local_agenda_options', $new_option);
                }
            } elseif (strpos($optionkey, 'u_') !== false){
                $optionid = str_replace('u_', '', $optionkey);
                if (empty($option) and isset($agenda_options[$optionid])){
                    $DB->delete_records('local_agenda_options', array('instanceid'=>$data->id, 'courseid'=>$course->id, 'id'=>$optionid));
                } elseif (!empty($option) and isset($agenda_options[$optionid])){
                    $new_option = $DB->get_record('local_agenda_options', array('instanceid'=>$data->id, 'courseid'=>$course->id, 'id'=>$optionid));
                    $new_option->name = $option;
                    $DB->update_record('local_agenda_options', $new_option);
                } elseif (!empty($option) and !isset($agenda_options[$optionid])){
                    $new_option = new stdClass();
                    $new_option->courseid = $course->id;
                    $new_option->instanceid = $data->id;
                    $new_option->name = $option;
                    $new_option->id = $DB->insert_record('local_agenda_options', $new_option);
                }
            }
        }
    }

    if (isset($post_data->courses) and count($post_data->courses) > 0){
        foreach($post_data->courses as $cid){
            unset($data->id);
            $data->courseid = $cid;
            $context = context_course::instance($cid, MUST_EXIST);
            $data->id = $DB->insert_record('local_agenda_posts', $data);

            $course_data = $editform->get_data();

            file_save_draft_area_files($course_data->files, $context->id, 'course', 'agenda', $data->id, array('subdirs' => 0, 'maxfiles' => 50));

            // store or update files
            file_save_draft_area_files($course_data->files_filemanager, $context->id, 'local_agenda', 'files', $data->id, $editoroptions);

            $data = file_postupdate_standard_editor($data, 'post', $editoroptions, $context, 'local_agenda', 'postfile', $data->id);
            $data = file_postupdate_standard_editor($data, 'question', $editoroptions, $context, 'local_agenda', 'questionfile', $data->id);
            $data = file_postupdate_standard_editor($data, 'poll', $editoroptions, $context, 'local_agenda', 'pollfile', $data->id);

            $DB->update_record('local_agenda_posts', $data);

            if (count($post_data->options) > 0){
                foreach ($post_data->options as $optionkey=>$option){
                    if (strpos($optionkey, 's_') !== false or strpos($optionkey, 'd_') !== false){
                        if (!empty($option)){
                            $new_option = new stdClass();
                            $new_option->courseid = $course->id;
                            $new_option->instanceid = $data->id;
                            $new_option->name = $option;
                            $new_option->id = $DB->insert_record('local_agenda_options', $new_option);
                        }
                    } elseif (strpos($optionkey, 'u_') !== false){
                        $optionid = str_replace('u_', '', $optionkey);
                        if (empty($option) and isset($agenda_options[$optionid])){
                            $DB->delete_records('local_agenda_options', array('instanceid'=>$data->id, 'courseid'=>$course->id, 'id'=>$optionid));
                        } elseif (!empty($option) and isset($agenda_options[$optionid])){
                            $new_option = $DB->get_record('local_agenda_options', array('instanceid'=>$data->id, 'courseid'=>$course->id, 'id'=>$optionid));
                            $new_option->name = $option;
                            $DB->update_record('local_agenda_options', $new_option);
                        } elseif (!empty($option) and !isset($agenda_options[$optionid])){
                            $new_option = new stdClass();
                            $new_option->courseid = $course->id;
                            $new_option->instanceid = $data->id;
                            $new_option->name = $option;
                            $new_option->id = $DB->insert_record('local_agenda_options', $new_option);
                        }
                    }
                }
            }

        }
    }

    redirect(new moodle_url('/course/view.php', array('id' => $courseid, 'tab' => 'agenda')));
}

$PAGE->requires->js_call_amd('local_agenda/agenda', 'edit', array());

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

echo $editform->display();

echo $OUTPUT->footer();
