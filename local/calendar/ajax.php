<?php
define('AJAX_SCRIPT', true);
require_once("../../config.php");
require_once("lib.php");

$id         = optional_param('id', 0, PARAM_INT);
$action     = optional_param('action', '', PARAM_RAW);
$form       = (object)optional_param_array('form', array(), PARAM_RAW);
$msg        = optional_param_array('msg', array(), PARAM_RAW);

$context = context_system::instance();
$PAGE->set_context($context);

?>

<?php if($action == 'loaddayevents') : ?>

<?php
    require_once($CFG->dirroot.'/calendar/lib.php');
    $date        = optional_param('date', '', PARAM_RAW);
    $output = ''; $events = array(); $visible_events = array(); $data = new stdClass();

    $filtercourses = array();
    $filtercourse = array();
    $courseshown = false;

    list($courses, $group, $user) = calendar_set_filters($filtercourse);
    $mcourses = enrol_get_my_courses();
    if (count($mcourses) > 0){
        $courses = array(1=>1);
        foreach($mcourses as $mc){
            $courses[$mc->id] = $mc->id;
            $filtercourses[$mc->id] = $mc->fullname;
        }
    }

    // course colors and settings
    $data->course_disables = array(); $data->course_colors = array();
    $courses_state = $DB->get_records('local_calendar_courses', array('userid'=>$USER->id));
    foreach ($courses_state as $cstate){
        if (!empty($cstate->color) and (strlen($cstate->color) == 3 or strlen($cstate->color) == 6)){
            $data->course_colors[$cstate->courseid] = $cstate->color;
        }
        if ($cstate->state == 0){
            $data->course_disables[$cstate->courseid] = $cstate->state;
        }
    }

    // subscriptions colors and settings
    $subscriptions = $DB->get_records_sql("
        SELECT s.*, cs.color, cs.state
          FROM {event_subscriptions} s
     LEFT JOIN {local_calendar_subscriptions} cs ON cs.subscriptionid = s.id AND cs.userid = s.userid
         WHERE s.userid = :userid", array('userid'=>$USER->id));

    $data->sub_disables = $DB->get_records_select_menu("local_calendar_subscriptions", "userid = :userid AND state = 0", array('userid'=>$USER->id), '', $fields='subscriptionid, state');

    $data->subscriptions = array(0=>'3a87ad');
    foreach ($subscriptions as $subscription){
        $data->subscriptions[$subscription->id] = $subscription;
    }

    $date = (!empty($date)) ? strtotime($date) : time();

    $utime = strtotime(userdate($date));
    if ($date < $utime){
        $datetrashold = $utime-$date;
    } elseif ($date > $utime){
        $datetrashold = $date-$utime;
    } else {
        $datetrashold = 0;
    }

    $starttime = $date-$datetrashold;
    $endtime = $date+86399+$datetrashold;

    $events_db = calendar_get_events($starttime, $endtime, $user, $group, $courses, true, true);
    $starttime = $date;
    $endtime = $date+86399;
    if (count($events_db) > 0){
        foreach ($events_db as $event){
            if ($event->eventtype == 'due') $event->timestart = strtotime(userdate($event->timestart));

            if (($event->timestart >= $starttime and $event->timestart <= $endtime) or
               ($event->timeduration > 0 and (($starttime >= $event->timestart and $starttime <= ($event->timestart+$event->timeduration)) or ($endtime >= $event->timestart and $endtime <= ($event->timestart+$event->timeduration))))){

                $color = ((isset($data->subscriptions[$event->subscriptionid]->color)) ? "#".$data->subscriptions[$event->subscriptionid]->color : (($event->courseid > 0 and isset($data->course_colors[$event->courseid])) ? "#".$data->course_colors[$event->courseid] : "#3a87ad"));
                $class = (($event->subscriptionid > 0) ? 'ev_'.$event->subscriptionid : 'ev_0').((isset($data->sub_disables[$event->subscriptionid]) or isset($data->course_disables[$event->courseid]) or ($event->subscriptionid == 0 and isset($data->sub_disables[0]))) ? ' hidden' : ' ').(($event->courseid > 0) ? ' ec_'.$event->courseid : (($event->subscriptionid > 0) ? '' : ' ec_0'));

                $event = calendar_add_event_metadata($event);
                $event->color = $color;
                $event->class = $class;
                $events[$event->id] = $event;
                if (!isset($data->sub_disables[$event->subscriptionid]) and !isset($data->course_disables[$event->courseid]) and !($event->subscriptionid == 0 and isset($data->sub_disables[0]))){
                    $visible_events[$event->id] = $event->id;
                }
            }
        }
    }
?>
<div class="title"><?php echo (date('Y-m-d', $date) == date('Y-m-d')) ? get_string('today', 'local_calendar').' ' : '' ?><?php echo date('F d, Y', $date); ?></div>
    <?php if (count($events) > 0) : ?>
        <ul class="events-list">
            <?php foreach ($events as $event) : ?>
                <li id="event_<?php echo $event->id; ?>" class="clearfix <?php echo $event->class; ?>" style="border-left:3px solid <?php echo $event->color; ?>; background-color:rgba(<?php echo (!empty($event->color)) ? local_calendar_hex2rgb($event->color, 'string') : '58, 135, 173'; ?>, 0.3);">
                    <div class="event-img"><?php echo $event->icon; ?></div>
                    <div class="event-body">
                        <div class="event-title"><?php echo (isset($event->referer)) ? $event->referer : strip_tags($event->name); ?></div>
                        <?php if ($event->courseid > 0 and isset($event->courselink)) : ?>
                            <div class="item-name"><i class="fa fa-folder-open-o"></i> <?php echo $event->courselink; ?></div>
                        <?php endif; ?>
                        <?php if ($event->timestart > 0 or $event->timeduration > 0) : ?>
                            <div class="event-time"><i class="fa fa-clock-o"></i> <?php echo ($event->eventtype == 'due') ? get_string('due', 'local_calendar').': ' : ''; ?>
                                <?php if ($event->timestart > 0) : ?>
                                    <?php if (date('j', $event->timestart) == date('j')){
                                            echo date('g:i a', $event->timestart);
                                        } else {
                                            echo date('m.j g:i a', $event->timestart);
                                        }
                                    ?>
                                <?php endif; ?>
                                <?php if ($event->timestart > 0 and $event->timeduration > 0) {
                                    echo ' - ';
                                } ?>
                                <?php if ($event->timeduration > 0) : ?>
                                    <?php if (date('j', $event->timestart+$event->timeduration) == date('j')){
                                            echo date('g:i a', $event->timestart+$event->timeduration);
                                        } else {
                                            echo date('m.j g:i a', $event->timestart+$event->timeduration);
                                        }
                                    ?>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                        <?php if (!empty($event->description)) : ?>
                            <div class="event-description">
                                <?php echo strip_tags($event->description); ?>
                                <?php if (strlen(strip_tags($event->description)) > 110) : ?>
                                    <i class="fa fa-chevron-up" title="<?php echo get_string('hide', 'local_calendar'); ?>"></i>
                                    <i class="fa fa-chevron-down" title="<?php echo get_string('showmore', 'local_calendar'); ?>"></i>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="event-actions">
                        <?php if ($event->userid == $USER->id or is_siteadmin()): ?>
                            <i class="fa fa-pencil" title="<?php echo get_string('edit'); ?>" onclick="calendarEventEdit(<?php echo $event->id; ?>);"></i>
                            <i class="fa fa-trash" title="<?php echo get_string('delete'); ?>" onclick="calendarEventDelete(<?php echo $event->id; ?>);"></i>
                        <?php endif; ?>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
        <?php if (count($visible_events) == 0) : ?>
            <div class="alert alert-success"><?php echo get_string('noevents', 'local_calendar'); ?></div>
        <?php endif; ?>
    <?php else : ?>
        <div class="alert alert-success"><?php echo get_string('noevents', 'local_calendar'); ?></div>
    <?php endif; ?>

<?php //load one selected event ?>
<?php elseif($action == 'loadevent') : ?>

    <?php
    require_once($CFG->dirroot.'/calendar/lib.php');

    $event = $DB->get_record('event', array('id'=>$id));
    if(! $event->id){die("Incorrect Event Id");}
    $event = calendar_add_event_metadata($event);
    if ($event->eventtype == 'due') $event->timestart = strtotime(userdate($event->timestart));
    $data = new stdCLass();

    // course colors and settings
    $data->course_disables = array(); $data->course_colors = array();
    $courses_state = $DB->get_records('local_calendar_courses', array('userid'=>$USER->id));
    foreach ($courses_state as $cstate){
        if (!empty($cstate->color) and (strlen($cstate->color) == 3 or strlen($cstate->color) == 6)){
            $data->course_colors[$cstate->courseid] = $cstate->color;
        }
        if ($cstate->state == 0){
            $data->course_disables[$cstate->courseid] = $cstate->state;
        }
    }

    // subscriptions colors and settings
    $subscriptions = $DB->get_records_sql("
        SELECT s.*, cs.color, cs.state
          FROM {event_subscriptions} s
     LEFT JOIN {local_calendar_subscriptions} cs ON cs.subscriptionid = s.id AND cs.userid = s.userid
         WHERE s.userid = :userid", array('userid'=>$USER->id));

    $data->sub_disables = $DB->get_records_select_menu("local_calendar_subscriptions", "userid = :userid AND state = 0", array('userid'=>$USER->id), '', $fields='id, state');
    $data->subscriptions = array(0=>'3a87ad');
    foreach ($subscriptions as $subscription){
        $data->subscriptions[$subscription->id] = $subscription;
    }

    $event->color = ((isset($data->subscriptions[$event->subscriptionid]->color)) ? "#".$data->subscriptions[$event->subscriptionid]->color : (($event->courseid > 0 and isset($data->course_colors[$event->courseid])) ? "#".$data->course_colors[$event->courseid] : "#3a87ad"));
    $event->class = (($event->subscriptionid > 0) ? 'ev_'.$event->subscriptionid : 'ev_0').((isset($data->sub_disables[$event->subscriptionid]) or isset($data->course_disables[$event->courseid]) or ($event->subscriptionid == 0 and isset($data->sub_disables[0]))) ? ' hidden' : ' ').(($event->courseid > 0) ? ' ec_'.$event->courseid : (($event->subscriptionid > 0) ? '' : ' ec_0'));
?>
<div class="title"><?php echo (date('Y-m-d', $event->timestart) == date('Y-m-d')) ? 'Today ' : '' ?><?php echo date('F d, Y', $event->timestart); ?></div>
    <ul class="events-list">
        <li id="event_<?php echo $event->id; ?>" class="clearfix <?php echo $event->class; ?>" style="border-left:3px solid <?php echo $event->color; ?>; background-color:rgba(<?php echo (!empty($event->color)) ? local_calendar_hex2rgb($event->color, 'string') : '58, 135, 173'; ?>, 0.3);">
            <div class="event-img"><?php echo $event->icon; ?></div>
                <div class="event-body">
                <div class="event-title"><?php echo ((isset($event->referer)) ? $event->referer : strip_tags($event->name)); ?></div>
                <?php if ($event->timestart > 0 or $event->timeduration > 0) : ?>
                    <?php if ($event->courseid > 0 and isset($event->courselink)) : ?>
                        <div class="item-name"><i class="fa fa-folder-open-o"></i> <?php echo $event->courselink; ?></div>
                    <?php endif; ?>
                    <div class="event-time"><i class="fa fa-clock-o"></i> <?php echo ($event->eventtype == 'due') ? 'Due: ' : ''; ?>
                        <?php if ($event->timestart > 0) : ?>
                            <?php if (date('j', $event->timestart) == date('j')){
                                    echo date('g:i a', $event->timestart);
                                } else {
                                    echo date('m.j g:i a', $event->timestart);
                                }
                            ?>
                        <?php endif; ?>
                        <?php if ($event->timestart > 0 and $event->timeduration > 0) {
                            echo ' - ';
                        } ?>
                        <?php if ($event->timeduration > 0) : ?>
                            <?php if (date('j', $event->timestart+$event->timeduration) == date('j')){
                                    echo date('g:i a', $event->timestart+$event->timeduration);
                                } else {
                                    echo date('m.j g:i a', $event->timestart+$event->timeduration);
                                }
                            ?>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
                <?php if (!empty($event->description)) : ?>
                    <div class="event-description">
                        <?php echo strip_tags($event->description); ?>
                        <?php if (strlen(strip_tags($event->description)) > 110) : ?>
                            <i class="fa fa-chevron-up" title="Hide"></i>
                            <i class="fa fa-chevron-down" title="Show More"></i>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="event-actions">
                <?php if($event->userid == $USER->id or is_siteadmin()) : ?>
                    <i class="fa fa-pencil" title="<?php echo get_string('edit'); ?>" onclick="calendarEventEdit(<?php echo $event->id; ?>);"></i>
                    <i class="fa fa-trash" title="<?php echo get_string('edit'); ?>" onclick="calendarEventDelete(<?php echo $event->id; ?>);"></i>
                <?php endif; ?>
            </div>
        </li>
    </ul>

<?php elseif($action == 'get_event_form') : ?>

    <?php $event = $DB->get_record('event', array('id'=>$id)); ?>
    <?php if (isset($event->eventtype) and $event->eventtype == 'due') $event->timestart = strtotime(userdate($event->timestart)); ?>

    <?php $courses = $DB->get_records_sql("SELECT c.id, c.fullname, cx.id as cxid, ra.roleid
                    FROM {user_enrolments} ue
                    LEFT JOIN {enrol} e ON e.id = ue.enrolid
                    LEFT JOIN {course} c ON c.id = e.courseid
                    LEFT JOIN {context} cx ON cx.instanceid = c.id AND cx.contextlevel = 50
                    LEFT JOIN {role_assignments} ra ON ra.contextid = cx.id AND ra.userid = ue.userid
                        WHERE c.category > 0 AND ue.userid = ".$USER->id." AND ra.roleid = 3 GROUP BY c.id");
    ?>
<div class="title">
    <?php if (isset($event->id)) {
        echo get_string('updateevent', 'local_calendar');
    } else {
        echo get_string('createevent', 'local_calendar');
    } ?>
</div>
<div class="calendar-form-body">
    <form id="cal-form" method="POST" action="<?php echo $CFG->wwwroot; ?>/local/calendar/ajax.php">
        <div class="field clearfix"><label for="name"><?php echo get_string('name', 'local_calendar'); ?>:</label>
        <div class="field-inner">
        <input name="form[name]" type="text" value="<?php echo strip_tags($event->name); ?>" class="form-control" />
        </div></div>
        <?php if (count($courses) > 0) : ?>
            <div class="field <?php echo (isset($event->eventtype) and $event->eventtype == 'due') ? 'hidden' : '' ;?> clearfix"><label for="access"><?php echo get_string('eventtype', 'local_calendar'); ?>:</label>
                <div class="field-inner">
                <select name="form[eventtype]" id="event_type" class="form-control">
                    <option value="private" <?php echo (isset($event->eventtype) and $event->eventtype == 'user') ? 'selected' : '' ?> ><?php echo get_string('privateevent', 'local_calendar'); ?></option>
                    <option value="course" <?php echo (isset($event->eventtype) and $event->eventtype == 'course') ? 'selected' : '' ?> ><?php echo get_string('courseevent', 'local_calendar'); ?></option>
                    <?php if (isset($event->eventtype) and $event->eventtype == 'due') : ?>
                      <option value="due" <?php echo (isset($event->eventtype) and $event->eventtype == 'due') ? 'selected' : '' ?> ><?php echo get_string('duedateevent', 'local_calendar'); ?></option>
                    <?php endif; ?>
                </select>
                </div>
            </div>
            <div class="eventform-coursetype <?php echo (isset($event->eventtype) and $event->eventtype == 'course') ? '' : 'hidden' ?>">
                <div class="field clearfix"><label for="visible"><?php echo get_string('visible', 'local_calendar'); ?>:</label>
                    <div class="field-inner">
                        <select name="form[visible]" class="form-control">
                            <option value="1" <?php echo (isset($event->visible) and $event->visible == 1) ? 'selected' : '' ?> ><?php echo get_string('visible', 'local_calendar'); ?></option>
                            <option value="0" <?php echo (isset($event->visible) and $event->visible == 0) ? 'selected' : '' ?> ><?php echo get_string('hidden', 'local_calendar'); ?></option>
                        </select>
                    </div>
                </div>
                <?php if (isset($event->id)) : ?>
                    <div class="field clearfix"><label for="access"><?php echo get_string('courses', 'local_calendar'); ?>:</label>
                        <div class="field-inner">
                        <select name="form[courseid]" id="event_courseid" class="form-control">
                            <?php if (count($courses) > 0) : ?>
                                <option><?php echo get_string('selectcourse', 'local_calendar'); ?></option>
                                <?php foreach ($courses as $course) : ?>
                                    <option value="<?php echo $course->id; ?>" <?php echo (isset($event->courseid) and $event->courseid == $course->id) ? 'selected' : '' ?> ><?php echo $course->fullname; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                        </div>
                    </div>
                <?php else : ?>
                    <div class="field clearfix"><label for="access"><?php echo get_string('courses', 'local_calendar'); ?>:</label>
                        <div class="field-inner">
                        <select name="courses[]" class="form-control" id="event_courses"  multiple="multiple">
                            <?php if (count($courses) > 0) : ?>
                                <?php foreach ($courses as $course) : ?>
                                    <option value="<?php echo $course->id; ?>" <?php echo (isset($event->courseid) and $event->courseid == $course->id) ? 'selected' : '' ?> ><?php echo $course->fullname; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </select>
                        </div>
                    </div>
                <?php endif;  ?>
            </div>
        <?php else: ?>
            <input name="form[visible]" type="hidden" value="1" />
        <?php endif; ?>

        <div class="field clearfix">
            <label for="description"><?php echo get_string('description', 'local_calendar'); ?>:</label>
            <div class="field-inner">
                <textarea id="description" class="form-control" name="form[description]"><?php echo $event->description; ?></textarea>
            </div>
        </div>
        <div class="field field-timestart clearfix">
            <label for="timestart"><?php echo (isset($event->eventtype) and $event->eventtype == 'due') ? get_string('duedate', 'local_calendar') : get_string('startson', 'local_calendar'); ?>:</label>
            <div class="field-inner">
                <div class="input-append datetimepicker"><input data-format="MM/dd/yyyy HH:mm PP" type="text" class="date-time dateTime_selector form-control" name="form[timestart]" value="<?php echo ($event->timestart) ? date('m/d/Y h:i A', $event->timestart) : ''; ?>" ></div>
            </div>
        </div>
        <div class="field field-timeduration <?php echo (isset($event->eventtype) and $event->eventtype == 'due') ? 'hidden' : '' ;?> clearfix">
            <label for="timeduration"><?php echo get_string('endson', 'local_calendar'); ?>:</label>
            <div class="field-inner">
                <div class="input-append datetimepicker"><input data-format="MM/dd/yyyy HH:mm PP" type="text" class="date-time dateTime_selector form-control" name="form[timeduration]" value="<?php echo ($event->timeduration) ? date('m/d/Y h:i A', $event->timestart + $event->timeduration) : ''; ?>"></div>
            </div>
        </div>

        <input name="form[id]" type="hidden" value="<?php echo $event->id; ?>" />
        <input name="action" type="hidden" value="save_event" />
        <div class="actions">
            <button class='cancel btn btn-warning'><?php echo get_string('cancel', 'local_calendar'); ?></button>
            <button class='save btn btn-success'><?php echo get_string('save', 'local_calendar'); ?></button>
        </div>
    </form>

</div>

<?php elseif($action == 'load_colorchooser'): ?>

    <?php
        $type     = optional_param('type', 'coursecolor', PARAM_RAW);
        if ($type == 'coursecolor') {
            $color_state = $DB->get_record('local_calendar_courses', array('courseid'=>$id, 'userid'=>$USER->id));
        } else {
            $color_state = $DB->get_record('local_calendar_subscriptions', array('subscriptionid'=>$id, 'userid'=>$USER->id));
        }

    ?>
    <div class="title"><?php echo get_string('selectcolor', 'local_calendar');?><span class="chose-color-close" onclick="calendarCloseColorchooser();"><i class="fa fa-times"></i></span></div>
    <ul class="clearfix">
        <li style="background-color:#3a87ad; border-color:#2a769b;" data-color="3a87ad" onclick="calendarSetColorColorchooser(<?php echo $id; ?>, '3a87ad', '<?php echo $type; ?>');" <?php echo ((isset($color_state->color) and $color_state->color == '3a87ad') or !isset($color_state->color)) ? 'class="active"' : ''?>><i class="fa fa-check"></i></li>
        <li style="background-color:#AAE5F3; border-color:#81B4B8;" data-color="AAE5F3" onclick="calendarSetColorColorchooser(<?php echo $id; ?>, 'AAE5F3', '<?php echo $type; ?>');" <?php echo (isset($color_state->color) and $color_state->color == 'AAE5F3') ? 'class="active"' : ''?>><i class="fa fa-check"></i></li>
        <li style="background-color:#A8F2BF; border-color:#88C59C;" data-color="A8F2BF" onclick="calendarSetColorColorchooser(<?php echo $id; ?>, 'A8F2BF', '<?php echo $type; ?>');" <?php echo (isset($color_state->color) and $color_state->color == 'A8F2BF') ? 'class="active"' : ''?>><i class="fa fa-check"></i></li>
        <li style="background-color:#EAF2A9; border-color:#BAC287;" data-color="EAF2A9" onclick="calendarSetColorColorchooser(<?php echo $id; ?>, 'EAF2A9', '<?php echo $type; ?>');" <?php echo (isset($color_state->color) and $color_state->color == 'EAF2A9') ? 'class="active"' : ''?>><i class="fa fa-check"></i></li>
        <li style="background-color:#F2CCA7; border-color:#C4A286;" data-color="F2CCA7" onclick="calendarSetColorColorchooser(<?php echo $id; ?>, 'F2CCA7', '<?php echo $type; ?>');" <?php echo (isset($color_state->color) and $color_state->color == 'F2CCA7') ? 'class="active"' : ''?>><i class="fa fa-check"></i></li>
        <li style="background-color:#F3A9AA; border-color:#BF8182;" data-color="F3A9AA" onclick="calendarSetColorColorchooser(<?php echo $id; ?>, 'F3A9AA', '<?php echo $type; ?>');" <?php echo (isset($color_state->color) and $color_state->color == 'F3A9AA') ? 'class="active"' : ''?>><i class="fa fa-check"></i></li>
        <li style="background-color:#AFA7F0; border-color:#9891D4;" data-color="AFA7F0" onclick="calendarSetColorColorchooser(<?php echo $id; ?>, 'AFA7F0', '<?php echo $type; ?>');" <?php echo (isset($color_state->color) and $color_state->color == 'AFA7F0') ? 'class="active"' : ''?>><i class="fa fa-check"></i></li>
        <li style="background-color:#DCA8F2; border-color:#B282BE;" data-color="DCA8F2" onclick="calendarSetColorColorchooser(<?php echo $id; ?>, 'DCA8F2', '<?php echo $type; ?>');" <?php echo (isset($color_state->color) and $color_state->color == 'DCA8F2') ? 'class="active"' : ''?>><i class="fa fa-check"></i></li>
        <li style="background-color:#cecece; border-color:#9b9b9b;" data-color="cecece" onclick="calendarSetColorColorchooser(<?php echo $id; ?>, 'cecece', '<?php echo $type; ?>');" <?php echo (isset($color_state->color) and $color_state->color == 'cecece') ? 'class="active"' : ''?>><i class="fa fa-check"></i></li>
        <li style="background-color:#f0cd62; border-color:#dbb33a;" data-color="f0cd62" onclick="calendarSetColorColorchooser(<?php echo $id; ?>, 'f0cd62', '<?php echo $type; ?>');" <?php echo (isset($color_state->color) and $color_state->color == 'f0cd62') ? 'class="active"' : ''?>><i class="fa fa-check"></i></li>
    </ul>
    <div class="color-input clearfix" data-id="<?php echo $id; ?>">
        <span <?php echo (isset($color_state->color) and !empty($color_state->color)) ? 'style="background-color:#'.$color_state->color.'; border-color:#'.$color_state->color.'"' : ''?>></span>
        <i class="cplh">#</i><input id="color_input" type="text" value="<?php echo (isset($color_state->color) and !empty($color_state->color)) ? $color_state->color : ''?>" name="color_value">
        <button class="btn btn-success"><?php echo get_string('apply', 'local_calendar'); ?></button>
    </div>
    <script>
        jQuery('#color_input').keyup( function () {
            var color = jQuery(this).val().toLowerCase();
            if (color.indexOf("#") >= 0){
                color = color.replace("#", "");
                jQuery(this).val(color);
            }
            if (color.length == 3 || color.length == 6) {
                jQuery(".color-input span").css('background-color', '#'+color);
                jQuery(".color-input span").css('border-color', '#'+color);
                jQuery('.color-input button').removeAttr('disabled');
            } else {
                jQuery('.color-input button').attr('disabled', 'disabled');
                jQuery(".color-input span").css('background-color', '#fff');
                jQuery(".color-input span").css('border-color', '#ccc');
            }
        });
        jQuery('.color-input button').click( function () {
            var color = jQuery('#color_input').val().toLowerCase();
            calendarSetColorColorchooser(<?php echo $id; ?>, color, '<?php echo $type; ?>');
        });
    </script>

<?php endif; ?>

<?php

if($action == 'deleteevent' and $id){
    $event = $DB->get_record('event', array('id'=>$id));
    if($event->userid == $USER->id){
        $DB->delete_records('event', array('id'=>$id));
        echo 1;
    }

} elseif ($action == 'save_event' and $form->name){

    $courses = (object)optional_param_array('courses', array(), PARAM_RAW);

    $msg = 'Error with creating event';
    $errorType = 'warning';
    $result = false;
    $returnId = 0;
    $multiEvents = 0;
    $returnDay = date('Y-m-d');
    $returnEvent = array();
    $returnEvents = array();

    if(!$form->timestart){
        die(json_encode(array()));
    }

    $start = $form->timestart;
    $end = $form->timeduration;
    $timestart = strtotime($form->timestart);

    if (isset($form->eventtype) and $form->eventtype == 'course' and ($form->courseid > 0 or count($courses))){
        $form->eventtype  = 'course';
    } elseif (isset($form->eventtype) and $form->eventtype == 'due'){
        $form->eventtype  = 'due';
        $utimestart = strtotime(userdate($timestart));
        if ($timestart < $utimestart){
            $timestart = $timestart - ($utimestart-$timestart);
        } elseif ($timestart > $utimestart){
            $timestart = $timestart + ($timestart-$utimestart);
        }
    } else {
        $form->eventtype  = 'user';
    }

    $form->name = strip_tags($form->name);
    $form->format  = 1;
    $form->userid  = $USER->id;
    $form->modulename  = 0;
    $form->timemodified  = time();
    $form->timestart  = $timestart;
    if($form->timeduration){
        $form->timeduration = strtotime($form->timeduration) - $form->timestart;
    }

    if (isset($form->courseid) or $form->eventtype != 'course'){
        $form->courseid = ($form->courseid > 0 ) ? $form->courseid : 0;
        if($form->id){
            $event = $DB->get_record('event', array('id'=>$form->id));
            if ($form->eventtype  == 'due'){
                $form->modulename = $event->modulename;
            }
            $DB->update_record('event', $form, 'id');

            if ($event->eventtype  == 'due'){
                $cm = get_coursemodule_from_instance($event->modulename, $event->instance, $event->courseid);
                if ($cm->completionexpected > 0 and $cm->completionexpected != $form->timestart){
                    $cm->completionexpected = $form->timestart;
                    $DB->update_record('course_modules', $cm);

                    // Reset completion
                    require_once($CFG->libdir . '/completionlib.php');
                    $course = $DB->get_record('course', array('id'=>$event->courseid));
                    $completion = new completion_info($course);
                    $completion->reset_all_state($cm);

                    // Trigger event based on the action we did.
                    $instance = $DB->get_record($event->modulename, array('id' => $event->instance));
                    $modcontext = context_module::instance($cm->id);
                    $e = \core\event\course_module_updated::create(array(
                        'courseid' => $event->courseid,
                        'context'  => $modcontext,
                        'objectid' => $cm->id,
                        'other'    => array(
                            'modulename' => $event->modulename,
                            'name'       => $instance->name,
                            'instanceid' => $event->instance
                        )
                    ));
                    $e->trigger();
                }
                if ($form->modulename == 'assign'){
                    $assign = $DB->get_record($event->modulename, array('id' => $event->instance));
                    if ($assign->duedate != $form->timestart and $assign->duedate > 0){
                        $assign->duedate = $form->timestart;
                        $DB->update_record($event->modulename, $assign);
                    }
                }
            }

            $msg = get_string('eventupdated', 'local_calendar');
        }else{
            $form->id = $DB->insert_record('event', $form);
            $msg = get_string('eventcreated', 'local_calendar');
        }

        $color = '#3a87ad';
        if ($form->courseid > 0){
            $course_state = $DB->get_record('local_calendar_courses', array('userid'=>$USER->id, 'courseid'=>$form->courseid));
            if (isset($course_state->color) and !empty($course_state->color) and (strlen($course_state->color) == 3 or strlen($course_state->color) == 6)){
                $color = '#'.str_replace('#', '', $course_state->color);
            }
        }
        $result = true;
        $errorType = 'success';
        $returnId = $form->id;
        $returnDay = date('Y-m-d', $form->timestart);
        $returnMulti = $multiEvents;
        $returnEvent['id'] = $form->id;
        $returnEvent['name'] = trim(addslashes(strip_tags($form->name)));
        $returnEvent['start'] = $start;
        $returnEvent['end'] = $end;
        $returnEvent['allDay'] = ($end > 0) ? false : true;
        $returnEvent['color'] = $color;
        $returnEvent['class'] = "ev_0 ".(($form->courseid > 0) ? ' ec_'.$form->courseid : ' ec_0')." eid_".$returnId;

        echo json_encode(array('result'=>$result, 'msg'=>$msg, 'errorType'=>$errorType, 'returnId'=>$returnId, 'returnMulti'=>$returnMulti, 'returnDay'=>$returnDay, 'event'=>$returnEvent));


    } elseif($form->eventtype == 'course' and count($courses)) {
        if (count($courses)){
            foreach ($courses as $courseid){
                $form->courseid = $courseid;
                $form->id = $DB->insert_record('event', $form);

                $color = '#3a87ad';
                $course_state = $DB->get_record('local_calendar_courses', array('userid'=>$USER->id, 'courseid'=>$form->courseid));
                if (isset($course_state->color) and !empty($course_state->color) and (strlen($course_state->color) == 3 or strlen($course_state->color) == 6)){
                    $color = '#'.str_replace('#', '', $course_state->color);
                }

                $returnEvent['id'] = $form->id;
                $returnEvent['name'] = trim(addslashes(strip_tags($form->name)));
                $returnEvent['start'] = $start;
                $returnEvent['end'] = $end;
                $returnEvent['allDay'] = ($end > 0) ? false : true;
                $returnEvent['color'] = $color;
                $returnEvent['class'] = "ev_0 ".(($form->courseid > 0) ? ' ec_'.$form->courseid : ' ec_0')." eid_".$returnId;
                $returnEvents[] = $returnEvent;
            }
            if (count($returnEvents)){
                $msg = get_string('eventcreated', 'local_calendar');
                $multiEvents = count($returnEvents);
                $errorType = 'success';
            }
        }

        $result = true;
        $returnId = 0;
        $returnDay = date('Y-m-d', $form->timestart);
        $returnMulti = $multiEvents;

        echo json_encode(array('result'=>$result, 'msg'=>$msg, 'errorType'=>$errorType, 'returnId'=>$returnId, 'returnMulti'=>$returnMulti, 'returnDay'=>$returnDay, 'events'=>$returnEvents));
    }
} elseif($action == 'update_calendar_subscription') {

    $state        = optional_param('state', 1, PARAM_INT);
    $sub_item = $DB->get_record('local_calendar_subscriptions', array('subscriptionid'=>$id, 'userid'=>$USER->id));
    $new_setting = new stdClass();
    $new_setting->userid = $USER->id;
    $new_setting->subscriptionid = $id;
    $new_setting->state = $state;
    if ($sub_item->id){
        $sub_item->state = $new_setting->state;
        $sub_item = $DB->update_record('local_calendar_subscriptions', $sub_item);
    } else {
        $sub_item = $DB->insert_record('local_calendar_subscriptions', $new_setting);
    }
    echo json_encode(array('state'=>1));

} elseif($action == 'set_color') {

    $color        = optional_param('color', '', PARAM_RAW);
    $type         = optional_param('type', 'coursecolor', PARAM_RAW);

    if ($type == 'coursecolor') {
        $courses_state = $DB->get_record('local_calendar_courses', array('courseid'=>$id, 'userid'=>$USER->id));
        if (isset($courses_state->id)){
            $new_setting = $courses_state;
            $new_setting->color = $color;
            $new_setting->id = $DB->update_record('local_calendar_courses', $new_setting);
        } else {
            $new_setting = new stdClass();
            $new_setting->userid = $USER->id;
            $new_setting->courseid = $id;
            $new_setting->state = 1;
            $new_setting->color = $color;
            $new_setting->id = $DB->insert_record('local_calendar_courses', $new_setting);
        }
    } else {
        $sub_state = $DB->get_record('local_calendar_subscriptions', array('subscriptionid'=>$id, 'userid'=>$USER->id));
        if (isset($sub_state->id)){
            $new_setting = $sub_state;
            $new_setting->color = $color;
            $new_setting->id = $DB->update_record('local_calendar_subscriptions', $new_setting);
        } else {
            $new_setting = new stdClass();
            $new_setting->userid = $USER->id;
            $new_setting->subscriptionid = $id;
            $new_setting->state = 1;
            $new_setting->color = $color;
            $new_setting->id = $DB->insert_record('local_calendar_subscriptions', $new_setting);
        }
    }


    echo json_encode(array('state'=>1));

} elseif($action == 'update_calendar_course') {

    $state        = optional_param('state', 1, PARAM_INT);
    $sub_item = $DB->get_record('local_calendar_courses', array('courseid'=>$id, 'userid'=>$USER->id));
    $new_setting = new stdClass();
    $new_setting->userid = $USER->id;
    $new_setting->courseid = $id;
    $new_setting->state = $state;
    if ($sub_item->id){
        $sub_item->state = $new_setting->state;
        $sub_item = $DB->update_record('local_calendar_courses', $sub_item);
    } else {
        $sub_item = $DB->insert_record('local_calendar_courses', $new_setting);
    }
    echo json_encode(array('state'=>1));

} elseif ($action == 'get_events') {

    require_once($CFG->dirroot.'/calendar/lib.php');
    require_once($CFG->dirroot.'/local/calendar/lib.php');

    $data = local_calendar_get_events();

    echo json_encode($data->events);

}

exit; ?>
