<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Displays information about all the assignment modules in the requested course
 *
 * @package   local_calendar
 * @copyright 2017 ISF
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


class local_calendar_renderer extends plugin_renderer_base {

    public function print_calendar($data) {

        $output = '';

        $output .= html_writer::start_tag('div', array('class'=>'main-calendar-box widget-frame-box clearfix'));

            $output .= html_writer::start_tag('div', array('class'=>'calendar-inner widget-left-box'));
                $output .= html_writer::start_tag('span', array('class'=>'settings-calendar-box'));
                    $output .= html_writer::start_tag('button', array('class'=>'calendar-settings btn btn-warning', 'id'=>'calendar-settings'));
                    $output .= html_writer::tag('i', '', array('class'=>'fa fa-cog'));
                    $output .= html_writer::end_tag('button');
                    $output .= $this->print_calendars_list($data);
                $output .= html_writer::end_tag('span');
                    if (has_capability('local/calendar:manage', context_system::instance())) {
                        $output .= html_writer::tag('button', get_string('createevent', 'local_calendar'), array('class'=>'create-event btn btn-success', 'onclick'=>'calendarEventCreate();'));
                    }
                $output .= html_writer::tag('div', '', array('class'=>'calendar', 'id'=>'local_calendar'));
            $output .= html_writer::end_tag('div');

            $output .= html_writer::start_tag('div', array('class'=>'calendar-action-box widget-right-box'));    $output .= html_writer::start_tag('span', array('class'=>'close-sidepanel', 'onclick'=>'calendarCloseSidepanel();'));
                $output .= html_writer::tag('i', '', array('class'=>'fa fa-times'));
                $output .= html_writer::end_tag('span');
                $output .= html_writer::tag('div', '', array('class'=>'calendar-action-inner'));
            $output .= html_writer::end_tag('div');

        $output .= html_writer::end_tag('div');

        return $output;
    }

    public function get_calendar_filters($data) {
        $output = '';

        $output .= html_writer::start_tag('div', array('class'=>'filter-wrapper'));
        $output .= html_writer::start_tag('ul');
            $output .= html_writer::start_tag('li', array('class'=>'cal-filters'));
            $output .= html_writer::tag('span', '', array('class'=>'calendar-filter-box'.((!isset($data->course_disables[0]) ) ? ' enabled' : ' disables'), 'id'=>'cf_0'));
            $output .= get_string('noncourseevents', 'local_calendar');

            foreach ($data->filtercourses as $fcid=>$fcname){
                $output .= html_writer::start_tag('li', array('class'=>'cal-filters'));
                $output .= html_writer::tag('span', '', array('class'=>'calendar-filter-box'.((!isset($data->course_disables[$fcid]) ) ? ' enabled' : ' disables'), 'id'=>'cf_'.$fcid, 'style'=>((isset($data->course_colors[$fcid])) ? 'background-color:#'.$data->course_colors[$fcid].'; border-color:#'.$data->course_colors[$fcid] : '')));
                $output .= addslashes($fcname);
                $output .= html_writer::tag('span', html_writer::tag('i', '', array('class'=>'fa fa-paint-brush')), array('class'=>'calendar-select-color-actions', 'id'=>'cfc_'.$fcid, 'data-type'=>'coursecolor'));
                $output .= html_writer::end_tag('li');
            }
            $output .= html_writer::end_tag('li');
        $output .= html_writer::end_tag('ul');
        $output .= html_writer::tag('div', '', array('class'=>'choose-color-box'));

        $output .= html_writer::end_tag('div');

        return $output;
    }

    public function print_calendars_list($data) {
        $output = '';

        $filters = $this->get_calendar_filters($data);

        $output .= html_writer::start_tag('ul');
        $output .= html_writer::tag('div', '', array('class'=>'subscription-color-box choose-color-box'));
        if (has_capability('local/calendar:manage', context_system::instance())) {
            $output .= html_writer::start_tag('li', array('id'=>'calendar_manage_link'));
            $output .= html_writer::tag('i', '', array('class'=>'fa fa-cog'));
            $output .= get_string('managecalendars', 'local_calendar');
            $output .= html_writer::end_tag('li');
        }
        $output .= html_writer::start_tag('li', array('id'=>'calendar_filter_link'));
        $output .= html_writer::tag('i', '', array('class'=>'fa fa-search'));
        $output .= get_string('filter', 'local_calendar').$filters;
        $output .= html_writer::end_tag('li');

        $output .= html_writer::start_tag('li', array('class'=>'cal-items'));
        $output .= html_writer::tag('span', '', array('class'=>'calendar-color-box'.((!isset($data->sub_disables[0]) ) ? ' enabled' : ' disabled'), 'id'=>'cl_0', 'box-color'=>'3a87ad'));
        $output .= get_string('systemevents', 'local_calendar');
        $output .= html_writer::end_tag('li');


        foreach ($data->subscriptions as $subscription){
            if (!isset($subscription->id)) continue;
            $output .= html_writer::start_tag('li', array('class'=>'cal-items'));
            $output .= html_writer::tag('span', '', array('class'=>'calendar-color-box'.((!isset($data->sub_disables[$subscription->id]) ) ? ' enabled' : ' disabled'), 'id'=>'cl_'.$subscription->id, 'box-color'=>(($subscription->color) ? $subscription->color : '3a87ad')));
            $output .= trim(addslashes($subscription->name));
            $output .= html_writer::tag('span', html_writer::tag('i', '', array('class'=>'fa fa-paint-brush')), array('class'=>'calendar-select-color-actions', 'id'=>'clc_'.$subscription->id, 'data-type'=>'subscriptioncolor'));
            $output .= html_writer::end_tag('li');
        }
        $output .= html_writer::end_tag('ul');

        return $output;
    }

    public function print_calendar_widget() {

        $output = '';

        $output .= html_writer::start_tag('div', array('class'=>'main-calendar-box calendar-widget-box widget-frame-box clearfix'));

            $output .= html_writer::start_tag('div', array('class'=>'calendar-widget-inner'));
                $output .= html_writer::tag('div', '', array('class'=>'calendar', 'id'=>'widget_calendar'));
            $output .= html_writer::end_tag('div');

        $output .= html_writer::end_tag('div');

        $this->page->requires->js_call_amd('local_calendar/calendar', 'initwidget', array('today'=>date('Y-m-d')));

        return $output;
    }


}

