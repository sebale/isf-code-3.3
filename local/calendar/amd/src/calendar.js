// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This is an empty module, that is required before all other modules.
 * Because every module is returned from a request for any other module, this
 * forces the loading of all modules with a single request.
 *
 * This function also sets up the listeners for ajax requests so we can tell
 * if any requests are still in progress.
 *
 * @package    local_calendar
 * @module     local_calendar/calendar
 * @copyright  2017 ISF
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define(['jquery', 'core/ajax', 'core/log', 'core/str', 'core/config', 'theme_primaryisf/colorpicker', 'theme_primaryisf/moment', 'theme_primaryisf/fullcalendar', 'theme_primaryisf/classyeditor', 'theme_primaryisf/dtpicker', 'theme_primaryisf/select2'], function($, ajax, log, str, mdlcfg, Colorpicker, moment, calendar, editor, datePicker, select2) {

    m = moment.init();
    Colorpicker.init($);
    calendar.init($, m);
    editor.init($);
    datePicker.init($, m);
    select2.init($);

    window.calendarCloseSettings = function() {
        $("#calendar-settings").trigger("click");
    }

    window.calendarGetToday = function() {
        var d = new Date();
        var curr_date = d.getDate();
        var curr_month = d.getMonth() + 1;
        var curr_year = d.getFullYear();

        return curr_year + "-" + curr_month + "-" + curr_date;
    }

    window.calendarLoadDayEvents = function(eventDay) {
        calendarOpenSidepanel();
        $(".widget-frame-box").removeClass('toggle');

        var url = mdlcfg.wwwroot + '/local/calendar/ajax.php?action=loaddayevents&date='+eventDay;

        $(".calendar-action-inner").html('<div class="loader"><i class="fa fa-spin fa-spinner"></i></div>').load(url, function(e){
            $(".event-description .fa").click(function(e){ $(this).parent().toggleClass("open");});
        });
    }

    window.calendarEventLoad = function(eventId) {
        calendarOpenSidepanel();
        $(".widget-frame-box").removeClass('toggle');
        var url = mdlcfg.wwwroot + '/local/calendar/ajax.php?action=loadevent&id='+eventId;
        $(".calendar-action-inner").html('<div class="loader"><i class="fa fa-spin fa-spinner"></i></div>').load(url, function(e){
            $(".event-description .fa").click(function(e){ $(this).parent().toggleClass("open");});
        });
    }

    window.calendarEventDelete = function(eventId) {
        if (confirm('Are you sure want to delete this event?') == true){
            $(".calendar-action-inner .events-list #event_"+eventId).remove();
            $('#local_calendar').fullCalendar( 'removeEvents', eventId );
            $.get(mdlcfg.wwwroot + '/local/calendar/ajax.php?action=deleteevent&id='+eventId);
        }
    }

    window.calendarEventCreate = function() {
        calendarOpenSidepanel();
        if ($(".settings-calendar-box ul").hasClass('active')){
            $(".calendar-wrapper").remove();
            $(".settings-calendar-box ul").toggleClass("active");
        }
        $(".widget-frame-box").addClass('toggle');
        $(".calendar-action-inner").html('<div class="loader"><i class="fa fa-spin fa-spinner"></i></div>').load(mdlcfg.wwwroot + '/local/calendar/ajax.php?action=get_event_form', function(e){
            $("#description").Editor({aligneffects: false, textformats: false, fonteffects: false, insertoptions: false, extraeffects: false, advancedoptions: false, screeneffects: false, undo: false, redo: false, 'l_align':false, 'r_align':false, 'c_align':false, 'justify':false, 'hr_line':false, strikeout:false, status_bar:false, insertoptions:false, advancedoptions: false, select_all:false, splchars:false});
            $(".date-time").datetimepicker();

            if ($("#event_courses").length) {
                $('#event_courses').select2();
            }

            $("#event_type").change(function() {
                if ($("#event_type").val() == 'course'){
                    $(".eventform-coursetype").removeClass('hidden');
                } else if ($("#event_type").val() == 'due'){
                    $(".eventform-coursetype").removeClass('hidden');
                } else {
                    $(".eventform-coursetype").addClass('hidden');
                }
            });
            $("#cal-form .actions .cancel").click(function(e) {
                e.preventDefault();
                calendarLoadDayEvents(calendarGetToday());
            });
            $("#cal-form .actions .save").click(function(e) {
                var text = $("#description").Editor("getText");
                $("#description").val(text);

                e.preventDefault();
                $.ajax({
                    url: $('#cal-form').attr('action'),
                    type: "POST",
                    data: $('#cal-form').serialize(),
                    dataType: "json",
                    beforeSend: function(){
                        $(".calendar-action-inner").html('<div class="loader"><i class="fa fa-spin fa-spinner"></i></div>');
                    }
                }).done(function( data ) {
                    if (data.returnId != '0' && data.returnMulti == '0'){
                        $(".calendar-action-inner").html('<div class="alert alert-'+data.errorType+'">'+data.msg+'<br /><br /><p align="center"><button onclick="calendarEventLoad('+data.returnId+');" class="btn btn-'+data.errorType+'">Continue</button></p></div>');
                    } else if (data.returnId == '0' && data.returnDay != 'undefined' && data.returnMulti != '0'){
                        $(".calendar-action-inner").html('<div class="alert alert-'+data.errorType+'">'+data.msg+'<br /><br /><p align="center"><button onclick="calendarLoadDayEvents(\''+data.returnDay+'\');" class="btn btn-'+data.errorType+'">Continue</button></p></div>');
                    } else {
                        $(".calendar-action-inner").html('<div class="alert alert-'+data.errorType+'">'+data.msg+'<br /><br /><p align="center"><button onclick="calendarLoadDayEvents(\''+calendarGetToday()+'\');" class="btn btn-'+data.errorType+'">Continue</button></p></div>');
                    }
                    if (data.errorType == 'success'){
                        if (data.returnMulti == '0'){
                            var event = [
                                {
                                    "id": data.event.id,
                                    "title": data.event.name,
                                    "start": data.event.start,
                                    "end": data.event.end,
                                    "allDay": data.event.allDay,
                                    "color": data.event.color,
                                    "className" : data.event.class
                                }
                            ];
                            $("#local_calendar").fullCalendar('removeEvents', data.event.id);
                            $("#local_calendar").fullCalendar('addEventSource', event);
                        } else {
                            var arr = data.events;
                            for (var k in arr){
                                if (typeof arr[k] !== 'function') {
                                    var dataevent = arr[k];
                                    var event = [
                                        {
                                            "id": dataevent.id,
                                            "title": dataevent.name,
                                            "start": dataevent.start,
                                            "end": dataevent.end,
                                            "allDay": dataevent.allDay,
                                            "color": dataevent.color,
                                            "className" : dataevent.class
                                        }
                                    ];
                                    $("#local_calendar").fullCalendar('removeEvents', dataevent.id);
                                    $("#local_calendar").fullCalendar('addEventSource', event);
                                }
                            }
                        }
                    }
                });
            });
        });
    }

    window.calendarEventEdit = function(eventId) {
        calendarOpenSidepanel();
        $(".widget-frame-box").addClass('toggle');
        $(".calendar-action-inner").html('<div class="loader"><i class="fa fa-spin fa-spinner"></i></div>').load(mdlcfg.wwwroot + '/local/calendar/ajax.php?action=get_event_form&id='+eventId, function(e){
            $("#description").Editor({aligneffects: false, textformats: false, fonteffects: false, insertoptions: false, extraeffects: false, advancedoptions: false, screeneffects: false, undo: false, redo: false, 'l_align':false, 'r_align':false, 'c_align':false, 'justify':false, 'hr_line':false, strikeout:false, status_bar:false, insertoptions:false, advancedoptions: false, select_all:false, splchars:false});
            $("#description").Editor("setText", $("#description").val());

            $(".date-time").datetimepicker();

            if ($("#event_courses").length) {
                $('#event_courses').select2();
            }

            $("#event_type").change(function() {
                if ($("#event_type").val() == 'course'){
                    $(".eventform-coursetype").removeClass('hidden');
                } else if ($("#event_type").val() == 'due'){
                    $(".eventform-coursetype").removeClass('hidden');
                } else {
                    $(".eventform-coursetype").addClass('hidden');
                }
            });
            $("#cal-form .actions .cancel").click(function(e) {
                e.preventDefault();
                calendarLoadDayEvents(calendarGetToday());
            });
            $("#cal-form .actions .save").click(function(e) {
                var text = $("#description").Editor("getText");
                $("#description").val(text);
                e.preventDefault();
                $.ajax({
                    url: $('#cal-form').attr('action'),
                    type: "POST",
                    data: $('#cal-form').serialize(),
                    dataType: "json",
                    beforeSend: function(){
                        $(".calendar-action-inner").html('<div class="loader"><i class="fa fa-spin fa-spinner"></i></div>');
                    }
                }).done(function( data ) {
                    if (data.returnId != '0' && data.returnMulti == '0'){
                        $(".calendar-action-inner").html('<div class="alert alert-'+data.errorType+'">'+data.msg+'<br /><br /><p align="center"><button onclick="calendarEventLoad('+data.returnId+');" class="btn btn-'+data.errorType+'">Continue</button></p></div>');
                    } else if (data.returnId == '0' && data.returnDay != 'undefined' && data.returnMulti != '0'){
                        $(".calendar-action-inner").html('<div class="alert alert-'+data.errorType+'">'+data.msg+'<br /><br /><p align="center"><button onclick="calendarLoadDayEvents(\''+data.returnDay+'\');" class="btn btn-'+data.errorType+'">Continue</button></p></div>');
                    } else {
                        $(".calendar-action-inner").html('<div class="alert alert-'+data.errorType+'">'+data.msg+'<br /><br /><p align="center"><button onclick="calendarLoadDayEvents(\''+calendarGetToday()+'\');" class="btn btn-'+data.errorType+'">Continue</button></p></div>');
                    }
                    if (data.errorType == 'success'){
                        if (data.returnMulti == '0'){
                            var event = [
                                {
                                    "id": data.event.id,
                                    "title": data.event.name,
                                    "start": data.event.start,
                                    "end": data.event.end,
                                    "allDay": data.event.allDay,
                                    "color": data.event.color,
                                    "className" : data.event.class
                                }
                            ];
                            $("#local_calendar").fullCalendar('removeEvents', data.event.id);
                            $("#local_calendar").fullCalendar('addEventSource', event);
                        } else {
                            var arr = data.events;
                            for (var k in arr){
                                if (typeof arr[k] !== 'function') {
                                    var dataevent = arr[k];
                                    var event = [
                                        {
                                            "id": dataevent.id,
                                            "title": dataevent.name,
                                            "start": dataevent.start,
                                            "end": dataevent.end,
                                            "allDay": dataevent.allDay,
                                            "color": dataevent.color,
                                            "className" : dataevent.class
                                        }
                                    ];
                                    $("#local_calendar").fullCalendar('removeEvents', dataevent.id);
                                    $("#local_calendar").fullCalendar('addEventSource', event);
                                }
                            }
                        }
                    }
                });
            });
        });
    }

    window.calendarCloseColorchooser = function() {
        $(".choose-color-box").removeClass("visible");
        $(".cal-filters").removeClass("active");
        $(".choose-color-box").html("");
    }

    window.calendarCloseSidepanel = function() {
        $(".main-calendar-box").addClass("fullview");
        $(".main-calendar-box").removeClass("toggle");
    }

    window.calendarOpenSidepanel = function() {
        $(".main-calendar-box").removeClass("fullview");
        calendarLeftRightSide();
    }

    window.calendarLeftRightSide = function() {
        $('.calendar-action-box').height($('.main-calendar-box').height());
        $('.calendar-action-inner').height($('.calendar-action-box').height());
    }

    window.calendarSetColorColorchooser = function(id, color, type) {
        $.ajax({
            url: mdlcfg.wwwroot + "/local/calendar/ajax.php?action=set_color&id="+id+"&color="+color+"&type="+type,
            type: "GET",
            dataType: "json",
            beforeSend: function(){
                $(".choose-color-box li").removeClass("active");
                $(".choose-color-box li[data-color=\""+color+"\"]").addClass("active");
                $(".choose-color-box li[data-color=\""+color+"\"]").html("<i class=\"fa fa-spin fa-spinner\"></i>");
                $(".color-input span").html("<i class=\"fa fa-spin fa-spinner\"></i>");
            }
        }).done(function( data ) {
            $(".choose-color-box li[data-color=\""+color+"\"]").html("<i class=\"fa fa-check\"></i>");
            $(".color-input span").css("background-color", "#"+color);
            $(".color-input span").css("border-color", "#"+color);
            $(".color-input input").val(color);
            $(".color-input span").html("");

            if (type == 'coursecolor') {
                $("#cf_"+id).css("background-color", "#"+color);
                $("#cf_"+id).css("border-color", "#"+color);
                if ($("#cf_"+id).hasClass("disables")){
                    $("#cf_"+id).trigger("click");
                }
                $(".ec_"+id).css("background-color", "#"+color);
                $(".ec_"+id).css("border-color", "#"+color);
                var rgb_color = calendarHexToRgb("#"+color);
                $(".events-list .ec_"+id).css("background-color", "rgba("+rgb_color.r+", "+rgb_color.g+", "+rgb_color.b+", 0.3)");
                $(".fc-event.ec_"+id).each(function(e){
                    var eventId = parseInt($(this).attr('data-event-id'));
                    var current_event = $('#local_calendar').fullCalendar('clientEvents', eventId);
                    if(current_event && current_event.length == 1){
                        //Retrieve current event from array
                        current_event = current_event[0];
                        //Set values
                        current_event.color = "#"+color;
                        //Update event
                        $('#local_calendar').fullCalendar('updateEvent', current_event);
                    }
                });
            } else {
                $("#cl_"+id).css("background-color", "#"+color);
                $("#cl_"+id).css("border-color", "#"+color);
                if ($("#cl_"+id).hasClass("disables")){
                    $("#cl_"+id).trigger("click");
                }
                $(".ev_"+id).css("background-color", "#"+color);
                $(".ev_"+id).css("border-color", "#"+color);
                var rgb_color = calendarHexToRgb("#"+color);
                $(".events-list .ev_"+id).css("background-color", "rgba("+rgb_color.r+", "+rgb_color.g+", "+rgb_color.b+", 0.3)");
                $(".fc-event.ev_"+id).each(function(e){
                    var eventId = parseInt($(this).attr('data-event-id'));
                    var current_event = $('#local_calendar').fullCalendar('clientEvents', eventId);
                    if(current_event && current_event.length == 1){
                        //Retrieve current event from array
                        current_event = current_event[0];
                        //Set values
                        current_event.color = "#"+color;
                        //Update event
                        $('#local_calendar').fullCalendar('updateEvent', current_event);
                    }
                });
            }


        });
    }
    window.calendarHexToRgb = function(hex) {
        var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }


    var Calendar = {

        init: function(day, eventid) {

            $('#local_calendar').fullCalendar({
                header: {
                    left: 'today,month,agendaWeek,agendaDay',
                    center: 'prev title next',
                    right: ''
                },
                businessHours: true,
                firstDay: 0,
                editable: false,
                eventLimit: true,
                fixedWeekCount: 5,
                views: {
                    agenda: {
                        eventLimit: 3
                    }
                },
                events: mdlcfg.wwwroot + "/local/calendar/ajax.php?action=get_events",
                dayClick: function(date, jsEvent, view) {
                    calendarLoadDayEvents(date.format());
                    $('#local_calendar').fullCalendar('gotoDate', date);
                },
                eventClick: function(calEvent, jsEvent, view) {
                    calendarEventLoad(calEvent.id);
                    var moment = calEvent.start;
                    $('#local_calendar').fullCalendar('gotoDate', moment.format());
                },
                eventRender: function(event, element, view) {
                    element.attr('data-event-id', event.id);
                },
                viewRender: function(view, element) {
                    if (view.name == 'agendaDay'){
                        var date = $('#local_calendar').fullCalendar('getDate');
                        calendarLoadDayEvents(date.format());
                    }
                },
            });

            $('#local_calendar').fullCalendar('gotoDate', day);

            calendarLeftRightSide();

            if (eventid > 0) {
                calendarEventLoad(eventid);
            } else {
                calendarLoadDayEvents(day);
            }

            $("#calendar-settings").click(function(){
                if ($(".settings-calendar-box ul").hasClass('active')){
                    $(".calendar-wrapper").remove();
                } else {
                    $(".main-calendar-box").append('<div class="calendar-wrapper" onclick="calendarCloseSettings();"></div>');
                }
                $(".settings-calendar-box ul").toggleClass("active");
            });

            $("#calendar-filter").click(function(){
                $(".filter-calendar-box .filter-wrapper").toggleClass("active");
            });

            $(".cal-items .calendar-color-box").each(function(e){
                var el = $(this);
                if (el.hasClass("enabled")){
                    el.css("background-color", "#"+el.attr("box-color"));
                    el.css("border-color", "#"+el.attr("box-color"));
                }
            });

            $(".cal-items .calendar-color-box").click(function(e){
                var el = $(this); var eid = el.attr("id");
                var eid = eid.replace("cl_","");
                if (el.hasClass("enabled")){
                    el.css("background-color", "#fff");
                    el.css("border-color", "#aaa");
                    el.removeClass("enabled"); el.addClass("disables");
                    $(".events-list .ev_"+eid).addClass("hidden");
                    $(".fc-event.ev_"+eid).each(function(e){
                        var eventId = parseInt($(this).attr('data-event-id'));
                        var current_event = $('#local_calendar').fullCalendar('clientEvents', eventId);
                        if(current_event && current_event.length == 1){
                            //Retrieve current event from array
                            current_event = current_event[0];
                            //Set values
                            var classes = String(current_event.className);
                            current_event.className = classes.replace(',', ' ') + ' hidden';
                            //Update event
                            $('#local_calendar').fullCalendar('updateEvent', current_event);
                        }
                    });
                    var state = 0;
                } else {
                    el.css("background-color", "#"+el.attr("box-color"));
                    el.css("border-color", "#"+el.attr("box-color"));
                    el.removeClass("disables"); el.addClass("enabled");
                    $(".events-list .ev_"+eid).removeClass("hidden");
                    $(".fc-event.ev_"+eid).each(function(e){
                        var eventId = parseInt($(this).attr('data-event-id'));
                        var current_event = $('#local_calendar').fullCalendar('clientEvents', eventId);
                        if(current_event && current_event.length == 1){
                            //Retrieve current event from array
                            current_event = current_event[0];
                            //Set values
                            var classes = String(current_event.className);
                            classes = classes.replace(',', ' ');
                            current_event.className = classes.replace('hidden', '');
                            //Update event
                            $('#local_calendar').fullCalendar('updateEvent', current_event);
                        }
                    });
                    var state = 1;
                }
                $.ajax({
                    url: mdlcfg.wwwroot + "/local/calendar/ajax.php?action=update_calendar_subscription&id="+eid+"&state="+state,
                    type: "GET",
                    dataType: "json",
                }).done(function( data ) {
                });
            });

            $(".cal-filters .calendar-filter-box").click(function(e){
                var el = $(this); var eid = el.attr("id");
                var eid = eid.replace("cf_","");
                if (el.hasClass("enabled")){
                    el.removeClass("enabled"); el.addClass("disables");
                    $(".events-list .ec_"+eid).addClass("hidden");
                    $(".fc-event.ec_"+eid).each(function(e){
                        var eventId = parseInt($(this).attr('data-event-id'));
                        var current_event = $('#local_calendar').fullCalendar('clientEvents', eventId);
                        if(current_event && current_event.length == 1){
                            //Retrieve current event from array
                            current_event = current_event[0];
                            //Set values
                            var classes = String(current_event.className);
                            current_event.className = classes.replace(',', ' ') + ' hidden';
                            //Update event
                            $('#local_calendar').fullCalendar('updateEvent', current_event);
                        }
                    });
                    var state = 0;
                } else {
                    el.removeClass("disables"); el.addClass("enabled");
                    $(".events-list .ec_"+eid).removeClass("hidden");
                    $(".fc-event.ec_"+eid).each(function(e){
                        var eventId = parseInt($(this).attr('data-event-id'));
                        var current_event = $('#local_calendar').fullCalendar('clientEvents', eventId);
                        if(current_event && current_event.length == 1){
                            //Retrieve current event from array
                            current_event = current_event[0];
                            //Set values
                            var classes = String(current_event.className);
                            classes = classes.replace(',hidden,', ' ');
                            classes = classes.replace('hidden', '');
                            classes = classes.replace(',', ' ');
                            current_event.className = classes;
                            //Update event
                            $('#local_calendar').fullCalendar('updateEvent', current_event);
                        }
                    });
                    var state = 1;
                }
                $.ajax({
                    url: mdlcfg.wwwroot + "/local/calendar/ajax.php?action=update_calendar_course&id="+eid+"&state="+state,
                    type: "GET",
                    dataType: "json",
                }).done(function( data ) {
                });
            });

            $(".calendar-select-color-actions").click(function(e){
                var el = $(this); var eid = el.attr("id");
                var type = el.attr("data-type");
                $(".cal-filters").removeClass("active");
                $(".choose-color-box").addClass("visible");
                $(".choose-color-box").html('');

                if (type == 'coursecolor') {
                    var eid = eid.replace("cfc_","");
                    $(".subscription-color-box").removeClass('visible');
                    $("#cf_"+eid).parent().addClass("active");
                    $("#calendar_filter_link .choose-color-box").load(mdlcfg.wwwroot + "/local/calendar/ajax.php?action=load_colorchooser&id="+eid+"&type="+type);
                } else {
                    var eid = eid.replace("clc_","");
                    $(".subscription-color-box").load(mdlcfg.wwwroot + "/local/calendar/ajax.php?action=load_colorchooser&id="+eid+"&type="+type);
                }

            });

            $("#calendar_manage_link").click(function(){
                location = mdlcfg.wwwroot + "/calendar/managesubscriptions.php"
            });

        },

        initwidget: function(day) {

            $('#widget_calendar').fullCalendar({
                header: {
                    left: 'today,month,agendaWeek,agendaDay',
                    center: 'prev title next',
                    right: ''
                },
                businessHours: true,
                firstDay: 0,
                editable: false,
                eventLimit: true,
                fixedWeekCount: 5,
                views: {
                    agenda: {
                        eventLimit: 3
                    }
                },
                events: mdlcfg.wwwroot + "/local/calendar/ajax.php?action=get_events",
                dayClick: function(date, jsEvent, view) {
                    $('#widget_calendar').fullCalendar('gotoDate', date);
                    location = mdlcfg.wwwroot + '/local/calendar/index.php?date=' + date.format();
                },
                eventClick: function(calEvent, jsEvent, view) {
                    calendarEventLoad(calEvent.id);
                    var moment = calEvent.start;
                    $('#widget_calendar').fullCalendar('gotoDate', moment.format());
                    location = mdlcfg.wwwroot + '/local/calendar/index.php?id=' + calEvent.id;
                },
                eventRender: function(event, element, view) {
                    element.attr('data-event-id', event.id);
                },
            });
        },
    };

    /**
    * @alias module:local_calendar/calendar
    */
    return Calendar;

});

